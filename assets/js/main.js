function notification(position = null, type = null, message = null, timeOut = null){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": position,
        "onclick": null,
        "fadeIn": 300,
        "fadeOut": 1000,
        "timeOut": timeOut
    }

    if(type.length > 0){
        if(type == 'success'){
            toastr.success(message);
        }else if(type == 'warning'){
            toastr.warning(message);
        }else if(type == 'error'){
            toastr.error(message);
        }else if(type == 'info'){
            toastr.info(message);
        }
    }
}

//panel refresh
function addPanelRefresh(selector = null){
    if(selector !== null){
        $(selector).addClass('panel-refresh');
    }else{
        $('div[name="panel-refresh"]').addClass('panel-refresh');
    }
}

//panel refresh
function removePanelRefresh(selector = null){
    $('.position-relative').each(function(){
        if(selector !== null){
            $(selector).find('.panel-refresh').removeClass('panel-refresh');
        }else{
            $(this).find('.panel-refresh').removeClass('panel-refresh');
        }
    });
}