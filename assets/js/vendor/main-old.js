/* ================================================
----------- Venedor ---------- */
(function ($) {
	"use strict";
	var Venedor = {
		initialised: false,
		version: 1.4,
		mobile: false,
		menuCollapse: false,
		isFirefox: (navigator.userAgent.toLowerCase().indexOf('firefox') > -1),
		navContainerHeight: null,
		container : $('.portfolio-item-container'),
		init: function () {

			if(!this.initialised) {
				this.initialised = true;
			} else {
				return;
			}

			// Call Venedor Functions
			this.checkMobile();
			this.checkPlaceholder();
			this.getNavContainerHeight();
			this.menuHover();
			this.responsiveMenu();
			this.itemHoverAnimation();
			this.searchInput();
			this.itemSizeFix();
			this.filterColorBg();
			this.productZoomImage();
			this.responsiveVideo();
			this.priceSlider();
			this.ratings();
			this.collapseArrows();
			this.owlCarousels();
			this.flexsliders();
			this.scrollTopAnimation();
			this.filterScrollBar();
			this.selectBox();
			this.bootstrapSwitch();
			this.tooltip();
			this.popover();
			this.progressBars();
			this.prettyPhoto();
			this.flickerFeed();
			this.parallax();
			this.twitterFeed();

			/* Portfolio */
			var self = this;
			if (typeof imagesLoaded === 'function') {
				imagesLoaded(self.container, function() {
					self.calculateWidth();
					self.isotopeActivate();
					// recall for plugin support
					self.isotopeFilter();
				});
				
			}

		},		
		productZoomImage: function () {
			var self = this,
				carouselContainer = $('#product-carousel'),
                productImg =  $('#product-image');

            if($.fn.elastislide) {
				carouselContainer.elastislide({
					orientation : 'vertical',
					minItems : 4
				});
			}

			// Product page zoom plugin settings
			if ($.fn.elevateZoom) {
				$('#product-image').elevateZoom({
					responsive: true, // simple solution for zoom plugin down here // it can cause bugs at resize
					zoomType: ( self.mobile || $(window).width() < 768 ) ? 'inner' : 'lens', // you can use 'inner' or 'window' // change inner and go to product.html page and see zoom plugin differances
					borderColour: '#d0d0d0',
					cursor: "crosshair",
					borderSize: 2,
					lensSize : 180,
					lensOpacity: 1,
					lensColour: 'rgba(255, 255, 255, 0.25)'
				});

				$('#product-carousel').find('a').on('mouseover', function (e) {
					var ez = $('#product-image').data('elevateZoom'),
						smallImg = $(this).data('image'),
						bigImg = $(this).data('zoom-image');

						ez.swaptheimage(smallImg, bigImg);
					e.preventDefault();
				});
			}
		}
	};


})(jQuery);