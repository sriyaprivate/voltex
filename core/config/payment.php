<?php

return [
	'hnb' => [
		'merchant_no' => '08238900',
		'password' 	=> '5ws3PMaJ',
		'currency' 	=> 'LKR',
		'currency_code' => 144,
		'acquirer_id' => '415738',
		'redirect_url' => 'https://www.hnbpg.hnb.lk/SENTRY/PaymentGateway/Application/ReDirectLink.aspx',
		'response_url' 	=> 'payment/hnb/response'
	],
	'amex' => [
		'merchant_id' => 'ORELCORP',
		'ipg_server_url' => 'https://www.ipayamex.lk/ipg/servlet_pay',
		'return_url' => 'payment/amex/response',
		'currency_code' => 'LKR',
		'action' => 'SaleTxn'
	]
];