<?php

/** REQEUST STATUS */
define('STATUS_TODO',1);
define('STATUS_PROCESSING',2);
define('STATUS_DONE',3);

//for pages
define('BIG_STORE', 'Big Store');
define('BULK_MARKET', 2);
define('HOME', 'Home');
define('UPLOAD_PATH', 'backend/core/storage/');


//for filters
define('SORT_OPTION', [
    '1' => 'date_desc', 
    '2' => 'date_asc',
    '3' => 'price_desc',
    '4' => 'price_asc',
    '5' => 'category_asc',
    '6' => 'category_desc'
]);

define('SHOW_OPTION', [
    '1' => '24', 
    '2' => '48',
    '3' => '96',
    '4' => '192',
    '5' => '500'
]);

define('DEFAULT_SELECTED_SHOW_OPTION', SHOW_OPTION[2]);
define('ALL_SHOW_OPTION', SHOW_OPTION[5]);

//for pricebook
define('CHANNEL_ONLINE', 'online');
define('CHANNEL_ONLINE_ID', '3');

//currency type
define('CURRENCY_SIGN', 'USD');

//promotions type
define('BANK_PROMOTION', 1);
define('DEAL_PROMOTION', 2);
define('DISCOUNT', 3);

//images
define('DEFAULT_PRODUCT_IMG', 'default/default.png');
//home default image
define('DEFAULT_HOME_PRODUCT_IMG', 'default/default-small.png');
define('DEFAULT_USER_IMAGE', 'default-user-profile.png');

//product have 2 status, 1 is in bigstore, 2 is this item in savings center
//this consts for cart.
define('DEFAULT_STATUS', 1);
define('SAVINGS_CENTER_STATUS', 2);
define('PRODUCT_CHECK_STATUS', [DEFAULT_STATUS, SAVINGS_CENTER_STATUS]);

//for savings center promotion
define('SAVING_CENTER_PROMOTION', [BANK_PROMOTION, DEAL_PROMOTION]);

//for order status manage
define('NEWLY_CREATED',0);
define('APPROVE_PENDING',1);
define('CUSTOMER_CONFIRMED',2);
define('CUSTOMER_REJECTED',3);

define('PENDING_TEXT', 'Order Pending');
define('APPROVED_TEXT', 'Order Approved');
define('REJECTED_TEXT', 'Order Rejected');
define('PREPARING_TEXT', 'Order Preparing');
define('PREPARED_TEXT', 'Order Prepared');
define('ONDELIVERY_TEXT', 'Order Ondelivery');
define('DELIVERED_TEXT', 'Order Delivered');
define('PICKUPED_TEXT', 'Order Pickuped');
define('ADVANCED_PAYMENT_TEXT', 'make Advanded payment.');

//users types
define('USER_TYPE_ADMIN', 1); // 1 for admin user
define('USER_TYPE_FRONT', 2); // 2 for front user
define('USER_TYPE_GUEST', 3); // 3 for guest user
define('ALL_USER_TYPES', [USER_TYPE_ADMIN, USER_TYPE_FRONT, USER_TYPE_GUEST]);

//stock type
define('MAIN_STOCK',1);
define('PROMO_STOCK',2);

define('OUT_OF_STOCK', 0);

//order number
define('ORDER_PREFIX', 'ODR'.date('ym').'-');

//cart status
define('CAN_PAYMENT', 1);
define('CANNOT_PAYMENT', 2);
define('CART_STATUS', [CAN_PAYMENT, CANNOT_PAYMENT]);

//custom dates
define('DATE_FROM',date('Y-m-d',strtotime('2000-01-01')));
define('DATE_TO',date('Y-m-d',strtotime('3000-01-01')));

//main warehouse
define('MAIN_WAREHOUSE', 1);

//category type
define('BIGSTORE_CATEGORY', 1);
define('BULK_MARKET_CATEGORY', 2);

//site information
define('SITE_COMPANY_NAME', '');
define('SITE_ADDRESS', '');
define('SITE_CONTACT_NO', '');
define('SITE_EMAIL', '');
define('SITE_COPYRIGHT', '');
define('SITE_FB_LINK', '');
define('WEB', '');
define('SITE_ADMIN_EMAIL','');
define('DEVELOPER_EMAIL','');
