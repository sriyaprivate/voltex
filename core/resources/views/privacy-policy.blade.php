@extends('layouts.master') @section('title','Privacy Policy')

@section('links')
@stop

@section('css')
<style type="text/css">
  a:focus{
    color: inherit;
  }
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item active">Privacy Policy</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="account-section mb-2">
    <div class="container">

      <div class="row profile">
        <div class="col-md-3 mb-3">
          <div class="profile-sidebar">

            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu m-0">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('about-us')}}">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('choose-us')}}">WHY CHOOSE US?</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('terms-condition')}}">TERMS & CONDITIONS</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link" href="{{url('privacy-policy')}}">PRIVACY POLICY</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">HELP & FAQ'S</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="#">WARRANTY & RETURNS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">DELIVERY METHODS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">REGIONAL CENTERS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">NEWS ROOM</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="#">BLOG</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">SITE MAP</a>
                </li>                
                <li class="nav-item">
                  <a class="nav-link" href="{{url('contact-us')}}">Contact Us</a>
                </li>

              </ul>
            </div>
            <!-- END MENU -->

          </div>
        </div>
        <div class="col-md-9 mb-3">
          <div class="profile-content mb-3">
            <h6 class="text-uppercase m-0"><strong>PRIVACY POLICY</strong></h6>
          </div>

          <hr>

         
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Introduction</strong></h6>
          </div>

          <p class="text-justify">This Privacy Policy (hereinafter sometimes “the Policy”) sets out what OREL CORPORATION (PVT)LTD (also referred to as “OREL”, “Us, “We”, “Our”, “Ostore” As the context may require) can and cannot do with the information provided by you in the use of Services under this Website. You accept this Privacy Policy when you sign up for, access, or use our products, services, content, features, technologies or functions offered on our website and all related sites, applications, and services (collectively referred to as “OREL Services”). This Privacy Policy is intended to govern the use of our website by our customers, suppliers and/or users (including, without limitation those who use OREL Services in the course of their trade or business) unless otherwise agreed through a separate contract. We may amend this Privacy Policy at any time by posting a revised version on our website. The revised version will be effective as of the published effective date.</p>

          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Is giving your information essential?</strong></h6>
          </div>

          <p class="text-justify">You are able to peruse the Website without sharing any personal information. However, in the event you wish to avail yourself of the Services you may be required to share certain personal information by creating an account on our website  with OREL to provide you a better service.</p>
          
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>What information do we request from you?</strong></h6>
          </div>

          <p class="text-justify">We collect personal information in order to provide you with the use of OREL Services, and to help us improve your experience. Information we collect includes the information sent to us by your computer, mobile phone or other access device.</p>

          <p class="text-justify">The information sent to us includes, but is not limited to, the following: </p>

          <p class="text-justify ml-2">
            <ul>
              <li>Your IP address and other information such as your location, type of sites and pages visited etc.</li>
              <li>We receive data whenever you visit a website that is connected, affiliated or runs on our website platform</li>
              <li>We also collect anonymous information through our use of cookies and web beacons.</li>
            </ul>
          </p>

          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Use of Cookies</strong></h6>
          </div>

          <p class="text-justify">When you access our website or use OREL Services, we (including companies we work with) may place small data files on your computer or other device. These data files may be cookies, pixel tags, "Flash cookies," or other local storage provided by your browser or associated applications (collectively "Cookies"). We use these technologies to recognize you as a customer/user; customize OREL Services, content, and advertising; measure promotional effectiveness; help ensure that your account security is not compromised; mitigate risk and prevent fraud; and to promote trust and safety across our sites and OREL Services. You are free to decline our Cookies if your browser or browser add-on permits, unless our Cookies are required to prevent fraud or ensure the security of websites we control.</p>

          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>What do we do with your information?</strong></h6>
          </div>

          <p class="text-justify">We do not share your personal information with third parties  and neither do we  sell or rent your personal information to third parties for their marketing purposes without your explicit consent.</p>

          <p class="text-justify">We may share your information based on a legitimate legal request such as by a court order, by law or regulation to prevent harm, crime or as part of an official investigation.</p>

          <p class="text-justify">We may combine your information with information we collect from other companies and use it to improve our Services, content, and advertising. If you do not wish to receive marketing communications from us or participate in our ad-customization programs, simply indicate your preference by logging into your account and going to the Notifications section and updating your preferences, or by following the directions that may be provided within the communication or advertisement. We respect your communication preferences.</p>

          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Acknowledgments</strong></h6>
          </div>

          <p class="text-justify">You accept and acknowledge that once you sign up on our Website, we will have access to the information shared by you.</p>

          <p class="text-justify">To a limited extent you will also be providing us with the authority to use the information provided by you in any marketing and / or promotional campaign, having due regard to your personal information.</p>

          <p class="text-justify">You also acknowledge and agree that in the event a requirement is made by law or under statute / regulation we may be compelled to share the information provided by you to the relevant authorities and that you will not hold us liable for the sharing of any information in such circumstances.</p>

          <p class="text-justify">In the event you subscribe to our Website when you sign up under the same, you agree and accept that we will be sending you emails, notifications and other information / updates, pertaining to any promotions, marketing and any other information that we require you to be notified of.</p>

          <p class="text-justify">In the event you purchase any products or services using our Website you will be directed to a payment gateway operated  by a third party financial institution which will  prompt you to provide your details including but not limited to  your  credit/debit card details. The information you provide to the payment gateway is not a website operated by Us and as such Orel shall not be responsible or liable in any manner for any information that you provide to such payment gateway.</p>

          <p class="text-justify">We ensure that your personal information pertaining to your mobile / telephone number, email address and other relevant information shall remain confidential unless and / or otherwise required to be disclosed by us under the terms and conditions of any third-party service provider attached to the Website and / or any legal, statutory, regulatory and / or governmental advice, regulation, direction or otherwise.</p>

          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Termination</strong></h6>
          </div>
          
          <p class="text-justify">We may terminate this Agreement at any time. Without limiting the foregoing, we shall have the right to immediately terminate any of your passwords or accounts in the event of any conduct which in our sole discretion, is considered to be unacceptable or in the event of any breach or violation of this Agreement or any term or condition pertaining to the use of our Website.</p>

          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Indemnity</strong></h6>
          </div>

          <p class="text-justify">You agree to free, indemnify and hold harmless OREL against any and all actions, suit, proceeding, claims, expenses, loss, damages (including legal fees) or otherwise suffered by us due to your act, omission, negligence and / or otherwise by your use of this Website and Services under this Website.</p>

          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Contacting Us</strong></h6>
          </div>

           <p class="text-justify">If you have questions or concerns regarding this Privacy Policy, you should contact us by telephone using the numbers or email addresses contained in this link.</p>

           <p>
            <i class="fa fa-phone"></i> <span class="ml-1">{{SITE_CONTACT_NO}}</span>
            <br/>
            <i class="fa fa-envelope-o"></i> <span class="ml-0">{{SITE_EMAIL}}</span>
            </p>
        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
@stop

@section('scripts')
@stop