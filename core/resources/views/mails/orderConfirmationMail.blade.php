<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Receipt for Order</title>
    <!-- 
    The style block is collapsed on page load to save you some scrolling.
    Postmark automatically inlines all CSS properties for maximum email client 
    compatibility. You can just update styles here, and Postmark does the rest.
    -->
    <style type="text/css" rel="stylesheet" media="all">
    /* Base ------------------------------ */
    
    *:not(br):not(tr):not(html) {
      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
      box-sizing: border-box;
    }
    
    body {
      width: 100% !important;
      height: 100%;
      margin: 0;
      line-height: 1.4;
      background-color: #F2F4F6;
      color: #74787E;
      -webkit-text-size-adjust: none;
    }
    
    p,
    ul,
    ol,
    blockquote {
      line-height: 1.4;
      text-align: left;
    }
    
    a {
      color: #3869D4;
    }
    
    a img {
      border: none;
    }
    
    td {
      word-break: break-word;
    }
    /* Layout ------------------------------ */
    
    .email-wrapper {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #F2F4F6;
    }
    
    .email-content {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    /* Masthead ----------------------- */
    
    .email-masthead {
      padding: 25px 0;
      text-align: center;
    }
    
    .email-masthead_logo {
      width: 94px;
    }
    
    .email-masthead_name {
      font-size: 16px;
      font-weight: bold;
      color: #bbbfc3;
      text-decoration: none;
      text-shadow: 0 1px 0 white;
    }
    /* Body ------------------------------ */
    
    .email-body {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      border-top: 1px solid #EDEFF2;
      border-bottom: 1px solid #EDEFF2;
      background-color: #FFFFFF;
    }
    
    .email-body_inner {
      width: 1000px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 570px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #FFFFFF;
    }
    
    .email-footer {
      width: 570px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 570px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }
    
    .email-footer p {
      color: #AEAEAE;
    }
    
    .body-action {
      width: 100%;
      margin: 30px auto;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }
    
    .body-sub {
      margin-top: 25px;
      padding-top: 25px;
      border-top: 1px solid #EDEFF2;
    }
    
    .content-cell {
      padding: 35px;
    }
    
    .preheader {
      display: none !important;
      visibility: hidden;
      mso-hide: all;
      font-size: 1px;
      line-height: 1px;
      max-height: 0;
      max-width: 0;
      opacity: 0;
      overflow: hidden;
    }
    /* Attribute list ------------------------------ */
    
    .attributes {
      margin: 0 0 21px;
    }
    
    .attributes_content {
      background-color: #EDEFF2;
      padding: 16px;
    }
    
    .attributes_item {
      padding: 0;
    }
    /* Related Items ------------------------------ */
    
    .related {
      width: 100%;
      margin: 0;
      padding: 25px 0 0 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .related_item {
      padding: 10px 0;
      color: #74787E;
      font-size: 15px;
      line-height: 18px;
    }
    
    .related_item-title {
      display: block;
      margin: .5em 0 0;
    }
    
    .related_item-thumb {
      display: block;
      padding-bottom: 10px;
    }
    
    .related_heading {
      border-top: 1px solid #EDEFF2;
      text-align: center;
      padding: 25px 0 10px;
    }
    /* Discount Code ------------------------------ */
    
    .discount {
      width: 100%;
      margin: 0;
      padding: 24px;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #EDEFF2;
      border: 2px dashed #9BA2AB;
    }
    
    .discount_heading {
      text-align: center;
    }
    
    .discount_body {
      text-align: center;
      font-size: 15px;
    }
    /* Social Icons ------------------------------ */
    
    .social {
      width: auto;
    }
    
    .social td {
      padding: 0;
      width: auto;
    }
    
    .social_icon {
      height: 20px;
      margin: 0 8px 10px 8px;
      padding: 0;
    }
    /* Data table ------------------------------ */
    
    .purchase {
      width: 100%;
      margin: 0;
      padding: 35px 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .purchase_content {
      width: 100%;
      margin: 0;
      padding: 25px 0 0 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .purchase_item {
      padding: 10px 0;
      color: #74787E;
      font-size: 15px;
      line-height: 18px;
    }
    
    .purchase_heading {
      padding-bottom: 8px;
      border-bottom: 1px solid #EDEFF2;
    }
    
    .purchase_heading p {
      margin: 0;
      color: #9BA2AB;
      font-size: 12px;
    }
    
    .purchase_footer {
      padding-top: 15px;
      border-top: 1px solid #EDEFF2;
    }
    
    .purchase_total {
      margin: 0;
      text-align: right;
      font-weight: bold;
      color: #2F3133;
    }
    
    .purchase_total--label {
      padding: 0 15px 0 0;
    }
    /* Utilities ------------------------------ */
    
    .align-right {
      text-align: right;
    }
    
    .align-left {
      text-align: left;
    }
    
    .align-center {
      text-align: center;
    }
    /*Media Queries ------------------------------ */
    
    @media only screen and (max-width: 600px) {
      .email-body_inner,
      .email-footer {
        width: 100% !important;
      }
    }
    
    @media only screen and (max-width: 500px) {
      .button {
        width: 100% !important;
      }
    }
    /* Buttons ------------------------------ */
    
    .button {
      background-color: #3869D4;
      border-top: 10px solid #3869D4;
      border-right: 18px solid #3869D4;
      border-bottom: 10px solid #3869D4;
      border-left: 18px solid #3869D4;
      display: inline-block;
      color: #FFF;
      text-decoration: none;
      border-radius: 3px;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
      -webkit-text-size-adjust: none;
    }
    
    .button--green {
      background-color: #22BC66;
      border-top: 10px solid #22BC66;
      border-right: 18px solid #22BC66;
      border-bottom: 10px solid #22BC66;
      border-left: 18px solid #22BC66;
    }
    
    .button--red {
      background-color: #FF6136;
      border-top: 10px solid #FF6136;
      border-right: 18px solid #FF6136;
      border-bottom: 10px solid #FF6136;
      border-left: 18px solid #FF6136;
    }
    /* Type ------------------------------ */
    
    h1 {
      margin-top: 0;
      color: #2F3133;
      font-size: 19px;
      font-weight: bold;
      text-align: left;
    }
    
    h2 {
      margin-top: 0;
      color: #2F3133;
      font-size: 16px;
      font-weight: bold;
      text-align: left;
    }
    
    h3 {
      margin-top: 0;
      color: #2F3133;
      font-size: 14px;
      font-weight: bold;
      text-align: left;
    }
    
    p {
      margin-top: 0;
      color: #74787E;
      font-size: 16px;
      line-height: 1.5em;
      text-align: left;
    }
    
    p.sub {
      font-size: 12px;
    }
    
    p.center {
      text-align: center;
    }
    .ostore-logo{
      margin-top: -50px;
      float: left;
      margin-left: -8px;
    }
    .sm-t{
      font-size: 14px;
    }
    .td-padding{
      padding-top: 5px !important;
      padding-bottom: 7px;
    }
    .border-dashed-top{
      border-top:dashed 2px #ccc;
    }
    .border-dashed-bottom{
      border-bottom:dashed 2px #ccc;
    }
    .detail-title{
      font-weight: bold;
    }
    .td-separator{
      padding-left:15px;
      padding-right:15px;
    }
    .purchase td, .purchase th{
      padding-left:10px !important;
      padding-right:10px !important;
    }
    </style>
  </head>
  <body>
    <table class="email-wrapper email-body_inner" width="100%" cellpadding="50" cellspacing="0" style="border:1px solid #b3b4b5;">
      <tr>
        <td align="center">
          <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td class="email-masthead">
                <div class="logo text-center">
                    <a href="{{url('/')}}" class="email-masthead_name"><img src="{{asset('assets/images/logo.png')}}" alt="logo" class="ostore-logo"></a>
                </div>
              </td>
            </tr>
            <!-- Email Body -->
            <tr>
              <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                <div style="padding-top: 40px;color:#74787e;text-align:center;font-size:35px;">Thank you for shopping with us.</div> 
                <h1 style="padding-top: 40px;color:#74787e;">Dear  {{ isValid($order->user->customer->first_name)?:''}},</h1>
                <p>Your order has been received and will process it shortly. Kindy note the following details as reference.<br>
                Meanwhile you may track your order details via <a href="{{ route('account.order.history') }}">Order History</a> using your Ostore Account.<br>
                If you have any clarifications please feel free to talk to us.<br>
                Thank you once again.</p>
                <!-- <p>This purchase will appear as “[Credit Card Statement Name]” on your credit card statement for your  credit_card_brand  ending in  credit_card_last_four . Need to <a href=" billing_url ">update your payment information</a>?</p>
                <br> -->
                <br>
                <table style="100%">
                  <tr>
                    <td width="60%">
                      <table>
                        <tr class="row">
                            <td class="detail-title">Order ID</td>
                            <td class="td-separator">:</td>
                            <td class="detail-value">{{ isValid($order['order_no'])?:'-' }}</td>
                        </tr>
                        <tr class="row">
                            <td class="detail-title">Order Date</td>
                            <td class="td-separator">:</td>
                            <td class="detail-value">{{ isValid($order['order_date'])?:'-' }}</td>
                        </tr>
                        <tr class="row">
                              <td class="detail-title">Delivery Type</td>
                              <td class="td-separator">:</td>
                              <td class="detail-value">{{ isValid($order['deliveryType']) && isValid($order['deliveryType']->name)? $order['deliveryType']->name :'-' }}</td>
                          </tr>
                          @if(isset($order) > 0 && isset($order['location']) && count($order['location']) && isset($order['deliveryType']) && isset($order['deliveryType']->name) && strtolower($order['deliveryType']->name) == PICKUP)
                            <tr class="row">
                                <td class="detail-title">Shipping Address</td>
                                <td class="td-separator">:</td>
                                <td class="detail-value">{{ isValid($order['delivery_address'])?:'-' }}</td>
                            </tr>
                          @else
                            <tr class="row">
                                <td class="detail-title">Shipping Address</td>
                                <td class="td-separator">:</td>
                                <td class="detail-value">{{ isValid($order['delivery_address'])?:'-' }}</td>
                            </tr>
                            <tr class="row">
                                <td class="detail-title">Billing Address</td>
                                <td class="td-separator">:</td>
                                <td class="detail-value">{{ isValid($order['billing_address'])?:'-' }}</td>
                            </tr>
                          @endif
                          <tr class="row">
                              <td class="detail-title">Transaction No</td>
                              <td class="td-separator">:</td>
                              <td class="detail-value">
                                  @if(isValid($order['payment']) && isValid($order['payment']) && isValid($order['payment']['transaction']))
                                      {{ isValid($order['payment']['transaction']->transaction_no)?:'-' }}
                                  @else
                                      -
                                  @endif
                              </td>
                          </tr>
                      </table>
                    </td>
                    <td valign="top" width="2%">

                    </td>
                    <td valign="top" width="38%">
                      <table>
                        @if(isset($order) > 0 && isset($order['location']) && count($order['location']) && isset($order['deliveryType']) && isset($order['deliveryType']->name) && strtolower($order['deliveryType']->name) == PICKUP)
                            <tr class="row">
                                <td class="detail-title">Delivery Type</td>
                                <td class="td-separator">:</td>
                                <td class="detail-value">{{ isValid($order['deliveryType']) && isValid($order['deliveryType']->name)? $order['deliveryType']->name :'-' }}</td>
                            </tr>
                            <tr class="row">
                                <td class="detail-title" style="width: 50%;">Regional Center</td>
                                <td class="td-separator">:</td>
                                <td class="detail-value">{{ isValid($order['location']) && isValid($order['location']->name)? $order['location']->name :'-' }}</td>
                            </tr>
                            <tr class="row">
                                <td class="detail-title">Address</td>
                                <td class="td-separator">:</td>
                                <td class="detail-value">{!! isValid($order['location']) && isValid($order['location']->address)? strExpImp($order['location']->address, ',', ',<br>') :'-' !!}</td>
                            </tr>
                            <tr class="row">
                                <td class="detail-title">Contact</td>
                                <td class="td-separator">:</td>
                                <td class="detail-value">{{ isValid($order['location']) && isValid($order['location']->contact_no)? $order['location']->contact_no :'-' }}</td>
                            </tr>
                        @endif
                      </table>
                    </td>
                  </tr>
                </table>
                
                <table class="purchase" width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td colspan="2">
                      <div style="font-size:20px;margin-bottom:-15px;margin-top:40px;">Order Summery</div>
                      <table class="purchase_content" width="100%" cellpadding="0" cellspacing="0">
                        <tr style="background-color: #f2f4f6;font-size:18px;color:#74787e;">
                          <th class="purchase_heading sm-t td-padding td-padding-lr" width="5%">
                            <p>#</p>
                          </th>
                          <th class="purchase_heading sm-t td-padding td-padding-lr" width="55%">
                            <p>Product</p>
                          </th>
                          <th class="purchase_heading sm-t td-padding td-padding-lr" width="10%">
                            <p class="align-right">Unit price</p>
                          </th>
                          <th class="purchase_heading td-padding td-padding-lr" width="10%">
                            <p class="align-right sm-t">Discount</p>
                          </th>
                          <th class="purchase_heading td-padding td-padding-lr" width="10%">
                            <p class="align-right sm-t">QTY</p>
                          </th>
                          <th class="purchase_heading td-padding td-padding-lr" width="20%">
                            <p class="align-right sm-t">Amount</p>
                          </th>
                        </tr>
                        @if(isValid($order->details))
                          <?php 
                            $row_price_reduce_dis = 0; 
                            $total                = 0; 
                            $total_discount       = 0; 
                          ?>
                          @foreach($order->details as $key => $detail)
                              @if(isValid($detail['discounted_price']) && $detail['discounted_price'] > 0)
                                  <?php 
                                    $row_price_reduce_dis = $detail['price'] - $detail['discounted_price']; 
                                    $total_discount += $detail['discounted_price'] * $detail['qty'];
                                  ?>
                              @endif
                                  <tr>
                                      <td class="align-left purchase_heading td-padding">{{ ++$key }}</td>
                                      <td class="purchase_heading td-padding">{{ isValid($detail['product']->display_name)?:'-' }}</td>
                                      <td class="align-right purchase_heading td-padding">{{ $detail['price'] }}</td>
                                      <td class="align-right purchase_heading td-padding">{{ $detail['discounted_price'] }}</td>
                                      <td class="align-right purchase_heading td-padding">{{ $detail['qty']?:'-' }}</td>
                                      @if(isset($detail['discounted_price']) && $detail['discounted_price'] > 0)
                                          <?php $total += $detail['qty'] * $detail['price']; ?>
                                      @else
                                          <?php $total += $detail['qty'] * $detail['price']; ?>
                                      @endif
                                      <td class="align-right purchase_heading td-padding">
                                          @if(isValid($detail['discounted_price']) && $detail['discounted_price'] > 0)
                                              <?php /*echo number_format($detail['qty'] * $row_price_reduce_dis, 2); */?>
                                              <?php echo number_format($detail['qty'] * $detail['price'], 2); ?>
                                          @else
                                              <?php echo number_format($detail['qty'] * $detail['price'], 2); ?>
                                          @endif
                                      </td>
                                  </tr>
                              @endforeach
                              <tr>
                                <th colspan="6"><br><br></th>
                              </tr>
                              <tr>
                                <th colspan="2"></th>
                                <td colspan="2" class="align-right sm-t td-padding">Sub Total</td>
                                <th colspan="2" class="align-right purchase_heading td-padding" style="font-size: 17px;">{{ number_format($total,2) }}</th>
                              </tr>
                              <tr>
                                <th colspan="2"></th>
                                <td colspan="2" class="align-right sm-t td-padding">Delivery Charge</td>
                                <th colspan="2" class="align-right purchase_heading td-padding" style="font-size: 17px;">+{{number_format($order['delivery_charges'], 2)}}</th>
                              </tr>
                              <tr>
                                <th colspan="2"></th>
                                <td colspan="2" class="align-right sm-t td-padding">Discount</td>
                                <th colspan="2" class="align-right purchase_heading td-padding" style="font-size: 17px;">-{{ number_format($total_discount,2) }}</th>
                              </tr>
                              <tr>
                                <th colspan="2"></th>
                                <td colspan="2" class="align-right td-padding" style="font-size: 20px;">Total</td>
                                <th colspan="2" class="align-right purchase_heading border-dashed-top border-dashed-bottom td-padding" style="font-size: 20px;">Rs. {{number_format((($total + $order['delivery_charges']) - $total_discount),2)}}</th>
                              </tr>
                          @else
                              <h3 class="text-center">Order not found!.</h3>
                          @endif
                      </table>
                    </td>
                  </tr>
                </table>
                <p>If you have any questions about this receipt, simply reply to this email or reach out to our <a href="{{ url('/contact-us') }}">support team</a> for help.</p>
                <p>Cheers,
                <br>The Ostore Team</p>
                <!-- Action -->
                <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center">
                      <!-- Border based button
                  https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design -->
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center">
                            <table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
                                  <a href="{{ url('/account/order-print/'.isValid($order->id).'?type=D') }}" class="button button--blue" target="_blank">Download as PDF</a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                <br>
                <br>
                <!-- <table class="discount" align="center" width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center">
                      <h1 class="discount_heading">10% off your next purchase!</h1>
                      <p class="discount_body">Thanks for your support! Here's a coupon for 10% off your next purchase if used by  expiration_date .</p>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center">
                            <table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
                                  <a href="http://example.com" class="button button--green" target="_blank">Use this discount now...</a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table> -->
                <!-- Sub copy -->
                <!-- <table class="body-sub">
                  <tr>
                    <td>
                      <p class="sub">Need a printable copy for your records?</strong> You can <a href=" action_url ">download a PDF version</a>.</p>
                      <p class="sub">Moved recently? Have a new credit card? You can easily <a href=" billing_url ">update your billing information</a>.</p>
                    </td>
                  </tr>
                </table> -->
              </td>
            </tr>
            <tr>
              <td>
                <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" style="margin-bottom: -50px">
                  <tr>
                    <td class="content-cell" align="center">
                      <p class="sub align-center">&copy; {{ SITE_COPYRIGHT }}</p>
                      <p class="sub align-center">
                        {!! strExpImp(SITE_ADDRESS, ',', '<br/>') !!}
                      </p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>