@extends('layouts.master') @section('title','Login')

@section('links')
  <!-- craftpip-jquery-confirm -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />
@stop

@section('css')
    <style type="text/css">
        .help-block{
            color: red !important;
        }
    </style>
@stop

@section('content')
  <!-- sign in/up panel -->
    <div class="sign-in-section mt-2 pt-4 pb-4">
    
        <div class="container-fluid">

            <!-- wrapper -->
            <div class="inside-wrapper">
    
                <div class="card-deck">

                    <div class="card border-0">
                        <div class="card-block p-0">
                            <div class="detail-back">
                                <div class="text-center desc">
                                    <h1 class="ionyx text-center text-uppercase">orelcorp</h1>
                                    <p class="ionyx text-center text-capitalize text-center text-uppercase">ECOMMERCE SOLUTIONS</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--login-form-->
                    <form class="card" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="card-body pb-0">
                            <div class="account-holder">              
                                
                                <h6 class="text-uppercase">sign in to ORANGE E-COMMERCE</h6>
                                <p>Please fill out the form below to login to your account.</p>
                            
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label required-control-label required" for="email">Email</label>
                                    <input id="email" name="email" type="text" class="form-control" placeholder="Enter Email Address">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label required-control-label required" for="email">Password</label>
                                    <input id="password" name="password" type="password" class="form-control" placeholder="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        <br>
                                    @endif
                                    <a href="#" class="forgot-password">Forgot Password?</a>
                                </div>
                            
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <p class="pull-right text-uppercase">Remember Me?</p>
                                        </label>                                  
                                  </div>
                                </div>

                            </div>                        
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary btn-block">Sign in</button>
                        </div>
                    </form>
                    <!--/.login-form-->
                    <div class="card border-0">
                        <div class="card-block p-0">
                            <div class="detail-back">
                                <div class="text-center desc">
                                    <h1 class="ionyx text-center text-uppercase">orelcorp</h1>
                                    <p class="ionyx text-center text-capitalize text-center text-uppercase">ECOMMERCE SOLUTIONS</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--sing-up-form-->         
                </div>
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.sign in/up panel -->
@stop

@section('js')
  <!-- craftpip-jquery-confirm -->
  <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
@stop

@section('scripts')
<script>
    $(document).ready(function(){
        $('.forgot-password').on('click', function () {
            $.confirm({
                title: false,
                content: '<h5 class="text-center text-uppercase">Forgot Your Password</h5>'
                    +'<p class="mt-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>'
                    +'<label class="control-label required" for="email">Email<sup style="color:red">*</sup></label>'
                    +'<input id="email" name="email" type="text" class="form-control" placeholder="Enter Email Address">',
                theme: 'material',
                animation: 'scale',
                closeAnimation: 'scale',
                columnClass: 'medium',
                opacity: 0.5,
                buttons: {
                    'confirm': {
                        text: 'Send',
                        btnClass: 'btn-red',
                        action: function () {
                            $.confirm({
                                title: 'This maybe critical',
                                content: 'Critical actions can have multiple confirmations like this one.',
                                icon: 'fa fa-warning',
                                animation: 'scale',
                                closeAnimation: 'zoom',
                                buttons: {
                                    confirm: {
                                        text: 'Yes, sure!',
                                        btnClass: 'btn-orange',
                                        action: function () {
                                            $.alert('A very critical action <strong>triggered!</strong>');
                                        }
                                    },
                                    cancel: function () {
                                        $.alert('you clicked on <strong>cancel</strong>');
                                    }
                                }
                            });
                        }
                    },
                    cancel: function () {
                        $.alert('you clicked on <strong>cancel</strong>');
                    },
                }
            });
        });
    });
</script>
@stop