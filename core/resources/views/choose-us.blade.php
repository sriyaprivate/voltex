@extends('layouts.master') @section('title','About Us')

@section('links')
@stop

@section('css')
<style type="text/css">
  a:focus{
    color: inherit;
  }
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item active">Why Choose Us</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="account-section mb-2">
    <div class="container">

      <div class="row profile">
        <div class="col-md-3 mb-3">
          <div class="profile-sidebar">

            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu m-0">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('about-us')}}">About Us</a>
                </li>
                <li class="active nav-item">
                  <a class="nav-link" href="{{url('choose-us')}}">WHY CHOOSE US?</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('terms-condition')}}">TERMS & CONDITIONS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('privacy-policy')}}">PRIVACY POLICY</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">HELP & FAQ'S</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="#">WARRANTY & RETURNS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">DELIVERY METHODS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">REGIONAL CENTERS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">NEWS ROOM</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="{{url('')}}">BLOG</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('')}}">SITE MAP</a>
                </li>                
                <li class="nav-item">
                  <a class="nav-link" href="{{url('contact-us')}}">Contact Us</a>
                </li>

              </ul>
            </div>
            <!-- END MENU -->

          </div>
        </div>
        <div class="col-md-9 mb-3">
          <div class="profile-content mb-3">
            <h5 class="text-uppercase m-0 text-center"><strong>WHY CHOOSE OSTORE.LK ?
</strong></h5>
          </div>

          <hr>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Quality Products</strong></h6>
          </div>
          <p class="text-justify">Keep originality at forefront.</p>
          <br/>
          <p class="text-justify">Ostore own branded products are born through a vigorous pre-tested quality mechanism to meet internationally acclaimed quality standards. Hence for all Orange brands and its associated product’s can be claimed it’s warranties at Orange. At the same time, it is our utmost priority to select most recognized vendors in the vendor selection process to get partnered with us for other branded products as well, where ensuring to meet you the best & quality products by providing with their warranty services.</p>
          <br/>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Value Driven</strong></h6>
          </div>
          <p class="text-justify">Pay exactly for what you buy.</p>
          <br/>
          <p class="text-justify">Ostore make every effort to offer you competitive prices to match with your limitless choices, in return the value you perceived each sum of every purchase you made. Generating value does not we limited to only for price or product but the moment you get to know us and surf us for something itself.</p>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Delivery at your choice.</strong></h6>
          </div>
          <p class="text-justify">Direct, Express & Pick Up</p>
          <br/>
          <p class="text-justify">Affordable shipping options exist for each of your purchases. Free Shipping is provided to certain areas.
Just shop, receive and enjoy your order without getting into any hassles.
          </p>
          <br/>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Mutual Trust & Relationship</strong></h6>
          </div>
        
          <p class="text-justify">Trust & Relationship derives from our commitment.</p>
          <br/>
          <p class="text-justify">We understand everyone is searching us it’s because of the trust that we kept from the starting point. Therefore, we as your online choice, always ensure & committed to maintain the same momentum throughout your journey of online shopping. It reflects our relationship beyond just a purchasing of a product or service on which you feel the best online shopping experience.</p>
          <br/>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Easy Shopping with Secured Payment</strong></h6>
          </div>
          <p class="text-justify">Feel free to access number of products with easy selection. Payments are highly secured with popular and most secured payment options.</p>
         
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
@stop

@section('scripts')
@stop