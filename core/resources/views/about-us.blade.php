@extends('layouts.master') @section('title','About Us')

@section('links')
@stop

@section('css')
<style type="text/css">
  a:focus{
    color: inherit;
  }
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item active">About Us</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="account-section mb-2">
    <div class="container">

      <div class="row profile">
        <div class="col-md-3 mb-3">
          <div class="profile-sidebar">

            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu m-0">
              <ul class="nav flex-column">
                <li class="active nav-item">
                  <a class="nav-link" href="{{url('about-us')}}">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('choose-us')}}">WHY CHOOSE US?</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('terms-condition')}}">TERMS & CONDITIONS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('privacy-policy')}}">PRIVACY POLICY</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">HELP & FAQ'S</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="#">WARRANTY & RETURNS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">DELIVERY METHODS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">REGIONAL CENTERS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">NEWS ROOM</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="#">BLOG</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">SITE MAP</a>
                </li>                
                <li class="nav-item">
                  <a class="nav-link" href="{{url('contact-us')}}">Contact Us</a>
                </li>

              </ul>
            </div>
            <!-- END MENU -->

          </div>
        </div>
        <div class="col-md-9 mb-3">
          <div class="profile-content mb-3">
            <h6 class="text-uppercase m-0"><strong>OVERVEIW</strong></h6>
          </div>

          <hr>

          <p class="text-justify">We are here to keep charging, for you to live better than you were yesterday. Let’s get to know us! </p>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>ABOUT OStore.lk</strong></h6>
          </div>
          <p class="text-justify">By envisioning the online shopping market, The Orel Corporation LTD known as “Orange Electric” proud to present Ostore.lk - leveraging a new era of virtual shopping experience in Sri Lanka.</p>
          <p class="text-justify">While you are in busy or spending highly sophisticated lifestyle which always looks for convenience, reliable and how things can be get fast as much as possible, besides even if you have a sufficient time to manage things but still you are struggling to upgrade your lifestyle, it’s time for you to look at sensible choice.</p>
          <p class="text-justify">Ostore.lk able to meet you wide-range of choices with number of value additions endeavoring the best way to shop online. It incorporates with genuine and extensive selection of products including different warranty schemes, credit card installment facilities, express delivery, various payment methods, exceptional service support and many more. Further most of our products will go through vigorous quality standard process to ensure to meet you a precise & quality product for what you exactly need to be paid.</p>
          <br/>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Buy at your Choice</strong></h6>
          </div>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Discount Hub</strong></h6>
          </div>
          <p class="text-justify">Ostore savings made you simple than ever.  Using savings center, you can buy various products/services at a greater reduction on prices which is available on daily basis. In return you may save your valuable time and money without spending all over again. All products are offered to you by maintaining the original product condition and standards hence value that you received cannot be matched with the price you pay for Ostore.lk.</p>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Big Store</strong></h6>
          </div>
          <p class="text-justify">Big Store consists with extensive product range in association with many local and international brands. Our online catalog is constantly expanding with very latest products and well-known brands ensuring to keep you updated always and you to buy with easy access via online. At the same time, we have facilitated ordering products online and picking them up at our nearest regional centers where if someone considered for touch and feel experience before the purchase is take place. The store products have different warranty schemes, credit card installment facilities, island wide door step delivery services across in Sri Lanka.   </p>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Bulk Market</strong></h6>
          </div>
          <p class="text-justify">We always wanted to match or exceeds your expectations. Also, your perceptions may be different from another buyer. If you are looking to purchase products with Big quantities, Ostore.lk facilitate you to make it real. You could select products from wide range of selections and provided you with unmatched price & warranty. Therefore, the advantage here is that you get an unbelievable discount for what you ordered quantity. Moreover, the products you ordered through bulk market, the delivery charge is totally Free for every corner in Sri Lanka.</p>
          <br/>
          <div class="profile-content mb-3">
            <h4 class="m-0"><strong>Values</strong></h4>
          </div>
          <div class="profile-content mb-3">
            <h5 class="m-0 text-center"><strong>ACT!</strong></h5>
          </div>
          <p class="text-justify">OStore “ACT” Value Framework (Affordability, Convenience, & Trust) gives an opportunity to create the best way to shop you online by honoring every single click you made on our website or App. We believe it gives you a delightful experience from appearance to purchase and further consuming our products and services. Nevertheless, we assure the service you are expecting from us not just limiting to the words but in practice to meet the best solution we can accommodate.</p>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Affordability</strong></h6>
          </div>
          <p class="text-justify">Following affordability, we ensure you to give the best price in the market, it make sense you to “pay exactly for what you buy”, which derives that we offer competitive price as well as superior service to long last. All prices shown here are actual price followed with warranty periods.</p>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Convenience</strong></h6>
          </div>
          <p class="text-justify">Convenience comes when you ordered something from Ostore by following just a simple process. Things you ordered will be delivered to your door step or your office with a shortest duration. But not limited either, we ensure easy navigation of the website and search results find you to purchase exact product or service and get delivered based on your choice with our different delivery options. Further we accept all Visa, Master and AMEX credit/debit cards with 0% installment facilities, Cash on Delivery, Bank Deposits, Ezy cash, Mcash too.</p>
          <div class="profile-content mb-3">
            <h6 class="m-0"><strong>Trust</strong></h6>
          </div>
          <p class="text-justify">Trust and ethics always carrying out hand to hand from our mother company- Orel Cooperation (PVT) LTD known as “Orange Electric” and it’s always most prioritized learning we continue to bring forward and to exceled, not only in Sri Lanka but most wider spectrum in the world. Hence with all those blessings and empowerment we at Ostore believe an authentic collaboration which will take to the highest root and success of the business with our valued customers. Our employees can trust each other, act honorably and to fulfill their defined roles share you the same experience as appropriate. </p>
        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
@stop

@section('scripts')
@stop