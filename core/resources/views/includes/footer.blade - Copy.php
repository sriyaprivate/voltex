<!-- footer bottom -->
<style type="text/css">
	.footer-bottom {
	    position: fixed;
	    left: 0;
	    bottom: 0;
	    width: 100%;
	    background: #eb232a;
	    color: white;
	    text-align: center;
	    height: 20px;
	}
</style>
<div class="footer-bottom">
    <div class="container-fluid">
        <p class="text-uppercase text-center">Copyright © 2018 Orel Corporation.</p>
    </div> <!-- /container -->
</div> <!-- /footer bottom