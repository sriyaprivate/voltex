<div class="header-wrap">
    <div class="header-gray-bar" style="display: none">
        <div class="header-gray-bar-inner">
            <ul class="left-nav">
                <li><a href="{{ url('backend') }}">Inter Company Order Portal</a></li>
            </ul>
            <ul class="right-nav">
                <li><span class="separator"></span></li>
                <li><a href="#"> My Account</a></li>
                <li><span class="separator"></span></li>

                @if(Illuminate\Support\Facades\Auth::check())   
                    <li>
                        <a href="#">
                            @if(count(Auth::user()) > 0 && count(Auth::user()->customer) > 0 && !empty(Auth::user()->customer->first_name))
                                {{ Auth::user()->customer->first_name }} {{ Auth::user()->customer->last_name }}
                            @else
                                @if(count(Auth::user()) > 0 && Auth::user()->user_type_id == USER_TYPE_GUEST)
                                    GUEST USER
                                @else
                                    Unknown user
                                @endif
                            @endif
                        </a>
                    </li>                    
                    <li><span class="separator"></span></li>                    
                    <li>
                        <form id="logout" method="post" action="{{url('logout')}}" style="margin: 0px !important">
                            {{ csrf_field() }}
                            <a href="#" onclick="document.getElementById('logout').submit();">Logout</a>
                        </form>
                    </li>

                @else
                    <li>
                        <a href="{{url('login')}}">
                            <i class="fa fa-power toggle-btn"></i>Login
                        </a>
                    </li>
                    <li><span class="separator"></span></li>
                @endif
            </ul>
            <input type="text" id="REG_MYACC_TEXT" value="Register" style="visibility:hidden">
        </div>
    </div>

    <div class="header-inner">
        <div class="clearboth"></div>
        <a href="{{url('cart/summary')}}" id="mobile-cart-btn" class="show-tablet">
            <span class="mini-cart-loading" style="display:none;"></span>
            <span class="fa fa-shopping-cart"></span>
            <span class="cart-count" id="cartNumItemTop"><span id="cartNumItem">@if(isset($cartItemCount)){{ $cartItemCount }}@else{{0}}@endif</span></span>
        </a>
        @if(!Illuminate\Support\Facades\Auth::check())
            <a href="{{url('login')}}" class="show-tablet" id="mobile-log-btn">
                <span class="fa fa-sign-in"></span>
            </a>            
        @else
            <a href="#" class="show-tablet" id="mobile-log-btn">
                <span class="fa fa-sign-out mobile-log-btn" onclick="document.getElementById('logout').submit();"></span>
            </a>
        @endif
        <div id="mobile-menu-btn" onclick="openNav()">
            <span class="line-bar"></span>
            <span class="line-bar"></span>
            <span class="line-bar"></span>
            <span class="text">menu</span>
        </div>
        <a class="logo-voltex" href="{{url('/')}}">
            <img src="{{asset('assets/images/logo-w-p.png')}}" alt="">
        </a>
        <div class="head-right-column">
            <span id="searchBoxSrc">
                <span class="search-box">
                    <form method="GET" action="{{url('store/search')}}">
                        <input type="text" name="w" class="textbox" placeholder="What can we help you find?" id="searchBox" autocomplete="off">
                            <button type="submit" id="Go" class="search-btn"><i class="fa fa-search"></i></button>
                    </form>
                </span>
            </span>
            
            <div class="phone-number">
                <span class="ctext">Call us</span>
                <span class="callusnow"><a href="tel:1300722275">+94 11 479 2100</a></span>
            </div>

            <div class="header-total-cart-item hide-mobile">
                <div class="mini-cart-loading" style="display:none;"></div>
                <a href="{{url('cart/summary')}}" id="shopping-cart" name="shopping-cart">
                    <span class="cart-icon"><span class="fa fa-shopping-cart"></span></span>
                    <span class="cart-count">
                        <span id="cartNumItem">@if(isset($cartItemCount)){{ $cartItemCount }}@else{{0}}@endif</span>
                        <span class="ctext">item(s)</span>
                    </span>
                </a>
            </div>
        </div>
    </div>

    <div class="navbar-wrap">
        <ul class="navbar">
            
            <li class="separator"></li>
            
            <li>
                <a href="{{url('store')}}?type_id=1">Light</a>
            </li>
            
            <li class="separator"></li>
            
            <li>
                <a href="{{url('store')}}?type_id=2">Water</a>
            </li>
                
            <li class="separator"></li>
                                    
            <li>
                <a href="{{url('store')}}?type_id=3">Electricity</a>
            </li>
                
            <li class="separator"></li>
               
            <li>
                <a href="{{url('store')}}?type_id=4">E3</a>
            </li>            
            
            <li class="separator"></li>

            <li>
                <a href="{{url('store')}}?type_id=5">Data</a>
            </li>
            
            <li class="separator"></li>

            <li>
                <a href="{{url('store')}}?type_id=6">Security</a>
            </li>
            
            <li class="separator"></li>

            <li>
                <a href="{{url('store')}}?type_id=7">Communication</a>
            </li>
            
            <li class="separator"></li>

            <li>
                <a href="{{url('store')}}?type_id=8">IOT</a>
            </li>
            
            <li class="separator"></li>

        </ul>
    </div>
</div>

<div id="mySidenav" class="sidenav">
    <li style="
        list-style-type: none;
        background: #fff;
        border-top: 5px solid crimson;
        padding-bottom: 12.5px
        ">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">
            <span>ICOP</span>
            <i class="fa fa-bars"></i>
        </a>
    </li>

    <li style="list-style-type: none;">
        <a href="http://localhost/html/voltex/store?type_id=&amp;cat_id=2"><i class="fa fa-bars"></i> Categories</a>
    </li>

    @foreach($categories as $node)
        {!! renderMobileSideMenuNode($node) !!}
    @endforeach

</div>