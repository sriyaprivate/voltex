<div class="header-wrap">
    <div class="header-gray-bar" style="display: none">
        <div class="header-gray-bar-inner">
            <ul class="left-nav">
                <li><a href="{{ url('backend') }}">Inter Company Order Portal</a></li>
            </ul>
            <ul class="right-nav">
                <li><span class="separator"></span></li>
                <li><a href="#"> My Account</a></li>
                <li><span class="separator"></span></li>

                @if(Illuminate\Support\Facades\Auth::check())   
                    <li>
                        <a href="#">
                            @if(count(Auth::user()) > 0 && count(Auth::user()->customer) > 0 && !empty(Auth::user()->customer->first_name))
                                {{ Auth::user()->customer->first_name }} {{ Auth::user()->customer->last_name }}
                            @else
                                @if(count(Auth::user()) > 0 && Auth::user()->user_type_id == USER_TYPE_GUEST)
                                    GUEST USER
                                @else
                                    Unknown user
                                @endif
                            @endif
                        </a>
                    </li>                    
                    <li><span class="separator"></span></li>                    
                    <li>
                        <form id="logout" method="post" action="{{url('logout')}}" style="margin: 0px !important">
                            {{ csrf_field() }}
                            <a href="#" onclick="document.getElementById('logout').submit();">Logout</a>
                        </form>
                    </li>

                @else
                    <li>
                        <a href="{{url('login')}}">
                            <i class="fa fa-power toggle-btn"></i>Login
                        </a>
                    </li>
                    <li><span class="separator"></span></li>
                @endif
            </ul>
            <input type="text" id="REG_MYACC_TEXT" value="Register" style="visibility:hidden">
        </div>
    </div>

    <style type="text/css">
        .badge {
            background-color: #000;
            border-radius: 10px;
            color: #fff;
            display: inline-block;
            font-size: 12px;
            line-height: 1;
            padding: 3px 7px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
        }

        .container-menu-cart {
            margin: auto;
            width: 80%;
            margin-left: 71.7%;
        }

        .container-menu-performa-invoice {
            margin: auto;
            width: 80%;
            margin-left: 78.4%;
        }
        
        .cart {
            margin: -40px -25px 0px 0;
            float: right;
            background: white;
            width: 300px;
            position: fixed;
            border-radius: 3px;
            padding: 20px;
            z-index: 9999;
            box-shadow: 0 1px 30px rgba(1, 1, 1, 0.5);
            -webkit-box-shadow: 0 1px 30px rgba(1, 1, 1, 0.5);
          
        }

        .performa-invoice-cart {
            margin: -40px -25px 0px 0;
            float: right;
            background: white;
            width: 300px;
            position: fixed;
            border-radius: 3px;
            padding: 20px;
            z-index: 9999;
            box-shadow: 0 1px 30px rgba(1, 1, 1, 0.5);
            -webkit-box-shadow: 0 1px 30px rgba(1, 1, 1, 0.5);
          
        }
          
        .cart-items {
            padding-top: 20px;
        }
        .cart-items li {
            list-style-type: none;
            border-bottom: 1px solid #E8E8E8;
            padding-bottom: 10px;
            cursor: pointer;
            margin-bottom: 8px;
        }

        .cart-items li:hover {
            background-color: whitesmoke;
        }

        .cart-items img {
            float: left;
            margin-right: 12px;
        }
            
        .cart-items .item-name {
          display: block;
          padding-top: 3px;
          font-size: 12px;
        }
        
        .cart-items .item-price {
          color: crimson;
          margin-right: 8px;
          font-weight: bold;
        }
        
        .cart-items .item-quantity {
          color: $light-text;
          font-weight: bold;
        }
          

        .cart-header {
            border-bottom: 1px solid #E8E8E8;
            padding-bottom: 15px;    
        }

        .cart-total {
          float: right;
          font-weight: bold;
        }

        .cart:after {
            bottom: 100%;
            left: 15%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            border-bottom-color: white;
            border-width: 8px;
            margin-left: -8px;
        }

        .performa-invoice-cart:after {
            bottom: 100%;
            left: 15%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
            border-bottom-color: white;
            border-width: 8px;
            margin-left: -8px;
        }

        .cart-icon {
          color: #515783;
          font-size: 24px;
          margin-right: 7px;
          float: left;
        }

        .clearfix:after {
          content: "";
          display: table;
          clear: both;
        }

        .selected_type{
            background-color: #000;
            color: #fff;
        }
    
        .search-result-list{
            position: fixed;
            margin-top: 48px;
            background: rgb(255, 255, 255);
            list-style: none;
            width: 35%;
            max-height: 200px;
            overflow-y: auto;
            margin-left: 1%;
            background-color: #fff;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            -moz-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
            -webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
            box-shadow: 0 1px 5px rgba(0,0,0,0.10);
            border: 1px solid transparent;
            -webkit-transition: all .5s ease;
            -moz-transition: all .5s ease;
            -ms-transition: all .5s ease;
        }
        .search-result-list li{
            padding: 15px;
        }

        .search-result-list li:hover{
            background: #eeeeee;
            color: #000;
            border: 1px solid #000;
        }

        .search-result-list li:hover a{
            color: #000!important;
        }

        .search-result-list li img{
            width: 40px;
            border: 1px solid red;
        }
    </style>

    <div class="header-inner">
        <div class="clearboth"></div>
        <a href="{{url('repeater/list')}}" id="mobile-cart-btn" class="show-tablet" style="right: 84px">
            <span class="mini-cart-loading" style="display:none;"></span>
            <span class="fa fa-plus"></span>
        </a>
        <a href="{{url('cart/summary')}}" id="mobile-cart-btn" class="show-tablet">
            <span class="mini-cart-loading" style="display:none;"></span>
            <span class="fa fa-shopping-cart"></span>
            <span class="cart-count" id="cartNumItemTop"><span id="cartNumItem">@if(isset($cartItemCount)){{ $cartItemCount }}@else{{0}}@endif</span></span>
        </a>
        @if(!Illuminate\Support\Facades\Auth::check())
            <a href="{{url('login')}}" class="show-tablet" id="mobile-log-btn">
                <span class="fa fa-sign-in"></span>
            </a>            
        @else
            <a href="#" class="show-tablet" id="mobile-log-btn">
                <span class="fa fa-sign-out mobile-log-btn" onclick="document.getElementById('logout').submit();"></span>
            </a>
        @endif
        <div id="mobile-menu-btn" onclick="openNav()">
            <span class="line-bar"></span>
            <span class="line-bar"></span>
            <span class="line-bar"></span>
            <span class="text">menu</span>
        </div>
        <a class="logo-voltex" href="{{url('/')}}">
            <img src="{{asset('assets/images/logo-w-p.png')}}" alt="">
        </a>
        <div class="phone-number" style="border: 0;padding-top: 32px">
            <span class="ctext">Call us</span>
            <span class="callusnow">011 4792 200</span>
        </div>
        <div class="head-right-column">
            <span id="searchBoxSrc">
                <span class="search-box">
                    <form method="GET" action="{{url('store/search')}}">
                        <input type="text" name="w" id="auto-product" class="textbox" placeholder="What can we help you find?" autocomplete="off">
                            <button type="submit" id="Go" class="search-btn"><i class="fa fa-search"></i></button>
                    </form>
                </span>

                <ul class="search-result-list" style="position: fixed;margin-top: 50px;background: #fff;list-style: none;display: none;" id="search-result-list">
                </ul>

            </span>
            
            <div class="header-total-cart-item hide-mobile" style="border-right: 1px solid #dde0e2;">
                <a href="{{url('repeater/list')}}" style="font-size: 16px;text-decoration: none;" id="repeater" name="repeater" alt="My Cart" title="My Cart">
                    Repeater
                </a>
            </div>

            <div class="header-total-cart-item hide-mobile" style="border-right: 1px solid #dde0e2;">
                <div class="mini-cart-loading" style="display:none;"></div>
                <a href="javascript:void(0);" style="font-size: 16px;text-decoration: none;" id="shopping-cart" name="shopping-cart" alt="My Cart" title="My Cart">
                    <i class="fa fa-shopping-cart" style="font-size: 25px;"></i> Cart 
                    <span class="badge">@if(isset($cartItemCount)){{ $cartItemCount }}@else{{0}}@endif</span>
                </a>
            </div>

            <div class="header-total-cart-item hide-mobile" style="border-right: 1px solid #dde0e2;text-align: center">
                <a href="javascript:void(0);" style="font-size: 16px;text-decoration: none;" id="performa-invoice-popup" name="performa-invoice-popup" alt="Pending Performa Invoices" title="Pending Performa Invoices">
                    <i class="fa fa-first-order" style="font-size: 25px;"></i> PIs 
                    <span class="badge">@if(isset($pfis_count)){{ $pfis_count }}@else{{0}}@endif</span>
                </a>
                
            </div>
            <div class="header-total-cart-item hide-mobile" style="border-right: 1px solid #dde0e2;text-align: center">
                @if(!Illuminate\Support\Facades\Auth::check())
                    <a href="{{url('login')}}" style="font-size: 16px;text-decoration: none">
                        <i class="fa fa-sign-in" style="font-size: 25px;"></i>Login 
                    </a>            
                @else
                    <a href="javascript:void(0)" onclick="document.getElementById('logout').submit();">
                        <i class="fa fa-sign-out" style="font-size: 25px;"></i>Logout
                    </a>
                @endif
            </div>
        </div>
    </div>
    <div class="navbar-wrap">
        <ul class="navbar">
            @foreach($cat_types as $key=>$value)
                <li class="separator"></li>
                <li>
                    <a href="{{url('store')}}?type_id={{$value->id}}" 
                        class="@if(isset($selected_categoryType) && $selected_categoryType->id==$value->id) selected_type @endif">{{$value->name}}</a>
                </li>
            @endforeach

            <li class="separator"></li>

        </ul>
    </div>
</div>

<div id="mySidenav" class="sidenav">
    <li style="
        list-style-type: none;
        background: #fff;
        border-top: 5px solid crimson;
        padding-bottom: 12.5px
        ">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">
            <span>ICOP</span>
            <i class="fa fa-bars"></i>
        </a>
    </li>

    <li style="list-style-type: none;">
        <a href="http://localhost/html/voltex/store?type_id=&amp;cat_id=2"><i class="fa fa-bars"></i> Categories</a>
    </li>

    @foreach($categories as $node)
        {!! renderMobileSideMenuNode($node) !!}
    @endforeach

</div>

<div class="container-menu-cart">
    <div class="cart" id="popup-cart" style="display: none">
        <div class="cart-header">
            Cart Details <span class="badge"></span>
            <div class="cart-total" style="color: crimson">
                <span class="lighter-text">Total:</span>
                <span class="main-color-text">$ 
                    @if(isset($cart_details->items))
                        <?php $total_amount = 0; ?>
                        @foreach($cart_details->items as $key=>$value)
                            <?php $total_amount += $value['item']->price*$value['qty']; ?>
                        @endforeach
                        <?php echo $total_amount; ?>
                    @else
                        0
                    @endif
                </span>
            </div>
        </div> <!--end shopping-cart-header -->

        <ul class="cart-items" style="max-height: 250px;overflow: auto">
            @if(isset($cart_details->items))
                
                @foreach($cart_details->items as $key=>$value)
                    <li class="clearfix">
                        <img style="width: 50px" src="{{ url('data/product_thumb/images/product/'.$value['item']->image)}}" alt="item1" />
                        <span class="item-name">{{$value['item']->name}}</span>
                        <span class="item-price">{{'$'}}{{$value['item']->price}}</span>
                        <span class="item-quantity badge">{{$value['qty']}}</span>
                    </li>

                    

                @endforeach
            @else
                <li class="clearfix">
                    <span class="item-name">Cart Empty...</span>
                </li>
            @endif
        </ul>

        <a href="javascript::void(0)" onclick="hidePopupCart()"  style="color: crimson;text-decoration: underline;">Close</i></a>
        <a href="{{url('cart/summary')}}" class="btn-sm btn-danger pull-right" style="color: #fff">Checkout</a>
    </div> <!--end shopping-cart -->
</div>

<div class="container-menu-performa-invoice">
    <div class="performa-invoice-cart" id="popup-performa-invoice" style="display: none">
        <div class="cart-header">
            Performa Invoice <span class="badge" style="background-color: crimson">Pending</span>

            <div class="cart-total" style="margin-top: 4px;">
                <span class="main-color-text badge" title="Approve pending invoice count">@if(isset($pfis_count)){{ $pfis_count }}@else{{0}}@endif</span>
            </div>
        </div> <!--end shopping-cart-header -->

        <ul class="cart-items" style="max-height: 250px;overflow: auto">
            @if(isset($pfis) && count($pfis)>0)
                @foreach($pfis as $key=>$value)

                    @if($value['order'])

                        <li class="clearfix" onclick="location.href='{{url('account/order-view/'.$value['order']->id)}}'">
                            <!-- <a href="{{url('account/order-view/'.$value['order']->id)}}"> -->
                                <img style="width: 50px" src="{{ asset('assets/images/icons/order-history.svg')}}" alt="item1" />
                                <span class="item-price">{{$value->pf_no}}</span><span class="item-quantity pull-right">$ {{$value['order']->amount}}</span>
                                <span class="item-name">{{$value['order']->order_no}}</span>
                                <span class="item-name">
                                    <strong class="pull-left">{{date('j M, Y',strtotime($value->created_at))}}</strong>
                                    <strong class="pull-right">{{$value['created_at']->diffForHumans()}}</strong>
                                </span>                            
                            <!-- </a> -->
                        </li>
                    @endif
                @endforeach
            @else
                <li class="clearfix">
                    <span class="item-name">No Pending Invoices...</span>
                </li>
            @endif
        </ul>

        <a href="javascript::void(0)" onclick="hidePopupPerformaINvoiceCart()"  style="color: crimson;text-decoration: underline;">Close</i></a>
        <a href="{{url('account/order-list')}}" class="btn-sm btn-danger pull-right" style="color: #fff">View All</a>
    </div> <!--end shopping-cart -->
</div>