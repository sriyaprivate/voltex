<style type="text/css">
    .text-15{
        color: #000000 !important;
    }

    .logo{
        padding: 45px 0 0 50px !important;
    }

    .item-title-name{
        font-family: Merriweather, Times, Times New Roman, serif !important;
        font-size: 18px !important;
        margin: 0.3em 0 !important;
        text-transform: none !important;
        letter-spacing: 0.03em !important;
        line-height: 20px !important;
        color: #fff !important;
    }

    .card-body-background{
        background-color: #081e33 !important;
    }

    #fixed-menu {
      position: fixed;
      right: 0;
      top: 25.6%;
      width: 2.5em;
      height: 4em;
      margin-top: -2.5em;
      background-color: crimson;
      color: #fff;
      cursor: pointer;
    }

    #fixed-cart-panel {
        position: fixed;
        right: 0;
        top: 24.2%;
        width: 2.5em;
        height: 4em;
        margin-top: -2.5em;
        /*background-color: gray;*/
        /*color: #fff;*/

        background-color: #fff;
        border: 1px solid transparent;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }

    #fixed-cart-panel>.panel-heading {
        padding: 10px 15px;
        border-bottom: 1px solid transparent;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
        background-color: crimson;
        color: #fff;
        font-size: large;
    }

    .samedaydespatch {
        float: left;
        width: 150px;
        position: relative;
        top: 5px;
        margin-right: 20px;
    }

    .samedaydispatch-text{
        float: left;
        color: #808080;
        font-size: 16px;
        line-height: 16px;
        font-weight: bold;
        text-transform: uppercase;
    }

    .title-row{
        overflow: hidden;
        border-bottom: 1px solid #dde0e2;
        padding-bottom: 5px;
    }

    .img-icon {
        float: left;
        padding-right: 8px;
        padding-left: 7px;
    }

    .textbox {
        width: 100%;
        height: 48px;
        line-height: normal !important;
        padding: 0 18px;
        line-height: normal;
        float: left;
        border: 1px solid;
        background: none;
        font-size: 18px;
        font-weight: 400!important;
        color: #3d4044;
        -webkit-border-radius: 36px !important;
        -moz-border-radius: 36px !important;
        border-radius: 36px;
    }

    .menutext{
        position: relative;
        display: block;
        padding: 0 0px;
        font-size: 14px;
        color: #fff;
        height: 50px;
        line-height: 50px;
        text-decoration: none;
        transition: background .35s ease-in-out;
        -moz-transition: background .35s ease-in-out;
        -webkit-transition: background .35s ease-in-out;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .seperator{
        width: 0;
        height: 50px;
        border-left: 1px solid rgba(255,255,255,0.2);
    }

    .menutext:hover,active,focus{
        color: black !important;
        font-weight: bolder !important;
    }

</style>
<!-- top header menu -->
<div class="top-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-6 col-sm-6 col-md-6">
                <ul class="nav top-header-nav-left justify-content-begin justify-content-left-mobile">
                    <li class="nav-item" style="margin-right: 10px">
                        <a class="nav-link red-on-hover" href="#">
                            <i class="fa fa-power toggle-btn"></i>Home
                        </a>
                    </li>
                    <li class="nav-item" style="margin-right: 10px"><a class="nav-link disabled" href="#">|</a></li>
                    <li class="nav-item" style="margin-right: 10px">
                        <a class="nav-link red-on-hover" href="#">
                            <i class="fa fa-power toggle-btn"></i>About Us
                        </a>
                    </li>
                    <li class="nav-item" style="margin-right: 10px"><a class="nav-link disabled" href="#">|</a></li>
                    <li class="nav-item" style="margin-right: 10px">
                        <a class="nav-link red-on-hover" href="#">
                            <i class="fa fa-power toggle-btn"></i>Contact
                        </a>
                    </li>
                    <li class="nav-item" style="margin-right: 10px"><a class="nav-link disabled" href="#">|</a></li>
                    <li class="nav-item" style="margin-right: 10px">
                        <a class="nav-link red-on-hover" href="#">
                            <i class="fa fa-power toggle-btn"></i>Terms
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-12 col-lg-6 col-sm-6 col-md-6">
            <!-- right top header menu -->
                <ul class="nav top-header-nav-right justify-content-end justify-content-center-mobile">
                    <li class="nav-item" style="margin-right: 10px">
                        <a class="nav-link red-on-hover" href="{{ url('backend') }}">
                            <i class="fa fa-power toggle-btn"></i>Backend
                        </a>
                    </li>
                    <li class="nav-item" style="margin-right: 10px"><a class="nav-link disabled" href="#">|</a></li>
                    <li class="nav-item" style="margin-right: 10px">
                        <a class="nav-link red-on-hover" href="{{ url('backend') }}">
                            <i class="fa fa-power toggle-btn"></i>My Account
                        </a>
                    </li>
                    <li class="nav-item" style="margin-right: 10px"><a class="nav-link disabled" href="#">|</a></li>
                    @if(Illuminate\Support\Facades\Auth::check())   
                        <li class="nav-item" style="margin-right: 10px">
                            <a class="nav-link red-on-hover mobile-btn" href="#">
                                @if(count(Auth::user()) > 0 && count(Auth::user()->customer) > 0 && !empty(Auth::user()->customer->first_name))
                                    {{ Auth::user()->customer->first_name }} {{ Auth::user()->customer->last_name }}
                                @else
                                    @if(count(Auth::user()) > 0 && Auth::user()->user_type_id == USER_TYPE_GUEST)
                                        GUEST USER
                                    @else
                                        Unknown user
                                    @endif
                                @endif
                            </a>
                        </li>
                        
                        <li class="nav-item" style="margin-right: 10px"><a class="nav-link disabled" href="#">|</a></li>
                        
                        <li class="nav-item" style="margin-right: 10px">
                            <form id="logout" method="post" action="{{url('logout')}}" style="margin: 0px !important">
                                {{ csrf_field() }}
                                <a class="nav-link red-on-hover mobile-btn" href="#" onclick="document.getElementById('logout').submit();">Logout</a>
                            </form>
                        </li>

                    @else
                        
                        <li class="nav-item" style="margin-right: 10px">
                            <a class="nav-link red-on-hover" href="{{url('login')}}">
                                <i class="fa fa-power toggle-btn"></i>Login
                            </a>
                        </li>

                        <li class="nav-item" style="margin-right: 10px"><a class="nav-link disabled" href="#">|</a></li>

                        <li class="nav-item" style="margin-right: 10px">
                            <a class="nav-link red-on-hover" href="{{url('register')}}">
                                <i class="fa fa-power toggle-btn"></i>Register
                            </a>
                        </li>
                    @endif
                    <li>
                        <a href="{{url('cart/summary')}}">
                            <div class="cf ml-1 mr-1 pull-left" style="margin-top: 2px;">                      
                                <img class="top animate-me" src="{{asset('assets/images/icons/shopping-cart-top.svg')}}" style="width: 25px;">
                            </div>
                            <div class="text-uppercase desc">
                                @if(isset($cartItemCount))
                                    <span class="active badge badge-cart badge-mobile" style="position: absolute;margin-top: 2px !important;margin-left: -10px !important;">{{ $cartItemCount }}</span>
                                @endif
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div> <!-- /container -->
</div> <!-- /top header menu -->

<!-- main header menu -->
<div class="main-header" style="background: #fff">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-12">
                <div class="" style="padding-top: 25px">
                    <a href="{{ url('/') }}"><h3><strong>BUY</strong>ORANGE</h3></a>
                </div>
            </div>
    
            <div class="col-md-10 col-sm-10 col-12">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="row" style="border-bottom: 1px solid #ccc; padding: 5px;margin-top: 5px">
                            <div class="col-sm-6" style="padding-right: 0px">
                                <img src="{{asset('assets/images/icons/delivery.png')}}" style="width: 57px;height: 40px">
                            </div>
                            <div class="col-sm-6" style="padding-left: 0px">
                                SameDay<br>
                                Dispatch
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="row" style="border-bottom: 1px solid #ccc; padding: 4px;margin-top: 5px">
                            <div class="col-sm-6">
                                <a href="{{ url('/') }}"><h3><strong>Free</strong></h3></a>
                            </div>
                            <div class="col-sm-6">
                                Shipping<br>
                                &Return
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row" style="padding: 4px;margin-top: 5px">
                            <input class="textbox" type="text" placeholder="What can we help you find ?" name="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <h3 style="text-align: center;margin-top: 15px">+9411 4792 100</h3>                        
                    </div>
                    <div class="col-sm-1">
                        <a href="{{url('cart/summary')}}">
                            <div class="cf ml-1 mr-1 pull-left" style="margin-top: 2px;width: 50px;height: 50px">                      
                                <img class="top animate-me" src="{{asset('assets/images/icons/shopping-cart-top.svg')}}" style="width: 100%;margin-top: 5px">
                            </div>
                            <div class="text-uppercase desc">
                                @if(isset($cartItemCount))
                                    <span class="active badge badge-cart badge-mobile" style="position: absolute;margin-top: 2px !important;margin-left: -10px !important;">{{ $cartItemCount }}</span>
                                @endif
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /container -->
</div> <!-- /main header menu -->

<!-- header search panel -->
<div class="search-panel" style="display: block;height: 50px;background-color: #eb232a;position: relative;z-index: 250;clear: both;">
    <div class="container-fluid">
        <ul class="nav top-header-nav-left justify-content-begin justify-content-left-mobile">
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=1">
                    <i class="fa fa-power toggle-btn" href="{{url('store')}}?type_id=1"></i>Outlet & Switches
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=2">
                    <i class="fa fa-power toggle-btn"></i>Electrical Accessories
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=3">
                    <i class="fa fa-power toggle-btn"></i>Circuit Protection
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=4">
                    <i class="fa fa-power toggle-btn"></i>Lighting
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=5">
                    <i class="fa fa-power toggle-btn"></i>Industrial
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=6">
                    <i class="fa fa-power toggle-btn"></i>Cable Management
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=7">
                    <i class="fa fa-power toggle-btn"></i>Fasteners
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=8">
                    <i class="fa fa-power toggle-btn"></i>Tools
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=9">
                    <i class="fa fa-power toggle-btn"></i>Ventilation
                </a>
            </li>
            <li class="nav-item seperator" style="margin-right: 10px"></li>
            <li class="nav-item" style="margin-right: 10px">
                <a class="nav-link red-on-hover menutext" href="{{url('store')}}?type_id=10">
                    <i class="fa fa-power toggle-btn"></i>DataComms
                </a>
            </li>
        </ul>
    </div>
</div> <!-- /header search panel -->