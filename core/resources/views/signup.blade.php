@extends('layouts.master') @section('title','Sign Up')

@section('links')
  <!-- craftpip-jquery-confirm -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />
  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style type="text/css">
    .help-block{
        color: red !important;
    }
</style>
@stop

@section('content')
    <!-- sign in/up panel -->
    <div class="sign-in-section mt-2 pt-4 pb-4">
        <div class="container-fluid">

            <!-- wrapper -->
            <div class="inside-wrapper">
    
                <!-- <div class="card-deck">-->
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-xm-4">
                    <!-- login-form-->
                       <!--  <div class="card border-0">
                            <div class="card-block p-0">
                                <div class="detail-back">
                                    <div class="text-center desc">
                                        <h1 class="ionyx text-center text-uppercase">orelcorp</h1>
                                        <p class="ionyx text-center text-capitalize text-center text-uppercase">ECOMMERCE SOLUTIONS</p>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-xm-4">
                        <!--sing-up-form-->
                        <form class="card" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="card-body pb-0">
                                
                                <div class="account-holder">
                                    <h6 class="text-uppercase text-center font-weight-bold">Join FREE</h6>
                                    <p><small>Free registration enable you to Keep track of your orders, check out faster, and save products into lists & many more.</small></p>
                                    
                                    <div class="row">
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-6">
                                            <label class="control-label required-control-label required" for="first_name"> First Name</label>
                                            <input id="first_name" name="first_name" type="text" value="{{ old('first_name') }}" class="form-control" placeholder="Enter First Name" autofocus>
                                            @if ($errors->has('first_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                
                                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-6">
                                            <label class="control-label required-control-label required" for="last_name"> Last Name</label>
                                            <input id="last_name" name="last_name" type="text" value="{{ old('last_name') }}" class="form-control" placeholder="Enter Last Name">
                                            @if ($errors->has('last_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label class="control-label" for="phone">Phone</label>
                                        <input id="phone" name="phone" type="text" value="{{ old('phone') }}" class="form-control" placeholder="Enter Mobile Number. Ex: 0760000000">
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label required-control-label required" for="email">Email</label>
                                        <input id="email" name="email" type="email" value="{{ old('email') }}" class="form-control" placeholder="Enter Email Address" autocomplete="off">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label required-control-label required" for="password">Password</label>
                                        <input id="password" name="password" type="password" class="form-control" placeholder="Enter Password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                 <p><small>By creating an account, you agree to Ostore <a href="{{ url('terms-condition') }}" class="text-underline">Terms & Conditions</a> of Use and <a href="{{url('privacy-policy')}}" class="text-underline">Privacy Policy</a>.</small></p>
                            </div>
                            
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary btn-block">Register</button>
                            </div>
                        </form> <!--/.sing-up-form-->
                    </div>
                    <div class="col-sm-4 col-md-4 col-xm-4">
                        <!-- <div class="card border-0">
                            <div class="card-block p-0">
                                <div class="detail-back">
                                    <div class="text-center desc">
                                        <h1 class="ionyx text-center text-uppercase">orelcorp</h1>
                                        <p class="ionyx text-center text-capitalize text-center text-uppercase">ECOMMERCE SOLUTIONS</p>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>    
                <!-- </div> -->

            </div> <!-- /.wrapper -->

        </div> <!-- /.container -->

  </div> <!-- /.sign in/up panel -->
@stop

@section('js')
  <!-- craftpip-jquery-confirm -->
  <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
  <!-- toastr notification -->
  <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

  <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
<script>
    $(document).ready(function(){
        @if(Request::get('guest') == true)
            notification("toast-top-right", "warning", "Please sign in to the system before adding items to the wishlist!.", "15000");    
        @endif
    });
</script>
@stop