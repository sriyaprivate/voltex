@extends('layouts.master') @section('title','Terms & Condition')

@section('links')
@stop

@section('css')
<style type="text/css">
  a:focus{
    color: inherit;
  }
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item active">Terms & Condition</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="account-section mb-2">
    <div class="container">

      <div class="row profile">
        <div class="col-md-3 mb-3">
          <div class="profile-sidebar">

            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu m-0">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('about-us')}}">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('choose-us')}}">WHY CHOOSE US?</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link" href="{{url('terms-condition')}}">TERMS & CONDITIONS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('privacy-policy')}}">PRIVACY POLICY</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('')}}">HELP & FAQ'S</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="{{url('')}}">WARRANTY & RETURNS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('')}}">DELIVERY METHODS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('')}}">REGIONAL CENTERS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">NEWS ROOM</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="{{url('')}}">BLOG</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('')}}">SITE MAP</a>
                </li>                
                <li class="nav-item">
                  <a class="nav-link" href="{{url('contact-us')}}">Contact Us</a>
                </li>

              </ul>
            </div>
            <!-- END MENU -->

          </div>
        </div>
        <div class="col-md-9 mb-3">
          <div class="profile-content mb-3">
            <h6 class="text-uppercase m-0"><strong>TERMS & CONDITIONS</strong></h6>
          </div>

          <hr>

          <p class="text-justify">Please read the following terms and conditions carefully. By using this site or any Orelcorporation.com services, you are agreeing to the terms and conditions stipulated by Orel Corporation (Pvt)Ltd</p>
          
          <p class="text-justify">This Site is operated by Orel Corporation (Pvt) Ltd. a company registered under the Company Laws in the Socialist Republic of Sri Lanka under company number PV72125 with our registered office at No 49, Sri Jinarathana Road , Colombo 02 (hereinafter sometimes referred to as Orel Corporation, Us, We, Our as the context requires).</p>
           <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>1. Definitions</strong></h6>
          </div>
          <p class="text-justify">"Access Information" means the access information that we require from you before entering certain parts of the Sites for which you have registered, which may be a username, password, your mobile phone number or similar recognition device;</p>

          <p class="text-justify">"Content" means all data, information, material and content, including but not limited to text, pictures, photographs, software, video, music, sound and graphics;</p>

          <p class="text-justify">"Functions" means the services offered on or via the Site, which may include (but is not limited to) chat, discussion group or bulletin board services or email, SMS or voice messaging services, online transactions, search engines and e-commerce facilitators;</p>

          <p class="text-justify">"Services" means the services that we are providing to you via the Site which may consist of the Content and the Functions available on the Site or otherwise provided to you as a result of your use of the Site.</p>

          <p class="text-justify">"Terms and Conditions" means this Agreement between Orel Corporation (Pvt) Ltd  and you incorporating these terms and conditions;</p>

           <p class="text-justify">“Third Party Supplier” means third party manufacturers, distributors or suppliers who are registered with Orel Corporation to advertise ,provide, supply, make available Products on this Site.</p>

           <p class="text-justify">"Trade Marks" means Orange Electric mark, words and any other  trade or service marks, logos belonging to  and used by Orel Corporation , including stylized representations, all associated logos and symbols, and combinations of the foregoing with another word or mark;</p>

           <p class="text-justify">"Product” means the Products which are displayed on the site available for purchase by the User</p>

           <p class="text-justify">“User” means the person who uses the Site and the Services and sometimes may also interchangeably be referred to as “you”, “yours”</p>
          <br/>
          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>2. User Responsibilities</strong></h6>
          </div>

          <p class="text-justify">You are responsible for providing accurate information at the time of registration and you are responsible for ensuring that details are updated and current. Orel Corporation reserves the right to terminate your membership at any time if  you breach these terms and conditions.</p>
         
          <p class="text-justify">Upon registration you are solely responsible for the User ID on the site and all the actions made on our site while logged in (at all times) as a member. We recognize any login and activity within the site, using the user ID and the login credentials as a valid activity by You as a registered user. You are completely responsible for the account being accessed and the use of our services while logged in. Maintaining the security and confidentiality of the login credentials is the total responsibility of the user.</p>

          <p class="text-justify">You represent that you are above the age of 18 in order to enter in to this Agreement and to use the Services provided by this Site.</p>

           <p class="text-justify">You agree that you will only use the Sites and the Services in accordance with these Terms and Conditions in an appropriate and lawful manner. By way of illustration and not as a limitation that you shall not (and shall not authorise or permit any other party to)</p>

           <p class="text-justify ml-5">Receive, access or transmit any Content which is obscene, pornographic, threatening, racist, menacing, offensive, defamatory, in breach of confidence, in breach of any intellectual property right or otherwise objectionable or unlawful;</p>
           
            <p class="text-justify ml-5">Circumvent user authentication or security of any host, network or account (referred to as "cracking" or "hacking") nor interfere with service to any user, host or network (referred to as "denial of service attacks") nor copy any pages or register identical keywords with search engines to mislead other users into thinking that they are reading Orel Corporation’s web pages (referred to as "page-jacking") or use the Sites or the Services for any other unlawful or objectionable conduct. Users who violate systems or network security may incur criminal or civil liability and we will in our absolute discretion fully co-operate with investigations of suspected criminal violations, violation of systems or network security according to the directions of law enforcement or relevant authorities;</p>

            <p class="text-justify ml-5">Knowingly or recklessly transmit any electronic Content (including viruses) through the Site and/or the Services which shall cause or is likely to cause detriment or harm, in any degree, to computer systems owned by us or other Internet users;</p>
            
            <p class="text-justify ml-5">Hack into, make excessive traffic demands, deliver or forward chain letters, "junk mail" or "spam" of any kind, surveys, contests, pyramid schemes or otherwise engage in any other behavior intended to inhibit other users from using and enjoying the Site and/or the Services or any other web site, or which is otherwise likely to damage or destroy our reputation or the reputation of any third party.</p>

            <p class="text-justify">You agree to not remove, change or obscure in any way anything on the Site and/or the Services or otherwise use any material obtained whilst using the Site and/or the Services except as set out in these Terms and Conditions.</p>

            <p class="text-justify">You agree not to reverse engineer or decompile (whether in whole or in part) any software used in the Site and/or the Services except to the extent expressly permitted by applicable law.</p>

            <p class="text-justify">You agree not to copy or use any material from the Site and/or the Services for any commercial purpose.</p>

            <p class="text-justify">You agree not to remove, obscure or change any copyright, trade mark or other intellectual property right notices contained in the original material or from any material copied or printed off from the Site or obtained as a result of the Services.</p>
            
            <p class="text-justify">In the event you are a User, who has not subscribed to Orel Corporation site, you acknowledge that access to the Site and/or the Services may be restricted at our sole discretion in the interest of fully subscribed customers.</p>  
        
            <div class="profile-content mb-3 ml-3">
              <h6 class="m-0"><strong>2. Placing your order and delivery</strong></h6>
            </div>
        
           <p class="text-justify">You can purchase a Product or Service by searching and selecting a specific Product category. Once you have selected a Product/s you will be prompted to  the shopping cart. The shopping cart indicates the summery of  the Products that you wish to purchase such as prices, quantities, discounts etc. At this point you are able to amend your order or make any changes to your order.</p>

           <p class="text-justify">If the order details are accurate you will be  prompted to the order check out process where you may be requested to provide your delivery details, the method of payment and details of any  promotional offers if you are entitled to, such special offers and you would be required to confirm the order.</p>

           <p class="text-justify">If you opt for the payment to take place through credit or debit card the transaction will then  be directed to the authorized internet payment gateway (IPG) which the user need to complete the transaction as instructions given by IPG provider.</p>

           <p class="text-justify">Please note that your order constitutes an offer to us to buy a Product. All orders require to be accepted by us, and we will confirm such acceptance to you by sending you an e-mail that confirms that the Product is available and will be dispatched upon confirmation of the payment by the relevant bank.</p>

           <p class="text-justify">The contract between us will be formed only when you receive the order confirmation email from Us. We may be unable to process your order in the event:</p>

            <p class="text-justify ml-2">(a) the Product you ordered is out of stock or discontinued; or</p>
            <p class="text-justify ml-2">(b) there is a problem with authorization of the payment on your debit or credit card.</p>
        
          <p class="text-justify">Upon the confirmation of your order by us, in the event you have opted for delivery to you, an order notification will be sent to the courier  who will contact you prior to delivery of the Product.</p>

          <p class="text-justify">The user may also order online and pick up from a nearest regional point by providing necessary proof of your identity.</p>
          
          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>4. Price of Products & Services  and making payments</strong></h6>
          </div>  
        
          <p class="text-justify">The price of any Products & Services will be as quoted on our Site from time to time. These prices include VAT and other taxes. Delivery costs are excluded from the price   which will be added to the total amount due once you have selected a delivery service from the available options as set out in delivery information.</p>

          <p class="text-justify">Prices are liable to change at any time, but changes will not affect orders in respect of which we or our courier have already sent you a Dispatch Confirmation.</p>

          <p class="text-justify">This site offers the following payment options :<br/>Credit/Debit Cards/ cash on Delivery/ Ezycash/Mcash/Bank Deposits</p>
          
          <p class="text-justify">In the case of Credit/Debit Card payments, you will receive an order confirmation and payment confirmation and your order will be processed after  you receive the order confirmation. By using a credit/debit card to pay for your order, you confirm that the card being used is yours. All credit/debit card holders are subject to validation checks and authorization by the card issuer. If the issuer of your card refuses to authorize payment we will not accept your order/offer and we will not be liable for any delay or non-delivery and we are not obliged to inform you of the reason for the refusal. We are not responsible for your card issuer or bank charging you as a result of our processing of your credit/debit card payment in accordance with your order.</p>
          
          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>5. Order Cancellation</strong></h6>
          </div>

          <p class="text-justify">You will not be able to cancel the order after  You receive the order confirmation from us.</p>
          
          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>6. Warranty Claims, Product Returns and Refunds</strong></h6>
          </div>

          <p class="text-justify">Products  may have a warranty  period specified with the Product documents supplied to you at delivery. You may contact us directly for Products which are under the warranty period by Orel Corporation. In the event the Product is of a Third party Supplier, manufacturer and /or distributor, you are required to contact the Third Party Supplier directly  for any warranty related matters as indicated in the Product documents.</p>

          <p class="text-justify">Product returns will only be accepted  within the warranty period only in the event there is a defect in material or workmanship. The Product returns should be accompanied with the warranty card. You will be required to contact the Third Party Supplier in the event your Product is from such Third Party Supplier. Orel Corporation shall not be liable for Product returns or refunds for Products of Third Party Suppliers.</p>

          <p class="text-justify">You will only be entitled to a refund in the event we accept a Product return wherein a Product  you have purchased contains a defect in workmanship or material. We will make the refund in the same manner  that you made the payment.</p>

          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>7. Trademarks</strong></h6>
          </div>

          <p class="text-justify">All Trade Marks, service marks, Logos , signs  or otherwise  of Orel Corporation used on the Site and/or the Products and Services are the Trade Marks owned by Orel Corporation. You shall not use these Trade Marks, whether design or word marks: (1) as or as part of your own trade marks; (2) in a manner which is likely to cause confusion; (3) to identify products to which they do not relate; (4) to imply endorsement or otherwise of products or services to which they do not relate; or (5) in any manner which does or may cause damage to the reputation of Orel Corporation or its Trade Marks.</p>

          <p class="text-justify">All trademarks, service marks, logos and designs of  Third Party Suppliers or manufacturers, suppliers and/or distributors  which are  displayed on this Site  are displayed by Orel  Corporation after having obtained the necessary licenses and permissions from such third parties.</p>

          <p class="text-justify">You acknowledge and agree that the Products & Services and the Site or any part thereof, whether presented to you by Orel Corporation, any advertisers or any third party are protected by copyrights, trademarks, service marks, patents, or other proprietary rights and laws. All rights are expressly reserved.</p>

          <p class="text-justify">You acknowledge to use the Site and the Services as set out in these Terms and Conditions and nothing on the Site and/or the Services shall be construed as conferring any license or any other transfer of rights to you of any intellectual property or other proprietary rights of Orel Corporation, any member of Orel Corporation or any of its partnerships or any third party, whether by estoppel, implication or otherwise.</p>

          <p class="text-justify">We assume no responsibility for the violation of any third-party copyrights, trademarks, service marks, patents, or other proprietary rights and laws by the third party web sites which maybe linked to this Site.</p>

          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>8. Liability for content</strong></h6>
          </div>

          <p class="text-justify">It is your responsibility to satisfy yourself prior to using the Site and the Services in any way that they are suitable for your purposes and up to date. The Services and in particular, prices are periodically updated and you should check the Site and the Services regularly to ensure that you have the latest information.</p>

          <p class="text-justify">The Sites and the Services are provided on an "as is" basis. Although every effort has been made to provide accurate information on these pages, neither Orel Corporation, nor any of its employees, nor any member of the Orel Corporation’s affiliate or partner companies, their suppliers, nor any of their employees, make any warranty, expressed or implied, or assume any legal liability (to the extent permitted by law) or responsibility for the suitability, reliability, timeliness, accuracy or completeness of the Products and Services or any part thereof contained on the Site or in the Products and Services.</p>

          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>9. Liability for third party content</strong></h6>
          </div>

          <p class="text-justify">The Site contains links to web sites, web pages, products and services also operated or offered by Third Party Suppliers and you agree that your use of each web site, web page and Products and Service is also subject to the terms and conditions, if any, contained within each website or webpage or attached to any products or services. These Terms and Conditions shall be deemed as incorporated into each set of terms and conditions. In the event that there is any conflict, the terms and conditions contained within the relevant website or web page or attached to the relevant products or services shall prevail over these Terms and Conditions.</p>

          <p class="text-justify">Orel Corporation assumes no responsibility for and does not endorse unless expressly stated, content created or published by third parties that is included in the Sites and the Services or which may be linked to and from the Sites. The Sites and/or the Services may be used by you to link into other websites, resources and/or networks worldwide. OREL accepts no responsibility for the content, services or otherwise in respect of these and you agree to conform to the acceptable use policies of such websites, resources and/or networks.</p>

          <p class="text-justify">You agree that OREL does not generally and is not required to monitor or edit the use to which you or others use the Sites and the Services or the nature of the Content and OREL is excluded from all liability of any kind arising from the use of the Services, and in particular but without limitation to the foregoing, the nature of any content. Notwithstanding the foregoing, OREL reserves the right to edit, bar or remove any Services and/or Content, at any time as OREL in its sole discretion believes to be necessary in order to prevent any breach of these Terms and Conditions or any breach of applicable laws or regulations.</p>

          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>10. Exclusion of liability</strong></h6>
          </div>
          
          <p class="text-justify">Nothing in these Terms and Conditions shall act to limit or exclude our liability for death or personal injury resulting from our negligence, fraud or any other liability, which may not by applicable law be limited or excluded.</p>

          <p class="text-justify">We shall use reasonable endeavors to ensure the maintenance and availability of the Site and the Services but availability may be affected by your equipment, other communications networks, user congestion of the Our network or the Internet at the same time or other causes of interference and may fail or require maintenance without notice.</p>

          <p class="text-justify">Neither Us nor any member of ours shall be liable for any special, indirect or consequential damages or any damages whatsoever, whether in an action of contract, negligence or other tortuous action, arising out of or in connection with the performance of or use of Services available on the Site and in particular, but without limitation to the foregoing, we specifically exclude all liability whatsoever in respect of any loss arising as a result of:</p>

          <p class="text-justify ml-2">a) use which you make of the Site and the Services or reliance on Services or any loss of any Services or your content resulting from delays, non-deliveries, missed deliveries, or service interruptions;</p>

          <p class="text-justify ml-2">b) and defects that may exist or for any costs, loss of profits, loss of your content or consequential losses arising from your use of, or inability to use or access or a failure, suspension or withdrawal of all or part of the Sites and the Services at any time.</p>  
          
          <p class="text-justify">All conditions or warranties which may be implied or incorporated into these Terms and Conditions by law or otherwise are hereby expressly excluded to the extent permitted by law.</p>

          <p class="text-justify">Your only remedy under these Terms and Conditions is to discontinue using the Site and the Services.</p>

          <p class="text-justify"> We make every effort to ensure the security of your communications. You are however advised that for reasons beyond our control, there is a risk that your communications may be unlawfully intercepted or accessed by those other than the intended recipient. For example, your communications may pass over third-party networks over which we have no control. The Internet is not a secure environment. Unwanted programs or material may be downloaded without your knowledge, which may give unauthorized persons access to your devices and the information stored on your devices. These programs may perform actions that you have not authorized, possibly without your knowledge.</p>

          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>11. Indemnity</strong></h6>
          </div>

          <p class="text-justify">You hereby agree to fully indemnify and to hold us harmless from and against any claim brought by a third party resulting from your use of the Site and the Services or the provision of Content to us by you and in respect of all losses, costs, actions, proceedings, claims, damages, expenses (including reasonable legal costs and expenses), or liabilities, whatsoever suffered or incurred directly or indirectly by us in consequence of such use of the Site and the Services or provision of content or your breach or non-observance of any of these Terms and Conditions.</p>

          <p class="text-justify">You shall defend and pay all costs, damages, awards, fees (including any reasonable legal fees) and judgments awarded against us arising from the above claims and shall provide us with notice of such claims, full authority to defend, compromise or settle such claims and reasonable assistance necessary to defend such claims, at your sole expense.</p>

          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>12. Termination</strong></h6>
          </div>

          <p class="text-justify">We may elect to suspend, vary or terminate the Services and the Site at any time without prior notice for repair or maintenance work or in order to upgrade or update the Site and the Services or for any other reason whatsoever.</p>

          <p class="text-justify">We may elect to terminate the Services or your access to the Site forthwith on breach of any of these Terms and Conditions by you, including, without limitation, delay or nonpayment of sums due to Us or if we cease to offer the Site and the Services to customers for any reason whatsoever.</p>

          <div class="profile-content mb-3 ml-3">
            <h6 class="m-0"><strong>13. Termination</strong></h6>
          </div>

          <p class="text-justify">Governing Law and Jurisdiction - These Terms and Conditions are governed by and construed in accordance with the laws of Sri Lanka and you hereby submit to the exclusive jurisdiction of the Sri Lankan courts.</p>

          <p class="text-justify">Severability - These Terms and Conditions are severable in that if any provision is determined to be illegal or unenforceable by any court of competent jurisdiction such provision shall be deemed to have been deleted without affecting the remaining provisions of these Terms and Conditions.</p>

          <p class="text-justify">Waiver -Our failure to exercise any particular right or provision of these Terms and Conditions shall not constitute a waiver of such right or provision unless acknowledged and agreed to by us in writing.</p>

          <p class="text-justify">Representations - You acknowledge and agree that in entering into these Terms and Conditions you do not rely on, and shall have no remedy in respect of, any statement, representation, warranty or understanding (whether negligently or innocently made) of any person (whether party to these Terms and Conditions or not) other than as expressly set out in these Terms and Conditions as a warranty. Nothing in this Clause shall, however, operate to limit or exclude any liability for fraud.</p>

          <p class="text-justify">Rights of Third Parties - Except in the case of any permitted assignment of this Agreement a person who is not a party to this Agreement has no right of enforcement of any term or condition contained in this Agreement.</p>

          <p class="text-justify">Force Majeure - We shall not be liable in respect of any breach of these Terms and Conditions due to any cause beyond its reasonable control including but not limited to, Act of God, inclement weather, act or omission of Government or public telephone operators or other competent authority or other party for whom we are not responsible.</p>

          <p class="text-justify">Assignment – You are not permitted to assign or transfer the rights and responsibilities of these Terms and conditions without our express written consent.</p>

          <p class="text-justify">Modification – We reserve the right to modify these terms & Conditions from time to time without prior notice. Such modifications will be effective immediately and your continued use of the Site will be deemed your conclusive acceptance of the modified terms and conditions. It is your responsibility to stay up to date with the latest terms and conditions present for review in the site.</p>
        </div>
      </div>
    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
@stop

@section('scripts')
@stop