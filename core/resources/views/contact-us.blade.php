@extends('layouts.master') @section('title','Contact Us')

@section('links')
  <!-- craftpip-jquery-confirm -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />
@stop

@section('css')
<style type="text/css">
  a:focus{
    color: inherit;
  }
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item active">Contact Us</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="account-section mb-2">
    <div class="container">

      <div class="row profile">
        <div class="col-md-3 mb-3">
          <div class="profile-sidebar">

            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu m-0">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('about-us')}}">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('choose-us')}}">WHY CHOOSE US?</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('terms-condition')}}">TERMS & CONDITIONS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('privacy-policy')}}">PRIVACY POLICY</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">HELP & FAQ'S</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="#">WARRANTY & RETURNS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">DELIVERY METHODS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">REGIONAL CENTERS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">NEWS ROOM</a>
                </li>
                 <li class="nav-item">
                  <a class="nav-link" href="#">BLOG</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">SITE MAP</a>
                </li>                
                <li class="nav-item active">
                  <a class="nav-link" href="{{url('contact-us')}}">Contact Us</a>
                </li>

              </ul>
            </div>
            <!-- END MENU -->

          </div>
        </div>
        <div class="col-md-9 mb-3">
          <div class="profile-content mb-3">
            <h6 class="text-uppercase m-0">Contact Us</h6>
          </div>

          <hr>

          <div style="width:100%;height:400px;" id="map"></div>

        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
  <!-- google map api -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJqJfplSgGegW19A-AB6-28xcQv60C864"></script>
@stop

@section('scripts')
<script>
  $(function () {
    function initMap() {
      //sri gangarama 6.9168825, 79.8572239
      var location = new google.maps.LatLng(6.9168825, 79.8572239);

      var mapCanvas = document.getElementById('map');
      var mapOptions = {
          center: location,
          zoom: 16,
          panControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      var map = new google.maps.Map(mapCanvas, mapOptions);

      var markerImage = 'images/icons/maps-and-flags.png';

      var marker = new google.maps.Marker({
          position: location,
          map: map,
          icon: markerImage
      });

      var contentString = '<div class="info-window p-2">' +
              '<h5 class="text-uppercase mb-3">{{ SITE_COMPANY_NAME }}</h5>' +
              '<div class="info-content">' +
                '<i class="fa fa-map-marker" aria-hidden="true"></i> {{SITE_ADDRESS}}<br><br>'+
                '<i class="fa fa-phone-square" aria-hidden="true"></i> {{SITE_CONTACT_NO}} &ensp;'+
                '<i class="fa fa-envelope" aria-hidden="true"></i> {{SITE_EMAIL}}'+
              '</div>' +
              '</div>';
              // <i class="fa fa-fax" aria-hidden="true"></i> (94) 11-4792119<br><br>
      var infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 400
      });

      infowindow.open(map,marker);

      var styles = [{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.fill","stylers":[{"color":"#d1c6c6"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#b09898"}]},{"featureType":"administrative.province","elementType":"geometry.fill","stylers":[{"color":"#ff0000"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#F3F3F3"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#dadada"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#a8a0a0"}]},{"featureType":"poi.government","elementType":"geometry.stroke","stylers":[{"color":"#bba2a2"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"transit","elementType":"all","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]}];

      map.set('styles', styles);

      marker.addListener('click', function () {
          infowindow.open(map, marker);
      });
    }
    google.maps.event.addDomListener(window, 'load', initMap);
  });
</script>
@stop