@extends('layouts.master') @section('title','Guest Login')

@section('links')
  <!-- craftpip-jquery-confirm -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />
  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
@stop

@section('content')
  <!-- sign in/up panel -->
  <div class="guest-login mt-2 pt-4 pb-5">
    <div class="container-fluid">

      <!-- wrapper -->
      <div class="inside-wrapper">
    
        <div class="row">
          <div class="col-lg-8 col-md-4 col-sm-4 offset-md-2 p-0 mb-4 rounded-0">
            <div class="alert alert-secondary alert-dismissable" role="alert">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <h4 class="alert-heading">Login to your {{ config('app.name')?:'' }} Account</h4>
              <p>MyAccount is a service where you can manage all your online purchases in one place.
You can manage orders, check order status and communicate with your stores all in one place.</p>
              <hr>
              <p class="mb-0">We noticed that you are currently not logged in. Please login or register below in order to proceed..</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 offset-md-2 left-side">
            <h3>Guest<br>Account</h3>
            <p class="first-letter-uppercase" style="color:#615c5c;">Just Check out faster Here.</p>
            <p class="first-letter-uppercase">However, You will have the opportunity to create an account and track your orders once you complete your checkout.</p>
            <form action="{{ route('guest.signup') }}" method="POST">
              {!! csrf_field() !!}
              <div class="form-group">
                <label class="control-label required" for="email">Email</label>
                <input  type="email" id="guest_email" name="guest_email" class="form-control{{ $errors->has('guest_email')? ' is-invalid':'' }}" placeholder="Enter Email Address">
                <div class="invalid-feedback">{{ $errors->first('guest_email') }}</div>
              </div>
              <div class="form-group mt-5">
                <button type="submit" class="btn btn-sm btn-primary btn-block mt-4 guest">Login as Guest</button>
              </div>
            </form>
          </div><!--col-sm-6-->
          
          <div class="col-lg-4 col-md-4 col-sm-4 right-side">
            <h3>Login</h3>
            <p>Please fill out the form below to login to your account.</p>              
            <!--Form with header-->
            <form action="{{ url('/login') }}" method="POST">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label required" for="email">Email</label>
                <input id="email" name="email" type="text" class="form-control{{ $errors->has('email')? ' is-invalid':'' }}" placeholder="Enter Email Address" value="{{ old('email') }}">
                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
              </div>
              <div class="form-group">
                <label class="control-label required" for="email">Password</label>
                <input id="password" name="password" type="password" class="form-control{{ $errors->has('password')? ' is-invalid':'' }}" placeholder="password">
                <div class="invalid-feedback mb-4">{{ $errors->first('password') }}</div>
                <a href="#" class="forgot-password">Forgot Password?</a>
              </div>
              <div class="form-group">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="">                                   
                    <p class="pull-right">Remember Me?</p>
                  </label>                                  
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-sm btn-primary btn-block mt-2">Login</button>
              </div>
              <div class="form-group text-left">
              Created an Account for free? <a href="{{ url('/signup') }}" class="text-right btn-link">click here</a>
              </div>
            </form>
            <!--/Form with header-->
          </div><!--col-sm-6-->           
          
        </div>

      </div> <!-- /.wrapper -->

    </div> <!-- /.container -->
  </div> <!-- /.sign in/up panel -->
@stop

@section('js')
  <!-- craftpip-jquery-confirm -->
  <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <!-- toastr notification -->
  <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>
@stop

@section('scripts')
<script>
  $(document).ready(function(){
    // confirm
    $('.forgot-password').on('click', function () {
        $(".ui-dialog-buttonpane button:contains('Confirm')").attr("disabled", true).addClass("ui-state-disabled");
        $.confirm({
            title: false,
            content: `<!--login-form-->
                        <form class="" method="POST" action="{{ route('password.email') }}">
                            {!! csrf_field() !!}
                            <h5 class="text-uppercase">Password Reset</h5>
                            <p class="mb-4">Please insert your email address.</p>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="control-label required-control-label required" for="email">Email</label>
                                <input id="password_reset_email" name="email" type="text" class="form-control" placeholder="Enter Email Address">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </form>`,
            theme: 'material',
            animation: 'scale',
            closeAnimation: 'scale',
            columnClass: 'medium',
            opacity: 0.5,
            buttons: {
                'confirm': {
                    text: 'Send Password Reset Email',
                    btnClass: 'btn-red',
                    action: function () {
                        var email = this.$content.find('#password_reset_email').val();
                        this.buttons.confirm.disable();

                        if(email){
                            // var data = new FormData;
                            // data.append('email', email);

                            $.confirm({
                                theme: 'material',
                                animation: 'scale',
                                closeAnimation: 'scale',
                                icon: 'fa fa-success',
                                columnClass: 'medium',
                                opacity: 0.5,
                                content: function () {
                                    var self = this;

                                    return $.ajax({
                                        url: '{{ route('password.email') }}',
                                        method: 'post',
                                        data: {
                                            email: email
                                        },
                                        headers: {
                                            'X-CSRF-TOKEN': $('input[name=_token]').val()
                                        },
                                        success: function(response){
                                            console.log(response);
                                            self.setContent('Verification details has been sent to your email!.');
                                            self.setTitle('Email sent');
                                        },
                                        error: function(xhr){
                                            if(xhr.responseJSON && xhr.responseJSON.errors && xhr.responseJSON.errors.email[0]){
                                                var error = xhr.responseJSON.errors.email[0];
                                                self.setContent(error);                                            
                                                self.setTitle('Something went wrong!');
                                            }else{
                                                self.setContent('Unexpected error occurd!, for more details see the console.');                                            
                                                self.setTitle('Something went wrong!');
                                            }
                                        }
                                    });
                                }
                            });
                        }else{
                            $.confirm({
                                title: 'Invalid email!.',
                                content: 'Please insert valid email address to continue!.',
                                icon: 'fa fa-warning',
                                animation: 'scale',
                                closeAnimation: 'zoom',
                                buttons: {
                                    ok: {

                                    }
                                }
                            });
                        }
                    }
                },
                cancel: function () {
                    // $.alert('you clicked on <strong>cancel</strong>');
                },
            }
        });
    });
  });

  //show message when user make changes in their cart & forget to updated there cart.
  @if(Request::get('cart-changes') == 'saved')
    notification("toast-bottom-right", "success", "Your cart has been updated!.", "10000");
  @endif
</script>
@stop