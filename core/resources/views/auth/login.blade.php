@extends('layouts.master') @section('title','Login')

@section('links')
  <!-- craftpip-jquery-confirm -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />
@stop

@section('css')
    <style type="text/css">
        .help-block{
            color: red !important;
        }
    </style>
@stop

@section('content')
    <div class="sub-content-wrap" style="display: flow-root;">

        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="c-272-outlets-switches.aspx">Orange Order Portal</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Login</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole signin-content" style="margin-bottom: 20px">
            <div id="FormPanel" style="width:100%;">
                <div class="each-column login-column" style="height: 520px;">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <h2>Great to see you again!</h2>
                        <div class="signin-form-wrap">
                            <div class="intro-text">
                                <p><span id="Label4">Enter your username and password below:</span></p>
                            </div>
                            <div class="each-row">
                                <div class="col-12">
                                    <span id="Label3">User Name</span>
                                </div>
                                <div class="col-12">

                                    <input class="light-style-input" id="email" name="username" type="text" class="form-control" placeholder="Enter username">
                                    @if ($errors->has('username'))
                                        <span class="error-text-message" style="visibility:hidden;">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="each-row">
                                <div class="col-12">
                                    <span id="Label2">Password</span>
                                </div>
                                <div class="col-12">
                                    <input id="password" name="password" type="password" class="form-control" placeholder="password" class="light-style-input" autocomplete="off">
                                    @if ($errors->has('password'))
                                        <span class="error-text-message" style="visibility:hidden;">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        <br>
                                    @endif
                                </div>
                            </div>
                            <div class="each-row checkbox">
                                <div class="col-12">
                                    <label>
                                        <span class="custom-checkbox">
                                            <input id="PersistLogin" type="checkbox" name="PersistLogin" aria-hidden="true" class="labelauty" style="display: none;">
                                            <label for="PersistLogin">
                                                <span class="labelauty-unchecked-image"></span>
                                                <span class="labelauty-checked-image"></span>
                                            </label>
                                        </span>
                                        <span class="remember-text"><span id="Label7">Remember Password</span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="each-row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <button class="red-btn" style="width: 97%">Sign in</button>
                                </div>
                            </div>
                        </div>
                        <h2></h2>
                    </form>
                </div>

                <div class="each-column forgotpass-column" style="height: 520px;">
                    <img src="{{asset('assets/images/login/bg3.jpg')}}">
                </div>

            </div>
        </div>

    </div>

@stop

@section('js')
  <!-- craftpip-jquery-confirm -->
  <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
@stop

@section('scripts')
<script>
    $(document).ready(function(){

        // $(".alert").fadeTo(4000, 500).slideUp(1500, function(){
        //     $(".alert").slideUp(1500);
        // });

        $('.forgot-password').on('click', function () {
            $(".ui-dialog-buttonpane button:contains('Confirm')").attr("disabled", true).addClass("ui-state-disabled");
            $.confirm({
                title: false,
                content: `<!--login-form-->
                            <form class="" method="POST" action="{{ route('password.email') }}">
                                {!! csrf_field() !!}
                                <h5 class="text-uppercase">Password Reset</h5>
                                <p class="mb-4">Please insert your email address.</p>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label required-control-label required" for="email">Email</label>
                                    <input id="password_reset_email" name="email" type="text" class="form-control" placeholder="Enter Email Address">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </form>`,
                theme: 'material',
                animation: 'scale',
                closeAnimation: 'scale',
                columnClass: 'medium',
                opacity: 0.5,
                buttons: {
                    'confirm': {
                        text: 'Send Password Reset Email',
                        btnClass: 'btn-red',
                        action: function () {
                            var email = this.$content.find('#password_reset_email').val();
                            this.buttons.confirm.disable();

                            if(email){
                                // var data = new FormData;
                                // data.append('email', email);

                                $.confirm({
                                    theme: 'material',
                                    animation: 'scale',
                                    closeAnimation: 'scale',
                                    icon: 'fa fa-success',
                                    columnClass: 'medium',
                                    opacity: 0.5,
                                    content: function () {
                                        var self = this;

                                        return $.ajax({
                                            url: '{{ route('password.email') }}',
                                            method: 'post',
                                            data: {
                                                email: email
                                            },
                                            headers: {
                                                'X-CSRF-TOKEN': $('input[name=_token]').val()
                                            },
                                            success: function(response){
                                                console.log(response);
                                                self.setContent('Verification details has been sent to your email!.');
                                                self.setTitle('Email sent');
                                            },
                                            error: function(xhr){
                                                if(xhr.responseJSON && xhr.responseJSON.errors && xhr.responseJSON.errors.email[0]){
                                                    var error = xhr.responseJSON.errors.email[0];
                                                    self.setContent(error);                                            
                                                    self.setTitle('Something went wrong!');
                                                }else{
                                                    self.setContent('Unexpected error occurd!, for more details see the console.');                                            
                                                    self.setTitle('Something went wrong!');
                                                }
                                            }
                                        });
                                    }
                                });
                            }else{
                                $.confirm({
                                    title: 'Invalid email!.',
                                    content: 'Please insert valid email address to continue!.',
                                    icon: 'fa fa-warning',
                                    animation: 'scale',
                                    closeAnimation: 'zoom',
                                    buttons: {
                                        ok: {

                                        }
                                    }
                                });
                            }
                        }
                    },
                    cancel: function () {
                        // $.alert('you clicked on <strong>cancel</strong>');
                    },
                }
            });
        });
    });
</script>
@stop