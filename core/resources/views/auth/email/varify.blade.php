<table border="0" cellspacing="0" cellpadding="0" style="padding-bottom:20px;max-width:600px;min-width:220px">
    <tbody>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td></td>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="direction:ltr;padding-bottom:7px">
                                    <tbody>
                                        <tr>
                                            <td align="left">
                                                <img width="92" height="32" src="{{ asset('assets/images/logo.png') }}" style="width:92px;height:32px" class="CToWUd">
                                            </td>
                                            <td align="right" style="font-family:Roboto-Light,Helvetica,Arial,sans-serif">{{$name}}</td>
                                            <td align="right" width="35">
                                                <img width="28" height="28" style="width:28px;height:28px;border-radius:50%" src="https://ci6.googleusercontent.com/proxy/_ehxLExCa2FPeTKuNVAgMUxyx7YBxMq8-qickdiS6h0GI2UChu_KZURQgNm3-OuvpRjUg26eTgHNny2H1gs6Pzzy81YKOLOVHegzDqMfEMQVAWTuszLuOL68hqTN=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/anonymous_profile_photo.png" class="CToWUd">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div style="background-color:#f5f5f5;direction:ltr;padding:22px 16px;margin-bottom:6px">
                                    <table class="m_8195265450399929929v2rsp" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top">
                                                    <img height="40px" src="https://ci6.googleusercontent.com/proxy/W-CJMHOnfCs5rEEobZ102RNeZCXCWLtlWToeEEEi4Rwoplbf6IvrPSe_1y4rmGyZsPUrKh2e_oUhkkch0qihpSxk1ldPXnAT0OmGnTmnwqWYuAd6DPAJ=s0-d-e1-ft#https://www.gstatic.com/images/branding/product/2x/email_48dp.png" class="CToWUd">
                                                </td>
                                                <td width="16px"></td>
                                                <td style="direction:ltr">
                                                    
                                                    <span style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:rgba(0,0,0,0.87);line-height:1.6;color:rgba(0,0,0,0.54)">Your email <a style="text-decoration:none;color:rgba(0,0,0,0.87)">{{$email}}</a> is listed in the {{ config('app.name') }} 
                                                        </span>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="background:url('https://ci5.googleusercontent.com/proxy/6NxNAmLKGq1e8ePcitdSiE-X-g8kwo2ATcZjIpFFPtHwgl7s6aanLDIF9dsO8K_I6mvnuTEPEOBsA1ofqn8y7FrVN0Arjzpe7m-ybWUmwNHmDkVVjLyV=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-nw.png') top left no-repeat" width="6" height="5">
                                <div></div>
                            </td>
                            <td style="background:url('https://ci5.googleusercontent.com/proxy/eketyJkGVxhK6Z5kXJPMvc_xp4ewMG0-rRub0qdMfuT8kRsGDhdrztbZqOttWDJvnFldtuGk_LaoOSBBNNxxI0PtZrvXy1Kt39bkZKAr5Fs0Qt0Puw=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-n.png') top center repeat-x" height="5">
                                <div></div>
                            </td>
                            <td style="background:url('https://ci5.googleusercontent.com/proxy/xXtFP3fp-NWW8Fb-jpdgVdKyl14_H1kufMnB0ms_EbTo-TtcXRkIcX0LK69J6deIRi7KtH9BXAlSZ709fcAywLyu6uHSgFLQ8kg3vVUZHZ310P_EbOIQ=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-ne.png') top right no-repeat" width="6" height="5">
                                <div></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="background:url('https://ci5.googleusercontent.com/proxy/jx-kL5P_JYf7EHBI57k0jTf0wQfWYZB9kjzqrzIUKgvE3oSR3AwtIrijgMsn2DzciTsgxv2g5Rs9DjUo3-CCepVGCikhcSsa_4WWHymP1-RbfK9Uxg=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-w.png') center left repeat-y" width="6">
                                <div></div>
                            </td>
                            <td>
                                <div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;padding-left:20px;padding-right:20px;border-bottom:thin solid #f0f0f0;color:rgba(0,0,0,0.87);font-size:24px;padding-bottom:38px;padding-top:40px;text-align:center;word-break:break-word">
                                    <div class="m_8195265450399929929v2sp">Thank you for joining with us<br><a style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.87);font-size:16px;line-height:1.8">{{$email}}</a>
                                    </div>
                                </div>

                                <div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:rgba(0,0,0,0.87);line-height:1.6;padding-left:20px;padding-right:20px;padding-bottom:32px;padding-top:24px">
                                    <div class="m_8195265450399929929v2sp">Thank you for joining with us. Your {{ config('app.name') }} account is successfully created.  Please confirm to proceed.
                                        <div style="padding-top:24px;text-align:center">
                                            <a href="" style="display:inline-block;text-decoration:none" target="_blank">
                                                <table border="0" cellspacing="0" cellpadding="0" style="background-color:#4184f3;border-radius:2px;min-width:90px">
                                                    <tbody>
                                                        <tr style="height:6px"></tr>
                                                        <tr>
                                                            <td style="padding-left:8px;padding-right:8px;text-align:center">
                                                                <a href="{{ url('register/verify') }}/{{$confirmation_code}}" style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:#ffffff;font-weight:400;line-height:20px;text-decoration:none;font-size:13px" target="_blank">CONFIRM</a>
                                                            </td>
                                                        </tr>
                                                        <tr style="height:6px"></tr>
                                                    </tbody>
                                                </table>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td style="background:url('https://ci6.googleusercontent.com/proxy/v4PZGPA_yWl9Ao0I9PMW-iusp_SIUwORiMYopVopB7tHHf5JrzCM8wjpZjUH8PCy1nP9bvypqYynsjnbqBKKV8fKuQyziI02mZiGELaNneCrxgcZ7g=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-e.png') center left repeat-y" width="6">
                                <div></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="background:url('https://ci4.googleusercontent.com/proxy/NmRFBb5WaOipoP4-CA_Wr23mwv5paJ8NxNkn-IFUdRudCxS35ItH90_LXh3XIbUzaYpYQ5GQCuxTn9ZNxP3TiEm4kraOE1ViKAaPcxDgFyGhLXwm7Vym=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-sw.png') top left no-repeat" width="6" height="5">
                                <div></div>
                            </td>
                            <td style="background:url('https://ci3.googleusercontent.com/proxy/NiBXJ6NLEKMReFj7Q_woMq9t-SpwXXuP1gUMLt5ayWo38pcgPoMyntxtn7uSckxGL8db40em6KTuoVGr5EvfgiVACFYRGWsD2e8zeNZ08VEMzxdCnA=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-pixel-s.png') top center repeat-x" height="5">
                                <div></div>
                            </td>
                            <td style="background:url('https://ci6.googleusercontent.com/proxy/Jyaq0B-T749z8QKm69foqx_50a92MjjSAeEkYA-z-7fa8yaIhCynKRmprO2-kCbtU-MBzXiYpWgX4rfuXWbD7zs0-TuMTr0MDBK7QWNhj0rX6boEmYWM=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/hodor/4-corner-se.png') top left no-repeat" width="6" height="5">
                                <div></div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div style="text-align:left">
                                    <div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;line-height:20px;padding-top:10px">
                                        <div>
                                            You received this email to let you know about important changes to your {{ config('app.name') }} services.
                                        </div>
                                        <div style="direction:ltr">{{ SITE_COPYRIGHT }}
                                            <a class="m_8195265450399929929afal" style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;line-height:20px;padding-top:10px">{{ SITE_ADDRESS }}
                                            </a>
                                        </div>
                                    </div>
                                    <div style="display:none!important;max-height:0px;max-width:0px">et:100</div>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>