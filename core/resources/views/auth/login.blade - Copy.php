@extends('layouts.master') @section('title','Login')

@section('links')
  <!-- craftpip-jquery-confirm -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />
@stop

@section('css')
    <style type="text/css">
        .help-block{
            color: red !important;
        }
    </style>
@stop

@section('content')
  <!-- sign in/up panel -->
    <div class="sign-in-section mt-2 pt-4 pb-4">
    
        <div class="container-fluid">

            <!-- wrapper -->
            <div class="inside-wrapper">
    
                <div class="card-deck">

                    <div class="card border-0">
                        <!-- <div class="card-block p-0">
                            <div class="detail-back">
                                <div class="text-center desc">
                                    <h1 class="ionyx text-center text-uppercase">orelcorp</h1>
                                    <p class="ionyx text-center text-capitalize text-center text-uppercase">ECOMMERCE SOLUTIONS</p>
                                </div>
                            </div>
                        </div> -->
                    </div>

                    <!--login-form-->
                    <form class="card" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="card-body pb-0">
                            <div class="account-holder">              
                                
                                @if (Session::has('code'))
                                    <div class="alert alert-success alert-dismissible">
                                        <strong>Verified!</strong> Now you can log into the system.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (Session::has('bad_code'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <strong>Verification faild.</strong> Verified link expired.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (Session::has('code_create'))
                                    <div class="alert alert-warning alert-dismissible">
                                        <strong>Account Created.</strong> {{Session::get('code_create')}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif (Session::has('bad_login'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <strong>Not Verified.</strong> {{Session::get('bad_login')}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                

                                <h6 class="text-uppercase font-weight-bold text-center">Login</h6>
                                <p>Please fill out the form below to login to the system.</p>
                            
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <label class="control-label required-control-label required" for="email">Username</label>
                                    <input id="email" name="username" type="text" class="form-control" placeholder="Enter username Address">
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label required-control-label required" for="password">Password</label>
                                    <input id="password" name="password" type="password" class="form-control" placeholder="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        <br>
                                    @endif
                                    <a href="#" class="forgot-password">Forgot Password?</a>
                                </div>
                            
                                <!-- <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <p class="pull-right text-uppercase">Remember Me?</p>
                                        </label>                                  
                                  </div>
                                </div> -->
                            </div>                        
                        </div>
                        <div class="card-footer mb-2">
                            <button class="btn btn-sm btn-primary btn-block">Sign in</button>
                             <!-- <a href="{{ route('user.guest') }}"><p class="pull-left pt-2">Guest user?</p></a>
                             <a href="{{url('signup')}}"><p class="pull-right pt-2">Free register?</p></a> -->
                        </div>
                    </form>
                    <!--/.login-form-->
                    <div class="card border-0">
                        <!-- <div class="card-block p-0">
                            <div class="detail-back">
                                <div class="text-center desc">
                                    <h1 class="ionyx text-center text-uppercase">orelcorp</h1>
                                    <p class="ionyx text-center text-capitalize text-center text-uppercase">ECOMMERCE SOLUTIONS</p>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <!--sing-up-form-->         
                </div>
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.sign in/up panel -->
@stop

@section('js')
  <!-- craftpip-jquery-confirm -->
  <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
@stop

@section('scripts')
<script>
    $(document).ready(function(){

        // $(".alert").fadeTo(4000, 500).slideUp(1500, function(){
        //     $(".alert").slideUp(1500);
        // });

        $('.forgot-password').on('click', function () {
            $(".ui-dialog-buttonpane button:contains('Confirm')").attr("disabled", true).addClass("ui-state-disabled");
            $.confirm({
                title: false,
                content: `<!--login-form-->
                            <form class="" method="POST" action="{{ route('password.email') }}">
                                {!! csrf_field() !!}
                                <h5 class="text-uppercase">Password Reset</h5>
                                <p class="mb-4">Please insert your email address.</p>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label required-control-label required" for="email">Email</label>
                                    <input id="password_reset_email" name="email" type="text" class="form-control" placeholder="Enter Email Address">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </form>`,
                theme: 'material',
                animation: 'scale',
                closeAnimation: 'scale',
                columnClass: 'medium',
                opacity: 0.5,
                buttons: {
                    'confirm': {
                        text: 'Send Password Reset Email',
                        btnClass: 'btn-red',
                        action: function () {
                            var email = this.$content.find('#password_reset_email').val();
                            this.buttons.confirm.disable();

                            if(email){
                                // var data = new FormData;
                                // data.append('email', email);

                                $.confirm({
                                    theme: 'material',
                                    animation: 'scale',
                                    closeAnimation: 'scale',
                                    icon: 'fa fa-success',
                                    columnClass: 'medium',
                                    opacity: 0.5,
                                    content: function () {
                                        var self = this;

                                        return $.ajax({
                                            url: '{{ route('password.email') }}',
                                            method: 'post',
                                            data: {
                                                email: email
                                            },
                                            headers: {
                                                'X-CSRF-TOKEN': $('input[name=_token]').val()
                                            },
                                            success: function(response){
                                                console.log(response);
                                                self.setContent('Verification details has been sent to your email!.');
                                                self.setTitle('Email sent');
                                            },
                                            error: function(xhr){
                                                if(xhr.responseJSON && xhr.responseJSON.errors && xhr.responseJSON.errors.email[0]){
                                                    var error = xhr.responseJSON.errors.email[0];
                                                    self.setContent(error);                                            
                                                    self.setTitle('Something went wrong!');
                                                }else{
                                                    self.setContent('Unexpected error occurd!, for more details see the console.');                                            
                                                    self.setTitle('Something went wrong!');
                                                }
                                            }
                                        });
                                    }
                                });
                            }else{
                                $.confirm({
                                    title: 'Invalid email!.',
                                    content: 'Please insert valid email address to continue!.',
                                    icon: 'fa fa-warning',
                                    animation: 'scale',
                                    closeAnimation: 'zoom',
                                    buttons: {
                                        ok: {

                                        }
                                    }
                                });
                            }
                        }
                    },
                    cancel: function () {
                        // $.alert('you clicked on <strong>cancel</strong>');
                    },
                }
            });
        });
    });
</script>
@stop