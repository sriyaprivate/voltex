@extends('layouts.master') @section('title','Ecommerce | Password Reset')

@section('links')
  <!-- craftpip-jquery-confirm -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />
@stop

@section('css')
    <style type="text/css">
        .help-block{
            color: red !important;
        }
        #result{
            padding-top: 6px;
        }
        .short, .weak, .good, .strong{
            font-size:small;
            
        }
        .short{
            color:#FF0000;
        }
        .weak{
            color:orange;
        }
        .good{
            color:#2D98F3;
        }
        .strong{
            color: limegreen;
        }
        .border{
            border-bottom: 2px solid !important;
            margin-top: -13px;
        }
        .border-short{
            border-bottom-color: #FF0000 !important;
        }
        .border-weak{
            border-bottom-color: orange !important;
        }
        .border-good{
            border-bottom-color: #2D98F3 !important;
        }
        .border-strong{
            border-bottom-color: limegreen !important;
        }
    </style>
@stop

@section('content')
  <!-- sign in/up panel -->
    <div class="sign-in-section mt-2 pt-4 pb-4">
    
        <div class="container-fluid">
            <!-- wrapper -->
            <div class="inside-wrapper">
                <div class="card-deck">
                    <div class="col-6 mx-auto">
                        <!--login-form-->
                        <form class="card" method="POST" action="{{ route('password.request', ['token' => $token]) }}" style="margin-top: 10%;margin-bottom: 10%;">
                            {{ csrf_field() }}
                            <div class="card-body pb-0">
                                <div class="account-holder">              
                                    
                                    <h6 class="text-uppercase">Set your new password</h6>
                                    <p>Please fill out the form below to set to your new password.</p>
                                
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label required-control-label required" for="email">Email</label>
                                        <input id="email" name="email" type="text" class="form-control" placeholder="Enter Email Address">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label required-control-label required" for="password">Password</label>
                                        <input id="password" name="password" type="password" class="form-control" placeholder="Password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            <br>
                                        @endif
                                        <div id="strength-meter_1"></div>
                                        <div id="result_1"></div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label class="control-label required-control-label required">Confirm Password</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            <br>
                                        @endif
                                        <div id="strength-meter_2"></div>
                                        <div id="result_2"></div>
                                    </div>

                                </div>                        
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary btn-block">Reset Password</button>
                            </div>
                        </form>
                        <!--/.login-form-->
                    </div>
                </div>
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.sign in/up panel -->
@stop

@section('js')
  <!-- craftpip-jquery-confirm -->
  <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
@stop

@section('scripts')
<script>
    $(document).ready(function() {
        $('#password, #password-confirm').keyup(function() {
            $('#result_1').html(checkStrength($('#password').val()))
        });

        $('#password-confirm').keyup(function() {
            if($('#password').val() !== $('#password-confirm').val()){
                $('#result_2').removeClass();
                $('#strength-meter_2').removeClass();
                $('#strength-meter_2').css('width', '5%');
                $('#result_2').addClass('short');
                $('#strength-meter_2').addClass('short border border-short');
                
                $('#result_2').html('Password is must be match.');
            }else{
                $('#result_2').removeClass();
                $('#strength-meter_2').removeClass();
                $('#strength-meter_2').css('width', '5%');
                $('#result_2').addClass('strong');
                $('#strength-meter_2').addClass('strong border border-strong');

                $('#result_1').removeClass();
                $('#strength-meter_2').removeClass();
                $('#strength-meter_2').css('width', '100%');
                $('#result_2').addClass('strong');
                $('#strength-meter_2').addClass('strong border border-strong');
                
                $('#result_2').html('Password matched.');
            }
        });

        function checkStrength(password) {
            var strength = 0

            if(password.length == 0){
                $('#result_1').removeClass();
                $('#strength-meter_1').removeClass();
                $('#strength-meter_1').css('width', '5%');
                $('#result_1').addClass('short');
                $('#strength-meter_1').addClass('short border border-short');
                return 'Password is required'
            }

            if(password.length < 6){
                $('#result_1').removeClass();
                $('#strength-meter_1').removeClass();
                $('#result_1').addClass('short');
                $('#strength-meter_1').addClass('short border border-short');
                $('#strength-meter_1').css('width', '10%');
                return 'Too short'
            }

            if (password.length > 7) strength += 1
            // If password contains both lower and uppercase characters, increase strength value.
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
            // If it has numbers and characters, increase strength value.
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
            // If it has one special character, increase strength value.
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
            // If it has two special characters, increase strength value.
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
            // Calculated strength value, we can return messages
            // If value is less than 2
            if (strength < 2) {
                $('#result_1').removeClass();
                $('#strength-meter_1').removeClass();
                $('#result_1').addClass('weak');
                $('#strength-meter_1').addClass('weak border border-weak');
                $('#strength-meter_1').css('width', '30%');
                return 'Weak';
            }else if(strength == 2){
                $('#result_1').removeClass();
                $('#strength-meter_1').removeClass();
                $('#result_1').addClass('good');
                $('#strength-meter_1').addClass('good border border-good');
                $('#strength-meter_1').css('width', '50%');
                return 'Good';
            }else{
                $('#result_1').removeClass();
                $('#strength-meter_1').removeClass();
                $('#result_1').addClass('strong');
                $('#strength-meter_1').addClass('strong border border-strong');
                $('#strength-meter_1').css('width', '100%');
                return 'Strong';
            }
        }
    });
</script>
@stop