@extends('layouts.master') @section('title','Ecommerce | Password Reset Link')

@section('links')
  <!-- craftpip-jquery-confirm -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />
@stop

@section('css')
    <style type="text/css">
        .help-block{
            color: red !important;
        }
    </style>
@stop

@section('content')
  <!-- sign in/up panel -->
    <div class="sign-in-section mt-2 pt-4 pb-4">
        <div class="container-fluid">
            <!-- wrapper -->
            <div class="inside-wrapper">
                <div class="card-deck">                    
                    <div class="col-6 mx-auto" style="margin-top: 10%;margin-bottom: 10%;">
                        <!--login-form-->
                        <form class="card p-4" method="POST" action="{{ route('password.email') }}">
                            {!! csrf_field() !!}
                            <div class="card-body pb-0">
                                <div class="account-holder">              
                                    <h6 class="text-uppercase">Password Reset of {{ config('app.name') }}</h6>
                                    <p class="mb-4">Please fill out the form below to reset to your password.</p>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label required-control-label required" for="email">Email</label>
                                        <input id="email" name="email" type="text" class="form-control" placeholder="Enter Email Address">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>                        
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary btn-block">Send Password Reset Email</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->
    </div> <!-- /.sign in/up panel -->
@stop

@section('js')
  <!-- craftpip-jquery-confirm -->
  <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>
@stop

@section('scripts')
<script>
    $(document).ready(function(){
        
    });
</script>
@stop
