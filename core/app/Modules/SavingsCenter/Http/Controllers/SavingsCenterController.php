<?php

namespace App\Modules\SavingsCenter\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Modules\SavingsCenter\Repositories\SavingsCenterRepository;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Cart\Repositories\CartRepository;
use App\Classes\Functions;
use Illuminate\Support\Collection;


class SavingsCenterController extends Controller
{
    protected $saving_center = null;
    protected $productRepo   = null;
    protected $cartRepo      = null;
    protected $common        = null;

    public function __construct(SavingsCenterRepository $saving_center,ProductRepository $productRepo,CartRepository $cartRepo, Functions  $common){
       $this->saving_center = $saving_center;
       $this->productRepo   = $productRepo;
       $this->cartRepo      = $cartRepo;    
       $this->common        = $common;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {   
        try{
            $page = SHOW_OPTION[2];  //paginate 48  
            $promotion_products = $this->saving_center->getPromotionProducts($page);
            return view('savingscenter::index')->with(['product' => $promotion_products]);
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'saving-center.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function view($item_name = null, $item_id)
    {
        try{

            $product_details  = $this->productRepo->savingProduct((int) $item_id);
            $related_products = $this->productRepo->getRelatedProducts((int) $item_id);
            $delivery_types   = $this->productRepo->getDeliveryType();
            $delivery_options = $this->cartRepo->getDeliveryOptionByProductId($item_id);
            
            if(count($product_details) > 0){
                return view('savingscenter::view', compact('product_details', 'related_products', 'delivery_options','delivery_types'));
            }else{
                throw new \Exception("Product not found!");
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'saving-center.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('savingscenter::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('savingscenter::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('savingscenter::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
