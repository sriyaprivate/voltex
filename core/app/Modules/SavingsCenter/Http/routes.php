<?php

Route::group(['prefix' => 'savingscenter', 'middleware' => ['web'], 'namespace' => 'App\\Modules\SavingsCenter\Http\Controllers'], 
	function()
{
    /*------------- GET ---------------------- */
    Route::get('/',[
    	'uses' => 'SavingsCenterController@index',
    	'as'   => 'saving-center.index'
    ]);

    Route::get('/item/{item_name?}/{item_id}', [
        'uses' => 'SavingsCenterController@view',
        'as'   => 'saving-center.item.view'
    ]);

    /*------------- POST ----------------------*/
});
