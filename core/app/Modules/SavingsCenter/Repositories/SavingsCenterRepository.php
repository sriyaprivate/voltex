<?php
/**
 * SAVINGS CENTER
 *
 * @version 1.0.0
 * @author Lahiru Madhusankha Perera <lahiru.perera1691@gmial.com>
 * @copyright 2017-12-08
 */

Namespace App\Modules\SavingsCenter\Repositories;

use Session;
use DB;
use App\Modules\Product\Models\Product;
use App\Classes\Functions;

class SavingsCenterRepository{    
    public function __construct(){
        
    }

    /**
     * Get promotion product (bank & deal promotin products)
     * @param Integer $perPage 
     * @return 
     */

    public function getPromotionProducts($perPage){
        $common     = new Functions();
        $loggedUser = $common->getLoggedUser();

        $promotion_product = Product::select(
                                'product.id',
                                'product.name as product_name',
                                'wishlist.product_id as hasInWishlist',
                                'product.display_name'
                            )
                            ->join('product_pricebook',function($join){
                                $join->on('product_pricebook.product_id','=','product.id')
                                ->where('product_pricebook.status',1)
                                ->whereNull('product_pricebook.deleted_at');
                            })
                            ->join('product_promotion',function($join){
                                $join->on('product_promotion.product_id','=','product.id')
                                ->where('product_promotion.status',1)
                                ->whereNull('product_promotion.deleted_at');
                            })
                            ->join('promotion',function($join){
                                $join->on('promotion.id','=','product_promotion.promotion_id')
                                ->where('promotion.status','=',1)
                                ->where('promotion.promotion_type',DEAL_PROMOTION)
                                ->where('promotion.period_from','<=',date('Y-m-d'))
                                ->whereNull('promotion.deleted_at');
                            })
                            ->leftjoin('wishlist', function($query) use($loggedUser){
                                $query->on('product.id', '=', 'wishlist.product_id')
                                    ->where('wishlist.status', 1)
                                    ->where('wishlist.user_id', $loggedUser? $loggedUser->id : '')
                                    ->whereNull('wishlist.deleted_at');
                            })
                            ->join('pricebook_channel', function($query){
                                $query->on('product_pricebook.pricebook_channel_id', '=', 'pricebook_channel.id')
                                    ->whereIn('pricebook_channel.channel_id', function($query){
                                        $query->select('id')
                                            ->from('channel')
                                            ->where('channel.name', 'LIKE', '%'.CHANNEL_ONLINE.'%')
                                            ->where('channel.status', 1)
                                            ->whereNull('channel.deleted_at');
                                    })
                                    ->where('pricebook_channel.status', 1)
                                    ->where('product_pricebook.status', 1)
                                    ->whereNull('pricebook_channel.deleted_at')
                                    ->whereNull('product_pricebook.deleted_at');
                            })
                            ->with('product_price','product_promotion.promotion.type','product_promotion.bank_promotion.bank_card','image','product_promotion.bank_promotion.bank','product_promotion.order_detail')
                            ->where('product.status',SAVINGS_CENTER_STATUS)
                            ->whereNull('product.deleted_at')
                            ->groupBy('product_promotion.product_id')
                            ->paginate($perPage);
        if(count($promotion_product) > 0){
            return $promotion_product;
        }else{
            return [];
        }
    	/*$promotion_product = DB::table('product_promotion')
    	->join('promotion',function($join1){
    		$join1->on('product_promotion.promotion_id','=','promotion.id')
    		->where('promotion.period_from','<=', date('Y-m-d'))
    		->where('promotion.period_to','>=', date('Y-m-d'))
    		->where('promotion.status',1)
    		->whereNull('promotion.deleted_at');
    	})
    	->join('product',function($join2){
    		$join2->on('product_promotion.product_id','=', 'product.id')
    		->where('product.status',2)
    		->whereNull('product.deleted_at');
    	})
        ->leftJoin('product_image',function($join3){
            $join3->on('product.id','=','product_image.product_id')
            ->where('product_image.status',1)
            ->whereNull('product_image.deleted_at');
        })
        ->leftJoin('product_pricebook',function($join4){
            $join4->on('product.id','=','product_pricebook.product_id')
            ->where('product_pricebook.status',1)
            ->whereNull('product_pricebook.deleted_at');
        })
        ->leftJoin('stock',function($join5){
            $join5->on('product.id','=','stock.product_id')
            ->where('stock.status',1)
            ->where('stock.qty','>',0)
            ->whereNull('stock.deleted_at');
        })
        // ->leftJoin('channel',function($join6){

        // })
    	->select('promotion.*','product_promotion.*','product.*','product_image.*','product_pricebook.mrp',
            DB::raw('product_pricebook.mrp - (product_pricebook.mrp * promotion.discount / 100) as promotion_price'),
            DB::raw('product_pricebook.mrp * promotion.discount / 100 as promotion'),
            DB::raw("DATEDIFF(promotion.period_to,CURDATE())AS remaining_days"),
            'stock.*'
        )
    	->where('product_promotion.status',1)
    	->whereNull('product_promotion.deleted_at')
    	->paginate($perPage);
        
        if(!$promotion_product){
            throw new Exception("Error Processing Request", 1);
        }else{
            return $promotion_product;
        }*/
    } 
}