@extends('layouts.master') @section('title','Savings Center')

@section('links')
    <!-- slick.css -->
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">
@stop

@section('css')
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container-fluid">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item active">Discount Hub</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->

  <!-- sign in/up panel -->
  <div class="market savings-center pb-4">
    <div class="container-fluid">

      <!-- wrapper -->
      <!-- <div class="inside-wrapper"> -->
    
        <div class="row">            

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 right">

            <!-- banner top -->
            <div class="card-group">
              <div class="card left-side col-lg-4 col-md-4 col-sm-12">
                <h3>Discount Hub</h3>
              </div>
              <div class="card right-side col-lg-8 col-md-8 col-sm-12">
                <p class="mt-3 text-uppercase"></p>
              </div>
            </div> <!-- /.banner top -->

            <!-- filters-container -->
            <div class="clearfix filters-container pt-4 mb-5 pb-4">
              <div class="row">
                <div class="col-lg-4 col-sm-4">
                  <div class="btn-group" style="display: none;">
                    <a href="#" id="list" class="btn btn-default btn-sm"><i class="fa fa-th-list" aria-hidden="true"></i> List</a>
                    <a href="#" id="grid" class="btn btn-default btn-sm"><i class="fa fa-th" aria-hidden="true"></i> Grid</a>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-8">
                  <label class="mr-2" for="inlineFormCustomSelect">Sort by</label>
                  <select class="custom-select mb-2 mr-3 mb-sm-0" id="inlineFormCustomSelect">
                    <option value="{{ SORT_OPTION[1] }}" {{ Request::get('sort') == SORT_OPTION[1]? 'selected':''}}>Date - Latest on top</option>
                    <option value="{{ SORT_OPTION[2] }}" {{ Request::get('sort') == SORT_OPTION[2]? 'selected':''}}>Date - Oldest on top</option>
                    <option value="{{ SORT_OPTION[3] }}" {{ Request::get('sort') == SORT_OPTION[3]? 'selected':''}}>Price - High to law</option>
                    <option value="{{ SORT_OPTION[4] }}" {{ Request::get('sort') == SORT_OPTION[4]? 'selected':''}}>Price - Low to high</option>
                  </select>
                  <label class="mr-2" for="inlineFormCustomSelect">Show</label>
                  <select class="custom-select mb-2 mb-sm-0" id="inlineFormCustomSelect">
                    <option value="{{ SHOW_OPTION[1] }}" {{ Request::get('show') == SHOW_OPTION[1]? 'selected':''}}>{{ SHOW_OPTION[1] }}</option>
                    <option value="{{ SHOW_OPTION[2] }}" {{ Request::get('show') == SHOW_OPTION[2]? 'selected':''}}>{{ SHOW_OPTION[2] }}</option>
                    <option value="{{ SHOW_OPTION[3] }}" {{ Request::get('show') == SHOW_OPTION[3]? 'selected':''}}>{{ SHOW_OPTION[3] }}</option>
                    <option value="{{ SHOW_OPTION[4] }}" {{ Request::get('show') == SHOW_OPTION[4]? 'selected':''}}>{{ SHOW_OPTION[4] }}</option>
                  </select>
                </div>
                <div class="col-lg-4 col-sm-12">
                  @if(!empty($product))
                    @if($product->total() > $product->perPage())
                    <nav aria-label="Page navigation example">
                      @include('includes.pagination', ['paginator' => $product])
                    </nav>
                    @endif
                  @endif
                </div>
              </div>
            </div> <!-- /.filters-container -->
          
            <!-- list/grid view -->
            <div id="products" class="row savings list-view">
              <?php 
                $promotion_arr = []; 
                $difference = [];
                $arr = [];
                $allocate_stock = '';
              ?>
              @if(!empty($product) && count($product) > 0)
                @foreach($product as $key => $value)
                  <!-- deal item -->
                  <div class="col-lg-12 mb-4">                
                    <div class="card mb-4 clearfix h-100">
                      @if(count($value->product_promotion) > 0 && !empty($value->product_promotion))
                        @foreach($value->product_promotion as $key => $pro_promotion)
                          @if(count($pro_promotion->promotion) > 0 && $pro_promotion->promotion->promotion_type == DEAL_PROMOTION)
                            @if(!empty($pro_promotion->promotion->discount))
                              <div class="promotion">
                                <span class="dis ng-binding">{{number_format($pro_promotion->promotion->discount)}}%</span>
                                <span class="save">OFF</span>
                              </div>
                            @endif
                          @endif
                        @endforeach
                      @endif
                      <div class="row">
                        <div class="col-6">
                          @if(!empty($value->image))
                            <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.$value->image->image) }}" alt="{{ $value->display_name?:'-' }}">
                          @else
                            <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="{{ $value->display_name?:'-' }}">
                          @endif  
                        </div>
                        <div class="col-6">
                          <div class="details position-relative">
                            <div class="d-inline-block w-100">
                              <div class="mb-4"><a href="{{ route('saving-center.item.view', ['item_name' => str_slug($value->display_name), 'item_id' => $value->id]) }}" class="text-underline-onHover mb-4 text-center">{{$value->display_name?:'-'}}</a></div>
                              <div class="w-50 float-left text-right">
                                <p class="h6 mb-2 price">
                                  <small>
                                    @if(count($value->product_price))
                                      <del>{{CURRENCY_SIGN}} {{number_format($value->product_price->mrp,2)}}</del>
                                    @endif
                                  </small> 
                                  @if(!empty($value->product_promotion))
                                    @foreach($value->product_promotion as $key => $pro_promotion)
                                      @if(count($pro_promotion->promotion) > 0 && count($pro_promotion->promotion->promotion_type) > 0 && $pro_promotion->promotion->promotion_type == DEAL_PROMOTION)
                                        <?php 
                                          $promotion_arr  = calculatePromtion($value->product_price->mrp,$pro_promotion->promotion->discount);
                                          $deal_promotion_price = $promotion_arr['promotion_price'];
                                          $allocate_stock = $pro_promotion->allocate_qty;
                                        ?>
                                        {{CURRENCY_SIGN}}  {{number_format($deal_promotion_price,2)}}
                                      @endif
                                    @endforeach
                                  @endif
                                </p>
                              </div>
                              <div class="w-50 float-right text-uppercase text-left">
                                <p class="save-price"><small class="text-uppercase">save</small>
                                @if(count($promotion_arr) > 0)
                                  {{CURRENCY_SIGN}} {{number_format($promotion_arr['promotion'],2)}}</p>
                                @endif
                              </div>
                            </div>
                            <!-- -->
                            @if(count($value->product_promotion) > 0)
                              @foreach($value->product_promotion as $key => $pro_promotion)
                                @if(count($pro_promotion->promotion) > 0 && $pro_promotion->promotion->promotion_type == DEAL_PROMOTION)
                                  @if($pro_promotion->promotion->period_to != DATE_TO)
                                    <?php 
                                      $difference = dateDifference(date('Y-m-d'),$pro_promotion->promotion->period_to,"%a");
                                    ?>
                                    @if($difference > 1)
                                      <div class="shape-box">Remaining {{$difference}} days</div>
                                    @else
                                      <div class="shape-box">Remaining {{$difference}} day</div>
                                    @endif
                                  @else
                                    <?php $difference = dateDifference(date('Y-m-d'),DATE_TO,"%a");?>
                                  @endif
                                  @if($pro_promotion->allocate_qty > 0)
                                    <div class="deal-progress">                          
                                      <div class="d-inline-block w-100">
                                        <div class="w-50 float-left text-uppercase text-left">0 items</div>
                                        <div class="w-50 float-right text-uppercase text-right">
                                          @if($pro_promotion->allocate_qty > 1) 
                                            {{$pro_promotion->allocate_qty}} items 
                                          @else 
                                            {{$pro_promotion->allocate_qty}} item 
                                          @endif</div>
                                      </div>
                                      <div class="progress mb-3">
                                        <?php 
                                          /*set sold stock */
                                          $progress_bar_value = 0;
                                          $progress_bar_value = ($pro_promotion->sold_qty / $pro_promotion->allocate_qty) * 100;
                                        ?>
                                        @if($progress_bar_value > 0)
                                          <div class="progress-bar text-uppercase" role="progressbar" style="width:{{$progress_bar_value}}%;" aria-valuenow="{{$pro_promotion->sold_qty}}" aria-valuemin="0" aria-valuemax="{{$pro_promotion->allocate_qty}}">
                                          @if(($pro_promotion->allocate_qty - $pro_promotion->sold_qty) !== 0)
                                            {{$pro_promotion->sold_qty}} sold
                                          @else
                                            Sold Out
                                          @endif
                                          </div>
                                        @else
                                          <div class="progress-bar text-uppercase" role="progressbar" style="width:20%;" aria-valuenow="{{$pro_promotion->sold_qty}}" aria-valuemin="0" aria-valuemax="{{$pro_promotion->allocate_qty}}">0 sold
                                          </div>
                                        @endif
                                      </div>
                                    </div>
                                    @endif
                                  @endif
                                @endforeach
                              @endif
                            <!-- --> 
                            <!-- If remaining days are over.. -->
                            @if($difference > 0) 
                              <div class="item-promotion mt-3 mb-3">
                                <div class="promos">
                                <!-- If product has no promtion -->
                                  @if(count($value->product_promotion) > 0)
                                    @foreach($value->product_promotion as $key => $promtion_details)
                                        @if(count($promtion_details) > 0 && count($promtion_details->promotion) && !empty($promtion_details->promotion->discount))
                                          <?php array_push($arr,$promtion_details->promotion->discount)?>
                                        @else
                                          <?php array_push($arr,[])?>
                                        @endif
                                        <!-- Promotion only for the bank  -->
                                        <?php 
                                          $promotion_price = $value->product_price->mrp - $promtion_details->promotion_price 
                                        ?>
                                        @if($promtion_details->promotion->promotion_type == BANK_PROMOTION)
                                          <?php 
                                            $promotion_arr = calculatePromtion($deal_promotion_price,$promtion_details->promotion->discount);
                                          ?>
                                          <div class="promo-item text-center">
                                            @if(count($promtion_details->bank_promotion) > 0)
                                              <img class="mx-auto d-block" src="{{url('data/bank_card/images/cards/'.$promtion_details->bank_promotion->bank_card->image)}}">
                                              <p class="mb-0 text-uppercase">
                                                {{$promtion_details->bank_promotion->bank_card->card_name}}
                                              </p>
                                              <p class="mb-0 text-uppercase">
                                                {{$promtion_details->bank_promotion->bank->name}}
                                              </p>
                                            @endif
                                            @if(count($promotion_arr) > 0)
                                              <p class="mb-0">
                                                {{CURRENCY_SIGN}} {{number_format($promotion_arr['promotion_price'],2)}}
                                              </p>
                                              <p class="mb-1"></p>
                                            @else
                                              <p class="mb-0"></p>
                                              <p class="mb-1"></p>
                                            @endif
                                          </div>
                                        @endif
                                    @endforeach
                                  @endif
                                </div>
                              </div>
                              @if($allocate_stock > 0 && isValid($value->product_promotion) && isValid($value->product_promotion[0]->allocate_qty) && $value->product_promotion[0]->allocate_qty - $value->product_promotion[0]->sold_qty) <!-- if stock available -->
                              <!-- Buy Now & Add to Card -->
                              <form action="{{ route('cart.add', ['product_id' => isValid($value->id)]) }}" method="get" id="form_{{ isValid($value->id) }}" class="product_form">
                                <input type="hidden" name="prev_url" value="{{ Request::getQueryString() }}">
                                <div class="cd-customization text-center">
                                  @if(count($value->product_promotion) > 0)
                                    @foreach($value->product_promotion as $key => $pro_promotion)
                                      @if(count($pro_promotion->promotion) > 0 && $pro_promotion->promotion->promotion_type == DEAL_PROMOTION)
                                        <div class="qty">
                                          <input type="number" name="qty" min="1" max="{{$pro_promotion->allocate_qty - $pro_promotion->sold_qty}}" class="form-control form-control-sm" value="1">
                                        </div>
                                      @endif
                                    @endforeach
                                  @endif
                                  <div class="add-cart">
                                    <button type="submit" name="action" value="buy" class="btn btn-sm btn-secondary btn-block"><span>Buy Now</span></button>
                                  </div>
                                  <button type="submit" name="action" value="cart" class="btn btn-link cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                                  <!-- <button type="submit" name="action" value="wishlist" class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></button> -->
                                  @if(count($loggedInUser) != 0 && isValid($value->hasInWishlist))
                                      <button class="btn text-secondary btn-link wishlist" title="Item Already In Wishlist" disabled><i class="fa fa-heart" aria-hidden="true"></i></button>
                                  @else
                                      <a href="{{ url('cart/add') }}/{{ isValid($value->id) }}?action=wishlist"  title="click to add wishlist." class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                  @endif
                                </div>
                              </form>
                              @else
                                <!-- <div class="text-uppercase message-box">
                                  Out of stock
                                </div> -->
                              @endif
                            <!-- Buy Now & Add to Card -->
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.deal item -->
                @endforeach
              @else
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                
                  <div class="text-center mb-4 text-secondary">
                    <div class="h2">Products not found.</div>
                    <div class="h6 mt--2">No Deals</div>
                  </div>
                </div>  
              @endif
            </div> <!-- /.list/grid view -->

             <!-- list/grid view bottom pagination -->
            <div class="search-result-container mt-4 pt-4">
              @if(!empty($product))
                @if($product->total() > $product->perPage())
                  <nav aria-label="Page navigation example">
                    @include('includes.pagination', ['paginator' => $product])
                  </nav>
                @endif
              @endif
            </div> <!-- /.list/grid view bottom pagination -->

          </div> <!-- /.col-lg-12 col-md-12 col-sm-12 -->
        </div> <!-- /.row -->

      <!-- </div> --> <!-- /.wrapper -->

    </div> <!-- /.container -->
  </div> <!-- /.sign in/up panel -->
@stop

@section('js')
    <!-- slick.js -->
    <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>
@stop

@section('scripts')
<script>
$(document).ready(function() {
  $('.cart-item-slider').slick({
    dots: true,
    arrows: false
  }); 
  /*Temporary - page load*/
  setTimeout(function() {
    $("#grid").trigger('click');
  },10);

  $('#list').click(function(event){
      event.preventDefault();
      $('#products').removeClass('grid-view').addClass('list-view');
      $('#products .card').removeClass('text-center').parent().removeClass().addClass('col-lg-12');
      $('#products .card .row > div:first-child').removeClass().addClass('col-6');
      $('#products .card .row > div:last-child').removeClass().addClass('col-6');
      $('.promos').slick('setPosition');
  });

  $('#grid').click(function(event){
      event.preventDefault();
      $('#products').removeClass('list-view').addClass('grid-view');
      $('#products .card').addClass('text-center').removeClass('row').parent().removeClass('col-lg-12').addClass('col-lg-3 col-md-3 col-sm-6 col-xs-12');
      $('#products .card .row > div').removeClass().addClass('col-12');
      $('.promos').slick('setPosition');
  });

  $('.promos').slick({
      infinite: true,
      dots: false,
      accessibility: true,
      speed: 1000,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,          
      arrows: true,// Enable Next/Prev arrows
      asNavFor: null,
      responsive: [{
          breakpoint: 1024,
          settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
          }
      },{
          breakpoint: 992,
          settings: {
              slidesToShow: 2,
              slidesToScroll: 1
          }
      },{
          breakpoint: 600,
          settings: {
              slidesToShow: 2,
              slidesToScroll: 1
          }
      },{
          breakpoint: 480,
          settings: {
              slidesToShow: 2,
              slidesToScroll: 1
          }
      }]
  });
});
</script>
@stop