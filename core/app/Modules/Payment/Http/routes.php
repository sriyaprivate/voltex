<?php
/**
 * PAYMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-08
 */

Route::group(['prefix' => 'payment', 'middleware' => 'web',  'namespace' => 'App\Modules\Payment\Http\Controllers'], function()
{
    /*+++++++++++++++++++++++++++++++
    *          GET Request
    ++++++++++++++++++++++++++++++++*/
    Route::get('test', [
        'uses' => 'PaymentController@test',
        'as'   => 'payment.test'
    ]);

    Route::get('hnb/response', [
        'uses' => 'PaymentController@response',
        'as'   => 'payment.hnb.response'
    ]);
    Route::get('gateway/response', [
        'uses' => 'PaymentController@showGatewayResponse',
        'as'   => 'payment.gateway.response'
    ]);
    Route::get('amex/gateway/response', [
        'uses' => 'PaymentController@showAmexGatewayResponse',
        'as'   => 'payment.amex.gateway.response'
    ]);
    Route::get('orelpay/gateway/response', [
        'uses' => 'PaymentController@orelPayRedirectResponse',
        'as'   => 'orelpay.gateway.response'
    ]);
    Route::get('cashondelivery/response', [
        'uses' => 'PaymentController@cashOnDeliveryresponse',
        'as'   => 'payment.cashondelivery.response'
    ]);
    Route::get('cashonpickup/response', [
        'uses' => 'PaymentController@cashOnPickupResponse',
        'as'   => 'payment.cashonpickup.response'
    ]);

    /*+++++++++++++++++++++++++++++++
    *          POST Request
    ++++++++++++++++++++++++++++++++*/
    Route::post('hnb/response', [
        'uses' => 'PaymentController@response',
        'as'   => 'payment.hnb.response'
    ]);

    Route::post('amex/response', [
        'uses' => 'PaymentController@amexGatewayResponse',
        'as'   => 'payment.test'
    ]);

    Route::post('orel-pay/response', [
        'uses' => 'PaymentController@orelpayGatewayResponse',
        'as'   => 'payment.orelpay.response'
    ]);
});

