<?php
/**
 * PAYMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-08
 */

namespace App\Modules\Payment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Classes\Payment\Contracts\PaymentGateway;
use App\Classes\Payment\HnbPaymentGateway;
use App\Classes\Payment\AmexPaymentGateway;
use App\Classes\Payment\OrelPayPaymentGateway;
use App\Classes\Payment\cashOnPickupPayment;

use App\Classes\Functions;
use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Modules\Cart\Repositories\CartRepository;
use App\Classes\DeliveryChargesCalculator;
use Illuminate\Support\Collection;
use App\Models\Stock;
use Session;
use Illuminate\Support\Facades\Crypt;
use App\Modules\Payment\Repositories\PaymentRepository;
use App\Events\Mails\OrderConfirmationEmail;
use App\Modules\Payment\Http\Requests\OrelPayResponseRequest;

use Pusher\Pusher;

class PaymentController extends Controller
{
    //variable declaration
    protected $paymentGateway = null;
    protected $paymentRepo    = null;
    protected $common         = null;
    protected $cartLogic      = null;

    //object assigned to the varible.
    public function __construct(Functions $common, PaymentRepository $paymentRepo, CartLogic $cartLogic){
        $this->common         = $common;
        $this->paymentRepo    = $paymentRepo;
        $this->cartLogic      = $cartLogic;
    }

    //HNB gateway
    public function response(Request $request){
        try{
            $cartLogic    = new CartLogic();
            $cartRepo     = new CartRepository();
            $loggedUser   = $cartLogic->getLoggedUser();
            $isPostMethod = $request->isMethod('POST');
            $getOrder     = [];

            if(count($loggedUser) > 0){
                if($isPostMethod){    
                    $HNBPaymentGateway = new HNBPaymentGateway();
                    $gatewayResponse = $HNBPaymentGateway->paymentResponse($request);
                    
                    //1 mean payment approved
                    if(count($gatewayResponse) > 0 && isset($gatewayResponse['response_code']) && $gatewayResponse['response_code'] == 1){
                        $deleted  = $cartRepo->removeItemInCartByStatus(DEFAULT_STATUS);
    
                        if(isset($gatewayResponse['order_no'])){
                            $getOrder = $cartRepo->getOrderByNo($gatewayResponse['order_no']);
                            
                            if(count($getOrder) > 0 && isset($getOrder['order_details']) && isset($getOrder['order'])){
                                
                                $data = collect($getOrder['order']);
                                //$paidAmount      = ($getOrderDetails->sum('price') - $getOrderDetails->sum('discounted_price'));

                                if($data->get('amount') == '0.0' && $data->get('delivery_charges') == '0.0'){
                                    throw new \Exception("Invalid payment value.");
                                }
                                
                                $paidAmount = ($data->get('amount') + $data->get('delivery_charges'));
                                
                                if(!empty($paidAmount)){
                                    if(isset($getOrder['order']) && count($getOrder['order']) && isset($getOrder['order']['id'])){
                                        //update payment amount
                                        //params: order record, paid amount
                                        $paid = $cartRepo->updatePayment($getOrder['order'], $paidAmount);
    
                                        if(count($paid) > 0 && isset($paid['id'])){   
                                            $paymentTransaction = $cartRepo->updatePaymenTransaction($paid['id'], $paidAmount);
    
                                            if(count($paymentTransaction) == 0){
                                                throw new \Exception("Payment transaction couldn't be updated!.");        
                                            }
                                        }else{
                                            throw new \Exception("Payment could't be updated!.");        
                                        }
    
                                        //changed order status
                                        //params: order id, status
                                        $updatedOrder = $cartRepo->updateOrder($getOrder['order']['id'], APPROVED);
                                        
                                        if(count($updatedOrder) == 0){
                                            throw new \Exception("Order couldn't be updated!.");        
                                        }

                                        $reduceStock = $cartRepo->reductStock($getOrder['order']['id']);
                                        
                                        if(count($reduceStock) == 0){
                                            throw new \Exception("Something went wrong, stock couldn't be reduce!.");        
                                        }
                                    }else{
                                        throw new \Exception("Order not found!.");    
                                    }
                                }else{
                                    throw new \Exception("Invalid sum of order details!.");    
                                }
                            }else{
                                throw new \Exception("Order not found!.");    
                            }
                        }else{
                            throw new \Exception("Order_no not found in gateway response!.");
                        }
    
                        //remove order in session cart if payment is success
                        $cartRepo->removeOrderInSession($loggedUser);

                        //fire order confirmation email event
                        event(new OrderConfirmationEmail($getOrder['order']['id']));
                    }
    
                    //get cart count
                    $cart            = $cartLogic->getCart($loggedUser);
                    $cartItemCount   = '';
    
                    if(count($cart) > 0 && !empty($cart->totalQty)){
                        $cartItemCount = $cart->totalQty;
                    }

                    if(!isset($gatewayResponse['id'])){
                        throw new \Exception('Something went wrong, transaction id not found!.');
                    }

                    $encripted_trans_id = Crypt::encrypt($gatewayResponse['id']);

                    return redirect('/payment/gateway/response?trans_id='.$encripted_trans_id);
                }else{
                    //if get request redirect user to home page
                    return redirect()->route('home.index');
                }
            }else{
                throw new \Exception("Logged user not found!.");
            }
        }catch(\Exception $e){
            //return $e->getMessage();
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );             
        }
    }

    //cash on delivery gateway
    public function cashOnDeliveryresponse(Request $request){
        try{
            $cartLogic    = new CartLogic();
            $cartRepo     = new CartRepository();
            $loggedUser   = $cartLogic->getLoggedUser();
            $getOrder     = [];

            $cashOnDeliveryPayment = new cashOnDeliveryPayment();
            $gatewayResponse = $cashOnDeliveryPayment->paymentResponse($request);
            
            //1 mean payment approved
            if(count($gatewayResponse) > 0 && isset($gatewayResponse['response_code']) && $gatewayResponse['response_code'] == 1){
                //$deleted  = $cartRepo->removeItemInCartByStatus(DEFAULT_STATUS);

                if(isset($gatewayResponse['order_no'])){
                    $getOrder = $cartRepo->getOrderByNo($gatewayResponse['order_no']);
                    
                    if(count($getOrder) > 0 && isset($getOrder['order_details']) && isset($getOrder['order'])){
                        
                        $data = collect($getOrder['order']);
                        //$paidAmount      = ($getOrderDetails->sum('price') - $getOrderDetails->sum('discounted_price'));

                        if($data->get('amount') == '0.0' && $data->get('delivery_charges') == '0.0'){
                            throw new \Exception("Invalid payment value.");
                        }
                        
                        $paidAmount = ($data->get('amount') + $data->get('delivery_charges'));
                        
                        if(!empty($paidAmount)){
                            if(isset($getOrder['order']) && count($getOrder['order']) && isset($getOrder['order']['id'])){
                                //update payment amount
                                //params: order record, paid amount
                                $paid = $cartRepo->updatePayment($getOrder['order'], $paidAmount);

                                if(count($paid) > 0 && isset($paid['id'])){   
                                    $paymentTransaction = $cartRepo->updatePaymenTransaction($paid['id'], $paidAmount);

                                    if(count($paymentTransaction) == 0){
                                        throw new \Exception("Payment transaction couldn't be updated!.");        
                                    }
                                }else{
                                    throw new \Exception("Payment could't be updated!.");        
                                }

                                //changed order status
                                //params: order id, status
                                $updatedOrder = $cartRepo->updateOrder($getOrder['order']['id'], APPROVED);
                                
                                if(count($updatedOrder) == 0){
                                    throw new \Exception("Order couldn't be updated!.");        
                                }

                                $reduceStock = $cartRepo->reductStock($getOrder['order']['id']);
                                
                                if(count($reduceStock) == 0){
                                    throw new \Exception("Something went wrong, stock couldn't be reduce!.");        
                                }
                            }else{
                                throw new \Exception("Order not found!.");    
                            }
                        }else{
                            throw new \Exception("Invalid sum of order details!.");    
                        }
                    }else{
                        throw new \Exception("Order not found!.");    
                    }
                }else{
                    throw new \Exception("Order_no not found in gateway response!.");
                }

                // //remove order in session cart if payment is success
                // $cartRepo->removeOrderInSession($loggedUser);

                //fire order confirmation email event
                event(new OrderConfirmationEmail($getOrder['order']['id']));
            }

            //get cart count
            $cart            = $cartLogic->getCart($loggedUser);
            $cartItemCount   = '';

            if(count($cart) > 0 && !empty($cart->totalQty)){
                $cartItemCount = $cart->totalQty;
            }

            if(!isset($gatewayResponse['id'])){
                throw new \Exception('Something went wrong, transaction id not found!.');
            }

            return response()->json(['success' => 'ok', 'msg' => 'Order deliveryed.']);
            //return redirect('/payment/gateway/response?trans_id='.$encripted_trans_id);
        }catch(\Exception $e){
            return response()->json(['success' => 'no', 'msg' => $e->getMessage()]);
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );             
        }
    }

    //cash on pickup gateway
    public function cashOnPickupResponse(Request $request){
        try{
            $cartLogic    = new CartLogic();
            $cartRepo     = new CartRepository();
            $loggedUser   = $cartLogic->getLoggedUser();
            $getOrder     = [];
            $paidAmount   = 0;
            $cashOnPickupPayment = new cashOnPickupPayment();
            $gatewayResponse = $cashOnPickupPayment->paymentResponse($request);
            
            //1 mean payment approved
            if(count($gatewayResponse) > 0 && isset($gatewayResponse['response_code']) && $gatewayResponse['response_code'] == 1){
                //$deleted  = $cartRepo->removeItemInCartByStatus(DEFAULT_STATUS);
                if(isset($gatewayResponse['order_no'])){
                    $getOrder = $cartRepo->getOrderByNo($gatewayResponse['order_no']);

                    if(count($getOrder) > 0 && isset($getOrder['order_details']) && isset($getOrder['order'])){
                        
                        $data = collect($getOrder['order']);
                        //$paidAmount      = ($getOrderDetails->sum('price') - $getOrderDetails->sum('discounted_price'));

                        if($data->get('amount') == '0.0' && $data->get('delivery_charges') == '0.0'){
                            throw new \Exception("Invalid payment value.");
                        }
                        
                        if($data->get('amount') >= $request->input('pay_amount')){
                            $paidAmount = $request->input('pay_amount');
                        }else{
                            throw new \Exception("Invalid payment amount passed!.");   
                        }

                        $paidAmount = ($paidAmount + $data->get('delivery_charges'));
                        
                        if(!empty($paidAmount)){
                            if(isset($getOrder['order']) && count($getOrder['order']) && isset($getOrder['order']['id'])){
                                //update payment amount
                                //params: order record, paid amount
                                //$paid = $cartRepo->addPayment($getOrder['order'], $paidAmount);
                                $paid = $cartRepo->updatePayment($getOrder['order'], $paidAmount);

                                if(count($paid) > 0 && isset($paid['id']) && !empty($paid['id'])){   
                                    //$paymentTransaction = $cartRepo->updatePaymenTransaction($paid['id'], $paidAmount);
                                    $paymentTransaction = $cartRepo->addPaymenTransaction($paid['id'], $paidAmount, $getOrder['order']['order_no']);

                                    if(count($paymentTransaction) == 0){
                                        throw new \Exception("Payment transaction couldn't be updated!.");        
                                    }
                                }else{
                                    throw new \Exception("Payment could't be updated!.");        
                                }
                                //changed order status
                                //params: order id, status

                                $updatedOrder = $cartRepo->updateOrder($getOrder['order']['id'], APPROVED, $paidAmount);
                                
                                if(count($updatedOrder) == 0){
                                    throw new \Exception("Order couldn't be updated!.");        
                                }

                                //$reduceStock = $cartRepo->reductStock($getOrder['order']['id']);
                                
                                // if(count($reduceStock) == 0){
                                //     throw new \Exception("Something went wrong, stock couldn't be reduce!.");        
                                // }
                            }else{
                                throw new \Exception("Order not found!.");    
                            }
                        }else{
                            throw new \Exception("Invalid sum of order details!.");    
                        }
                    }else{
                        throw new \Exception("Order not found!.");    
                    }
                }else{
                    throw new \Exception("Order_no not found in gateway response!.");
                }

                // //remove order in session cart if payment is success
                // $cartRepo->removeOrderInSession($loggedUser);

                //fire order confirmation email event
                //event(new OrderConfirmationEmail($getOrder['order']['id']));
            }

            //get cart count
            $cart            = $cartLogic->getCart($loggedUser);
            $cartItemCount   = '';

            if(count($cart) > 0 && !empty($cart->totalQty)){
                $cartItemCount = $cart->totalQty;
            }

            if(!isset($gatewayResponse['id'])){
                throw new \Exception('Something went wrong, transaction id not found!.');
            }

            return response()->json(['success' => 'ok', 'msg' => 'Order deliveryed.']);
            //return redirect('/payment/gateway/response?trans_id='.$encripted_trans_id);
        }catch(\Exception $e){
            return response()->json(['success' => 'no', 'msg' => $e->getMessage()]);
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );             
        }
    }

    //amex gateway
    public function amexGatewayResponse(Request $request){
        try{
            $cartLogic    = new CartLogic();
            $cartRepo     = new CartRepository();
            $loggedUser   = $cartLogic->getLoggedUser();
            $isPostMethod = $request->isMethod('POST');

            if(count($loggedUser) > 0){
                if($isPostMethod){    
                    $AmexPaymentGateway = new AmexPaymentGateway();
                    $gatewayResponse    = $AmexPaymentGateway->paymentResponse($request);   

                    //1 mean payment approved
                    if(count($gatewayResponse) > 0 && isset($gatewayResponse['response_code']) && $gatewayResponse['response_code'] == 1){
                        $deleted  = $cartRepo->removeItemInCartByStatus(DEFAULT_STATUS);

                        if(isset($gatewayResponse['order_no'])){
                            $getOrder = $cartRepo->getOrderByNo($gatewayResponse['order_no']);

                            if(count($getOrder) > 0 && isset($getOrder['order_details']) && isset($getOrder['order'])){
                                
                                $data = collect($getOrder['order']);
                                //$paidAmount      = ($getOrderDetails->sum('price') - $getOrderDetails->sum('discounted_price'));

                                if($data->get('amount') == '0.0' && $data->get('delivery_charges') == '0.0'){
                                    throw new \Exception("Invalid payment value.");
                                }
                                
                                $paidAmount = ($data->get('amount') + $data->get('delivery_charges'));
                                
                                if(!empty($paidAmount)){
                                    if(isset($getOrder['order']) && count($getOrder['order']) && isset($getOrder['order']['id'])){
                                        //update payment amount
                                        //params: order record, paid amount
                                        $paid = $cartRepo->updatePayment($getOrder['order'], $paidAmount);
                                        
                                        if(count($paid) > 0 && isset($paid['id'])){   
                                            $paymentTransaction = $cartRepo->updatePaymenTransaction($paid['id'], $paidAmount);

                                            if(count($paymentTransaction) == 0){
                                                throw new \Exception("Payment transaction couldn't be updated!.");        
                                            }
                                        }else{
                                            throw new \Exception("Payment could't be updated!.");        
                                        }
    
                                        //changed order status
                                        //params: order id, status
                                        $updatedOrder = $cartRepo->updateOrder($getOrder['order']['id'], APPROVED);
                                        
                                        if(count($updatedOrder) == 0){
                                            throw new \Exception("Order couldn't be updated!.");        
                                        }

                                        $reduceStock = $cartRepo->reductStock($getOrder['order']['id']);
                                        
                                        if(count($reduceStock) == 0){
                                            throw new \Exception("Something went wrong, stock couldn't be reduce!.");        
                                        }
                                    }else{
                                        throw new \Exception("Order not found!.");    
                                    }
                                }else{
                                    throw new \Exception("Invalid sum of order details!.");    
                                }
                            }else{
                                throw new \Exception("Order not found!.");    
                            }
                        }else{
                            throw new \Exception("Order_no not found in gateway response!.");
                        }
    
                        //remove order in session cart if payment is success
                        $cartRepo->removeOrderInSession($loggedUser);

                        //fire order confirmation email event
                        event(new OrderConfirmationEmail($getOrder['order']['id']));
                    }
    
                    //get cart count
                    $cart            = $cartLogic->getCart($loggedUser);
                    $cartItemCount   = '';
    
                    if(count($cart) > 0 && !empty($cart->totalQty)){
                        $cartItemCount = $cart->totalQty;
                    }

                    if(!isset($gatewayResponse['id'])){
                        throw new \Exception('Something went wrong, transaction id not found!.');
                    }

                    $encripted_trans_id = Crypt::encrypt($gatewayResponse['id']);
                    
                    return redirect('/payment/amex/gateway/response?trans_id='.$encripted_trans_id);
                }else{
                    //if get request redirect user to home page
                    return redirect()->route('home.index');
                }
            }else{
                throw new \Exception("Logged user not found!.");
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );             
        }
    }

    //show HNB response
    public function showGatewayResponse(Request $request){
        try{
            $trans_id = $request->input('trans_id');

            if(isset($trans_id) && !empty($trans_id)){
                $trans_id = Crypt::decrypt($request->input('trans_id'));    

                if(!empty($trans_id)){
                    //get cart count
                    $loggedUser    = $this->common->getLoggedUser();
                    $cart          = $this->cartLogic->getCart($loggedUser);
                    $cartItemCount = '';

                    if(count($cart) > 0 && !empty($cart->totalQty)){
                        $cartItemCount = $cart->totalQty;
                    }

                    $gatewayResponse = $this->paymentRepo->getPaymentTransactionById($trans_id);

                    if(count($gatewayResponse) > 0){
                        if(isset($gatewayResponse->response) && !empty($gatewayResponse->response)){
                            $gatewayResponse = json_decode($gatewayResponse->response);
                        }else{
                            throw new \Exception("Invalid gateway response");    
                        }
                    }else{
                        throw new \Exception("Transaction record not found!.");
                    }
                    
                    return view('cart::payment.gatewayResponse', compact('gatewayResponse', 'cartItemCount'));
                }else{
                    throw new \Exception('Invalid Transaction id.');    
                }
            }else{
                throw new \Exception('Invalid Transaction id.');
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );             
        }
    }

    //show amex response
    public function showAmexGatewayResponse(Request $request){
        try{
            $trans_id = $request->input('trans_id');

            if(isset($trans_id) && !empty($trans_id)){
                $trans_id = Crypt::decrypt($request->input('trans_id'));    

                if(!empty($trans_id)){
                    //get cart count
                    $loggedUser    = $this->common->getLoggedUser();
                    $cart          = $this->cartLogic->getCart($loggedUser);
                    $cartItemCount = '';

                    if(count($cart) > 0 && !empty($cart->totalQty)){
                        $cartItemCount = $cart->totalQty;
                    }

                    $gatewayResponse = $this->paymentRepo->getPaymentTransactionById($trans_id);
                    if(count($gatewayResponse) > 0){
                        $gatewayResponse = json_decode($gatewayResponse);
                    }else{
                        throw new \Exception("Transaction record not found!.");
                    }
                    
                    return view('cart::payment.amexGatewayResponse', compact('gatewayResponse', 'cartItemCount'));
                }else{
                    throw new \Exception('Invalid Transaction id.');    
                }
            }else{
                throw new \Exception('Invalid Transaction id.');
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );             
        }
    }

    //show HNB response
    public function orelPayRedirectResponse(Request $request){
        try{
            $trans_id = $request->input('trans_id');

            if(isset($trans_id) && !empty($trans_id)){
                $trans_id = Crypt::decrypt($request->input('trans_id'));    

                if(!empty($trans_id)){
                    //get cart count
                    $loggedUser    = $this->common->getLoggedUser();
                    $cart          = $this->cartLogic->getCart($loggedUser);
                    $cartItemCount = '';

                    if(count($cart) > 0 && !empty($cart->totalQty)){
                        $cartItemCount = $cart->totalQty;
                    }

                    $gatewayResponse = $this->paymentRepo->getPaymentTransactionById($trans_id);

                    if($gatewayResponse && !$gatewayResponse->response){
                        $gatewayResponse->error_code = 2;
                        $gatewayResponse->error_message = 'Payment failed due to timeout';
                        $gatewayResponse->response = json_encode(['error_code' => 2, 'error' => 'Payment failed due to timeout']);
                        $gatewayResponse->save();
                    }


                    if(count($gatewayResponse) > 0){
                        if(isset($gatewayResponse->response) && !empty($gatewayResponse->response)){
                            $gatewayResponse = json_decode($gatewayResponse);
                        }else{
                            throw new \Exception("Invalid gateway response");    
                        }
                    }else{
                        throw new \Exception("Transaction record not found!.");
                    }
                    
                    return view('cart::payment.orelPayGatewayResponse', compact('gatewayResponse', 'cartItemCount'));
                }else{
                    throw new \Exception('Invalid Transaction id.');    
                }
            }else{
                throw new \Exception('Invalid Transaction id.');
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );             
        }
    }

    //orel pay payment gateway response
    //OrelPayResponseRequest
    public function orelpayGatewayResponse(Request $request){
        try{
            $merchant_id     = $request->input('merchant_id');
            $order_id        = $request->input('order_id');
            $transaction_id  = $request->input('transaction_id');
            $paid_amount     = $request->input('paid_amount');
            $received_amount = $request->input('received_amount');
            $response_signature       = $request->input('signature');

            $cartLogic       = new CartLogic();
            $cartRepo        = new CartRepository();
            $isPostMethod    = $request->isMethod('POST');

            if($isPostMethod){
                $orelPayPaymentGateway = new OrelPayPaymentGateway();
                $gatewayResponse    = $orelPayPaymentGateway->paymentResponse($request);

                //generate signature
                $signature = strtoupper(md5(config('payment.orelPay.merchant_id').$order_id.$transaction_id.$paid_amount.$received_amount.strtoupper(md5(config('payment.orelPay.secret')))));

                /*if($signature != $response_signature){
                    return response()->json(['message' => 'invalid signature'], 405);
                }*/

                //1 mean payment approved
                if(count($gatewayResponse) > 0 && isset($gatewayResponse['response_code']) && $gatewayResponse['response_code'] == 1){
                    $deleted  = $cartRepo->removeItemInCartByStatus(DEFAULT_STATUS);

                    if(isset($gatewayResponse['order_no'])){
                        $getOrder = $cartRepo->getOrderByNo($gatewayResponse['order_no']);

                        if(count($getOrder) > 0 && isset($getOrder['order_details']) && isset($getOrder['order'])){
                            
                            $data = collect($getOrder['order']);

                            if($data->get('amount') == '0.0' && $data->get('delivery_charges') == '0.0'){
                                throw new \Exception("Invalid payment value.");
                            }
                            
                            $paidAmount = ($data->get('amount') + $data->get('delivery_charges'));
                            
                            if(!empty($paidAmount)){
                                if(isset($getOrder['order']) && count($getOrder['order']) && isset($getOrder['order']['id'])){
                                    //update payment amount
                                    //params: order record, paid amount
                                    $paid = $cartRepo->updatePayment($getOrder['order'], $paidAmount);
                                    
                                    if(count($paid) > 0 && isset($paid['id'])){   
                                        $paymentTransaction = $cartRepo->updatePaymenTransaction($paid['id'], $paidAmount);

                                        if(count($paymentTransaction) == 0){
                                            throw new \Exception("Payment transaction couldn't be updated!.");        
                                        }
                                    }else{
                                        throw new \Exception("Payment could't be updated!.");        
                                    }

                                    //changed order status
                                    //params: order id, status
                                    $updatedOrder = $cartRepo->updateOrder($getOrder['order']['id'], APPROVED);
                                    
                                    if(count($updatedOrder) == 0){
                                        throw new \Exception("Order couldn't be updated!.");        
                                    }

                                    $reduceStock = $cartRepo->reductStock($getOrder['order']['id']);
                                    
                                    if(count($reduceStock) == 0){
                                        throw new \Exception("Something went wrong, stock couldn't be reduce!.");        
                                    }
                                }else{
                                    throw new \Exception("Order not found!.");    
                                }
                            }else{
                                throw new \Exception("Invalid sum of order details!.");    
                            }
                        }else{
                            throw new \Exception("Order not found!.");    
                        }
                    }else{
                        throw new \Exception("Order_no not found in gateway response!.");
                    }

                    //remove order in session cart if payment is success
                    //$cartRepo->removeOrderInSession($loggedUser);

                    //fire order confirmation email event
                    event(new OrderConfirmationEmail($getOrder['order']['id']));
                }

                $loggedUser = $cartLogic->getLoggedUser($getOrder['order']['user_id']);

                //get cart count
                $cart            = $cartLogic->getCart($loggedUser);
                $cartItemCount   = '';

                if(count($cart) > 0 && !empty($cart->totalQty)){
                    $cartItemCount = $cart->totalQty;
                }

                if(!isset($gatewayResponse['id'])){
                    throw new \Exception('Something went wrong, transaction id not found!.');
                }

                $encripted_trans_id = Crypt::encrypt($gatewayResponse['id']);

                $options = array(
                    'cluster' => 'ap1',
                    'encrypted' => true
                );

                $pusher = new Pusher(
                    '40a253ba37961e021448',
                    '5ea33666362c6866e4f2',
                    '496311',
                    $options
                );

                $orderInfo['message']            = 'success';
                $orderInfo['order_no']           = $getOrder['order']['order_no'];
                $orderInfo['transaction_id']     = $order_id;
                $orderInfo['ipg_transaction_id'] = $transaction_id;
                $pusher->trigger('orel-pay', 'payment-success', $orderInfo);
                
                return response()->json(['message' => 'success'],200);
            }else{
                return response()->json(['message' => $e->getMessage()],400);
            }
        }catch(\Exception $e){
            //return response()->json(['message' => $e->getMessage()],400);
            //return $e->getMessage();
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );             
        }
    }
}
