<?php
/**
 * SHOPPING CART
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-28
 */

Namespace App\Modules\Payment\Repositories;

use App\Models\Payment;
use App\Models\PaymentTransaction;

class PaymentRepository{    
    public function __construct(){

    }

    /**
     * get payment transaction
     * @return data
     * @param product_id, qty
     */

    public function getPaymentTransactionById($transaction_id){
        if(isset($transaction_id) && !empty($transaction_id)){
            $transaction = PaymentTransaction::where('id', $transaction_id)->orderBy('id', 'DESC')->first();
            return $transaction;
        }else{
            throw new \Exception("Invalid transaction id passed!.");
        }
    }
}