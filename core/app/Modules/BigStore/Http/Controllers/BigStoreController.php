<?php
/**
 * BIGSTORE
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-08
 */

namespace App\Modules\BigStore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\Category\CategoryLogic\CategoryLogic;
use App\Modules\Category\CategoryRepository\CategoryRepository;
use App\Modules\Product\BusinessLogics\ProductLogic;
use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Cart\Repositories\CartRepository;

use Illuminate\Support\Collection;
use App\Classes\Functions;

class BigStoreController extends Controller
{
    //variable declaration
    protected $categoryRepo     = null;
    protected $categoryLogic    = null;
    protected $productRepo      = null;
    protected $common           = null;
    protected $cartLogic        = null;
    protected $cartRepo         = null;

    //object assigned to the varible.
    public function __construct(CategoryLogic $categoryLogic, CategoryRepository $categoryRepo, ProductRepository $productRepo, Functions $common, CartLogic $cartLogic, CartRepository $cartRepo){
        $this->categoryLogic = $categoryLogic;
        $this->categoryRepo  = $categoryRepo;
        $this->productRepo   = $productRepo;
        $this->common        = $common;
        $this->cartLogic     = $cartLogic;
        $this->cartRepo      = $cartRepo;
    }

    /**
     * index
     * @return view
     */
    public function index(Request $request, $mainSlug = null, $slug = null, $category_id = null)
    {
        try{
            $category_id = (int) trim($category_id);

            /**
             * @param : page name, return type tree or parents
             * @return array
             */
            if($mainSlug == null){
                $mainSlug = GENTLEMEN;
            }

            $categories    = $this->categoryRepo->getMainCategoriesByPage($mainSlug, 'tree');

            if(isset($mainSlug) && !empty($mainSlug)){
                if($mainSlug == GENTLEMEN){
                    //params: category id,  $request, main category id
                    $products = $this->productRepo->getProductByCategory($category_id, $request, GENTLEMEN_ID);
                }elseif($mainSlug == LADIES){
                    //params: category id,  $request, main category id
                    $products = $this->productRepo->getProductByCategory($category_id, $request, LADIES_ID);
                }elseif($mainSlug == CHILDREN){
                    //params: category id,  $request, main category id
                    $products = $this->productRepo->getProductByCategory($category_id, $request, CHILDREN_ID);
                }elseif($mainSlug == OTHER){
                    //params: category id,  $request, main category id
                    $products = $this->productRepo->getProductByCategory($category_id, $request, OTHER_ID);
                }else{
                    throw new \Exception("Invalid url");
                }
            }else{
                //params: category id,  $request, main category id
                $products = $this->productRepo->getProductByCategory(null, $request, GENTLEMEN_ID);
            }
            
            return view('bigstore::index', compact('categories', 'products'));

        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    public function cart(Request $request)
    {
        try{
            return view('bigstore::cart');
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    
    public function view($mainSlug = null, $item_name, $item_id)
    {
        try{
            $product_details = $this->productRepo->getProductById((int) $item_id);

            if(count($product_details) > 0){
                return view('bigstore::view', compact('product_details'));
            }else{
                throw new \Exception("Product not found!");
            }
        }catch(\Exception $e){
            return $e->getMessage();
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the check-out view.
     * @return Response
     */
    public function checkoutView()
    {
        try{
            $product_details  = $this->productRepo->getProductById(1);

            if(count($product_details) > 0){
                return view('bigstore::check-out', compact('product_details'));
            }else{
                throw new \Exception("Product not found!");
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * get basic measurement detail of product.
     * @return Response
     */
    public function getMeasureDetails(Request $request)
    {
        try{

            $measure_details  = $this->productRepo->getProductMeasurement($request->product_id);
            
            if($measure_details){
                return $measure_details;
            }else{
                return [];
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * get basic choices detail of product.
     * @return Response
     */
    public function getProductChoicesModals(Request $request)
    {
        try{

            $choices_details  = $this->productRepo->getProductChoicesModals($request->pro_id);
            
            if($choices_details){
                return $choices_details;
            }else{
                return [];
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * get choice options detail of choice.
     * @return Response
     */
    public function getProductChoiceOptions(Request $request)
    {
        try{

            $option_details  = $this->productRepo->getProductChoiceOptions($request->choice_id);
            
            if($option_details){
                return $option_details;
            }else{
                return [];
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * get fabric detail of product.
     * @return Response
     */
    public function getProductFabrics(Request $request)
    {
        try{

            $fabrics  = $this->productRepo->getProductFabrics($request->pro_id);
            
            if($fabrics){
                return $fabrics;
            }else{
                return [];
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bigstore::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bigstore::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('bigstore::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
