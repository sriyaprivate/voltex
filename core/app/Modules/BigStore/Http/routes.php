<?php
/**
 * BIGSTORE
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-08
 */

Route::group(['prefix' => 'bigstore', 'middleware' => 'web',  'namespace' => 'App\\Modules\BigStore\Http\Controllers'], function()
{
    /*+++++++++++++++++++++++++++++++
    *          GET Request
    ++++++++++++++++++++++++++++++++*/

    // Route::get('check-out', [
    //     'uses' => 'BigStoreController@checkoutView',
    //     'as'   => 'bigstore.item.checkout'
    // ]);

    Route::get('get-measure-detail', [
        'uses' => 'BigStoreController@getMeasureDetails',
        'as'   => 'bigstore.item.checkout'
    ]);

    Route::get('get-product-choices-modals', [
        'uses' => 'BigStoreController@getProductChoicesModals',
        'as'   => 'bigstore.item.checkout'
    ]);

    Route::get('get-choice-options', [
        'uses' => 'BigStoreController@getProductChoiceOptions',
        'as'   => 'bigstore.item.checkout'
    ]);

    Route::get('get-product-fabrics', [
        'uses' => 'BigStoreController@getProductFabrics',
        'as'   => 'bigstore.item.checkout'
    ]);

    //this route put bottom of the page because if it put in top of the page other routes may be not working probably.

    Route::get('cart', [
        'uses' => 'BigStoreController@cart',
        'as'   => 'bigstore.cart'
    ]);

    Route::get('gentlements/trouser', [
        'uses' => 'BigStoreController@gentlementsTrouser',
        'as'   => 'bigstore.gents'
    ]);
    
    // Route::get('gentlements/{slug?}/{category_id?}', [
    //     'uses' => 'BigStoreController@gentlements',
    //     'as'   => 'bigstore.index'
    // ]);

    // Route::get('ladies/{slug?}/{category_id?}', [
    //     'uses' => 'BigStoreController@ladies',
    //     'as'   => 'bigstore.ladies'
    // ]);

    // Route::get('children/{slug?}/{category_id?}', [
    //     'uses' => 'BigStoreController@children',
    //     'as'   => 'bigstore.children'
    // ]);

    // Route::get('other/{slug?}/{category_id?}', [
    //     'uses' => 'BigStoreController@other',
    //     'as'   => 'bigstore.other'
    // ]);


    Route::get('/', [
        'uses' => 'BigStoreController@index',
        'as'   => 'bigstore.index'
    ]);
    Route::get('category/{slug?}/{category_id?}', [
        'uses' => 'BigStoreController@productByCategory',
        'as'   => 'bigstore.category'
    ]);
    Route::get('{mainSlug?}/item/{item_name?}/{item_id}', [
        'uses' => 'BigStoreController@view',
        'as'   => 'bigstore.item.view'
    ]);
    Route::get('{mainSlug?}/{slug?}/{category_id?}', [
        'uses' => 'BigStoreController@index',
        'as'   => 'bigstore.store'
    ]);

    /*+++++++++++++++++++++++++++++++
    *          POST Request
    ++++++++++++++++++++++++++++++++*/
});

