@extends('layouts.master') @section('title','Big Store View')

@section('links')
  <!-- slick.css -->
  <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
  <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">
  
  <!-- owl.carousel.js -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

  <!-- PgwSlider-master -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/PgwSlider-master/pgwslider.min.css')}}" />

  <!-- jquery.rateyo.v2.3.2 -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/jquery.rateyo.v2.3.2/jquery.rateyo.min.css')}}" />

  <link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style type="text/css">
    
    .mtb1{
        font-family: "Lato", sans-serif;
        font-size: 14px;
        font-weight: 300;
        letter-spacing: 0.15em;
        text-transform: uppercase;
        text-decoration: none;
        text-align: center;
        padding: 1em 1.15em 1em 1.3em;
        border: 2px solid transparent;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border-radius: 0;
        color: white;
        cursor: pointer;
        /* height: 40px; */
        display: -webkit-inline-box;
        display: -webkit-inline-flex;
        display: -moz-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-direction: normal;
        -webkit-box-orient: horizontal;
        -webkit-flex-direction: row;
        -moz-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: baseline;
        -ms-flex-align: baseline;
        -webkit-align-items: baseline;
        -moz-align-items: baseline;
        align-items: baseline;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        -moz-justify-content: center;
        justify-content: center;
    }

    .bread-style{
        font-size: 15px;
        color: #000;
        font-weight: 300;
    }

    .item-title-name{
        font-family: Merriweather, Times, Times New Roman, serif !important;
        font-size: 28px !important;
        margin: 0.3em 0 !important;
        text-transform: none !important;
        letter-spacing: 0.03em !important;
    }

    @media (max-width: 500px) {
        .zoomContainer{
            height: 230px !important;
        }
    }

    .cart-row-container {
        overflow: hidden;
        border-bottom: 1px dashed #e3e3e3;
        border-top: 1px dashed #e3e3e3;
        margin-top: -1px;
        width: -webkit-fill-available;
    }

    .cart-row {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        padding: 10px 0px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        -moz-align-items: center;
        align-items: center;
        clear: both;
        overflow: hidden;
    }

    .c-24{
        position: relative;
        float: left;
        width: 100%;
        padding-left: 15px;
        padding-right: 15px;
    }

    .cart-row .product-image-placeholder {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -moz-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        min-width: 0;
        min-height: 0;
        position: relative;
        display: inline-block;
        vertical-align: middle;
        text-align: center;
        background-color: white;
        margin: 0em 0em 0 5em;
        width: 150px;
    }

    .cart-row .product-qty {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -moz-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        min-width: 0;
        min-height: 0;
        position: relative;
        vertical-align: middle;
        display: inline-block;
        margin-left: 0em;
    }

    .cart-row .product-qty .buttons {
        position: relative;
        display: inline-block;
        vertical-align: middle;
    }

    .cart-row .product-qty .count {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        margin-left: 0.3em;
        font-size: 4em;
        color: #555;
    }

    .cart-row .product-qty .buttons .plusminus {
        float: left;
        clear: both;
    }

    .cart-row .product-qty .buttons .plusminus.minus {
        margin-top: 0.3em;
    }

    .cart-row .product-qty .buttons .plusminus {
        float: left;
        clear: both;
    }

    .cart-row .product-info {
        -webkit-box-flex: 1;
        -webkit-flex: 1 1 auto;
        -moz-box-flex: 1;
        -moz-flex: 1 1 auto;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        min-width: 0;
        min-height: 0;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-direction: normal;
        -webkit-box-orient: horizontal;
        -webkit-flex-direction: row;
        -moz-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        -moz-align-items: center;
        align-items: center;
        cursor: pointer;
        margin-left: 5em;
        min-width: 120px;
    }

    .cart-row .product-info .essentials {
        -webkit-box-flex: 1;
        -webkit-flex: 1 1 auto;
        -moz-box-flex: 1;
        -moz-flex: 1 1 auto;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        min-width: 0;
        min-height: 0;
        height: 10em;
    }

    .cart-row .product-info .essentials h2 {
        display: inline-block;
        font-size: 2.7em;
        margin: 0;
        font-family: "Merriweather", Times, "Times New Roman", serif;
        text-transform: none;
        letter-spacing: 0.03em;
    }

    .cart-row .product-info .essentials .subtitle {
        display: block;
    }

    .cart-row .product-info .essentials .actions {
        margin-top: 1em;
        font-size: 0.8em;
    }

    .cart-row .product-info .essentials .actions a {
        border-right: 1px solid #ababab;
        padding: 0em 0.6em 0em 0.4em;
    }

    .cart-row .product-info .message.measurements {
        color: #c10006;
    }

    .cart-row .product-info .message {
        font-size: 1em;
        font-weight: 600;
        color: #c10006;
    }
    .m-1-v {
        margin-top: 1em !important;
        margin-bottom: 1em !important;
    }

    .cart-row-container a {
        font-weight: 400;
    }
    a {
        color: #081e33;
    }

    @media (min-width: 1152px)
    .responsive .cart-row .product-info .more {
        min-width: 280px;
        max-width: 280px;
    }

    .cart-row .product-info .more {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -moz-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        min-width: 0;
        min-height: 0;
        min-width: 296px;
        max-width: 296px;
        margin: 0px 0px 0px 0px;
        display: inline-block;
        vertical-align: middle;
        text-align: center;
        height: 9em;
    }

    .cart-row .product-price {
        width: 120px;
        height: 7.5em;
        max-width: 120px;
        display: inline-block;
        vertical-align: middle;
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -moz-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        min-width: 0;
        min-height: 0;
        font-size: 1.35em;
        color: #333;
        text-align: right;
    }

    .img-row {
        vertical-align: middle;
        border-style: none;
    }

    .cart-row .product-price span {
        position: relative;
        display: inline-block;
        vertical-align: middle;
    }

    .price {
        font-weight: 300;
        font-family: "Merriweather", Times, "Times New Roman", serif;
    }

    .beauty-heading {
        text-align: center;
        display: flex;
        flex-direction: row;
        justify-content: center;
        line-height: 1.5;
        width: 100%;
        color: #555;
        border-bottom: 1px solid #aaa;
        padding: 0.5em 0 0.3em;
        margin: 0 0 1em;
    }

    .measurement-value {
        width: 100px;
        font-size: 1.5em;
        margin-right: 5px;
        padding: 0.3em;
        text-align: center;
    }

    .unit {
        font-size: 1.5em;
    }

</style>
@stop

@section('content')
  <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container-fluid">

            <ol class="breadcrumb bread-style">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="{{ route('bigstore.index') }}">Shopping Cart</a></li>                
                <li class="breadcrumb-item active"><a href="{{ route('bigstore.index') }}">Check-out</a></li>                
            </ol>

        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->

  
  <!-- item view -->
    <div class="item-view pt-1">
        <div class="container-fluid">
      
            <div class="row" style="margin: 0px auto">
                <div class="cart-row-container">
                    <div class="c-24 cart-row">
                        <div class="product-image-placeholder">
                            <img src="https://dyn1.tailorstore.com/MTUwfDE1MHw5MHxmZmZmZmY,/images/catalog/mens-shirt-melide-white-1804-jpg" alt="" class="img-responsive img-row">
                        </div>
                        <div class="product-qty">
                            <div class="buttons">
                                <div class="discreteButton mini symbolonly plusminus plus"><i class="fa fa-plus" aria-hidden="true" style="font-size: 1.8em"></i></div>
                                <div class="discreteButton mini symbolonly plusminus minus"><i class="fa fa-minus" aria-hidden="true" style="font-size: 1.8em"></i></div>
                            </div>
                            <div class="count" id="count_1">2</div>
                        </div>
                        <div class="product-info">
                            <div class="essentials">
                                <h2>
                                    Long Sleeve (MEN/SHIRT/LS)
                                    <span class="el-icon-info-sign"></span>
                                </h2>                           
                                <div class="subtitle">Melide, white</div>
                                <div class="actions">
                                    <a href="#" class="changeItem">Change</a>
                                    <a href="#" class="copyItem">Copy to designer</a>
                                    <a href="https://www.tailorstore.sg/made-to-measure-shirts-for-men/button-down-shirts/white-shirt-with-button-down-classic-collar">Show</a>
                                </div>
                                <div class="messages"></div>
                            </div>
                            <div class="more">
                                <div class="select-measurements">
                                    <button class="btn btn-measure" style="background-color: #081e33;color: white;height: 30px;padding: 0 0.3rem;" data-id="1">Choose measurements / size</button>
                                </div>
                                <p class="message measurements m-1-v">Please specify measurements.</p>
                            </div>
                        </div>

                        <div class="product-price" id="price_1">
                            <span class="price">USD&nbsp;198</span>
                        </div>
                    </div>
                </div>
                <div class="cart-row-container"></div>
                <div class="cart-row-container"></div>
            </div>
        
        </div>
    </div> <!-- /.item specification -->

    <div class="modal" tabindex="-1" role="dialog" id="data_modal" name="data_modal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center; border-bottom: 0;padding: 10px; border-bottom: 1px solid #e3e3e3;">
                    <h4 class="beauty-heading" style="border: 0;padding: 0;margin: 0">Choose measurements / size</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-left: 16px;padding-right: 16px">
                    <div class="cart-row-container" id="measure-body" style="border-top: 0;">                        
                        
                    </div>
                </div>
                
                <div class="modal-footer" style="border-top: 0;">
                     <button class="btn btn-measure" style="background-color: #081e33;color: white;height: 30px;padding: 0 0.3rem;" data-id="1">Save</button>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){

            $('#fixed-menu').hide();

            $('.btn-measure').click(function(){

                $.ajax({
                    url: "{{URL::to('bigstore/get-measure-detail')}}",
                    method: 'GET',
                    data: { 'product_id': 7 },
                    async: false,
                    success: function (data) {
                        $('.content').removeClass('panel-refreshing');

                        $('#measure-body').html('');

                        $.each(data,function(key,value){
                            $('#measure-body').append(
                                '<div class="c-24 cart-row" style="border-bottom: 1px dashed #e3e3e3;">'
                                    +'<div class="product-image-placeholder" style="margin: 0px;width: 35%">'
                                        +'<img src="{{ url('data/product_thumb/images/measurement') }}/'+value.smallfilename+'" style="width: 100px" alt="" class="img-responsive img-row">'
                                        +'<div class="count" style="font-size: 15px;color: #000">'+value.name+'</div>'
                                    +'</div>'
                                    +'<div class="product-qty" style="margin-right: 5%">'
                                        +'<input class="measurement-value" id="inch_value_1" name="inch_value_1" size="6" type="number" value="" min="30" max="200">'
                                        +'<span class="unit unitlength">in</span>'
                                    +'</div>'
                                    +'<div class="product-qty">'
                                        +'<input class="measurement-value" id="cm_value_1" name="cm_value_1" size="6" type="number" value="" min="30" max="200">'
                                        +'<span class="unit unitlength">cm</span>'
                                    +'</div>'
                                +'</div>'
                            );
                        });

                        $('#inch_value_1').focus();
                        
                    },error: function () {
                        $.alert({
                            theme: 'material',
                            title: 'Error Occured',
                            type: 'red',
                            content: 'Error Occured when getting filters'
                        }); 
                    }
                });

                $('.modal').modal('toggle');                
            });
        });
    </script>
@stop