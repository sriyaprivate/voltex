@extends('layouts.master') @section('title','Big Store')

@section('links')
    <!-- slick.css -->
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">

    <!-- hover effects -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/hover-effect-ideas/css/set2.css')}}" />

    <!-- owl.carousel.js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/mtreeJs/mtree.css')}}" />

    <!-- ion.rangeSlider.skinFlat.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.skinFlat.css')}}" />

    <!-- jquery confirm alert -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />

    <!-- toastr notification -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop
<style type="text/css">
    .mb-44{
        margin-top: 1.1rem !important;
        margin-bottom: 0 !important;
    }

    .mb-top-44{
        margin-top: 25px;
        margin-bottom: 25px;
    }

    .promotion{
        padding: 3px 20px 4px 0px !important;
    }

    .promotion img{
        width: 70% !important;
    }

    .mb-3{
        text-decoration: underline;
        font-size: 15px !important;
        font-family: 'Helvetica Neue LT Std' !important;
    }

    .mb-3:hover{
        color: red;
    }

    .img-fluid{
        max-width: 55% !important;
    }

    .btn-measure{
        background-color: crimson;
        color: white;
        height: 25px;
        font-size: 12px !important;
        padding: 0 0.3rem !important;
        margin-top: 10px;
    }



</style>
@section('css')
@stop

@section('content')
    <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container-fluid">            

        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->

  <!-- sign in/up panel -->
    <div class="market big-store pb-4">
        <div class="container-fluid">
          <!-- wrapper -->
          <!-- <div class="inside-wrapper"> -->
        
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pr-4 sidebar">
                    
                    <!-- sidebar navigation -->
                    <div class="side-menu mb-4">
                        <!-- mtree menu -->
                        <ul class="mtree transit collapsed show" id="menu">
                            <li {{ Request::route('category_id') == null? 'class=mtree-active':'' }}>
                                <a href="{{ route('bigstore.index') }}">Gents - All Categories</a>
                            </li>

                            @foreach($categories as $node)
                                {!! renderNode($node) !!}
                            @endforeach
                        </ul><!-- /.mtree menu -->
                    </div> <!-- /.sidebar navigation -->

                    
                </div> <!-- /.col-lg-12 col-md-12 col-sm-12 -->

                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pl-0 right position-relative">
                    <div class="row grid-view">

                        @if(count($products) > 0)
                            @foreach($products as $product)
                                <div class="col-md-3 col-sm-3 col-xs-12">                
                                    <div class="card text-center mb-44 clearfix">
                                        <div class="">
                                            <button class="btn btn-measure add-to-cart" style="" data-product="{{$product->id}}">Add to Cart</button>
                                            <!-- <img class="img-fluid" src="{{asset('assets/images/icons/cart-white.png')}}"> -->
                                        </div>                                   

                                        <div class="row">
                                            <div class="col-12 mb-top-44">
                                                @if(isset($product->images[0]->image))
                                                    <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.$product->images[0]->image) }}" alt="{{ $product->name?:'-' }}">
                                                @else
                                                    <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="{{ $product->name?:'-' }}">
                                                @endif                                            
                                            </div>
                                            <div class="col-12">
                                                <div class="details arrow_box_top top">
                                                    <h5 class="mb-3"><a href="{{ url('bigstore/item') }}/{{$product->id}}">{{$product->display_name}}</a></h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                
                            <div class="text-center mb-4 text-secondary">
                                <div class="h5">Sorry!, Your favorite stores will be available soon.</div>
                            </div>
                        </div>
                        @endif                    
                    </div>                
                </div>

                <!-- </div> --> <!-- /.wrapper -->

            </div> <!-- /.container -->
        </div> <!-- /.sign in/up panel -->
    </div>
@stop

@section('js')
    <!-- slick.js -->
    <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>

    <!-- owl.carousel.js -->
    <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <script src="{{asset('assets/libraries/mtreeJs/jquery.velocity.min.js')}}"></script>
    <script src="{{asset('assets/libraries/mtreeJs/mtree.js')}}"></script>

    <!-- ion.rangeSlider-2.2.0 -->
    <script src="{{asset('assets/libraries/ion.rangeSlider-2.2.0/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>

    <!-- jquery confirm alert -->
    <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>

    <!-- toastr notification -->
    <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
@include('cart::includes.cart-js')
    <script>
        $(document).ready(function() {
            
            $('.add-to-cart').click(function(){
                console.log($(this).data('product'));
            });

            $('#fixed-menu').click(function(){
                $('#fixed-cart-panel').show();
            });

            $('#order-list-hide').click(function(){
                console.log("skldfnlsdnf");
                $('#fixed-cart-panel').hide();
            });
        });
    
    </script>
@stop