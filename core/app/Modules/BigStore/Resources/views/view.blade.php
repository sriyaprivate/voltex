@extends('layouts.master') @section('title','Item view')

@section('links')
  <!-- slick.css -->
  <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
  <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">
  
  <!-- owl.carousel.js -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

  <!-- PgwSlider-master -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/PgwSlider-master/pgwslider.min.css')}}" />

  <!-- jquery.rateyo.v2.3.2 -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/jquery.rateyo.v2.3.2/jquery.rateyo.min.css')}}" />

  <link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style type="text/css">
    
    .mtb1{
        font-family: "Lato", sans-serif;
        font-size: 14px;
        font-weight: 300;
        letter-spacing: 0.15em;
        text-transform: uppercase;
        text-decoration: none;
        text-align: center;
        padding: 1em 1.15em 1em 1.3em;
        border: 2px solid transparent;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border-radius: 0;
        color: white;
        cursor: pointer;
        /* height: 40px; */
        display: -webkit-inline-box;
        display: -webkit-inline-flex;
        display: -moz-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-direction: normal;
        -webkit-box-orient: horizontal;
        -webkit-flex-direction: row;
        -moz-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: baseline;
        -ms-flex-align: baseline;
        -webkit-align-items: baseline;
        -moz-align-items: baseline;
        align-items: baseline;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        -moz-justify-content: center;
        justify-content: center;
    }

    .bread-style{
        font-size: 15px;
        color: #000;
        font-weight: 300;
    }

    .item-title-name{
        font-family: Merriweather, Times, Times New Roman, serif !important;
        font-size: 28px !important;
        margin: 0.3em 0 !important;
        text-transform: none !important;
        letter-spacing: 0.03em !important;
    }

    @media (max-width: 500px) {
        .zoomContainer{
            height: 230px !important;
        }
    }
</style>
@stop

@section('content')
  <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container-fluid">

            <ol class="breadcrumb bread-style">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                @if(count($product_details) > 0)
                    @if(count($product_details->product_category->category->type->name) 
                      && !empty($product_details->product_category->category->type->name) 
                      && isset($product_details->product_category->category->type->name))
                        <li class="breadcrumb-item"><a href="{{ route('bigstore.store', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'slug' =>  strtolower($product_details->product_category->category->type->name?:'-')]) }}">{{ $product_details->product_category->category->type->name?:'-' }}</a></li>
                    @endif

                    @if(count($product_details->display_name) 
                      && !empty($product_details->display_name) 
                      && isset($product_details->display_name))
                    <li class="breadcrumb-item active">{{ $product_details->display_name?:'-' }}</li>
                    @endif
                @endif
            </ol>

        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->

  
  <!-- item view -->
    <div class="item-view pt-1">
        <div class="container-fluid">
      
        @if(count($product_details) > 0 && !empty(count($product_details)))
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-3">
                    
                </div>
          
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-3">
                    
                </div>          

                <div class="col-md-6 col-sm-12 col-xs-12 product-viewer clearfix">
                    <div id="product-image-carousel-container">
                    @if(count($product_details->images))
                        <ul id="product-carousel" class="celastislide-list">
                        <?php $count = 0; ?>
                        @foreach($product_details->images as $image)
                            @if($count == 1)
                                <li class="active-slide">
                                    <a data-rel="prettyPhoto[product]" href="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" class="product-gallery-item">
                                        <img src="{{ url('data/product_thumb/images/product/'.$image->image) }}" alt="{{ $image->image }}" class="item-bordered">
                                    </a>
                                </li>
                            @else
                            <li>
                                <a data-rel="prettyPhoto[product]" href="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" class="product-gallery-item">
                                    <img src="{{ url('data/product_thumb/images/product/'.$image->image) }}" alt="{{ $image->image }}" class="item-bordered">
                                </a>
                            </li>
                            @endif
                            
                            <?php $count++; ?>
                        @endforeach
                        </ul><!-- End product-carousel -->
                    @endif
                    </div>

                    <div id="product-image-container">
                    @if(isset($product_details->discount_rate) && !empty($product_details->discount_rate))
                        <div class="promotion-right r--0">
                            <span class="dis ng-binding">{{ number_format($product_details->discount_rate?: '0') }}%</span>
                            <span class="save">OFF</span>
                        </div>
                    @endif
                        <figure>                
                        @if(count($product_details->images))
                            <img src="{{ url('data/product_thumb/images/product/'.$product_details->images[0]->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$product_details->images[0]->image) }}" alt="sdf" id="product-image">
                        @else
                            <img src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="default product image." id="product-image">
                        @endif                
                        </figure>
                    </div><!-- product-image-container -->
                </div><!-- End .col-md-6 -->

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 position-relative">
                    <!-- panel refresh -->
                    <div name="panel-refresh"></div>

                    <h6 class="item-name h6 item-title-name" style="color: #000 !important">
                        <strong>
                            {{ $product_details->display_name?:'-' }} 
                            <span style="opacity: 0.5;">({{ $product_details->code?:'-' }})</span>
                        </strong>
                    </h6>

                    <p style="line-height: 1.7;font-size: 14px;margin-top: 1.5rem">
                        {!! $product_details->description !!}
                    </p>

                    <div class="item-title-name" style="color: #000 !important;margin-top: 1.5rem !important">
                        <p>
                            <span>RS <strong style="color: #ff0000">{{number_format($product_details->selling_price,2)}}</strong></span>
                        </p>
                    </div>

                    <div>
                        <form action="{{ route('cart.add', ['product_id' => isValid($product_details->id)]) }}" method="get" id="form_{{ isValid($product_details->id) }}"  class="product_form">
                            
                            <input type="hidden" name="prev_url" value="{{ Request::getQueryString() }}">
                            <button type="submit" name="action" value="cart" title="Click to buy item" class="mtb1 btn btn-measure add-to-cart" style="background-color: #081e33;color: #fff !important;" data-product="{{$product_details->id}}">
                                <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 25px;padding-right: 10px;"></i>
                                <span>ADD TO CART</span>
                            </button>
                        </form>
                    </div>

                </div>
            </div>

        @else
            <h2 class="text-center text-secoundary not-found">Product not found!.</h2>
        @endif

        </div>
    </div> <!-- /.item specification -->

@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>
  <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>
  <script src="{{asset('assets/libraries/PgwSlider-master/pgwslider.min.js')}}"></script>
  <script src="{{asset('assets/libraries/jquery.rateyo.v2.3.2/jquery.rateyo.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/modernizr.custom.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.prettyPhoto.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.elastislide.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.elevateZoom.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>
  <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>
  <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $(".rateYo").rateYo({
                rating: 3.6,
                starWidth: "20px"
            });

            $('.pgwSlider').pgwSlider({
                listPosition: 'left',
                verticalCentering: 'true',
                adaptiveHeight: 'false',
                autoSlide: 'false',
                transitionEffect: 'Sliding',
                intervalDuration: '5000'
            });

            $('.promos').slick({
                infinite: true, dots: false, accessibility: true, speed: 1000, slidesToShow: 4, slidesToScroll: 1, autoplay: false, arrows: true, asNavFor: null,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },{
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },{
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }]
            });

            
        });
    </script>
@stop