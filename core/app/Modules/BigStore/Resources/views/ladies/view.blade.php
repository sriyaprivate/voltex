@extends('layouts.master') @section('title','Big Store View')

@section('links')
  <!-- slick.css -->
  <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
  <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">
  
  <!-- owl.carousel.js -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

  <!-- PgwSlider-master -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/PgwSlider-master/pgwslider.min.css')}}" />

  <!-- jquery.rateyo.v2.3.2 -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/jquery.rateyo.v2.3.2/jquery.rateyo.min.css')}}" />

  <link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style type="text/css">
  @media (max-width: 500px) {
    .zoomContainer{
      height: 230px !important;
    }
  }
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container-fluid">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        @if(count($product_details) > 0)
          @if(count($product_details->product_category->category->type->name) 
              && !empty($product_details->product_category->category->type->name) 
              && isset($product_details->product_category->category->type->name))
            <li class="breadcrumb-item"><a href="{{ route('bigstore.index') }}">{{ $product_details->product_category->category->type->name?:'-' }}</a></li>
          @endif

          @if(count($product_details->product_category->category->display_name) 
              && !empty($product_details->product_category->category->display_name) 
              && isset($product_details->product_category->category->display_name))
            <li class="breadcrumb-item"><a href="{{ url('bigstore/'.str_slug($product_details->product_category->category->display_name).'/'.$product_details->product_category->category->id) }}">{{ $product_details->product_category->category->display_name?:'-' }}</a></li>
          @endif

          @if(count($product_details->display_name) 
              && !empty($product_details->display_name) 
              && isset($product_details->display_name))
            <li class="breadcrumb-item active"><a href="#">{{ $product_details->display_name?:'-' }}</a></li>
          @endif
        @endif
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->

  
  <!-- item view -->
  <div class="item-view pt-1">
    <div class="container-fluid">
      
      @if(empty($product_details->availableQty) || $product_details->availableQty <= 0)
        <div class="card stock-available mb-4 d-none">
          <div class="card-body p-2">
            <div class="row text-uppercase">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="p-2 mb-0">this item is out of stock</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <button href="#" class="btn btn-sm btn-primary float-right mb-0 pt-2 pb-2">notify me when stock is available</button>
              </div>
            </div>
          </div>
        </div>
      @else
        <div class="mb-4"></div>
      @endif

      @if(count($product_details) > 0 && !empty(count($product_details)))
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-3">
            <h6 class="item-name h6">{{ $product_details->display_name?:'-' }} <span style="opacity: 0.5;">({{ $product_details->code?:'-' }})</span></h6>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-3">
            <button type="button" class="btn btn-sm btn-outline-dark float-right mb-0 pt-2 pb-2 btn-share"><i class="fa fa-facebook-official mr-2" aria-hidden="true"></i> Share</button>
          </div>          

          <div class="col-md-6 col-sm-12 col-xs-12 product-viewer clearfix">
            <div id="product-image-carousel-container">
              @if(count($product_details->images))
              <ul id="product-carousel" class="celastislide-list">
                <?php $count = 0; ?>
                @foreach($product_details->images as $image)
                  @if($count == 1)
                  <li class="active-slide">
                    <a data-rel="prettyPhoto[product]" href="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" class="product-gallery-item">
                      <img src="{{ url('data/product_thumb/images/product/'.$image->image) }}" alt="{{ $image->image }}" class="item-bordered">
                    </a>
                  </li>
                  @else
                  <li>
                    <a data-rel="prettyPhoto[product]" href="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" class="product-gallery-item">
                      <img src="{{ url('data/product_thumb/images/product/'.$image->image) }}" alt="{{ $image->image }}" class="item-bordered">
                    </a>
                  </li>
                  @endif
                  <?php $count++; ?>
                @endforeach
              </ul><!-- End product-carousel -->
              @endif
            </div>

            <div id="product-image-container">
              @if(isset($product_details->discount_rate) && !empty($product_details->discount_rate))
                <div class="promotion-right r--0">
                  <span class="dis ng-binding">{{ number_format($product_details->discount_rate?: '0') }}%</span>
                  <span class="save">OFF</span>
                </div>
              @endif
              <figure>                
                @if(count($product_details->images))
                  <img src="{{ url('data/product_thumb/images/product/'.$product_details->images[0]->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$product_details->images[0]->image) }}" alt="sdf" id="product-image">
                @else
                  <img src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="default product image." id="product-image">
                @endif                
              </figure>
            </div><!-- product-image-container -->
          </div><!-- End .col-md-6 -->

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 position-relative">
            <!-- panel refresh -->
            <div name="panel-refresh"></div>

            <div class="d-inline-block w-100">
              <div class="w-100 float-left text-left mt">
                <p class="m-0 price text-25">
                  @if(isset($product_details->discounted_amount) && !empty($product_details->discounted_amount))
                    <small><del>{{ CURRENCY_SIGN }} {{ number_format($product_details->price?:0, 2) }}</del></small>
                    {{ CURRENCY_SIGN }} {{ number_format($product_details->discounted_amount?:0, 2) }}
                  @else
                    <span class="text-left">{{ CURRENCY_SIGN }} {{ number_format($product_details->price?:0, 2) }}</span>
                  @endif
                </p>
              </div>
              @if(isset($product_details->balance) && !empty($product_details->balance))
                <div class="w-50 float-right text-uppercase text-left d-none">
                  <p class="save-price"><small class="text-uppercase">save</small> {{ CURRENCY_SIGN }} {{ number_format($product_details->balance, 2) }}</p>
                </div>
              @endif
            </div>
            <hr>
            <div class="row">
              <!-- <div class="col-lg-4 col-sm-12">
                <form>
                  <div class="form-group">
                    <label class="text-uppercase font-weight-bold" for="specification1">Specification 01</label>
                    <select class="form-control custom-select form-control-sm mb-0" id="specification1">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="text-uppercase font-weight-bold" for="specification2">Specification 02</label>
                    <select class="form-control custom-select form-control-sm mb-0" id="specification2">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="text-uppercase font-weight-bold" for="specification3">Specification 03</label>
                    <select class="form-control custom-select form-control-sm mb-0" id="specification3">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
                </form>
              </div> -->
              <!-- <div class="col-lg-8 col-sm-12"> -->
              <div class="col-lg-12 col-sm-12">
                <h6 class="h6 text-uppercase mb-2 font-weight-bold">delivery options</h6>
                <div class="card delivery-methods p-3">
                  <div class="card-body p-0 pb-0">
                    <table class="table mb-0 td-p-5">
                      <tbody>
                        @if(count($delivery_types) > 0)
                          @foreach($delivery_types as $key => $type)
                            @if(count($type) > 0 && count($delivery_options) > 0)
                              @if(count($delivery_options) && isset($type->id) && in_array($type->id, $delivery_options->toArray()))
                                <tr>
                                  <td style="width: 30%;" class="font-weight-bold"><i class="icon-radius fa {{ isvalid($type->icon) }} mr-2" aria-hidden="true"></i>{{ isValid($type->name)?:'-' }}</td>
                                  <td style="width: 5%;">:</td>
                                  <td style="width: 60%;"><span class="fa fa-check text-success pr-1"></span> <strong>{{ isValid($type->description)?:'-' }}</strong></td>
                                </tr>
                              @else
                                <tr>
                                  <td style="width: 30%;" class="font-weight-bold"><i class="icon-radius fa {{ isvalid($type->icon) }} mr-2" aria-hidden="true"></i>{{ isValid($type->name)?:'-' }}</td>
                                  <td style="width: 5%;">:</td>
                                  <td style="width: 60%;"><span class="fa fa-times text-danger pr-1"></span> <strong>Not available</strong></td>
                                </tr>
                              @endif
                            @endif                            
                          @endforeach
                          @if(count($delivery_options) == 0)
                            <h5 class="text-center text-secondary text-uppercase">No delivery types</h5>
                          @endif
                        @else
                          <h5 class="text-center text-secondary text-uppercase">No delivery types</h5>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              @if(count($product_details->warrenty) && isValid($product_details->warrenty))
                <div class="col-lg-12 col-sm-12">
                  <h6 class="h6 text-uppercase mt-3 mb-2 font-weight-bold">Other terms</h6>
                  <div class="card delivery-methods p-3">
                    <div class="card-body p-0 pb-0">
                      <table class="table mb-2 td-p-5">
                        <tbody>
                          <tr>
                            <td style="width: 30%;" class="font-weight-bold"><i class="icon-radius fa fa-lock mr-2" aria-hidden="true"></i>Warrenty</td>
                            <td style="width: 5%;">:</td>
                            <td style="width: 60%;">
                              @if(count($product_details->warrenty) && isValid($product_details->warrenty))
                                <span class="fa fa-check text-success pr-1"></span><strong> {{ showWarranty(isValid($product_details)) }}</strong>
                              @else
                                <span class="fa fa-times text-danger pr-1"></span><strong> Not available</strong>
                              @endif
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              @endif
            </div>

            @if(count($product_details) > 0 && count($product_details->active_bank_promotions) > 0)
              <div class="item-promotion mt-4 mb-4 bordered-top-bottom">
                <div class="promos">
                  @foreach($product_details->active_bank_promotions as $promotion)
                    @if(count($promotion) > 0)
                      <div class="promo-item text-center">
                        @if(count($promotion) > 0 && !empty($promotion->card_image))
                          <img class="mx-auto d-block" src="{{ url('data/bank_card/images/cards/'.$promotion->card_image) }}">
                        @else
                          <img class="mx-auto d-block" src="{{ url('data/bank_card/images/cards/default.png') }}">
                        @endif
                        <!-- <p class="mb-0 text-uppercase">12M installment</p> -->
                        <p class="mb-0 text-uppercase">{{ $promotion->card_name?:'-' }}</p>
                        @if(count($product_details) > 0)
                          <p class="mb-1">{{ CURRENCY_SIGN }} {{ calculatePromotionPrice($product_details, $promotion) }}</p>
                        @else
                          -
                        @endif
                      </div>
                    @endif
                  @endforeach
                </div>
              </div>
            @endif

            <!-- <div class="item-promotion mt-2 mb-4">
              <div class="promos">
                <div class="promo-item text-center">
                  <img class="mx-auto d-block" src="{{asset('assets/images/cards/visa.png')}}">
                  <p class="mb-0 text-uppercase">credit card</p>
                  <p class="mb-1">Rs 2,500.00</p>
                </div>
                <div class="promo-item text-center">
                  <img class="mx-auto d-block" src="{{asset('assets/images/cards/mastercard.png')}}">
                  <p class="mb-0 text-uppercase">12M installment</p>
                  <p class="mb-1">Rs 2,500.00</p>
                </div>
                <div class="promo-item text-center">
                  <img class="mx-auto d-block" src="{{asset('assets/images/cards/visa.png')}}">
                  <p class="mb-0 text-uppercase">credit card</p>
                  <p class="mb-1">Rs 2,500.00</p>
                </div>
                <div class="promo-item text-center">
                  <img class="mx-auto d-block" src="{{asset('assets/images/cards/visa.png')}}">
                  <p class="mb-0 text-uppercase">credit card</p>
                  <p class="mb-1">Rs 2,500.00</p>
                </div>
                <div class="promo-item text-center">
                  <img class="mx-auto d-block" src="{{asset('assets/images/cards/visa.png')}}">
                  <p class="mb-0 text-uppercase">credit card</p>
                  <p class="mb-1">Rs 2,500.00</p>
                </div>
              </div>
            </div> -->

            @if(!empty($product_details->availableQty) && $product_details->availableQty > 0)
              <form action="{{ route('cart.add', ['product_id' => isValid($product_details->id)]) }}" method="get" class="product_form mt-4">
                <input type="hidden" name="prev_url" value="{{ Request::getQueryString() }}">
                <div class="cd-customization mt-2">
                  <div class="qty">
                    <input type="number" name="qty" min="1" max="{{ $product_details->availableQty }}" class="form-control form-control-sm" value="1">
                  </div>
                  <div class="add-cart">
                    <button type="submit" name="action" value="buy" class="btn btn-sm btn-secondary btn-block"><span>Buy Now</span></button>
                  </div>
                  <button type="submit" name="action" value="cart" class="btn btn-link cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                  @if(count($loggedInUser) != 0 && isValid($product_details->hasInWishlist))
                      <button class="btn text-secondary btn-link wishlist" title="Item Already In Wishlist" disabled><i class="fa fa-heart" aria-hidden="true"></i></button>
                  @else
                      <a href="{{ url('cart/add') }}/{{ isValid($product_details->id) }}?action=wishlist"  title="click to add wishlist." class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                  @endif
                </div>
              </form>
            @else
                <div class="text-uppercase message-box mt-4">
                  Out of stock
                </div>
            @endif
          </div>
        </div>

        <ul class="nav nav-tabs mt-4" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="desc-tab" data-toggle="tab" href="#desc" role="tab" aria-controls="desc" aria-expanded="true">
              <h6 class="text-uppercase">Product Description</h6>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="spec-tab" data-toggle="tab" href="#spec" role="tab" aria-controls="spec">
              <h6 class="text-uppercase">Item Specification</h6>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews">
              <h6 class="text-uppercase">Reviews</h6>
            </a>
          </li> -->
        </ul>
        <div class="tab-content mb-5" id="myTabContent">
          <div class="tab-pane fade show active" id="desc" role="tabpanel" aria-labelledby="desc-tab">
            @if(isset($product_details->description) && !empty($product_details->description))
              {!! $product_details->description?:'-' !!}
            @else
              <div class="text-center mb-4 text-secondary pt-4">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <tr>     
                      <td colspan="2" class="bg-light text-center h6">Product description not found!</td>
                    </tr>
                  </tbod>
                </table>
              </div>
            @endif
          </div>
          <div class="tab-pane fade pt-4 pb-2" id="spec" role="tabpanel" aria-labelledby="spec-tab">
            <table class="table table-bordered" border="0" cellspacing="0" cellpadding="0" width="100%">
              <tbody>
                @if(count($product_details) > 0 && count($product_details->specification_title) > 0)
                  <tr>     
                    <td colspan="2">
                      <div>Model Name - {{ count($product_details) > 0 && isset($product_details->display_name)? $product_details->display_name :'-' }}</div>
                    </td>   
                  </tr>
                  @foreach($product_details->specification_title as $title)
                    <tr>     
                      <td colspan="2" class="bg-light">
                        <strong>
                          @if(isset($title->title) && !empty($title->title))
                            {{ $title->title?:'-' }}
                          @else
                            Unknown
                          @endif
                        </strong>
                      </td>   
                    </tr>
                    @if(count($title->specification) > 0)
                      @foreach($title->specification as $spec)
                        <tr>
                          <td width="30%" class="pl-4">{{ $spec->specification }}</td>
                          <td>{{ $spec->value }}</td>
                        </tr>
                      @endforeach
                    @else
                    <tr>
                      <td width="30%" class="pl-4">-</td>
                      <td>-</td>
                    </tr>
                    @endif
                  @endforeach
                @else
                  <tr>     
                    <td colspan="2" class="bg-light text-center h6  text-secondary">Product specification not found!</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
          <div class="pl-5 pr-5 tab-pane fade reviews" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
            <form>
              <div class="row">
                <div class="col-md-12 mb-2">
                  <label class="text-uppercase font-weight-bold">Write a Review</label>
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="p-0 rateYo"></div>
                </div>
                <div class="col">
                  <button type="submit" class="btn btn-primary btn-sm pt-2 pb-2 float-right">submit</button>
                </div>
              </div>
            </form>

            <hr>

            <div class="row">
              <div class="col-md-12">
                <ul id="comments-list" class="comments-list">
                  <li>
                    <div class="comment-main-level">
                      <!-- Avatar -->
                      <div class="comment-avatar"><img src="https://randomuser.me/api/portraits/men/78.jpg" alt=""></div>
                      <!-- Contenedor del Comentario -->
                      <div class="comment-box">
                        <div class="comment-head">
                          <h6 class="comment-name by-author"><a href="#">Agustin Ortiz</a></h6>
                          <span>hace 20 minutos</span>
                          <div class="p-0 float-right rateYo"></div>
                        </div>
                        <div class="comment-content">
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                        </div>
                      </div>
                    </div>
                  </li>

                  <li>
                    <div class="comment-main-level">
                      <!-- Avatar -->
                      <div class="comment-avatar"><img src="https://randomuser.me/api/portraits/men/78.jpg" alt=""></div>
                      <!-- Contenedor del Comentario -->
                      <div class="comment-box">
                        <div class="comment-head">
                          <h6 class="comment-name"><a href="#">Lorena Rojero</a></h6>
                          <span>hace 10 minutos</span>
                          <div class="p-0 float-right rateYo"></div>
                          <!-- <i class="fa fa-heart"></i> -->
                        </div>
                        <div class="comment-content">
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

          </div>
        </div>
      @else
        <h2 class="text-center text-secoundary not-found">Product not found!.</h2>
      @endif

    </div>
  </div> <!-- /.item specification -->


  <!-- buy latest section -->
  <div class="related-items pb-5">
    <div class="container-fluid">

      <!-- heading -->
      <div class="header">
        <p class="h6 font-weight-bold text-uppercase">Related Items</p>
        <hr class="line-rose-left">
        <hr class="line-normal">
      </div> <!-- /heading -->

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="related-item-slider owl-carousel">
            @if(count($related_products) > 0)
              @foreach($related_products as $related_product)
                @if(count($related_product) > 0)
                  <div class="single-item">
                  <a href="{{ route('bigstore.item.view', ['item_name' => str_slug($related_product->display_name?:''), 'item_id' => $related_product->id?:'']) }}">
                      <div class="card">
                        @if(count($related_product->images) > 0 && isset($related_product->images[0]->image))
                          <img class="card-img-top" src="{{ url('data/product_thumb/images/product/'.$related_product->images[0]->image) }}" alt="{{ isValid($related_product->display_name)?:'image' }}">
                        @else
                          <img class="card-img-top" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="default product image">
                        @endif
                        <div class="card-body">
                          <h6 class="card-title text-uppercase">{{ $related_product->display_name?: $related_product->name?: '-' }}</h6>
                          @if(!empty($related_product->discounted_amount) && !empty($related_product->discount_rate))
                            <p class="card-text m-0"><del>{{ CURRENCY_SIGN }} {{ number_format($related_product->price?:0, 2) }}</del></p>
                            @if(!empty($related_product->price))
                              <p class="card-text m-0  price">{{ CURRENCY_SIGN }} {{ number_format($related_product->discounted_amount?:0, 2) }}</p>
                            @endif
                          @else
                            @if(!empty($related_product->price))
                              <p class="card-text m-0  price">{{ CURRENCY_SIGN }} {{ number_format($related_product->price?:0, 2) }}</p>
                            @endif
                          @endif
                        </div>
                      </div>
                    </a>
                  </div>
                @endif
              @endforeach
            @else
              
            @endif
          </div>
        </div>
      </div>

    </div> <!-- /container -->
  </div> <!-- /buy latest section -->
@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>

  <!-- owl.carousel.js -->
  <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>

  <!-- PgwSlider-master -->
  <script src="{{asset('assets/libraries/PgwSlider-master/pgwslider.min.js')}}"></script>

  <!-- jquery.rateyo.v2.3.2 -->
  <script src="{{asset('assets/libraries/jquery.rateyo.v2.3.2/jquery.rateyo.min.js')}}"></script>

  <script src="{{asset('assets/js/vendor/modernizr.custom.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.prettyPhoto.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.elastislide.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.elevateZoom.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>

  <!-- toastr notification -->
  <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

  <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $(".rateYo").rateYo({
                rating: 3.6,
                starWidth: "20px"
            });

            $('.pgwSlider').pgwSlider({
                listPosition: 'left',
                verticalCentering: 'true',
                adaptiveHeight: 'false',
                autoSlide: 'false',
                transitionEffect: 'Sliding',
                intervalDuration: '5000'
            });

            $('.promos').slick({
                infinite: true, dots: false, accessibility: true, speed: 1000, slidesToShow: 4, slidesToScroll: 1, autoplay: false, arrows: true, asNavFor: null,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },{
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },{
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }]
            });

            $('.related-item-slider').owlCarousel({
                // loop:true,
                margin:30,
                navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
                dotsSpeed: 2000, 
                navSpeed: 2000, 
                animateOut: 'slideOutLeft', 
                animateIn: 'flipInY', 
                nav:true,
                // items: 10,
                autoplay: true, 
                // autoplayTimeout: 1000, 
                // smartSpeed: 5000, 
                autoplaySpeed:1000, 
                dots: false, 
                responsiveClass:true,
                responsive:{ 0:{items:1,} , 300:{items:1,} , 479:{items:2,} , 600:{items:3,} , 991:{items:4,} ,1000:{items:4} }
            });
        });

        //qty validation
        var lastCurrentQty = null;
        $('input[type=number]').bind('change keyup', function(e){
            //validate qty
            var pattern = /^([1-9]*[1-9][0-9]*)$/;
            var maxQty  = $(this).prop('max');

            if($(this).val().length > 0 && pattern.test($(this).val()) && parseInt($(this).val()) <= parseInt(maxQty)){
                $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', false);
                lastCurrentQty = $(this).val();
            }else{
                //show notification
                replaceQty = 1;

                if(lastCurrentQty !== null){ 
                    if($(this).val().length == 0){
                        replaceQty = '';
                        $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', true);
                    }else{
                        replaceQty = lastCurrentQty;
                        notification("toast-bottom-right", "warning", "Limited stock available!.", "5000");
                        $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', false);
                    } 
                }else{ 
                    if($(this).val().length == 0){
                        replaceQty = '';
                        $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', true);
                    }else{
                        replaceQty = replaceQty;
                        $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', false);
                        //notification("toast-bottom-right", "warning", "Limited stock available!.", "5000");
                    }
                }

                $(this).val(replaceQty);
            }
        });
        
        //panel refresh
        $('.product_form').on('submit', function(e){
            addPanelRefresh();
        });
    </script>
@stop