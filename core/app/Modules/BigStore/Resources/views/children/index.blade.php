@extends('layouts.master') @section('title','Big Store')

@section('links')
    <!-- slick.css -->
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">

    <!-- hover effects -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/hover-effect-ideas/css/set2.css')}}" />

    <!-- owl.carousel.js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/mtreeJs/mtree.css')}}" />

    <!-- ion.rangeSlider.skinFlat.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.skinFlat.css')}}" />

    <!-- jquery confirm alert -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />

    <!-- toastr notification -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop
<style type="text/css">
    .mb-44{
        margin-top: 1.1rem !important;
        margin-bottom: 0 !important;
    }

    .mb-top-44{
        margin-top: 25px;
        margin-bottom: 25px;
    }

    .promotion{
        padding: 3px 20px 4px 0px !important;
    }

    .promotion img{
        width: 70% !important;
    }
</style>
@section('css')
@stop

@section('content')
    <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container-fluid">            

        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->

  <!-- sign in/up panel -->
  <div class="market big-store pb-4">
    <div class="container-fluid">
      <!-- wrapper -->
      <!-- <div class="inside-wrapper"> -->
    
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pr-4 sidebar">
                
                <!-- sidebar navigation -->
                <div class="side-menu mb-4">
                    <!-- mtree menu -->
                    <ul class="mtree transit collapsed show" id="menu">
                        <li {{ Request::route('category_id') == null? 'class=mtree-active':'' }}>
                            <a href="{{ route('bigstore.index') }}">Children - All Categories</a>
                        </li>
                        <li class="mtree-node mtree-closed" style="opacity: 1; transform: translateY(0px);">
                            <a href="http://localhost/pettahtailor/bigstore/batteries/1" style="cursor: pointer;">Shirts</a>
                        </li>
                        <li class="mtree-node mtree-closed" style="opacity: 1; transform: translateY(0px);">
                            <a href="http://localhost/pettahtailor/bigstore/batteries/1" style="cursor: pointer;">Trouser</a>
                        </li>
                        <li class="mtree-node mtree-closed" style="opacity: 1; transform: translateY(0px);">
                            <a href="http://localhost/pettahtailor/bigstore/batteries/1" style="cursor: pointer;">Jacket</a>
                        </li>
                        <li class="mtree-node mtree-closed" style="opacity: 1; transform: translateY(0px);">
                            <a href="http://localhost/pettahtailor/bigstore/batteries/1" style="cursor: pointer;">Waist Coat</a>
                        </li>
                        <li class="mtree-node mtree-closed" style="opacity: 1; transform: translateY(0px);">
                            <a href="http://localhost/pettahtailor/bigstore/batteries/1" style="cursor: pointer;">Skirt</a>
                        </li>
                        <li class="mtree-node mtree-closed" style="opacity: 1; transform: translateY(0px);">
                            <a href="http://localhost/pettahtailor/bigstore/batteries/1" style="cursor: pointer;">Blouse</a>
                        </li>
                        <li class="mtree-node mtree-closed" style="opacity: 1; transform: translateY(0px);">
                            <a href="http://localhost/pettahtailor/bigstore/batteries/1" style="cursor: pointer;">Frock</a>
                        </li>
                        <li class="mtree-node mtree-closed" style="opacity: 1; transform: translateY(0px);">
                            <a href="http://localhost/pettahtailor/bigstore/batteries/1" style="cursor: pointer;">Accessories</a>
                        </li>
                    </ul><!-- /.mtree menu -->
                </div> <!-- /.sidebar navigation -->
z
                
            </div> <!-- /.col-lg-12 col-md-12 col-sm-12 -->

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pl-0 right position-relative">
                <div class="row grid-view">
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/5.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">Long Sleeve</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/6.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">Short Sleeve</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/7.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">Long Sleeve Roll Up</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/1.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">Dress Shirt</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/2.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">National Suits</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/3.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">Kurtha</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/7.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">Safari</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/2.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">Uniform Shirt</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">                
                        <div class="card text-center mb-44 clearfix">
                            <div class="promotion">
                                <img class="img-fluid animate-me animated bounceIn" src="{{asset('assets/images/icons/cart-white.png')}}">
                            </div>                            
                            <div class="row">
                                <div class="col-12 mb-top-44">
                                    <img class="img-fluid" src="{{asset('assets/images/dress-category/men-shirt/6.png')}}">
                                </div>
                                <div class="col-12">
                                    <div class="details arrow_box_top top">
                                        <h5 class="mb-3">Special Design</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>

            <!-- </div> --> <!-- /.wrapper -->

        </div> <!-- /.container -->
    </div> <!-- /.sign in/up panel -->
@stop

@section('js')
    <!-- slick.js -->
    <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>

    <!-- owl.carousel.js -->
    <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <script src="{{asset('assets/libraries/mtreeJs/jquery.velocity.min.js')}}"></script>
    <script src="{{asset('assets/libraries/mtreeJs/mtree.js')}}"></script>

    <!-- ion.rangeSlider-2.2.0 -->
    <script src="{{asset('assets/libraries/ion.rangeSlider-2.2.0/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>

    <!-- jquery confirm alert -->
    <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>

    <!-- toastr notification -->
    <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
@include('cart::includes.cart-js')
    <script>
        $(document).ready(function() {
            
        });
    
    </script>
@stop