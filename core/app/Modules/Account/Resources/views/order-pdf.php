<style type="text/css">
	@page {
	   size: 7in 8in;
	   margin: 1in 16mm 27mm 16mm;
	}

	table{
		font-size: 8px;
	}
</style>
@if(isValid($order))
	<table style="border-collapse: collapse;">
		<tr>
			<td rowspan="4" style="border-bottom: 1px solid #000;vertical-align: bottom" width="64%">
				<br/><br/><br/><br/>Supplier: {{ $grn->supplier_name }} | {{$grn->supplier_company}}</td>
			<td rowspan="4" width="10%" style="vertical-align: top;border-bottom: 1px solid #000;"><img src="data:image/png;base64,{!!base64_encode($qr)!!}"></td>
			<td width="12%">Order No #</td>
			<td width="2%">:</td>
			<td width="12%">
				@if(count($order) > 0)
                    {{ $order->order_no }}
                @else
                    -
                @endif
			</td>
		</tr>
		<tr>
			<td width="12%">Order Date</td>
			<td width="2%">:</td>
			<td width="12%">
				@if(count($order) > 0 && isValid($order->order_date))
                    {{ $order->order_date }}
                @else
                    -
                @endif
			</td>
		</tr>
	</table>
	<table style="border-collapse: collapse;">
		<tr>
			<td width="100%"></td>
		</tr>
		<tr>
			<td width="28%" style="line-height: 14px;"><strong>BILL TO</strong></td>
			<td width="28%" style="line-height: 14px;"><strong>SHIP TO</strong></td>
			<td width="44%" colspan="3" style="line-height: 14px;"><strong>TERMS & NOTES</strong></td>
		</tr>
		<tr>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td style="font-size: 8px;line-height:11px;" width="12%">Payment Terms</td>
			<td style="font-size: 8px;line-height:11px;" width="2%">:</td>
			<td style="font-size: 8px;line-height:11px;" width="30%">
				
			</td>
		</tr>
		<tr>
			<td width="28%" style="font-size:8px;line-height:11px;">
				{{$grn->warehouse_name}}
			</td>
			<td width="28%" style="font-size:8px;line-height:11px;">
				{{$grn->warehouse_name}}
			</td>
			<td style="font-size: 8px;line-height:11px;" width="12%">Price</td>
			<td style="font-size: 8px;line-height:11px;" width="2%">:</td>
			<td style="font-size: 8px;line-height:11px;" width="30%">All inclusive</td>
		</tr>
		<tr>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td width="28%" style="font-size:8px;line-height:11px;">
				
			</td>
			<td style="font-size: 8px;line-height:11px;" width="12%">Price Validity</td>
			<td style="font-size: 8px;line-height:11px;" width="2%">:</td>
			<td style="font-size: 8px;line-height:11px;" width="30%">14 days</td>
		</tr>
		<tr>
			<td width="28%" style="font-size:8px;line-height:11px;">
			</td>
			<td width="28%" style="font-size:8px;line-height:11px;">
			</td>
			<td style="font-size: 8px;line-height:11px;" width="12%">Delivery</td>
			<td style="font-size: 8px;line-height:11px;" width="2%">:</td>
			<td style="font-size: 8px;line-height:11px;" width="30%"></td>
		</tr>
	</table>
	<table style="border-collapse: collapse;">
		<tr>
			<td colspan="7" style="line-height: 11px;"></td>
		</tr>	
		<tr>
			<td colspan="7" style="border-top: 1px solid #000;line-height: 11px;"></td>
		</tr>
		<tr>
			<td style="line-height:14px;font-size:8px;" width="5%"><strong>NO</strong></td>
			<td style="line-height:14px;font-size:8px;" width="15%"><strong>CODE</strong></td>
			<td style="line-height:14px;font-size:8px;" width="35%"><strong>ITEM</strong></td>
			<td style="line-height:14px;font-size:8px;" width="10%"><strong>QTY</strong></td>
			<td style="line-height:14px;font-size:8px;" width="7%"><strong>UNIT PRICE</strong></td>
			<td style="line-height:14px;font-size:8px;text-align: right;" width="10%"><strong>DISCOUNT</strong></td>
			<td style="line-height:14px;font-size:8px;text-align: right;" width="18%"><strong>DISCOUNTED PRICE</strong></td>
		</tr>
		<tr>
			<td colspan="7" style="line-height: 5px;">&nbsp;</td>
		</tr>

		<?php 
            $i  = 1;
            $total_discount = 0;
            $total_gross_amount = 0;
            $total_amount = 0;
            $total_without_delivery_charge = 0;
            $totalCustomizeCost=0;
        ?>

        @if(isValid($order['details']))
        	@foreach ($order['details'] as $detail)

        		<?php $totalCustomizeCost += ($detail->customizeData->sum('price') * $detail['qty']) ?>
                <?php $pro_dis_price; ?>

                @if(isValid($detail['discounted_price']))
                    <?php 
                    $pro_dis_price = $detail['price'] - (($detail['price'] * $detail['discounted_price'])/100); 
                    ?>
                @endif
		
				<tr>
					<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" width="5%">
						{{$i}}
					</td>
					<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" width="15%">
						{{ $detail->product->code?:'-' }}
					</td>
					<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" width="35%">
						{{ $detail->product->display_name?:'-' }}
					</td>
					<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" width="10%">
						{{ number_format($detail->qty, 2)?:'-' }}
					</td>
					<td style="line-height:15px;font-size:8px;border-bottom: 1px solid #ddd;" width="7%">
						{{ (number_format($detail['price'],2))?:'-' }}
					</td>
					<td style="line-height:15px;font-size:8px;text-align: right;border-bottom: 1px solid #ddd;" width="10%">
						@if(isValid($detail['discounted_price']))
                            {{ $detail['discounted_price'] }}
                        @else
                            -
                        @endif
					</td>
					<td style="line-height:15px;font-size:8px;text-align: right;border-bottom: 1px solid #ddd;" width="18%">
						@if(isValid($detail['discounted_price']))
                            <?php 
                            $discounted_item_price = $detail['price'] - $detail['discounted_price'] 
                            ?>
                            {{ number_format($discounted_item_price, 2) }}
                        @else
                            {{ number_format($detail['price'],2) }}
                        @endif
					</td>

					<?php 
                        $total_gross_amount += $detail['qty'] * $detail['price']; 
                    ?>

                    @if(isValid($detail['discounted_price']))
                        <?php $total_discount += $detail['qty'] * $detail['discounted_price'] ?>
                    @endif

				</tr>
				<?php $i++; ?>
			@endforeach
		@endif
		<tr>
			<td colspan="7" style="line-height: 5px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="7" style="line-height: 5px;border-bottom: 1px solid #000;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="7" style="line-height: 3px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" style="line-height: 14px;">&nbsp;</td>
			<td style="line-height: 14px;border-bottom: 1px solid #ddd;" width="20%"><strong>Subtotal</strong></td>
			<td colspan="3" style="line-height: 14px;text-align:right;border-bottom: 1px solid #ddd;" width="25%">
				{{ number_format($total_gross_amount, 2) }}
			</td>
		</tr>
		<tr>
			<td colspan="3" style="line-height: 14px;">&nbsp;</td>
			<td style="line-height: 14px;border-bottom: 1px solid #ddd;" width="20%">
				<strong>Total Discount (Rs.)</strong>
			</td>
			<td colspan="3" style="line-height: 14px;text-align:right;border-bottom: 1px solid #ddd;" width="25%">
				-{{ number_format($total_discount, 2) }}
			</td>
		</tr>

		@if(isset($cartDetails) && count($cartDetails) > 0 && 
        isset($cartDetails['voucher_code']) && 
        isset($cartDetails['voucher_type']) && 
        $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
        isset($cartDetails['voucher_percentage']) && 
        !empty($cartDetails['voucher_percentage']))
            <tr>
                <td colspan="3" style="line-height: 14px;">&nbsp;</td>
				<td style="line-height: 14px;border-bottom: 1px solid #ddd;" width="20%">
                	<strong>{{ $cartDetails['voucher_type_name'] }} ({{ $cartDetails['voucher_percentage'] }}%)</strong>
                </td>
                <td colspan="3" style="line-height: 14px;text-align:right;border-bottom: 1px solid #ddd;" width="25%">
                	<strong>
                		-{{ number_format(calculatePercentage($total_without_delivery_charge, $cartDetails['voucher_percentage']), 2) }}
                	</strong>
                </td>
            </tr>
        @endif

		<tr>
			<td colspan="3" style="line-height: 18px;border-bottom: 1px solid #000;">&nbsp;</td>
			<td style="line-height: 18px;border-bottom: 1px solid #000;" width="20%"><strong>Grand Total (Rs)</strong></td>
			<td colspan="3" 
				style="line-height: 18px;text-align:right;border-bottom: 1px solid #000;" width="25%">
				@if(isset($order) && count($order) > 0 && isset($order->delivery_charges) && !empty($order->delivery_charges) && $order->delivery_charges !== '0.00')
                    @if(FREE_DELIVERY_ENABLED == true && checkRule($total_without_delivery_charge, FREE_DELIVERY_RULE, FREE_DELIVERY_OPARETOR))
                        @if(isset($cartDetails) && count($cartDetails) > 0 && 
                            isset($cartDetails['voucher_code']) && 
                            isset($cartDetails['voucher_type']) && 
                            $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                            isset($cartDetails['voucher_percentage']) && 
                            !empty($cartDetails['voucher_percentage']))

                            <strong>{{ CURRENCY_SIGN }} {{ number_format((($total_without_delivery_charge - calculatePercentage($total_without_delivery_charge, $cartDetails['voucher_percentage'])) + $totalCustomizeCost), 2) }}</strong>
                        @else
                            <strong>{{ CURRENCY_SIGN }} {{ number_format($total_without_delivery_charge + $totalCustomizeCost, 2) }}</strong>
                        @endif                            
                    @else
                        @if(isset($cartDetails) && count($cartDetails) > 0 && 
                            isset($cartDetails['voucher_code']) && 
                            $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                            isset($cartDetails['voucher_percentage']) && 
                            !empty($cartDetails['voucher_percentage']))

                            <strong>{{ CURRENCY_SIGN }} {{ number_format(((($total_without_delivery_charge - calculatePercentage($total_without_delivery_charge, $cartDetails['voucher_percentage'])) + $order->delivery_charges) + $totalCustomizeCost), 2) }}</strong>
                        @else
                            <strong>{{ CURRENCY_SIGN }} {{ number_format(($total_without_delivery_charge + $order->delivery_charges) + $totalCustomizeCost, 2) }}</strong>
                        @endif                            
                    @endif
                @else
                    @if(isset($cartDetails) && count($cartDetails) > 0 && 
                        isset($cartDetails['voucher_code']) && 
                        isset($cartDetails['voucher_type']) && 
                        $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                        isset($cartDetails['voucher_percentage']) && 
                        !empty($cartDetails['voucher_percentage']))

                        <strong>{{ CURRENCY_SIGN }} {{ number_format(($total_without_delivery_charge - calculatePercentage($total_without_delivery_charge, $cartDetails['voucher_percentage']) + $totalCustomizeCost), 2) }}</strong>
                    @else
                        <strong>{{ CURRENCY_SIGN }} {{ number_format(($total_without_delivery_charge) + $totalCustomizeCost, 2) }}</strong>
                    @endif
                @endif
			</td>
		</tr>
	</table>
	<table style="border-collapse: collapse;">
		<tr>
			<td colspan="4" style="line-height: 5px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 5px;"></td>
		</tr>
		<tr>
			<td width="17%" style="font-size:7px;">Benificiary Details</td>
			<td width="2%" style="font-size:7px;">:</td>
			<td width="51%" style="font-size:7px;">Orel Corporation Pvt Ltd.</td>
			<td width="30%" style="font-size:7px;"></td>
		</tr>
		<tr>
			<td width="17%" style="font-size:7px;">Bank Details</td>
			<td width="2%" style="font-size:7px;">:</td>
			<td width="51%" style="font-size:7px;">Sampath Bank</td>
			<td width="30%" style="font-size:7px;"></td>
		</tr>
		<tr>
			<td width="17%" style="font-size:7px;">Bank A/C</td>
			<td width="2%" style="font-size:7px;">:</td>
			<td width="51%" style="font-size:7px;">0001 1008 3351</td>
			<td width="30%" style="font-size:7px;"></td>
		</tr>
		<tr>
			<td width="17%" style="font-size:7px;">Warranty</td>
			<td width="2%" style="font-size:7px;">:</td>
			<td width="81%" style="font-size:7px;" colspan="2">For ORANGE ELECTRIC Products, Lifetime-Warranty for all Switches and Sockets, Low Voltage Switchgear 10kA & SIGMA Ranges. 5-Years Warranty for all Fan Controllers, Shaver-Outlet, Low Voltage Switchgear Alpha.<br/>
			24-Months Warranty for LED Lamps and Industrial Products. 12 to 18-Months Warranty for all CFLs. 12 Months Warranty for Home Appliances.<br/>
			The above Warranties would be applicable from Date of Invoice.
			</td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 5px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 5px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 7px;"><strong>Other Terms & Conditions :-</strong></td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 7px;">All payment to be made in a/c payee cheque in favor of OREL CORPORATION (PVT) LTD.</td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 7px;">Items are non-returnable, unless otherwise due to a manufacturing defect.</td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 6px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 7px;text-align: center;">This is a computer generated document. no signature is required and deemed as official</td>
		</tr>
		<tr>
			<td colspan="4" style="line-height: 6px;"></td>
		</tr>
		<tr>
			<td colspan="4" style="font-size: 8px;text-align: center;line-height: 13px;">THANK YOU</td>
		</tr>
	</table>
@endif