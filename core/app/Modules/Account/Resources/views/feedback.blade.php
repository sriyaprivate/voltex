@extends('layouts.master') @section('title','Order History')

@section('links')
@stop

@section('css')
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">My Account</a></li>
        <li class="breadcrumb-item active">Feedback</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="account-section mb-2">
    <div class="container">

      <div class="row profile">
        <div class="col-md-3 mb-3">
          <div class="profile-sidebar">
            
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic text-center">
              <img src="{{ url('assets/images/'.DEFAULT_USER_IMAGE) }}" class="img-responsive" alt="User image">
            </div>
            <!-- END SIDEBAR USERPIC -->

            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
              <div class="profile-usertitle-name">
                Nishan O A R
              </div>
              <div class="profile-usertitle-job">
                nishanran@gmail.com<br>
                +94 71 378 9660
              </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->

            <!-- SIDEBAR BUTTONS -->
            <!-- <div class="profile-userbuttons">
              <button type="button" class="btn btn-success btn-sm">Follow</button>
              <button type="button" class="btn btn-danger btn-sm">Message</button>
            </div> -->
            <!-- END SIDEBAR BUTTONS -->
            
            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/order-history')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> Wishlist</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-bitbucket" aria-hidden="true"></i> Bulk Quotations</a>
                </li>
                <li class="active nav-item">
                  <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                </li>
              </ul>
            </div>
            <!-- END MENU -->
          </div>
        </div>
        <div class="col-md-9 mb-3">
          <div class="profile-content mb-3">
            <h6 class="text-uppercase m-0">Feedback</h6>
          </div>

          <hr>

          <form>
            <div class="form-group">
              <label>How Would You Rate Our Products?</label>
              <div class="feedback-rating">
                <input type="radio" id="radio1" name="radios" value="all">
                  <label for="radio1"><i class="fa fa-smile-o" aria-hidden="true"></i></label>
                <input type="radio" id="radio2" name="radios"value="false">
                  <label for="radio2"><i class="fa fa-frown-o" aria-hidden="true"></i></label>
                <input type="radio" id="radio3" name="radios" value="true">
                  <label for="radio3"><i class="fa fa-meh-o" aria-hidden="true"></i></label>
                <input type="radio" id="radio4" name="radios" value="true">
                  <label for="radio4"><i class="fa fa-heart-o" aria-hidden="true"></i></label>
              </div>
            </div>
            <div class="form-group">
              <label>What Would You Like To Share With Us?</label>
              <textarea class="form-control" rows="4"></textarea>
            </div>
            <button type="submit" class="btn btn-sm btn-primary float-right"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Feedback</button>
          </form>

        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
@stop

@section('scripts')
@stop