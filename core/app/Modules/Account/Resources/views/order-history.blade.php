@extends('layouts.master') @section('title','Order History')

@section('links')
    <!-- Gijgo -->
    <link href="{{asset('assets/libraries/Gijgo/gijgo.css')}}" rel="stylesheet" type="text/css" />

    <!-- select2 -->
    <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
@stop

@section('content')
    <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="#">My Account</a></li>
                <li class="breadcrumb-item active">Order History</li>
            </ol>
        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->

    <!-- account-section -->
    <div class="account-section mb-2">
        <div class="container">

            <div class="row profile">
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">

                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic text-center">
                          <img src="{{ url('assets/images/'.DEFAULT_USER_IMAGE) }}" class="img-responsive" alt="{{ isValid($usr['customer']->first_name)}}">
                        </div>
                        <!-- END SIDEBAR USERPIC -->

                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                {{$usr['customer']->first_name}} {{$usr['customer']->last_name}}
                            </div>
                            <div class="profile-usertitle-job">
                                {{$usr['customer']->email}}<br>
                                {{$usr['customer']->contact_no}}
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->

                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav flex-column">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('account/order-history')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> Wishlist</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-bitbucket" aria-hidden="true"></i> Bulk Quotations</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
            
                    </div>
                </div>
        
                <div class="col-md-9 mb-3">
                    <div class="profile-content mb-3">
                        <h6 class="text-uppercase m-0">Order History</h6>
                    </div>    

                    <hr>

                    <form class="mb-2">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="text-uppercase">Order No</label>
                                <input type="text" class="form-control form-control-sm" name="order_no" id="order_no" placeholder="Enter Order No">
                            </div>
                          
                            <div class="col-md-4">
                                <label class="text-uppercase">Order Date</label>
                                <input type="text" class="form-control form-control-sm" id="datepicker" name="datepicker" placeholder="Enter Order Date">
                            </div>

                            <div class="col-md-4">
                                <label class="text-uppercase">Status</label>
                                <select class="js-example-basic-single" name="status" id="status">
                                    <option value="">All</option>
                                    <option value="1">PAID</option>
                                    <option value="2">REJECTED</option>
                                    <option value="3">PREPARING</option>
                                    <option value="4">PREPARED</option>
                                    <option value="5">ONDELIVERY</option>
                                    <option value="6">DELIVERED</option>
                                    <option value="7">PICKUPED</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-sm float-right mt-3-mobile" type="submit"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                            </div>
                        </div>
                    </form>

                    <div class="mb-3 overflow-x-scroll">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th width="15%">OrderID</th>
                                    <th class="text-center" >DeliveryType</th>
                                    <th>Amount</th>
                                    <th>Delivery Charges</th>
                                    <th>Total Amount</th>
                                    <!-- <th>Customer</th>
                                    <th>Dealer/WH</th> -->
                                    <th class="text-center">Order Date</th>
                                    <th class="text-center">Status</th>
                                    <th width="10%" class="text-center" colspan="3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($orders)>0)
                                    @foreach ($orders as $order)
                                        <tr>
                                            <td>{{$order['order_no']}}</td>
                                            <td class="text-center" >{{$order['deliveryType']->name}}</td>
                                            <td class="text-right">{{$order['amount']}}</td>
                                            <td class="text-right">{{$order['delivery_charges']}}</td>
                                            <td class="text-right">{{ number_format(($order['amount'] + $order['delivery_charges']), 2)}}</td>
                                            <!-- <td>P.Dias</td>
                                            <td>DL874</td> -->
                                            <td class="text-center">{{$order['order_date']}}</td>

                                            <td class="text-center">
                                                @if($order['status']->status == 0)
                                                    <span class="badge badge-custome pending">PENDING</span>
                                                @elseif($order['status']->status == 1)
                                                    <span class="badge badge-custome approve">PAID</span>
                                                @elseif($order['status']->status == 2)
                                                    <span class="badge badge-custome rejected">REJECTED</span>
                                                @elseif($order['status']->status == 8)
                                                    <span class="badge badge-custome approved">ADVANCE</span>
                                                @endif
                                            </td>
                                            <td>
                                                <center>
                                                    @if($order['status'] == 0 || $order['status'] == 2)
                                                        <!-- <a style="font-size:15px;" class="link-disabled" data-toggle="tooltip" title="View order" href="#" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a> -->
                                                        <a style="font-size:15px;" data-toggle="tooltip" title="View order" href="{{url('account/order-view')}}/{{$order['id']}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    @else
                                                        <a style="font-size:15px;" data-toggle="tooltip" title="View order" href="{{url('account/order-view')}}/{{$order['id']}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    @endif                                                        
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    @if($order['status'] == 0 || $order['status'] == 2)
                                                        <!-- <a href="#" class="link-disabled" style="font-size:15px;" data-toggle="tooltip" title="View payment transaction history." role="button"><i class="fa fa-history" aria-hidden="true"></i></a> -->
                                                        <a href="{{ route('user.account.order.transaction.history', ['order_no' => $order['id']]) }}" style="font-size:15px;" data-toggle="tooltip" title="View payment transaction history." role="button"><i class="fa fa-history" aria-hidden="true"></i></a>
                                                    @else
                                                        <a href="{{ route('user.account.order.transaction.history', ['order_no' => $order['id']]) }}" style="font-size:15px;" data-toggle="tooltip" title="View payment transaction history." role="button"><i class="fa fa-history" aria-hidden="true"></i></a>
                                                    @endif
                                                </center>
                                            </td>    
                                            <!-- <td>
                                                <center>
                                                    @if(isset($order['status']) && $order['status'] == 0)
                                                        <a style="font-size:15px;" data-toggle="tooltip" title="{{ $order['status'] == 1? 'Payment Complete' : 'Make order payment' }}" href="{{ route('cart.get.make.payment') }}?order_no={{ isValid($order['order_no']) }}" role="button"><span class="fa fa-credit-card-alt"></span></a>
                                                    @else
                                                        <div style="font-size:15px;" class="btn text-success btn-link btn-sm disabled" data-toggle="tooltip" title="Payment done" role="button"><span class="fa fa-credit-card-alt"></span></div>
                                                    @endif
                                                </center>
                                            </td> -->
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" style="text-align: center">No Orders</td>
                                    </td>
                                @endif
                            
                            </tbody>
                        </table>
                    </div>                    

                </div>
            </div>

        </div> <!-- /.container -->
    </div> <!-- /.account-section -->
@stop

@section('js')
    <!-- Gijgo -->
    <script src="{{asset('assets/libraries/Gijgo/gijgo.js')}}" type="text/javascript"></script>

    <!-- select2 -->
    <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'yyyy-mm-dd'
            });

            // In your Javascript (external .js resource or <script> tag)
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        });
</script>
@stop