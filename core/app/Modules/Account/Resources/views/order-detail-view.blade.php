@extends('layouts.master') @section('title',"Customer's order view")

@section('links')
@stop

@section('css')
    <style type="text/css">
        .reject{color: white !important;background-color: #eb3b5a !important;}.open{color: white !important;background-color: green !important;}.pending{color: white !important;background-color: #2c3e50 !important;}.approved{color: white !important;background-color: #16a085 !important;}.rejected{color: white !important;background-color: #eb3b5a !important;}.preparing{color: white !important;background-color: sandybrown !important;}.prepared{color: white !important;background-color: #0fb9b1 !important;}.ondelivery{color: white !important;background-color: #f7b731 !important;}.delivered{color: white !important;background-color: #2980b9 !important;}.pickuped{color: white !important;background-color: #0a635a !important;}.preparing-status{color: white !important;background-color: sandybrown !important;} 

        .reject:disabled{color: white !important;background-color: gray !important;}.open:disabled{color: white !important;background-color: gray !important;}.pending:disabled{color: white !important;background-color: gray !important;}.approved:disabled{color: white !important;background-color: gray !important;}.rejected:disabled{color: white !important;background-color: gray !important;}.preparing:disabled{color: white !important;background-color: gray !important;}.prepared:disabled{color: white !important;background-color: gray !important;}.ondelivery:disabled{color: white !important;background-color: gray !important;}.delivered:disabled{color: white !important;background-color: gray !important;}.pickuped:disabled{color: white !important;background-color: gray !important;}
        .summery{
            text-align: right;
            padding: 5px;
            border-bottom: 1px solid black;
        }
        .summery-footer{
            text-align: right;
            padding: 5px;
            border-bottom-style: double;
        }
    </style>
@stop

@section('content')
    <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="#">My Account</a></li>
                <li class="breadcrumb-item"><a href="#">Order History</a></li>
                <li class="breadcrumb-item active">Order View</li>
            </ol>
        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->


    <!-- account-section -->
    <div class="account-section mb-2">
        <div class="container">

            <div class="row profile">            
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">

                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav flex-column">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('account/order-history')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> Wishlist</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-bitbucket" aria-hidden="true"></i> Bulk Quotations</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
            
                    </div>
                </div>
                <div class="col-md-9 mb-3">
                    @if(isValid($order))
                        @if(isset($order->paymentMethod->id) && !empty($order->paymentMethod->id) && $order->paymentMethod->id == CASH_ON_DELIVERY)
                            <div class="alert alert-success alert-dismissible fade show {{ isset($_GET['content'])? 'd-block' : 'd-none' }}" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                              <h4 class="alert-heading">The order has been successfully placed!.</h4>
                              <p>{!! isValid($order->paymentMethod->description)?:'' !!}</p>
                              <hr>
                              <p class="mb-0">Cash will collect when the order has delivered to your hand</p>
                            </div>
                        @elseif(isset($order->paymentMethod->id) && !empty($order->paymentMethod->id) && $order->paymentMethod->id == CASH_ON_PICKUP)
                            <div class="alert alert-success alert-dismissible fade show {{ isset($_GET['content'])? 'd-block' : 'd-none' }}" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                              <h4 class="alert-heading">The order has been successfully placed!.</h4>
                              <p>{!! isValid($order->paymentMethod->description)?:'' !!}</p>
                              <hr>
                              <p class="mb-0">Cash will collect on the store</p>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col">
                                <a class="btn btn-primary btn-sm float-right mb-4" target="_blank" href="{{url('account/order-print')}}/{{ isValid($order) && isValid($order->id)? $order->id : '' }}"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                            </div>
                        </div>
                    
                        <div class="mt-1 mb-3">
                            <div class="row word-wrap">
                                <div class="col-12 col-lg-7 col-md-7 col-sm-7">
                                    <div class="row">
                                        <div class="col-lg-4 col-4 font-weight-bold">Order ID</div>
                                        <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                        <div class="col-lg-7 col-6">{{$order['order_no']}}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4 col-4 font-weight-bold">Order Date</div>
                                        <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                        <div class="col-lg-7 col-6">{{$order['order_date']}}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4 col-4 font-weight-bold">Customer</div>
                                        <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                        <div class="col-lg-7 col-6">{{$order->customer->first_name}} {{$order->customer->last_name}}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4 col-4 font-weight-bold">Order Date</div>
                                        <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                        <div class="col-lg-7 col-6">{{$order['order_date']}}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4 col-4 font-weight-bold">Transaction No</div>
                                        <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                        <div class="col-lg-7 col-6">
                                            @if($order['payment'] && $order['payment']['transaction'])
                                                {{$order['payment']['transaction']->transaction_no}}
                                            @else
                                                -
                                            @endif
                                        </div>
                                    </div>
        
                                    <div class="row">
                                        <div class="col-lg-4 col-4 font-weight-bold">Status</div>
                                        <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                        <div class="col-lg-7 col-6">
                                            @if($order->status == 0)
                                                <span class="badge badge-custome pending">PENDING</span>
                                            @elseif($order->status == 1)
                                                <span class="badge badge-custome approved">PAID</span>
                                            @elseif($order->status == 2)
                                                <span class="badge badge-custome rejected">REJECTED</span>
                                            @elseif($order->status == 8)
                                                <span class="badge badge-custome approved">ADVANCE</span>
                                            @endif
                                        </div>
                                    </div>
                                    @if(isset($cartDetails) && count($cartDetails) > 0 && 
                                    isset($cartDetails['voucher_code']) && 
                                    isset($cartDetails['voucher_type']) && 
                                    $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                                    isset($cartDetails['voucher_percentage']) && 
                                    !empty($cartDetails['voucher_percentage'])) 
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-4 font-weight-bold">Others</div>
                                        <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                        <div class="col-lg-7 col-6">
                                            <span class="badge badge-success p-1 text-uppercase" style="border-radius:0px;padding: 5px 10px !important; !important;width: auto;">
                                                <span class="hide-on-mobile">{{ $cartDetails['voucher_type_name'] }} code is </span>{{ $cartDetails['voucher_code'] }} | {{ $cartDetails['voucher_percentage'] }}% off from total
                                            </span>
                                        </div>
                                    </div>
                                    @endif 
                                </div>

                                <div class="col-12 col-lg-5 col-md-5 col-sm-5">
                                    @if(isset($order) && isset($order['billing_address']))
                                        <div class="row">
                                            <div class="col-4 font-weight-bold">Billing Address</div>
                                            <div class="col-1 font-weight-bold">:</div>
                                            <div class="col-6">{{$order['billing_address']}}</div>
                                        </div>
                                    @endif
                                    
                                    <div class="row">
                                        <div class="col-4 font-weight-bold">Delivered Date</div>
                                        <div class="col-1 font-weight-bold">:</div>
                                        <div class="col-6">{{$order['delivery_date']}}</div>
                                    </div>
                                </div>

                            </div>                        
                        </div>

                        <div class="mt-2 mb-3 overflow-x-scroll">
                            <table class="table table-sm table-striped table-bordered fs-11">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th width="23%">Product</th>
                                        <th>ProductCode</th>
                                        <th class="text-right">Measurement</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-right">UnitPrice</th>
                                        <th class="text-right">Dis. Amount(Per Unit)</th>
                                        <th class="text-right">Discounted Price(Per Unit)</th>
                                        <th class="text-right">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;$total_dis=0;$total=0;$total_discount=0;$total_gross_amount=0; ?>
                                    @if(isValid($details))
                                        @foreach($details as $detail)
                                            <?php $pro_dis_price; ?>

                                            @if($detail['discounted_price']>0)
                                                <?php 
                                                    $pro_dis_price = $detail['price']-$detail['discounted_price'];
                                                    $total_discount += $detail['qty'] * $detail['discounted_price'];
                                                ?>
                                            @endif

                                            <tr>
                                                <td class="text-center">{{$i}}</td>
                                                <td>{{$detail['product']->display_name?:'-'}}</td>
                                                <td>{{$detail['product']->code}}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-primary" style="padding: 0px 10px 0 10px;font-size: 12px">Add</button>
                                                </td>
                                                <td class="text-center">{{$detail['qty']}}</td>
                                                <td class="text-right">{{number_format($detail['price'],2)}}</td>

                                                <td class="text-right">
                                                    @if($detail['discounted_price']>0)
                                                        {{$detail['discounted_price']}}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-right">
                                                    @if($detail['discounted_price']>0)
                                                        {{number_format($pro_dis_price,2)}}
                                                    @else
                                                        -
                                                    @endif
                                                </td>

                                                <?php $total+= $detail['qty']*$detail['price']; ?>

                                                @if($detail['discounted_price']>0)
                                                    <?php $total_dis += $detail['qty']*$pro_dis_price; ?>
                                                @else
                                                    <?php $total_dis += $detail['qty']*$detail['price']; ?>
                                                @endif

                                                <?php 
                                                    $total_gross_amount += $detail['qty'] * $detail['price']; 
                                                ?>

                                                <td class="text-right">
                                                    @if($detail['discounted_price']>0)
                                                        <?php echo number_format(($detail['qty']*$pro_dis_price),2); ?>
                                                    @else
                                                        <?php echo number_format(($detail['qty']*$detail['price']),2); ?>
                                                    @endif
                                                </td>
                                                
                                            </tr>
                                            <?php $i++; ?>    
                                        @endforeach
                                    @else
                                        <h3 class="text-center">Order not found!.</h3>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5" class="text-right"><strong></strong></td>
                                        <td class="text-right"><strong>{{ number_format($total_gross_amount, 2) }}</strong></td>
                                        <td colspan="2" class="text-right"><strong></strong></td>
                                        <td class="text-right"><strong>{{number_format($total_dis, 2)}}</strong></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="">
                            <div class="mt-1 mb-3" style="margin-bottom: 5px">
                                <div class="row">
                                    <div class="col-6 col-lg-10" style="text-align: right;"><strong>Amount ({{ CURRENCY_SIGN }})</strong></div>
                                    <div class="col-6 col-lg-2" style="text-align: right;"><strong>{{number_format($total_gross_amount,2)}}</strong></div>
                                </div>
                            </div>
                            
                            <div class="mt-1 mb-3" style="margin-bottom: 5px">
                                <div class="row">
                                    <div class="col-6 col-lg-10" style="text-align: right;"><strong>Discount ({{ CURRENCY_SIGN }})</strong></div>
                                    <div class="col-6 col-lg-2" style="text-align: right;"><strong>-({{number_format($total_discount,2)}})</strong></div>
                                </div>
                            </div>
                            @if(isset($cartDetails) && count($cartDetails) > 0 && 
                            isset($cartDetails['voucher_code']) && 
                            isset($cartDetails['voucher_type']) && 
                            $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                            isset($cartDetails['voucher_percentage']) && 
                            !empty($cartDetails['voucher_percentage']))

                                <div class="mt-1 mb-3" style="margin-bottom: 5px">
                                    <div class="row">
                                        <div class="col-6 col-lg-10" style="text-align: right;"><strong>{{ $cartDetails['voucher_type_name'] }} ({{ $cartDetails['voucher_percentage'] }}%)</strong></div>
                                        <div class="col-6 col-lg-2" style="text-align: right;">
                                            <strong>-({{ number_format(calculatePercentage($total_dis, $cartDetails['voucher_percentage']), 2) }})</strong>
                                        </div>
                                    </div>
                                </div>
                            @endif  

                            <div class="mt-1 mb-3" style="margin-bottom: 5px">
                                <div class="row">
                                    <div class="col-6 col-lg-10" style="text-align: right;"><strong>Total Amount ({{ CURRENCY_SIGN }})</strong></div>
                                    <div class="col-6 col-lg-2" style="text-align: right;">
                                    <strong style="border-bottom-style: double;border-top-style: double;">
                                        @if(isset($cartDetails) && count($cartDetails) > 0 && 
                                        isset($cartDetails['voucher_code']) && 
                                        isset($cartDetails['voucher_type']) && 
                                        $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                                        isset($cartDetails['voucher_percentage']) && 
                                        !empty($cartDetails['voucher_percentage']))

                                            @if(FREE_DELIVERY_ENABLED == true && checkRule($total_dis, FREE_DELIVERY_RULE, FREE_DELIVERY_OPARETOR) && $order->created_at >= FREE_DELIVERY_ENABLED_DATE)
                                                <strong>{{ CURRENCY_SIGN }} {{ number_format(($total_dis - calculatePercentage($total_dis, $cartDetails['voucher_percentage'])), 2) }}</strong>
                                            @else
                                                <strong>{{ CURRENCY_SIGN }} {{ number_format((($order['delivery_charges']+$total_dis) - calculatePercentage($total_dis, $cartDetails['voucher_percentage'])), 2) }}</strong>
                                            @endif
                                        @else
                                            @if(FREE_DELIVERY_ENABLED == true && checkRule($total_dis, FREE_DELIVERY_RULE, FREE_DELIVERY_OPARETOR) && $order->created_at >= FREE_DELIVERY_ENABLED_DATE)
                                                <strong>{{ CURRENCY_SIGN }} {{number_format($total_dis, 2)}}</strong>
                                            @else
                                                <strong>{{ CURRENCY_SIGN }} {{number_format(($order['delivery_charges']+$total_dis), 2)}}</strong>
                                            @endif
                                        @endif  
                                    </strong>
                                </div>
                                </div>
                            </div>  
                        </div>                  
                    @else
                        <div class="text-center mb-4 h4 text-gray">Order not found!.</div>
                    @endif

                </div>
            </div>
        </div> <!-- /.container -->
    </div> <!-- /.account-section -->
@stop

@section('js')
@stop

@section('scripts')
@stop