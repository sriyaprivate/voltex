@extends('layouts.master') @section('title','Bulk Quotation')

@section('links')
  <!-- select2 -->
  <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">My Account</a></li>
        <li class="breadcrumb-item active">Bulk Quotations</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="account-section mb-2">
    <div class="container">

      <div class="row profile">
        <div class="col-md-3 mb-3">
          <div class="profile-sidebar">
            
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic text-center">
              <img src="{{ url('assets/images/'.DEFAULT_USER_IMAGE) }}" class="img-responsive" alt="{{ isValid($user['customer']->first_name)}}">
            </div>
            <!-- END SIDEBAR USERPIC -->

            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                    {{$user->customer->first_name?:'-'}} {{$user->customer->last_name?:'-'}}
                </div>
                
                <div class="profile-usertitle-job">
                    {{$user->customer->email?:'-'}}<br>
                    {{$user->customer->phone_no?:'-'}}
                </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->

            <!-- SIDEBAR BUTTONS -->
            <!-- <div class="profile-userbuttons">
              <button type="button" class="btn btn-success btn-sm">Follow</button>
              <button type="button" class="btn btn-danger btn-sm">Message</button>
            </div> -->
            <!-- END SIDEBAR BUTTONS -->

            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/order-history')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> Wishlist</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                </li>
                <li class="active nav-item">
                  <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-bitbucket" aria-hidden="true"></i> Bulk Quotations</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                </li>
              </ul>
            </div>
            <!-- END MENU -->
          </div>
        </div>
        <div class="col-md-9 mb-3">
          <div class="profile-content mb-3">
            <h6 class="text-uppercase m-0">Bulk Quotations</h6>
          </div>

          <hr>

          <form class="mb-2">
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <label for="validationCustom03" class="text-uppercase">Quotation No</label>
                <input type="text" class="form-control form-control-sm" id="validationCustom03" placeholder="Enter Quotation No">
              </div>
              <div class="col-md-6 col-xs-12">
                <label for="validationCustom05" class="text-uppercase">Status</label>
                <select class="js-example-basic-single">
                  <option value="AL">- Select -</option>
                  <option value="AL">Status 01</option>
                  <option value="WY">Status 02</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-sm float-right" type="submit"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
              </div>
            </div>
          </form>

          <div class="mb-3">
            <table class="table table-sm table-bordered">
              <thead>
                <tr>
                  <th>QuotationNo</th>
                  <th>Status</th>
                  <th width="10%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Q101</td>
                  <td class="text-center"><span class="badge badge-custome">Quotaion Recieved</span></td>
                  <td>
                    <center>
                      <a class="btn btn-link btn-sm" href="{{url('account/bulk-quotation-view')}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </center>
                  </td>
                </tr>
                <tr>
                  <td>Q102</td>
                  <td class="text-center"><span class="badge badge-custome">Quotaion Accepted</span></td>
                  <td>
                    <center>
                      <a class="btn btn-link btn-sm" href="{{url('account/bulk-quotation-view')}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </center>
                  </td>
                </tr>
                <tr>
                  <td>Q103</td>
                  <td class="text-center"><span class="badge badge-custome">Quotaion Recieved</span></td>
                  <td>
                    <center>
                      <a class="btn btn-link btn-sm" href="{{url('account/bulk-quotation-view')}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </center>
                  </td>
                </tr>
                <tr>
                  <td>Q104</td>
                  <td class="text-center"><span class="badge badge-custome">Quotaion Recieved</span></td>
                  <td>
                    <center>
                      <a class="btn btn-link btn-sm" href="bulk-quotation-view.html" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </center>
                  </td>
                </tr>
                <tr>
                  <td>Q105</td>
                  <td class="text-center"><span class="badge badge-custome">Quotaion Recieved</span></td>
                  <td>
                    <center>
                      <a class="btn btn-link btn-sm" href="{{url('account/bulk-quotation-view')}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </center>
                  </td>
                </tr>
                <tr>
                  <td>Q106</td>
                  <td class="text-center"><span class="badge badge-custome">Quotaion Recieved</span></td>
                  <td>
                    <center>
                      <a class="btn btn-link btn-sm" href="{{url('account/bulk-quotation-view')}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </center>
                  </td>
                </tr>
                <tr>
                  <td>Q107</td>
                  <td class="text-center"><span class="badge badge-custome">Quotaion Recieved</span></td>
                  <td>
                    <center>
                      <a class="btn btn-link btn-sm" href="{{url('account/bulk-quotation-view')}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </center>
                  </td>
                </tr>
                <tr>
                  <td>Q108</td>
                  <td class="text-center"><span class="badge badge-custome">Quotaion Recieved</span></td>
                  <td>
                    <center>
                      <a class="btn btn-link btn-sm" href="{{url('account/bulk-quotation-view')}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="btn btn-link btn-sm" href="#" role="button"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </center>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="row">
            <div class="col d-flex align-items-center">
              Showing 1 to 6 of 57 entries
            </div>
            <div class="col">
              <ul class="pagination pagination-sm justify-content-end mb-0">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>

        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
  <!-- select2 -->
  <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
@stop

@section('scripts')
<script>
  $(document).ready(function(){
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
      $('.js-example-basic-single').select2();
    });
  });
</script>
@stop