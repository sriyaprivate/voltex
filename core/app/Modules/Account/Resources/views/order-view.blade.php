@extends('layouts.master') @section('title',"Customer's order view")

@section('links')
@stop

@section('css')
    <style type="text/css">

        .cart{
            margin: 90px -25px 0px 0 !important;
        }

        .performa-invoice-cart{
            margin: 90px -25px 0px 0 !important;
        }
        
    </style>
@stop

@section('content')
    
    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="">My Account</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Order History</li>
                <li class="arrow" style="float: left"> → </li><li>Order View</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <div class="row">
                <div class="col-md-2 mb-3">
                    <div class="profile-sidebar">

                        <!-- SIDEBAR MENU -->
                        <div class="">
                            <ul class="nav flex-column" style="list-style-type: none;">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('account/order-list')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> My Cart</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
            
                    </div>
                </div>

                <div class="col-md-10 mb-3" style="font-size: 12px">
                    @if(isValid($order))
                        <div class="row">
                            <div class="col-md-9" style="padding-left: 0px">
                                <h3 class="content-title col-4">Order Details</h3>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-primary btn-sm float-right mb-4" style="margin-top: 17px" target="_blank" href="{{url('account/order-print')}}/{{ isValid($order) && isValid($order->id)? $order->id : '' }}"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-md-7 col-sm-7">
                                <div class="row">
                                    <div class="col-lg-4 col-4 font-weight-bold">Order ID</div>
                                    <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                    <div class="col-lg-7 col-6">{{$order['order_no']}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-4 font-weight-bold">Order Date</div>
                                    <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                    <div class="col-lg-7 col-6">{{$order['order_date']}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-4 font-weight-bold">Customer</div>
                                    <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                    <div class="col-lg-7 col-6">{{$order->customer->username}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-4 font-weight-bold">Order Date</div>
                                    <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                    <div class="col-lg-7 col-6">{{$order['order_date']}}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-4 font-weight-bold">Transaction No</div>
                                    <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                    <div class="col-lg-7 col-6">
                                        @if($order['payment'] && $order['payment']['transaction'])
                                            {{$order['payment']['transaction']->transaction_no}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-4 font-weight-bold">Status</div>
                                    <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                    <div class="col-lg-7 col-6">
                                        @if($order['status'] == NEWLY_CREATED)
                                            <span class="badge badge-custome pending">NEWLY_CREATED</span>
                                        @elseif($order['status'] == CUSTOMER_CONFIRMED)
                                            <span class="badge badge-custome approve">CUSTOMER_CONFIRMED</span>
                                        @elseif($order['status'] == CUSTOMER_REJECTED)
                                            <span class="badge badge-custome rejected">CUSTOMER_REJECTED</span>
                                        @elseif($order['status'] == APPROVE_PENDING)
                                            <span class="badge badge-custome preparing">APPROVE_PENDING</span>
                                        @endif
                                    </div>
                                </div>
                                @if(isset($cartDetails) && count($cartDetails) > 0 && 
                                isset($cartDetails['voucher_code']) && 
                                isset($cartDetails['voucher_type']) && 
                                $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                                isset($cartDetails['voucher_percentage']) && 
                                !empty($cartDetails['voucher_percentage'])) 
                                <div class="row mt-2">
                                    <div class="col-lg-4 col-4 font-weight-bold">Others</div>
                                    <div class="col-lg-1 col-1 font-weight-bold">:</div>
                                    <div class="col-lg-7 col-6">
                                        <span class="badge badge-success p-1 text-uppercase" style="border-radius:0px;padding: 5px 10px !important; !important;width: auto;">
                                            <span class="hide-on-mobile">{{ $cartDetails['voucher_type_name'] }} code is </span>{{ $cartDetails['voucher_code'] }} | {{ $cartDetails['voucher_percentage'] }}% off from total
                                        </span>
                                    </div>
                                </div>
                                @endif 
                            </div>

                            <div class="col-md-5 col-sm-5">
                                @if(isset($order) && isset($order['billing_address']))
                                    <div class="row">
                                        <div class="col-4 font-weight-bold">Billing Address</div>
                                        <div class="col-1 font-weight-bold">:</div>
                                        <div class="col-6">{{$order['billing_address']}}</div>
                                    </div>
                                @endif
                                
                                <div class="row">
                                    <div class="col-4 font-weight-bold">Delivered Date</div>
                                    <div class="col-1 font-weight-bold">:</div>
                                    <div class="col-6">{{$order['delivery_date']}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-2 mb-3 overflow-x-scroll">
                            <table class="table table-sm table-striped table-bordered fs-11">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th width="23%">Product</th>
                                        <th>ProductCode</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-right">UnitPrice</th>
                                        <th class="text-right">Dis. Amount(Per Unit)</th>
                                        <th class="text-right">Discounted Price(Per Unit)</th>
                                        <th class="text-right">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;$ii=0;$total_dis=0;$total=0;$total_discount=0;$total_gross_amount=0; ?>
                                    @if(isValid($details))
                                        @foreach($details as $detail)
                                            <?php $pro_dis_price; ?>

                                            @if($detail['discounted_price']>0)
                                                <?php 
                                                    $pro_dis_price = $detail['price']-$detail['discounted_price'];
                                                    $total_discount += $detail['qty'] * $detail['discounted_price'];
                                                ?>
                                            @endif

                                            <tr>
                                                <td class="text-center">{{$i}}</td>
                                                <td>{{$detail['product']->display_name?:'-'}}</td>
                                                <td>{{$detail['product']->code}}</td>
                                                <td class="text-center">{{$detail['qty']}}</td>
                                                <td class="text-right">{{number_format($detail['price'],2)}}</td>

                                                <td class="text-right">
                                                    @if($detail['discounted_price']>0)
                                                        {{$detail['discounted_price']}}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-right">
                                                    @if($detail['discounted_price']>0)
                                                        {{number_format($pro_dis_price,2)}}
                                                    @else
                                                        -
                                                    @endif
                                                </td>

                                                <?php $total+= $detail['qty']*$detail['price']; ?>

                                                @if($detail['discounted_price']>0)
                                                    <?php $total_dis += $detail['qty']*$pro_dis_price; ?>
                                                @else
                                                    <?php $total_dis += $detail['qty']*$detail['price']; ?>
                                                @endif

                                                <?php 
                                                    $total_gross_amount += $detail['qty'] * $detail['price']; 
                                                ?>

                                                <td class="text-right">
                                                    @if($detail['discounted_price']>0)
                                                        <?php echo number_format(($detail['qty']*$pro_dis_price),2); ?>
                                                    @else
                                                        <?php echo number_format(($detail['qty']*$detail['price']),2); ?>
                                                    @endif
                                                </td>
                                                
                                            </tr>
                                                
                                        @endforeach
                                    @else
                                        <h3 class="text-center">Order not found!.</h3>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7" class="text-right"><strong></strong></td>
                                        <td class="text-right"><strong>{{number_format($total_dis, 2)}}</strong></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="">
                            <div class="mt-1 mb-3" style="margin-bottom: 5px">
                                <div class="row">
                                    <div class="col-6 col-lg-10" style="text-align: right;"><strong>Amount ({{ CURRENCY_SIGN }})</strong></div>
                                    <div class="col-6 col-lg-2" style="text-align: right;"><strong>{{number_format($total_gross_amount,2)}}</strong></div>
                                </div>
                            </div>
                            
                            <div class="mt-1 mb-3" style="margin-bottom: 5px">
                                <div class="row">
                                    <div class="col-6 col-lg-10" style="text-align: right;"><strong>Discount ({{ CURRENCY_SIGN }})</strong></div>
                                    <div class="col-6 col-lg-2" style="text-align: right;"><strong>-({{number_format($total_discount,2)}})</strong></div>
                                </div>
                            </div>

                            @if(isset($cartDetails) && count($cartDetails) > 0 && isset($cartDetails['voucher_code']) && isset($cartDetails['voucher_type']) && $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && isset($cartDetails['voucher_percentage']) && !empty($cartDetails['voucher_percentage']))

                                <div class="mt-1 mb-3" style="margin-bottom: 5px">
                                    <div class="row">
                                        <div class="col-6 col-lg-10" style="text-align: right;"><strong>{{ $cartDetails['voucher_type_name'] }} ({{ $cartDetails['voucher_percentage'] }}%)</strong></div>
                                        <div class="col-6 col-lg-2" style="text-align: right;">
                                            <strong>-({{ number_format(calculatePercentage($total_dis, $cartDetails['voucher_percentage']), 2) }})</strong>
                                        </div>
                                    </div>
                                </div>
                            @endif  

                            <div class="mt-1 mb-3" style="margin-bottom: 5px">
                                <div class="row">
                                    <div class="col-6 col-lg-10" style="text-align: right;"><strong>Total Amount ({{ CURRENCY_SIGN }})</strong></div>
                                    <div class="col-6 col-lg-2" style="text-align: right;">
                                    <strong style="border-bottom-style: double;border-top-style: double;">
                                        @if(isset($cartDetails) && count($cartDetails) > 0 && isset($cartDetails['voucher_code']) && isset($cartDetails['voucher_type']) && $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && isset($cartDetails['voucher_percentage']) && !empty($cartDetails['voucher_percentage']))

                                            @if(FREE_DELIVERY_ENABLED == true && checkRule($total_dis, FREE_DELIVERY_RULE, FREE_DELIVERY_OPARETOR) && $order->created_at >= FREE_DELIVERY_ENABLED_DATE)
                                                <strong>{{ CURRENCY_SIGN }} {{ number_format(($total_dis - calculatePercentage($total_dis, $cartDetails['voucher_percentage'])), 2) }}</strong>
                                            @else
                                                <strong>{{ CURRENCY_SIGN }} {{ number_format((($order['delivery_charges']+$total_dis) - calculatePercentage($total_dis, $cartDetails['voucher_percentage'])), 2) }}</strong>
                                            @endif
                                        @else
                                            <strong>{{ CURRENCY_SIGN }} {{number_format(($order['delivery_charges']+$total_dis), 2)}}</strong>
                                        @endif  
                                    </strong>
                                </div>
                                </div>
                            </div>  
                        </div>                  
                    @else
                        <div class="text-center mb-4 h4 text-gray">Order not found!.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
@stop

@section('scripts')
@stop