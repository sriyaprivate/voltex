<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<style>
    table{
        font-size:8px;
    }
    
    .full-border{
        border: 1px solid #000;
        width: 99%
    }
    
    .border{
        border-color: #000;
        border-style: solid;
    }
    .price{
        text-align: center;
    }
</style>
<body>
@if(isValid($order))
    <!-- <img src="{{url('assets/images/invoice-header.jpg')}}"> -->
    <div style="margin-left: -20px !important;">
        <table>
            <tr>
                <td style="text-align:left;width:20%;">
                    <strong>Order ID</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td style="width: 30%;">
                    @if(count($order) > 0)
                        {{ $order->order_no }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:left;width:20%;">
                    <strong>Payment Transaction No</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td style="width: 50%;">
                    @if(count($order) > 0 && isValid($order['payment']) && isValid($order['payment']['transaction']))
                        {{ $order['payment']['transaction']->transaction_no }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align:left;width:20%;">
                    <strong>Order Date</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td>
                    @if(count($order) > 0 && isValid($order->order_date))
                        {{ $order->order_date }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:left;width:20%;">
                    <strong>Status</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td>
                    @if(count($order) > 0)
                        @if($order->status == 0)
                            <span class="badge badge-custome pending">PENDING</span>
                        @elseif($order->status == 1)
                            <span class="badge badge-custome approve">PAID</span>
                        @elseif($order->status == 2)
                            <span class="badge badge-custome rejected">REJECTED</span>
                        @elseif($order->status == 8)
                            <span class="badge badge-custome approved">ADVANCE</span>
                        @endif
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align:left;width:20%;">
                    <strong>Customer</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td>
                    @if(isValid($order))
                        {{$order->customer->username}}
                    @else
                        -
                    @endif
                </td>

                <td style="text-align:left;width:20%;">
                    <strong>Email</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td style="text-align:left">
                    @if(isValid($order) && isValid($order->customer))
                        {{ $order->customer->email }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align:left;width:20%;">
                    <strong>Contact No</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td>
                    @if(isValid($order) && isValid($order->customer->phone_no))
                        {{ $order->customer->phone_no }}
                    @else
                        -
                    @endif
                </td>
                <td style="text-align:left;width:20%;">
                    <strong>Billing Address</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td>
                    @if(isValid($order) && isValid($order->billing_address))
                        {!! strExpImp(trim($order->billing_address), ',', ',<br>&nbsp;') !!}
                    @else
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align:left;width:20%;">
                    <strong>Payment Date</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td>
                    @if(isValid($order) && isValid($order->paid_date))
                        {{ $order->paid_date }}
                    @else
                        -
                    @endif
                </td>
            </tr>
            @if(isset($cartDetails) && count($cartDetails) > 0 && 
            isset($cartDetails['voucher_code']) && 
            isset($cartDetails['voucher_type']) && 
            $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
            isset($cartDetails['voucher_percentage']) && 
            !empty($cartDetails['voucher_percentage'])) 
            <tr>
                <td style="text-align:left;width:20%;">
                    <strong>Others</strong>
                </td>
                <td style="width: 2%"> : </td>
                <td>
                    @if(isValid($order) && isValid($order['paymentMethod']) && isValid($order['paymentMethod']->card_name))
                        {{ $cartDetails['voucher_type_name'] }} code is {{ $cartDetails['voucher_code'] }} | {{ $cartDetails['voucher_percentage'] }}% off from total
                    @else
                        -
                    @endif
                </td>
            </tr>
            @endif
        </table>
    </div>
    <br/>
    <table class="full-border">
        <tr style="background-color:#000;line-height: 3">
            <th style="color:#fff;width: 5%">#</th>
            <th style="color:#fff;">Code</th>
            <th style="color:#fff;width:25%">Item</th>
            <th style="color:#fff;">Quantity</th>
            <th style="color:#fff;">Unit Price (Rs.)</th>
            <th style="color:#fff;">Discount (Rs.)</th>
            <th style="color:#fff;">Discounted Price</th>
        </tr>
        <?php 
            $i  = 1;
            $total_discount = 0;
            $total_gross_amount = 0;
            $total_amount = 0;
            $total_without_delivery_charge = 0;
        ?>
        @if(isValid($order['details']))
            @foreach ($order['details'] as $detail)
                
                <?php $pro_dis_price; ?>

                @if(isValid($detail['discounted_price']))
                    <?php 
                    $pro_dis_price = $detail['price'] - (($detail['price'] * $detail['discounted_price'])/100); 
                    ?>
                @endif
                <tr style="line-height: 2">
                    <td>{{$i}}</td>
                    <td class="text-right">{{$detail['product']->code}}</td>
                    <td>{{$detail['product']->display_name}}</td>
                    <td>{{$detail['qty']}}</td>
                    <td class="price" style="text-align: right;">{{number_format($detail['price'],2)}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="price">
                        @if(isValid($detail['discounted_price']))
                            {{ $detail['discounted_price'] }}
                        @else
                            -
                        @endif
                    </td>

                    <td class="price">
                        @if(isValid($detail['discounted_price']))
                            <?php 
                            $discounted_item_price = $detail['price'] - $detail['discounted_price'] 
                            ?>
                            {{ number_format($discounted_item_price, 2) }}
                        @else
                            {{ number_format($detail['price'],2) }}
                        @endif
                    </td>
                    
                    <?php 
                        $total_gross_amount += $detail['qty'] * $detail['price']; 
                    ?>

                    @if(isValid($detail['discounted_price']))
                        <?php $total_discount += $detail['qty'] * $detail['discounted_price'] ?>
                    @endif
                </tr>
            @endforeach
        @endif

        <?php $total_without_delivery_charge +=  ($total_gross_amount - $total_discount) ?>
        <?php $total_amount +=  ($total_gross_amount - $total_discount) + $order->delivery_charges ?>
    </table>

    <div></div>
    <br/>
    <table class="">
        <tr style="line-height: 2">
            <td colspan="6" style="text-align:right"><strong>Gross Amount (Rs.)</strong></td>
            <td style="text-align:right"><strong>{{ number_format($total_gross_amount, 2) }}</strong></td>
        </tr>
        <tr style="line-height: 2">
            <td colspan="6" style="text-align:right"><strong>Total Discount (Rs.)</strong></td>
            <td style="text-align:right"><strong> -{{ number_format($total_discount, 2) }}</strong></td>
        </tr>
        @if(isset($cartDetails) && count($cartDetails) > 0 && 
        isset($cartDetails['voucher_code']) && 
        isset($cartDetails['voucher_type']) && 
        $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
        isset($cartDetails['voucher_percentage']) && 
        !empty($cartDetails['voucher_percentage']))
            <tr style="line-height: 2">
                <td colspan="6" style="text-align:right"><strong>{{ $cartDetails['voucher_type_name'] }} ({{ $cartDetails['voucher_percentage'] }}%)</strong></td>
                <td style="text-align:right"><strong>-{{ number_format(calculatePercentage($total_without_delivery_charge, $cartDetails['voucher_percentage']), 2) }}</strong></td>
            </tr>
        @endif

        <tr style="line-height: 2">
            <td colspan="6" style="text-align:right;"><strong>Total Amount (Rs.)</strong></td>
            <td style="text-align:right;border-bottom-style:double;border-top-style:solid;">

                @if(isset($order) && count($order) > 0 && isset($order->delivery_charges) && !empty($order->delivery_charges) && $order->delivery_charges !== '0.00')
                    @if(FREE_DELIVERY_ENABLED == true && checkRule($total_without_delivery_charge, FREE_DELIVERY_RULE, FREE_DELIVERY_OPARETOR))
                        @if(isset($cartDetails) && count($cartDetails) > 0 && 
                            isset($cartDetails['voucher_code']) && 
                            isset($cartDetails['voucher_type']) && 
                            $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                            isset($cartDetails['voucher_percentage']) && 
                            !empty($cartDetails['voucher_percentage']))

                            <strong>{{ CURRENCY_SIGN }} {{ number_format((($total_without_delivery_charge - calculatePercentage($total_without_delivery_charge, $cartDetails['voucher_percentage']))), 2) }}</strong>
                        @else
                            <strong>{{ CURRENCY_SIGN }} {{ number_format($total_without_delivery_charge, 2) }}</strong>
                        @endif                            
                    @else
                        @if(isset($cartDetails) && count($cartDetails) > 0 && 
                            isset($cartDetails['voucher_code']) && 
                            $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                            isset($cartDetails['voucher_percentage']) && 
                            !empty($cartDetails['voucher_percentage']))

                            <strong>{{ CURRENCY_SIGN }} {{ number_format(((($total_without_delivery_charge - calculatePercentage($total_without_delivery_charge, $cartDetails['voucher_percentage'])) + $order->delivery_charges)), 2) }}</strong>
                        @else
                            <strong>{{ CURRENCY_SIGN }} {{ number_format(($total_without_delivery_charge + $order->delivery_charges), 2) }}</strong>
                        @endif                            
                    @endif
                @else
                    @if(isset($cartDetails) && count($cartDetails) > 0 && 
                        isset($cartDetails['voucher_code']) && 
                        isset($cartDetails['voucher_type']) && 
                        $cartDetails['voucher_type'] == VOUCHER_PERCENTAGE && 
                        isset($cartDetails['voucher_percentage']) && 
                        !empty($cartDetails['voucher_percentage']))

                        <strong>{{ CURRENCY_SIGN }} {{ number_format(($total_without_delivery_charge - calculatePercentage($total_without_delivery_charge, $cartDetails['voucher_percentage'])), 2) }}</strong>
                    @else
                        <strong>{{ CURRENCY_SIGN }} {{ number_format(($total_without_delivery_charge), 2) }}</strong>
                    @endif
                @endif
            </td>
        </tr>
    </table>
@endif
</body>
</html>
<!-- <img style="bottom:0;" src="{{url('assets/images/invoice-footer.jpg')}}"> -->