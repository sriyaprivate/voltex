@extends('layouts.master') @section('title','Wishlist')

@section('links')
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
@stop

@section('css')
@stop

@section('content')
  <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="#">My Account</a></li>
                <li class="breadcrumb-item active">Wishlist</li>
            </ol>

        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->


  <!-- account-section -->
    <div class="account-section mb-4">
        <div class="container">

            <div class="row profile">
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">            
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic text-center">
                            <img src="{{ url('assets/images/'.DEFAULT_USER_IMAGE) }}" class="img-responsive" alt="{{ isValid($user['customer']->first_name)}}">
                        </div>
                        <!-- END SIDEBAR USERPIC -->

                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                {{ isValid($user->customer->first_name) }} {{ isValid($user->customer->last_name) }}
                            </div>
                            <div class="profile-usertitle-job">
                                {{ isValid($user->customer->email) }}<br>
                                {{ isValid($user->customer->contact_no) }}
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
            
                        <!-- SIDEBAR BUTTONS -->
                        <!-- <div class="profile-userbuttons">
                          <button type="button" class="btn btn-success btn-sm">Follow</button>
                          <button type="button" class="btn btn-danger btn-sm">Message</button>
                        </div> -->
                        <!-- END SIDEBAR BUTTONS -->

                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/order-history')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                                </li>
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> Wishlist</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-bitbucket" aria-hidden="true"></i> Bulk Quotations</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9 mb-3">
                    <div class="card product-list position-relative m--2">
                        <div class="panel-refresh" name="panel-refresh"></div>
                        <div class="card-header border-bottom-0 bg-gray">
                            <h6 class="h6 text-uppercase mt-2 font-weight-bold">My Wishlist</h6>
                        </div>
                        <div class="account-wishlist cart-body pl-4 pr-4 mb-4 mt-1">
                            @if(count($wishlists)>0)
                                @foreach($wishlists as $wishlist)
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 position-relative" style="padding-top: 15px">
                                        @if(count($wishlist) > 0 && isset($wishlist->discount))
                                            <span class="badge badge-danger rounded-badge" style="right: 26px;top: 0px;">
                                                {{ number_format($wishlist->discount)?:'-' }}%<br>OFF
                                            </span>
                                        @endif
                                        @if(isset($wishlist->product->image))
                                            <img src="{{ url('data/product_thumb/images/product/'.$wishlist->product->image->image) }}" class="img-fluid w-100 border" alt="Responsive image">
                                        @else
                                            <img src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" class="img-fluid w-100 border" alt="{{ $wishlist->product->display_name?:'-' }}">
                                        @endif
                                    </div>
                                
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 mt-3-mobile d-flex align-items-center">
                                        <h6 class="mb-3 text-uppercase"><a href="{{ route(isValid($wishlist->product->status) && $wishlist->product->status == DEAL_PROMOTION? 'saving-center.item.view' : 'bigstore.item.view', ['item_name' => str_slug($wishlist->product->display_name), 'item_id' => $wishlist->product->id]) }}">{{ $wishlist->product->display_name?:'-' }}</a></h6>
                                    </div>
                                
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 d-flex align-items-center">
                                        <h6 class="text-uppercase text-right product-price">
                                            @if(isValid($wishlist->discount))
                                                <span><strong class="f14">{{ CURRENCY_SIGN }} {{ number_format((isValid($wishlist->product->product_price->mrp) * (100 - isValid($wishlist->discount)) / 100), 2)?: '-' }}</strong></span>
                                                <br>
                                                <span class="text-secondary">
                                                    <del class="pr-1">{{ CURRENCY_SIGN }} {{ number_format(isValid($wishlist->product->product_price->mrp), 2)?: '-' }}</del>
                                                </span>
                                            @else
                                                <span><strong class="f14 pr-1">{{ CURRENCY_SIGN }} {{ number_format(isValid($wishlist->product->product_price->mrp), 2)?: '-' }}</strong></span>
                                            @endif 
                                        </h6>
                                    </div>
                                
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 d-flex align-items-center">
                                        <div class="w-100 text-right text-left-mobile">
                                            <form action="{{ route('cart.add', ['product_id' => isValid($wishlist->product->id)]) }}" method="get" id="form_{{ isValid($wishlist->id) }}"  class="product_form">
                                                <input type="hidden" name="prev_url" value="{{ Request::getQueryString() }}">
                                                <div class="cd-customization">
                                                    <input type="hidden" name="qty" class="form-control form-control-sm" value="1">
                                                    <button type="submit" name="action" value="cart" class="btn btn-sm btn-primary"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                                                    <button type="submit" name="action" value="buy" class="btn btn-sm btn-primary">Buy it Now</button>
                                                    <button class="btn btn-sm btn-primary" type="button" onclick="return window.open('{{ route('account.wishlist.remove', isValid($wishlist->product->id)) }}', '_self')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>                        
                                @endforeach
                            @else
                                <div class="h3 text-secondary mt-4 text-center">There no item in wishlist.</div>
                                <div class="h6 text-secondary text-center">click <a href="{{ route('bigstore.index') }}" class="text-primary">here</a> to add item to wishlist</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- /.container -->
    </div> <!-- /.account-section -->
@stop

@section('js')
<script>
    $(document).ready(function(){
        removePanelRefresh();
    });
</script>
@stop

@section('scripts')
@stop