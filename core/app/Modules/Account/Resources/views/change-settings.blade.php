@extends('layouts.master') @section('title','Change Settings')

@section('links')
<!-- select2 -->
<link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
    <style type="text/css">
        .help-block{
            color: red;
        }
    </style>
@stop

@section('content')
  <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="#">My Account</a></li>
                <li class="breadcrumb-item active">Change Settings</li>
            </ol>
        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->


  <!-- account-section -->
    <div class="account-section mb-2">
        <div class="container">

            <div class="row profile">
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">
            
                    <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic text-center">
                            <img src="{{ url('assets/images/'.DEFAULT_USER_IMAGE) }}" class="img-responsive" alt="{{ isValid($user['customer']->first_name)}}">
                        </div>
                    <!-- END SIDEBAR USERPIC -->

                    <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                {{$user->customer->first_name?:'-'}} {{$user->customer->last_name?:'-'}}
                            </div>
                            
                            <div class="profile-usertitle-job">
                                {{$user->customer->email?:'-'}}<br>
                                {{$user->customer->phone_no?:'-'}}
                            </div>
                        </div>
                    <!-- END SIDEBAR USER TITLE -->

                    <!-- SIDEBAR BUTTONS -->
                        <!-- <div class="profile-userbuttons">
                            <button type="button" class="btn btn-success btn-sm">Follow</button>
                            <button type="button" class="btn btn-danger btn-sm">Message</button>
                        </div>   -->
                    <!-- END SIDEBAR BUTTONS -->
            
                    <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                  <a class="nav-link" href="{{url('account/order-history')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> Wishlist</a>
                                </li>
                                <li class="active nav-item">
                                  <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-bitbucket" aria-hidden="true"></i> Bulk Quotations</a><!-- {{url('account/bulk-quotations')}} -->
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a> <!-- {{url('account/feedback')}} -->
                                </li>
                            </ul>
                        </div>
                    <!-- END MENU -->
                    </div>
                </div>
                
                <div class="col-md-9 mb-3">
                    <div class="profile-content mb-3">
                        <h6 class="text-uppercase m-0">Change Settings</h6>
                    </div>
                    <hr>

                    <form method="POST" action="{{ url('account/change-settings') }}" id="form-setting-change">
                        {{ csrf_field() }}
                        <input type="hidden" name="customer_id" id="customer_id" value="{{$user->customer->id}}">

                        <div class="row">

                            <div class="col-md-6 mb-3 {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label class="required-control-label">First Name</label>
                                <input type="text" class="form-control form-control-sm" placeholder="First name" name="first_name" id="first_name" value="{{ Request::old('first_name')?:$user->customer->first_name}}">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                        
                            <div class="col-md-6 mb-3 {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label class="required-control-label">Last Name</label>
                                <input type="text" class="form-control form-control-sm" placeholder="Last name" name="last_name" id="last_name" value="{{Request::old('last_name')?:$user->customer->last_name}}">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6 mb-3 {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="required-control-label">Email</label>
                                <input type="text" class="form-control form-control-sm" placeholder="Enter Your Email" name="email" id="email" value="{{Request::old('email')?:$user->customer->email}}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                            <div class="col-md-3 mb-3">
                                <label class="required-control-label">Phone No</label>
                                <input type="number" class="form-control form-control-sm" placeholder="Enter Your Phone No" name="phone_no" id="phone_no" value="{{Request::old('phone_no')?:$user->customer->phone_no}}" maxlength="10" minlength="10">
                                @if ($errors->has('phone_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                          
                            <div class="col-md-3 mb-3 {{ $errors->has('mobile_no') ? ' has-error' : '' }}">
                                <label>Mobile No</label>
                                <input type="number" class="form-control form-control-sm" placeholder="Enter Your Mobile No" name="mobile_no" id="mobile_no" value="{{Request::old('mobile_no')?:$user->customer->mobile_no}}" maxlength="10" minlength="10">
                                @if ($errors->has('mobile_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 mb-3 {{ $errors->has('mobile_no') ? ' has-error' : '' }}">
                                <label class="required-control-label">Address Line 1</label>
                                <input type="text" class="form-control form-control-sm" placeholder="Enter Address Line 1" name="address_line_1" id="address_line_1" value="{{Request::old('address_line_1')?:$user->customer->address_line_1}}">
                                @if ($errors->has('address_line_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label>Address Line 2</label>
                                <input type="text" class="form-control form-control-sm" placeholder="Enter Address Line 1" name="address_line_2" id="address_line_2" value="{{Request::old('address_line_2')?:$user->customer->address_line_2}}">
                                @if ($errors->has('address_line_2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6 mb-3 {{ $errors->has('city') ? ' has-error' : '' }}">
                                <label class="required-control-label">City</label>
                                <select name="city" id="city" class="js-example-basic-single select2-hidden-accessible">
                                    <option value="">-- city --</option>
                                    @if(count($cities) > 0)
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}" data-postal-code="{{ $city->postal_code }}" {{ isValid($user->customer) && isValid($user->customer->city) && isValid($user->customer->city->id) && $user->customer->city->id == $city->id? 'selected' : '' }}>{{ $city->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                          
                            <div class="col-md-6 mb-3 {{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                <label class="required-control-label">ZIP/Postal Code</label>
                                <input type="text" class="form-control form-control-sm" placeholder="Enter Your ZIP/Postal Code" name="postal_code" id="postal_code" value="{{ isValid($user->customer) && isValid($user->customer->city) && isValid($user->customer->city->postal_code)? $user->customer->city->postal_code : '' }}" readonly>
                                @if ($errors->has('postal_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postal_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6 mb-3 {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <input id="password" type="password" class="form-control form-control-sm" placeholder="Enter password" name="password" id="password">

                                @if($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                          
                            <div class="col-md-6 mb-3">
                                <label>Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control form-control-sm" placeholder="Enter password again" name="password_confirmation">
                            </div>
                        </div>
                        
                        <input type="hidden" name="next_url" value="{{ Request::get('next_url') }}">
                        <button id="btn-update" class="btn btn-sm btn-primary float-right cursor-not-allowed" type="submit" disabled><i class="fa fa-pencil" aria-hidden="true"></i> Update</button>
                    </form>

                </div>
            </div>

        </div> <!-- /.container -->
    </div> <!-- /.account-section -->
@stop

@section('js')
<script>
    $(document).ready(function(){
        // In your Javascript (external .js resource or <script> tag)
        $('.js-example-basic-single').select2();    
        
        $('#form-setting-change').on('change keyup keydown keypress', 'input, select', function(e){
            var pettern = new RegExp('([0-9]{10})');
            
            if($('#first_name').val().trim().length > 0 &&
                $('#last_name').val().trim().length > 0 &&
                $('#email').val().trim().length > 0 &&
                $('#address_line_1').val().trim().length > 0 &&
                $('#phone_no').val().trim().length > 0 && pettern.test($('#phone_no').val()) &&
                $('#city option:selected').val().length > 0 &&
                $('#postal_code').val().trim().length > 0){
                    $('#btn-update').prop('disabled', false);
                    $('#btn-update').removeClass('cursor-not-allowed');
            }else{
                $('#btn-update').prop('disabled', true);
                $('#btn-update').addClass('cursor-not-allowed');
            }
        });
    });

    //submit for
    $('#btn-update').click(function(){
        $('#form-setting-change').submit();
    });

    //set postal_code
    $('#city').on('change', function(){
        var city = $('option:selected', this);
        $(this).closest('.row').find('#postal_code').val(city.data('postal-code'));
    });
</script>
@stop

@section('scripts')
<!-- select2 -->
<script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
@stop