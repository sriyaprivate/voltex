@extends('layouts.master') @section('title','Ecommerce | Order History')

@section('links')
    <!-- Gijgo -->
    <link href="{{asset('assets/libraries/Gijgo/gijgo.css')}}" rel="stylesheet" type="text/css" />

    <!-- select2 -->
    <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
 
@stop

@section('content')
    <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="#">My Account</a></li>
                <li class="breadcrumb-item">Order History</li>
                <li class="breadcrumb-item active">Payment Transaction History</li>
            </ol>
        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->

    <!-- account-section -->
    <div class="account-section mb-2">
        <div class="container">

            <div class="row profile">
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">

                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic text-center">
                            <img src="{{ url('assets/images/'.DEFAULT_USER_IMAGE) }}" class="img-responsive" alt="{{ isValid($user->customer->first_name)}}">
                        </div>
                        <!-- END SIDEBAR USERPIC -->

                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                {{$user->customer->first_name}} {{$user->customer->last_name}}
                            </div>
                            <div class="profile-usertitle-job">
                                {{$user->customer->email}}<br>
                                {{$user->customer->contact_no}}
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->

                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav flex-column">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('account/order-history')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> Wishlist</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/bulk-quotations')}}"><i class="fa fa-bitbucket" aria-hidden="true"></i> Bulk Quotations</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/feedback')}}"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
            
                    </div>
                </div>
        
                <div class="col-md-9 mb-3">
                    <div class="profile-content mb-2">
                        <h6 class="text-uppercase m-0">Order History</h6>
                    </div>    
                    <hr>

                    <div class="mt-4">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">#</th>
                                    <th class="text-center">Order ID</th>
                                    <th class="text-center">Transaction No</th>
                                    <th>Amount</th>
                                    <th class="text-center">Message</th>
                                    <th class="text-center">Time</th>
                                    <th width="10%" class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($transactions) > 0)
                                    @foreach ($transactions as $key => $transaction)
                                        <tr>
                                            <td class="text-center">{{ ++$key }}</td>
                                            <td class="text-center">{{ $transaction->order_no?:'-' }}</td>
                                            <td class="text-center">{{ $transaction->transaction_no?:'-' }}</td>
                                            <td class="text-right">{{ $transaction->paid_amount > 0? $transaction->paid_amount :'-' }}</td>
                                            <td class="text-center">{{ $transaction->error_message?:'-' }}</td>
                                            <td class="text-center">{{ $transaction->created_at?:'-' }}</td>
                                            <td class="text-center">{!! $transaction->status == 1? '<span class="fa fa-check text-success"></span> Success' : '<span class="fa fa-times text-danger"></span> Failded' !!}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" class="p-2" style="text-align: center">
                                            <div>No Transaction found for this order!.</div>
                                        </td>
                                    </td>
                                @endif
                            
                            </tbody>
                        </table>
                    </div>                    

                </div>
            </div>

        </div> <!-- /.container -->
    </div> <!-- /.account-section -->
@stop

@section('js')
   
@stop

@section('scripts')
   
@stop