@extends('layouts.master') @section('title','Bulk Quotation View')

@section('links')
@stop

@section('css')
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="#">My Account</a></li>
        <li class="breadcrumb-item"><a href="#">Bulk Quotations</a></li>
        <li class="breadcrumb-item active">Bulk Quotation View</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="account-section mb-2">
    <div class="container">

      <div class="row profile">
        <div class="col-md-3 mb-3">
          <div class="profile-sidebar">

            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic text-center">
              <img src="https://i1.wp.com/gocartoonme.com/wp-content/uploads/cartoon-avatar.png?resize=300%2C350" class="img-responsive" alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->

            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
              <div class="profile-usertitle-name">
                Nishan O A R
              </div>
              <div class="profile-usertitle-job">
                nishanran@gmail.com<br>
                +94 71 378 9660
              </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->

            <!-- SIDEBAR BUTTONS -->
            <!-- <div class="profile-userbuttons">
              <button type="button" class="btn btn-success btn-sm">Follow</button>
              <button type="button" class="btn btn-danger btn-sm">Message</button>
            </div> -->
            <!-- END SIDEBAR BUTTONS -->
            
            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/order-history')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> Wishlist</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                </li>
                <li class="active nav-item">
                  <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-bitbucket" aria-hidden="true"></i> Bulk Quotations</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                </li>
              </ul>
            </div>
            <!-- END MENU -->
          </div>
        </div>
        <div class="col-md-9 mb-3">
          <div class="profile-content mb-3">
            <h6 class="text-uppercase m-0">Bulk Quotation View</h6>
          </div>

          <hr>

          <div class="mt-1 mb-3">
            <div class="row">
              <div class="col-3 font-weight-bold">Quotation No:</div>
              <div class="col-9">Q12354</div>
            </div>
            <div class="row">
              <div class="col-3 font-weight-bold">Sent Date:</div>
              <div class="col-9">15/10/2017</div>
            </div>
          </div>

          <div class="mt-2 mb-3">
            <table class="table table-sm table-striped table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Product</th>
                  <th>ProductCode</th>
                  <th>Qty.</th>
                  <th>UnitPrice</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center">1</td>
                  <td>Fan</td>
                  <td>EL-123</td>
                  <td class="text-center">52</td>
                  <td class="text-right">1,000</td>
                  <td class="text-right">52,000</td>
                </tr>
                <tr>
                  <td class="text-center">2</td>
                  <td>Frock</td>
                  <td>AC-789</td>
                  <td class="text-center">18</td>
                  <td class="text-right">700</td>
                  <td class="text-right">12,600</td>
                </tr>
              </tbody>
            </table>
          </div>

          <form>
            <div class="form-group row">
              <label class="col-sm-10 col-form-label text-right">Total</label>
              <div class="col-sm-2">
                <input type="text" readonly class="form-control form-control-sm text-right" value="64,000.00">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12">
                <button class="btn btn-primary btn-sm float-right" type="submit"><i class="fa fa-check" aria-hidden="true"></i> Accept Quotation</button>
              </div>
            </div>
          </form>

        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
@stop

@section('scripts')
@stop