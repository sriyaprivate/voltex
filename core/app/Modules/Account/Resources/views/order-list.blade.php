@extends('layouts.master') @section('title','Order History')

@section('links')
    <!-- Gijgo -->
    <link href="{{asset('assets/libraries/Gijgo/gijgo.css')}}" rel="stylesheet" type="text/css" />

    <!-- select2 -->
    <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
    <style type="text/css">
        .footer-wrap {
            bottom: -32.5% !important;
        }

        .cart{
            margin: 90px -25px 0px 0 !important;
        }

        .performa-invoice-cart{
            margin: 90px -25px 0px 0 !important;
        }
    </style>
@stop

@section('content')
    
    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="#">My Account</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Order History</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <div class="row profile">
                <div class="col-md-2 mb-3">
                    <div class="profile-sidebar">

                        <!-- SIDEBAR MENU -->
                        <div class="">
                            <ul class="nav flex-column" style="list-style-type: none;">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('account/order-list')}}"><i class="fa fa-history" aria-hidden="true"></i> Order History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/wishlist')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> My Cart</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('account/change-settings')}}"><i class="fa fa-cog" aria-hidden="true"></i> change settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="Javascript:void(0);"><i class="fa fa-rss" aria-hidden="true"></i> Feedback</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END MENU -->
            
                    </div>
                </div>
        
                <div class="col-md-10 mb-3" style="font-size: 12px">

                    <form class="mb-2">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="text-uppercase">Order No</label>
                                <input type="text" class="form-control form-control-sm" name="order_no" id="order_no" placeholder="Enter Order No">
                            </div>
                          
                            <div class="col-md-4">
                                <label class="text-uppercase">Order Date</label>
                                <input type="text" class="form-control form-control-sm" id="datepicker" name="datepicker" placeholder="Enter Order Date">
                            </div>

                            <div class="col-md-4">
                                <label class="text-uppercase">Status</label>
                                <select class="js-example-basic-single" name="status" id="status">
                                    <option value="">All</option>
                                    <option value="{{NEWLY_CREATED}}">NEWLY_CREATED</option>
                                    <option value="{{CUSTOMER_CONFIRMED}}">CUSTOMER_CONFIRMED</option>
                                    <option value="{{CUSTOMER_REJECTED}}">CUSTOMER_REJECTED</option>
                                    <option value="{{APPROVE_PENDING}}">APPROVE_PENDING</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-sm float-right mt-3-mobile" type="submit"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                            </div>
                        </div>
                    </form>

                    <div class="mb-3 overflow-x-scroll">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th width="20%">OrderID</th>
                                    <th>Country</th>
                                    <th>Customer</th>
                                    <th>Amount</th>
                                    <th width="15%" class="text-center">Order Date</th>
                                    <!-- <th class="text-center">Delivery Date</th> -->
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Created By</th>
                                    <th width="10%" class="text-center" colspan="3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($orders)>0)
                                    @foreach ($orders as $order)
                                        <tr>
                                            <td><a href="" style="cursor: pointer !important;">{{$order['order_no']}}</a></td>
                                            <td>{{$order['country']->name}}</td>

                                            <td class="text-center">
                                                @if($order['customer'])
                                                    {{$order['customer']->username}}
                                                    </br>
                                                    <span class="badge badge-primary">{{$order['customer']->code}}</span>
                                                    </br>
                                                    <span class="badge badge-primary">{{$order['customer']->email}}</span>
                                                @endif
                                            </td>
                                            
                                            <td class="text-right">{{number_format($order['amount'],2)}}</td>
                                            
                                            <td class="text-center">{{$order['order_date']}}</td>
                                            <!-- <td class="text-center">
                                                @if($order['delivery_date'])
                                                    {{$order['delivery_date']}}
                                                @endif
                                            </td> -->

                                            <td class="text-center">
                                                @if($order['status'] == NEWLY_CREATED)
                                                    <span class="badge badge-custome pending">NEWLY_CREATED</span>
                                                @elseif($order['status'] == CUSTOMER_CONFIRMED)
                                                    <span class="badge badge-custome approve">CUSTOMER_CONFIRMED</span>
                                                @elseif($order['status'] == CUSTOMER_REJECTED)
                                                    <span class="badge badge-custome rejected">CUSTOMER_REJECTED</span>
                                                @elseif($order['status'] == APPROVE_PENDING)
                                                    <span class="badge badge-custome preparing">APPROVE_PENDING</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($order['user'])
                                                    {{$order['user']->username}}
                                                @endif
                                            </td>
                                            <td>
                                                <center>
                                                    @if($order['status'] == 0 || $order['status'] == 2)
                                                        <a style="font-size:15px;" data-toggle="tooltip" title="View order" href="{{url('account/order-view')}}/{{$order['id']}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    @else
                                                        <a style="font-size:15px;" data-toggle="tooltip" title="View order" href="{{url('account/order-view')}}/{{$order['id']}}" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    @endif                                                        
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    @if($order['status'] == APPROVE_PENDING)
                                                        <a href="{{ route('account.confirm-performa-invoice', ['order_no' => $order['id']]) }}" style="font-size:15px;" data-toggle="tooltip" title="Click to confirm performa invoice." role="button"><i class="fa fa-check" aria-hidden="true"></i></a>
                                                    @else
                                                        <a href="#" style="font-size:15px;color: silver" data-toggle="tooltip" title="Disabled." role="button"><i class="fa fa-check" aria-hidden="true"></i></a>
                                                    @endif
                                                </center>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" style="text-align: center">No Orders</td>
                                    </td>
                                @endif
                            
                            </tbody>
                        </table>
                    </div>                    

                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <!-- Gijgo -->
    <script src="{{asset('assets/libraries/Gijgo/gijgo.js')}}" type="text/javascript"></script>

    <!-- select2 -->
    <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'yyyy-mm-dd'
            });

            // In your Javascript (external .js resource or <script> tag)
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        });
</script>
@stop