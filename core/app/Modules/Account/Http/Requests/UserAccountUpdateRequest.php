<?php

namespace App\Modules\Account\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use URL;

class UserAccountUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $url = parse_url(URL::previous());

        if(count($url) > 0 && isset($url['query']) && !empty($url['query'])){
            $query = explode("&", $url['query']);

            //check validation if request is POST else return  empty array
            if($this->method() == 'POST'){
                if(count($query) > 0){
                    foreach($query as $parameter) {
                        $param = explode("=", $parameter);
                        if(!empty($param) && count($param) == 2 && $param[1] == DIRECT){
                            
                            $id = Auth::id();
                            
                            if(!empty($this->input('delivery_form.first_name'))){
                                return [
                                    'delivery_form.first_name'     => 'required',
                                    'delivery_form.last_name'      => 'required',
                                    // 'delivery_form.email'          => 'required|email|unique:users,email,'.$id.',,deleted_at,NULL',
                                    'delivery_form.phone_no'       => 'required|max:10|min:10',
                                    // 'delivery_form.mobile_no'      => 'max:10|min:10',
                                    'delivery_form.address_line_1' => 'required',
                                    'delivery_form.city'           => 'required',
                                    'delivery_form.postal_code'    => 'required',
                                ];
                            }else{
                                return [
                                    'first_name'     => 'required',
                                    'last_name'      => 'required',
                                    'email'          => 'required|email|unique:users,email,'.$id.',,deleted_at,NULL,user_type_id,USER_TYPE_FRONT',
                                    'phone_no'       => 'required|required|max:10|min:10',
                                    // 'mobile_no'       => 'regex:/[0-9]{10}/',
                                    'address_line_1' => 'required',
                                    'city'           => 'required',
                                    'postal_code'    => 'required',
                                ];
                            }
                        }else if(!empty($param) && count($param) == 2 && $param[1] == PICKUP){
                            return [
                                'pickup.location_id' => 'required'
                            ];
                        }
                    }
                }else{
                    return [
                        
                    ];
                }
            }else{
                return [
                        
                ];
            }
        }else{
            return [
                
            ];
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
