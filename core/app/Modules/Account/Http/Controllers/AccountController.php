<?php

namespace App\Modules\Account\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

use Auth;
use App\Models\Activations;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Wishlist;
use App\Classes\Functions;
use App\Modules\Account\Http\Requests\UserAccountUpdateRequest;
use App\Classes\PdfTemplate;
use App\Modules\Account\Repositories\AccountRepo;
use App\Models\User;
use App\Models\PerformaInvoice;

use PDF;
use QrCode;

use DB;
use File;

class AccountController extends Controller
{
    protected $common    = null;
    private $accountRepo = null;

    function __construct(Functions $common, AccountRepo $accountRepo){
        $this->common      = $common;
        $this->accountRepo = $accountRepo;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('account::index');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function orderHistory(Request $request)
    {
        $usr = auth()->user();
        
        $cust = Customer::where('user_id',$usr->id)->first();

        $usr['customer']=$cust;

        $orders = Order::where('customer_id',$cust->id)->with(['user.customer','customer','deliveryType']);

        if($request->get('order_no')!=''){
            $orders = $orders->where('order_no','like','%'.$request->get('order_no').'%');
        }

        if($request->get('datepicker')!=''){
            $orders = $orders->where('order_date','like','%'.$request->get('datepicker').'%');
        }

        if($request->get('status')!=''){
            $orders = $orders->where('status','=',$request->get('status'));
        }else{
            $orders = $orders->where('status', '!=', PENDING);
        }

        $orders = $orders->get();
        
        return view('account::order-history')->with(['usr'=>$usr,'orders'=>$orders]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function orderView($id)
    {
        $usr = auth()->user();
        
        $order = Order::where('id',$id)->with(['customer', 'location'])->first();
        
        $details = OrderDetail::where('order_id',$id)->with(['product'])->get();

        return view('account::order-view')->with(['usr'=>$usr, 'order'=>$order, 'details'=>$details]);
    }

    public function orderList(Request $request)
    {
        
        $orders = Order::whereNull('deleted_at')->with(['user','customer','country']);

        if($request->get('order_no')!=''){
            $orders = $orders->where('order_no','like','%'.$request->get('order_no').'%');
        }

        if($request->get('datepicker')!=''){
            $orders = $orders->where('order_date','like','%'.$request->get('datepicker').'%');
        }

        if($request->get('status')!='' && $request->get('status')!=0){
            $orders = $orders->where('status','=',$request->get('status'));
        }else

        $orders = $orders->get();
        
        return view('account::order-list')->with(['orders'=>$orders]);
    }

    public function orderDetailView($id)
    {
        $usr = auth()->user();
        
        $cust = Customer::where('user_id',$usr->id)->first();

        $usr['customer']=$cust;

        $order = Order::where('id',$id)->with(['customer','deliveryType','payment.transaction', 'location'])->first();
        
        $details = OrderDetail::where('order_id',$id)->with(['product'])->get();

        return view('account::order-detail-view')->with(['usr'=>$usr, 'order'=>$order, 'details'=>$details]);
    }

    public function orderPrint($id, Request $request)
    {
        // try{
            $type    = $request->input('type')?:'I';
            $order   = Order::where('id',$id)->with(['customer'])->first();

            $details = OrderDetail::where('order_id',$id)->with(['product'])->get();

            $qr = QrCode::format('png')
                ->errorCorrection('L')
                ->size(200)
                ->margin(2)
                ->generate($order->order_no);

            $fileName = 'order/order-'.$order->id.'-'.date('YmdHis').'.pdf';

            $page   =  view('account::order-print')->with(['order' => $order, 'details' => $details, 'qr'=>$qr])->render();

            PDF::reset();PDF::reset();
            //PDF::setFont('myriadpro');
            PDF::setMargins(10,18,10);
            PDF::setAutoPageBreak(TRUE, 32);
            PDF::setHeaderCallback(function($pdf){
                $image_file = url('assets/pi-images/h-img02.png');
                $pdf->Image($image_file, 10, 5, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            });

            PDF::setFooterCallback(function($pdf){
                $image_file = url('assets/pi-images/f-img01.png');
                $pdf->Image($image_file, 10, 265, 195, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            });
            PDF::AddPage();
            PDF::writeHTML($page);
            if (ob_get_contents()) ob_end_clean();
            return PDF::Output(public_path($fileName),'I');
        // }catch(\Exception $e){
        //     return $this->common->redirectWithAlert(
        //         'home.index', 
        //         'error', 
        //         'Error!.', 
        //         $e->getMessage()
        //     ); 
        // }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function wishlist()
    {   
        $user = $this->common->getLoggedUser();

        if(count($user) == 0 && isset($user->id)){
            throw new \Exception("Logged user not found!.");
        }

        $wishlists = $this->accountRepo->getWishlistByUser($user->id);

        return view('account::wishlist')->with(['user' => $user, 'wishlists'=> $wishlists]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function changeSettings(Request $request)
    {   
        $user   = $this->common->getLoggedUser();
        $cities = $this->common->getCities('', 'get');

        return view('account::change-settings', compact('user', 'cities'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function saveChanges(Request $request)
    {
        try{
            $user     = $this->common->getLoggedUser();
            $next_url = $request->input('next_url');
            
            if(count($user) > 0 && count($user->customer) > 0){
                $validator = Validator::make($request->all(), [
                    'first_name'     => 'required',
                    'last_name'      => 'required',
                    'email'          => 'required|email|unique:users,email,'.$user->id.',,deleted_at,NULL,user_type_id,USER_TYPE_FRONT',
                    'phone_no'       => 'required|regex:/[0-9]{10}/',
                    'address_line_1' => 'required',
                    'city'           => 'required',
                    'postal_code'    => 'required',
                    'password'       => 'sometimes|confirmed'
                ]);
        
                if($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
        
                // $customer             = Customer::find($request->input('customer_id'));
                $customer                 = $user->customer;
                
                $customer->first_name     = $request->input('first_name');
                $customer->last_name      = $request->input('last_name');
                $customer->email          = $request->input('email');
                $customer->phone_no       = $request->input('phone_no');
                $customer->mobile_no      = $request->input('mobile_no');
                $customer->address_line_1 = $request->input('address_line_1');
                $customer->address_line_2 = $request->input('address_line_2');
                $customer->city_id        = $request->input('city');
                $customer->city_id        = $request->input('city');
                $customer->save();

                if(count($customer) > 0){
                    $user->username     = $request->input('email');
                    $user->channel_id   = CHANNEL_ONLINE_ID;
                    $user->email        = $request->input('email');
                    
                    if(!empty($request->input('password')))
                    {
                        $activations = Activations::where('user_id' , $user->id)->where('completed', DEFAULT_STATUS)->first();

                        if(count($activations) == 0){
                            $activations               = new Activations();
                            $activations->user_id      = $user->id;
                            $activations->code         = md5($user->id);
                            $activations->completed    = DEFAULT_STATUS;
                            $activations->completed_at = date('Y-m-d h:i:s');
                            $activations->created_at   = date('Y-m-d h:i:s');
                            $activations->updated_at   = date('Y-m-d h:i:s');
                            $activations->save();

                            if(count($activations) > 0){
                                $user->confirmed    = DEFAULT_STATUS;
                                $user->updated_at   = date('Y-m-d h:i:s');
                            }
                        }

                        $user->password     = bcrypt($request->input('password'));
                        $user->user_type_id = USER_TYPE_FRONT;
                    }
        
                    $user = $user->save();
                    
                    if(count($user) > 0){
                        $redirectTo = '';
                        
                        if(isset($next_url)){
                            $redirectTo   = $next_url;
                            $redirectType = 'url';
                        }else{
                            $redirectTo   = 'user.account.info.view';
                            $redirectType = 'route';
                        }
                        
                        return $this->common->redirectWithAlert(
                            $redirectTo,
                            'success', 
                            'Done!.', 
                            'User information has been successfully updated!.',
                            $redirectType
                        ); 
                    }else{
                        throw new \Exception("User couldn't be updated!.");        
                    }
                }
            }else{
                throw new \Exception("Logged user not found!.");
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'user.account.info.view', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function bulkQuotations()
    {
        $user = $this->common->getLoggedUser();
        return view('account::bulk-quotations', compact('user'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function bulkQuotationView()
    {
        $user = $this->common->getLoggedUser();
        return view('account::bulk-quotation-view', compact('user'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function feedback()
    {
        return view('account::feedback');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('account::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('account::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('account::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UserAccountUpdateRequest $request)
    {
        try{            
            $customer                  = Customer::find(Auth::user()->customer->id);
            $customer->first_name      = $request->input('first_name');
            $customer->last_name       = $request->input('last_name');
            $customer->email           = $request->input('email');
            $customer->phone_no        = $request->input('phone_no');
            $customer->mobile_no       = $request->input('mobile_no');
            $customer->address_line_1  = $request->input('address_line_1');
            $customer->address_line_2  = $request->input('address_line_2');
            $customer->city_id         = $request->input('city');
            $customer->save();

            if(count($customer)){
                return redirect()->back()->with([
                    'notification' => 'Saved changes!.', 
                    'option' => [
                        'timeOut'  => '15000', 
                        'type'     => 'success',
                        'position' => 'toast-bottom-right'
                    ]
                ]);
            }else{
                throw new \Exception("Something went wrong!, Customer couldn't be updated!.");
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'user.account.update.info', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * get specific order's transaction history
     * @return view
     */
    public function getOrderTransactionHistory($order_id){
        if(isset($order_id)){
            return $this->paymentTransactionHistory($order_id, $page = 'front');
        }else{
            throw new \Exception("Invalid order no passed!.");
        }
    }

    public function paymentTransactionHistory($order_id, $page = 'front'){
        if(isset($order_id)){
            $transactions = $this->accountRepo->getPaymentTransactionHistoryByOrderId($order_id);
            $user         = $this->common->getLoggedUser();

            if($page == 'front'){
                return view('account::payment.payment-transaction', compact('transactions', 'user'));
            }else{
                return [
                    'transactions' => $transactions,
                    'user'         => $user
                ];
            }
        }else{
            throw new \Exception("Invalid order no passed!.");
        }
    }

    //remove item in wishlist
    public function removeWishlist($product_id){
        try{
            if(isset($product_id) && !empty($product_id)){
                $user = $this->common->getLoggedUser();

                if(count($user) > 0 && isset($user->id)){
                    //delete user wishlist item.
                    //params: customer_id, product_id
                    $wishlist_item = $this->accountRepo->deleteWishlistItem($user->id, $product_id);
                        
                    if($wishlist_item){
                        return $this->common->redirectWithAlert(
                            'account.wishlist', 
                            'success', 
                            'Done!.', 
                            'Item has been successfuly removed from wishlist!.'
                        ); 
                    }else{
                        throw new \Exception("item in wishlist couldn't be delete");    
                    }
                }else{
                    throw new \Exception('Logged user not found!.');    
                }
            }else{
                throw new \Exception('Invalid product id passed for remove wishlist item.');
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'account.wishlist', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    //remove item in wishlist
    public function customerPiConfirmation(Request $request){
        try{
            
            $_pfi = PerformaInvoice::where('order_id',$request->order_no)->first();

            if($_pfi){
                $_pfi->confirmed_date = date('Y-m-d H:i:s');
                $_pfi->status = CUSTOMER_CONFIRMED;

                if($_pfi->save()){
                    $_order = Order::find($request->order_no);

                    $_order->status = CUSTOMER_CONFIRMED;

                    if($_order->save()){
                        return $this->common->redirectWithAlert(
                            'account.order.list', 
                            'success', 
                            'Done!.', 
                            'Performa Invoice now confirmed!.'
                        );
                    }else{
                        throw new \Exception("Order status couldn't update");
                    }
                }else{
                    throw new \Exception("Performa invoice couldn't confirmed");
                }
            }else{
                throw new \Exception("Performa invoice couldn't found");
            }

        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'account.order.list', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }
}
