<?php

Route::group(['prefix' => 'account', 'middleware' => ['web', 'auth'], 'namespace' => 'App\\Modules\Account\Http\Controllers'], function()
{
    /*
    |--------------------------------------------------------------------------
    | Order
    |--------------------------------------------------------------------------
    */
    Route::get('/order-history', [
        'uses'  => 'AccountController@orderHistory',
        'as'    => 'account.order.history'
    ]);
    
    Route::get('/order-view/{id}', 'AccountController@orderView');

    Route::get('order-list', [
        'uses'  => 'AccountController@orderList',
        'as'    => 'account.order.list'
    ]);

    Route::get('/order-detail-view/{id}', 'AccountController@orderDetailView');

    Route::get('order/{order_no}/transaction-history', [
        'uses'  => 'AccountController@getOrderTransactionHistory',
        'as'    => 'user.account.order.transaction.history'
    ]);
    
    Route::get('confirm-performa-invoice', [
        'uses'  => 'AccountController@customerPiConfirmation',
        'as'    => 'account.confirm-performa-invoice'
    ]);
    /*
    |--------------------------------------------------------------------------
    | Wishlist
    |--------------------------------------------------------------------------
    */
    Route::get('/wishlist', [
        'uses' => 'AccountController@wishlist',
        'as'   => 'account.wishlist'
    ]);
    Route::get('/wishlist/remove/{id}', [
        'uses' => 'AccountController@removeWishlist',
        'as'   => 'account.wishlist.remove'
    ]);

    /*
    |--------------------------------------------------------------------------
    | user information change
    |--------------------------------------------------------------------------
    */
    Route::get('/change-settings', [
        'uses'  => 'AccountController@changeSettings',
        'as'    => 'user.account.info.view'
    ]);
    Route::post('/change-settings', 'AccountController@saveChanges');
    Route::post('/update/user', [
        'uses'  => 'AccountController@update',
        'as'    => 'user.account.update.info'
    ]);    
    
    /*
    |--------------------------------------------------------------------------
    | Bulk
    |--------------------------------------------------------------------------
    */
    Route::get('/bulk-quotations', 'AccountController@bulkQuotations');
    Route::get('/bulk-quotation-view', 'AccountController@bulkQuotationView');
    Route::get('/feedback', 'AccountController@feedback');
});

Route::get('account/order-print/{id}{type?}', 'App\\Modules\Account\Http\Controllers\AccountController@orderPrint');