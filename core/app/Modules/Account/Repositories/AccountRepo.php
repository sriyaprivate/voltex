<?php
namespace App\Modules\Account\Repositories;
use App\Models\Payment;
use App\Models\Wishlist;
use DB;

class AccountRepo {
    public function __construct(){

    }

    public function getPaymentTransactionHistoryByOrderId($order_id){
        if(isset($order_id)){

            $order = Payment::select('payment.order_id', 'orders.order_no', 'payment_transaction.*')
                ->join('payment_transaction', function($query){
                    $query->on('payment.id', '=', 'payment_transaction.payment_id')
                        ->where('payment.status', DEFAULT_STATUS)
                        ->where('payment_transaction.error_code', '!=', '')
                        ->whereNull('payment.deleted_at')
                        ->whereNull('payment_transaction.deleted_at');
                })
                ->join('orders', function($query){
                    $query->on('payment.order_id', '=', 'orders.id')
                        ->where('payment.status', DEFAULT_STATUS)
                        ->whereNull('payment.deleted_at')
                        ->whereNull('orders.deleted_at');
                })
                ->where('payment.order_id', $order_id)
                ->where('payment.status', DEFAULT_STATUS)
                ->whereNull('payment.deleted_at')
                ->orderBy('payment.id', 'DESC')
                ->get();
            
            return $order;
        }else{
            throw new \Exception("Invalid order no!.");
        }
    }

    //get wishlist by user
    public function getWishlistByUser($user_id){
        if(count($user_id) > 0 && isset($user_id) && !empty($user_id)){
            $wishlist_item = Wishlist::where('wishlist.user_id',  $user_id)
                ->where('wishlist.status', DEFAULT_STATUS)
                ->whereNull('wishlist.deleted_at')
                ->groupBy('wishlist.product_id')
                ->get();
            
            return $wishlist_item;
        }else{
            return [];
        }
    }

    //remove wishlist item
    //params: customer_id, product_id
    public function deleteWishlistItem($user_id, $product_id){
        if(isset($user_id) && isset($product_id) && !empty($user_id) && !empty($product_id)){
            DB::beginTransaction();

            $wishlist_item = Wishlist::where('user_id', $user_id)
                ->where('product_id', $product_id)
                ->where('status', DEFAULT_STATUS)
                ->whereNull('deleted_at')
                ->delete();

            if(!$wishlist_item){
                DB::rollBack();
                throw new \Exception("item couldn't be delete from wishlist!.");
            }

            DB::commit();
            return $wishlist_item;
        }else{
            throw new \Exception("Invalid params passed for 'deleteWishlistItem' function.");
        }
    }
}