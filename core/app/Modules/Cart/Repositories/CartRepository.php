<?php
/**
 * SHOPPING CART
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-28
 */

Namespace App\Modules\Cart\Repositories;

use App\Modules\Product\Models\Product;
use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Modules\Cart\Entities\CartDetail;
use App\Modules\Cart\Entities\Cart;
use App\Models\ProductDelivery;
use App\Models\CustomerAddresses;
use App\Classes\Functions;
use App\Models\Customer;
use App\Models\DeliveryType;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderTransaction;
use App\Models\Payment;
use App\Models\PaymentTransaction;
use App\Models\Stock;
use App\Models\StockTransaction;
use App\Models\Wishlist;
use App\Models\ProductPromotion;
use Session;
use Auth;
use DB;

class CartRepository{    
    public function __construct(){

    }

    /**
     * Add product to shopping cart
     * @return data
     * @param product_id, qty
     */

    public function getProductById($id){

        if(isset($id) && !empty($id)){
            $toDay          = date('Y-m-d');
            $columns        = "";

            $product = Product::select(
                    'product.id', 
                    'product.code', 
                    'product.name', 
                    'product.display_name', 
                    'product.created_at', 
                    'product.selling_price as price',
                    'product.weight',
                    'product.status',
                    'country.name as country_name',
                    'country.flag as country_flag',
                    'product.country_id',
                    'product_image.image'
                )
                
                ->leftjoin('product_image', function($query){
                    $query->on('product.id', '=', 'product_image.product_id')
                        ->where('product_image.status', 1)
                        ->whereIn('product.status', PRODUCT_CHECK_STATUS)
                        ->whereNull('product_image.deleted_at')
                        ->whereNull('product.deleted_at')
                        ->orderBy('product_image.id', 'DESC');
                })
                ->leftjoin('country', function($query){
                    $query->on('product.country_id', '=', 'country.id');
                });
                
                $product = $product->where('product.id', $id)
                    ->whereIn('product.status', PRODUCT_CHECK_STATUS)
                    ->whereNull('product.deleted_at')
                    ->first();

            return $product; 
        }else{
            throw new \Exception("Invalid product id passed!.");
        }
    }

    //check is product already in cart
    public function isProductExistInCart($product_id, $loggedUser = null){
        if(count($loggedUser) > 0 && isset($loggedUser->id) && !empty($loggedUser->id)){
            $user_id = $loggedUser->id;
            
            if(!empty($product_id) && !empty($user_id)){
                $product = Cart::where('user_id', $user_id)
                    ->where('product_id', $product_id)
                    ->whereIn('status', CART_STATUS)
                    ->whereNull('deleted_at')
                    ->first();
    
                if(count($product) > 0){
                    return true;
                }else{
                    return false;
                }
            }else{
                throw new \Exception("Invalid product ID!.");
            }
        }else{
            throw new \Exception("User not logged in!.");
        }
    }

    //get count of shopping cart by customer_id
    public function getCartCountByCustomer($user_id){
        if(!empty($user_id)){
            return $cart = Cart::where('user_id', $user_id)
                ->where('status', 1)
                ->whereNull('deleted_at')
                ->count();
        }else{
            throw new \Exception("Invalid customer ID!.");
        }
    }

    //get available qty by product id
    public function getAvailableQtyByProductId($product_id){
        if(!empty($product_id)){
            $availableQty = Product::select(
                    'product.code as code', 
                    'product.display_name as display_name', 
                    'product.created_at as product_created_at', 
                    'product.id as product_id', 
                    'stock.id as stock_id', 
                    'stock.status as stock_status', 
                    'stock.warehouse_id as warehouse_id', 
                    'stock.created_at as stock_created_at',
                    DB::raw('SUM(stock.qty) as availableQty'),
                    DB::raw('SUM(product.status) as status')
                )
                ->leftjoin('stock', function($query){
                    $query->on('product.id', '=', 'stock.product_id')
                        ->where('stock.status', DEFAULT_STATUS)
                        ->whereIn('product.status', PRODUCT_CHECK_STATUS)
                        ->whereNull('stock.deleted_at')
                        ->whereNull('product.deleted_at');
                })
                ->where('product.id', $product_id)
                ->first();
            
            return $availableQty;
        }else{
            throw new \Exception("Invalid product ID!.");
        }
    }

    //get item qty in cart table or Session
    public function getItemQtyByProduct($item_id, $loggedUser = null){
        if(!empty($item_id)){
            if(count($loggedUser) > 0 && isset($loggedUser->id)){
                $itemQty = Cart::select('*')
                    ->where('user_id', $loggedUser->id)
                    ->where('product_id', $item_id)
                    ->first();

                return $itemQty;
            }else{
                if(Session::has('cart')){
                    if(count(Session::get('cart')->items)){
                        if(isset(Session::get('cart')->items[$item_id])){
                            return (object) Session::get('cart')->items[$item_id];
                        }
                    }else{
                        throw new \Exception('The "'.$item_id.'" Product is not found in cart!.');
                    }
                }
            }
        }else{
            throw new \Exception("Invalid Item ID!.");
        }
    }

    //get cart's item list
    public function getCartItemListByUser($user, $status = null){
        if($status == null){
            $list = Cart::select(
                    'cart.qty as cart_qty', 
                    'cart.status as cart_item_status', 
                    'product.id', 
                    'product.name', 
                    'product.display_name', 
                    'product.weight', 
                    'product.status', 
                    'product.created_at', 
                    'product.code', 
                    'product.display_name as name', 
                    DB::raw('GROUP_CONCAT(delivery_type.name SEPARATOR ", ") as delivery_options')
                )
                ->join('product', function($query){
                    $query->on('cart.product_id', '=', 'product.id')
                        ->whereIn('cart.status', CART_STATUS)
                        ->whereIn('product.status', PRODUCT_CHECK_STATUS)
                        ->whereNull('cart.deleted_at')
                        ->whereNull('product.deleted_at');
                })
                ->join('stock', function($query){
                    $query->on('cart.product_id', '=', 'stock.product_id')
                        ->whereIn('cart.status', CART_STATUS)
                        ->where('stock.status', DEFAULT_STATUS)
                        ->whereNull('cart.deleted_at')
                        ->whereNull('stock.deleted_at');
                })
                ->leftjoin('product_delivery', function($query){
                    $query->on('product.id', '=', 'product_delivery.product_id')
                        ->whereIn('product.status', PRODUCT_CHECK_STATUS)
                        ->where('product_delivery.status', DEFAULT_STATUS)
                        ->whereNull('product.deleted_at')
                        ->whereNull('product_delivery.deleted_at');
                })
                ->leftjoin('delivery_type', function($query){
                    $query->on('product_delivery.delivery_type_id', '=', 'delivery_type.id')
                        ->where('delivery_type.status', DEFAULT_STATUS)
                        ->where('product_delivery.status', DEFAULT_STATUS)
                        ->whereNull('delivery_type.deleted_at')
                        ->whereNull('product_delivery.deleted_at');
                })
                ->where('cart.user_id', $user->id)
                ->whereIn('cart.status', CART_STATUS)
                ->where('cart.qty', '!=', 0)
                ->havingRaw('SUM(stock.qty) > 0')
                ->whereNull('cart.deleted_at')
                ->groupBy('cart.product_id')
                ->get();
        }else{
            $list = Cart::select(
                    'cart.qty as cart_qty', 
                    'cart.status as cart_item_status', 
                    'product.id', 
                    'product.name', 
                    'product.display_name', 
                    'product.weight', 
                    'product.status', 
                    'product.created_at', 
                    'product.code', 
                    'product.display_name as name', 
                    DB::raw('GROUP_CONCAT(delivery_type.name SEPARATOR ", ") as delivery_options')
                )
                ->join('product', function($query) use($status){
                    $query->on('cart.product_id', '=', 'product.id')
                        ->where('cart.status', $status)
                        ->whereIn('product.status', PRODUCT_CHECK_STATUS)
                        ->whereNull('cart.deleted_at')
                        ->whereNull('product.deleted_at');
                })
                ->join('stock', function($query){
                    $query->on('cart.product_id', '=', 'stock.product_id')
                        ->whereIn('cart.status', CART_STATUS)
                        ->where('stock.status', DEFAULT_STATUS)
                        ->whereNull('cart.deleted_at')
                        ->whereNull('stock.deleted_at');
                })
                ->leftjoin('product_delivery', function($query){
                    $query->on('product.id', '=', 'product_delivery.product_id')
                        ->whereIn('product.status', PRODUCT_CHECK_STATUS)
                        ->where('product_delivery.status', DEFAULT_STATUS)
                        ->whereNull('product.deleted_at')
                        ->whereNull('product_delivery.deleted_at');
                })
                ->leftjoin('delivery_type', function($query){
                    $query->on('product_delivery.delivery_type_id', '=', 'delivery_type.id')
                        ->where('delivery_type.status', DEFAULT_STATUS)
                        ->where('product_delivery.status', DEFAULT_STATUS)
                        ->whereNull('delivery_type.deleted_at')
                        ->whereNull('product_delivery.deleted_at');
                })
                ->where('cart.user_id', $user->id)
                ->where('cart.status', $status)
                ->where('cart.qty', '!=', 0)
                ->havingRaw('SUM(stock.qty) > 0')
                ->whereNull('cart.deleted_at')
                ->groupBy('cart.product_id')
                ->get();
        }

        return $list;
    }

    //get delivery option by item id
    public function getDeliveryOptionByProductId($item_id){
        $list = ProductDelivery::join('delivery_type', function($query){
                $query->on('product_delivery.delivery_type_id', '=', 'delivery_type.id')
                    ->where('delivery_type.status', 1)
                    ->where('product_delivery.status', 1)
                    ->whereNull('delivery_type.deleted_at')
                    ->whereNull('product_delivery.deleted_at');
            })
            ->where('product_delivery.product_id', $item_id)
            ->where('product_delivery.status', 1)
            ->whereNull('product_delivery.deleted_at')
            ->pluck('product_delivery.delivery_type_id');
                
        return $list;
    }

    //save user delivery details in customer_address table
    public function saveCustomerAddressesDetails($request = null){
        if(count($request->all())){
            DB::beginTransaction();

            $cartDetails    = null;
            $first_name     = trim($request->input('delivery_form.first_name'));
            $last_name      = trim($request->input('delivery_form.last_name'));
            $phone_no       = trim($request->input('delivery_form.phone_no'));
            $mobile_no      = trim($request->input('delivery_form.mobile_no'));
            $address_line_1 = trim($request->input('delivery_form.address_line_1'));
            $address_line_2 = trim($request->input('delivery_form.address_line_2'));
            $city_id        = trim($request->input('delivery_form.city'))?:'';
            
            if(!empty($first_name) && !empty($last_name) && !empty($phone_no) && !empty($address_line_1) && !empty($city_id)){
                //save customer delivery details in db
                $customer                 = new CustomerAddresses();
                $customer->user_id        = Auth::id();
                $customer->first_name     = $first_name;
                $customer->last_name      = $last_name;
                $customer->phone_no       = $phone_no;
                $customer->mobile_no      = $mobile_no;
                $customer->address_line_1 = $address_line_1;
                $customer->address_line_2 = $address_line_2;
                $customer->city_id        = $city_id;
                $customer->status         = DEFAULT_STATUS;
                $customer->created_at     = date('Y-m-d h:i:s');
                $customer->updated_at     = date('Y-m-d h:i:s');
                $customer->save();
                // commit all ok and rollBack if error occored
                // params: $array , msg if error occored
                if(count($customer) > 0){
                    $cartDetails  = $this->saveOrUpdateCartDetail($request, $customer);
                }else{
                    DB::rollBack();
                    throw new \Exception("customer couldn't be inserted to 'customer_address' table!.");
                }
            }else{
                $cartDetails  = $this->saveOrUpdateCartDetail($request);
            }

            DB::commit();
            return $cartDetails;
        }else{
            DB::rollBack();
            throw new \Exception("Invalid request pass for 'saveCustomerDeliveryDetails'.");
        }
    }

    //get delivery type id by name
    public function getDeliveryTypeByName($deliveryType){
        if(!empty($deliveryType)){
            $deliveryType = DeliveryType::where('name', 'LIKE', '%'.$deliveryType.'%')
                    ->where('status', DEFAULT_STATUS)
                    ->whereNull('deleted_at')
                    ->first();

            return $deliveryType;
        }else{
            throw new \Exception("Delivery type doen't supplied!.");
        }
    }

    //get delivery type by id
    public function getDeliveryTypeById($deliveryTypeId){
        if(!empty($deliveryTypeId)){
            $deliveryType = DeliveryType::where('id', $deliveryTypeId)
                    ->where('status', DEFAULT_STATUS)
                    ->whereNull('deleted_at')
                    ->first();

            return $deliveryType;
        }else{
            throw new \Exception("Delivery type doen't supplied!.");
        }
    }

    //save cart details 
    public function saveOrUpdateCartDetail($request, $customerAddress = null){
        $cartLogic = new CartLogic();
        $loggedUser =  $cartLogic->getLoggedUser();

        if(count($loggedUser) > 0){
            $deliveryType = $this->getDeliveryTypeByName($request->input('type'));
            
            $cartDetail = CartDetail::where('user_id', $loggedUser->id)->first();
            
            if(count($cartDetail) > 0){
                $cartDetail->user_id         = $loggedUser->id;
                
                if(count($customerAddress) > 0 && !empty($customerAddress->id)){
                    $cartDetail->customer_addresses_id = $customerAddress->id ? : null;
                }else{
                    $cartDetail->customer_addresses_id = null;
                }
                
                $cartDetail->delivery_method = $deliveryType->id ? : null;
                $cartDetail->payment_method  = $cartDetail->payment_method? $cartDetail->payment_method : null;
                $cartDetail->stage           = ORDER_STAGES[2]; //delivery & pickup
                $cartDetail->status          = DEFAULT_STATUS;
                $cartDetail->updated_at      = date('Y-m-d H:i:s');
                $cartDetail->save();

                return $cartDetail;
            }else{
                $cartDetail                  = new CartDetail(); 
                $cartDetail->user_id         = $loggedUser->id;

                if(count($customerAddress) > 0 && !empty($customerAddress->id)){
                    $cartDetail->customer_addresses_id = $customerAddress->id ? : null;
                }else{
                    $cartDetail->customer_addresses_id = null;
                }

                $cartDetail->delivery_method = $deliveryType->id ? : null;
                // $cartDetail->payment_method  = null;
                $cartDetail->stage           = ORDER_STAGES[2]; //delivery & pickup
                $cartDetail->status          = DEFAULT_STATUS;
                $cartDetail->save();

                return $cartDetail;
            }
        }else{
            throw new \Exception("User not logged in!.");
        }
    }

    //update cart_detail table's payment method column
    //params: payment_method_id
    public function saveOrderPaymentMethod($option = null){
        //option is something like this "1_2" 1st element is cart_installment id and 2nd is bank_cart id
        if(isset($option) && count($option) > 0){
            DB::beginTransaction();

            $cartLogic = new CartLogic();
            $loggedUser = $cartLogic->getLoggedUser();

            if(count($loggedUser) > 0){
                $cartDetail = $this->getCartDetails($loggedUser->id);

                if(count($cartDetail) > 0){
                    $cartDetail->payment_method     = isset($option[0])? $option[0] : NULL;
                    $cartDetail->installment_method = isset($option[1])? $option[1] : NULL;
                    $cartDetail->updated_at         = date('Y-m-d h:i:s');
                    $cartDetail->save();

                    if(count($cartDetail) > 0){
                        DB::commit();
                        return $cartDetail;
                    }else{
                        DB::rollBack();
                        throw new \Exception("cart_detail table can not be updated!.");      
                    }
                }
            }else{
                throw new \Exception("Something went wrong, User must be logged in!.");    
            }
        }else{
            throw new \Exception("Couldn't updated cart_detail table's payment_method column!.");
        }
    }

    //get user selected cart details e.g:- delivery method, payment method
    public function getCartDetails($user_id = null, $joinType = null, $order_id = null){
        if(!empty($user_id)){
            $cartDetails = CartDetail::select(
                    'cart_detail.*', 
                    'delivery_type.name as delivery_method_name', 
                    'bank_card.card_name as payment_method_name', 
                    'bank_card.image as image_path',
                    'bank_card.payment_gateway',
                    'location.city_id as regional_center_city_id',
                    'location.name as regional_center_name',
                    'location.address as regional_center_address',
                    'location.contact_no as regional_center_contact_no',
                    'card_installment.merchant_id as installment_merchant_id',
                    'card_installment.interest_rate',
                    'bank_card.id as bank_card_id',
                    'voucher.code as voucher_code',
                    'voucher.type as voucher_type',
                    'voucher.percentage as voucher_percentage',
                    'voucher.value as voucher_value',
                    'voucher.voucher_type_id',
                    'voucher.status as voucher_active',
                    DB::raw('IF(voucher.period_to >= CURDATE(), 0, 1) as coupon_is_expired'),
                    'voucher.voucher_status',
                    'voucher.period_from as voucher_period_from',
                    'voucher.period_to as voucher_period_to',
                    'voucher_type.name as voucher_type_name'
                )
                ->join('delivery_type', function($query){
                    $query->on('delivery_type.id', '=', 'cart_detail.delivery_method')
                        ->where('cart_detail.status', DEFAULT_STATUS)
                        ->whereNull('cart_detail.deleted_at');
                })
                ->leftjoin('bank_card', function($query){
                    $query->on('bank_card.id', '=', 'cart_detail.payment_method')
                        ->where('bank_card.status', DEFAULT_STATUS)
                        ->whereNull('bank_card.deleted_at');
                });

                if(isset($joinType) && !empty($joinType) && $joinType == 'with_order' && !empty($order_id)){
                    $cartDetails = $cartDetails->leftjoin('orders', function($query) use($order_id){
                        $query->on('cart_detail.user_id', '=', 'orders.user_id')
                            ->where('cart_detail.status', DEFAULT_STATUS)
                            ->where('orders.id', $order_id)
                            ->whereNull('cart_detail.deleted_at')
                            ->whereNull('orders.deleted_at');
                    })->leftjoin('voucher', function($query){
                        $query->on('orders.voucher_id', '=', 'voucher.id');
                    });
                }else{
                    $cartDetails = $cartDetails->leftjoin('voucher', function($query){
                        $query->on('cart_detail.voucher_id', '=', 'voucher.id')
                            ->where('cart_detail.status', DEFAULT_STATUS)
                            ->where('voucher.status', DEFAULT_STATUS)
                            ->whereNull('cart_detail.deleted_at')
                            ->whereNull('voucher.deleted_at');
                    });
                }

                $cartDetails = $cartDetails->leftjoin('voucher_type', function($query){
                    $query->on('voucher.voucher_type_id', '=', 'voucher_type.id')
                        ->where('voucher_type.status', DEFAULT_STATUS)
                        ->whereNull('voucher_type.deleted_at');
                })
                ->leftjoin('location', function($query){
                    $query->on('cart_detail.location_id', '=', 'location.id')
                        ->where('location.status', DEFAULT_STATUS)
                        ->where('cart_detail.status', DEFAULT_STATUS)
                        ->whereNull('location.deleted_at')
                        ->whereNull('cart_detail.deleted_at');
                })
                ->leftjoin('card_installment', function($query){
                    $query->on('cart_detail.installment_method', '=', 'card_installment.id')
                        ->where('card_installment.status', DEFAULT_STATUS)
                        ->where('cart_detail.status', DEFAULT_STATUS)
                        ->whereNull('card_installment.deleted_at')
                        ->whereNull('cart_detail.deleted_at');
                })
                ->where('cart_detail.user_id', $user_id)
                ->orderBy('cart_detail.id', 'DESC')
                ->first();

            return $cartDetails;
        }else{
            throw new \Exception("User must be logged in to get cart details");
        }
    }

    //get delivery information
    //params: customer_address_table_id is NULL mean customer table address and if not get address from customer_address table according to the customer_address_table_id
    public function getDeliveryInformation($customer_address_table_id){
        if(isset($customer_address_table_id)){
            $delivery_info = CustomerAddresses::select('customer_addresses.*', 'city.name as city', 'city.postal_code', 'users.email')
                ->join('city', function($query){
                    $query->on('city.id', '=', 'customer_addresses.city_id')
                        ->where('city.status', DEFAULT_STATUS)
                        ->whereNull('city.deleted_at');
                })
                ->leftjoin('users', function($query){
                    $query->on('users.id', '=', 'customer_addresses.user_id')
                        ->where('users.status', DEFAULT_STATUS)
                        ->where('customer_addresses.status', DEFAULT_STATUS)
                        ->whereNull('users.deleted_at')
                        ->whereNull('customer_addresses.deleted_at');
                })
                ->where('customer_addresses.id', $customer_address_table_id)
                ->where('customer_addresses.status', DEFAULT_STATUS)
                ->whereNull('customer_addresses.deleted_at')
                ->orderBy('customer_addresses.id', 'DESC')
                ->first();

            return $delivery_info;
        }else{
            $cartLogic = new CartLogic();
            $loggedUser = $cartLogic->getLoggedUser();

            if(count($loggedUser) > 0 && isset($loggedUser->id)){
                $delivery_info = Customer::select('customer.*', 'city.name as city', 'city.postal_code')
                    ->leftjoin('city', function($query){
                        $query->on('city.id', '=', 'customer.city_id')
                            ->where('city.status', DEFAULT_STATUS)
                            ->whereNull('city.deleted_at');
                    })
                    ->where('customer.user_id', $loggedUser->id)
                    ->orderBy('customer.id', 'DESC')
                    ->first();

                return $delivery_info;
            }else{
                throw new \Exception('Logged user not found!.');
            }
        }
    }

    //change status of cart items
    //params: ids and status
    public function changeCartItemStatus($ids, $status){
        if(count($ids) > 0 && !empty($status)){
            $ids = explode(',', $ids);

            if(count($ids)){
                $items = Cart::whereIn('product_id', $ids)
                    ->whereNull('deleted_at')
                    ->update([
                        'status'     => $status,
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);

                return $items;
            }else{
                throw new \Exception("Ids array cannot be empty!.");    
            }
        }else{
            throw new \Exception("Ids array or status invalid!.");
        }
    } 

    //reset status of carts item
    public function resetCartItemsStatus($loggedUser, $status){
        if(count($loggedUser) > 0 && isset($loggedUser->id)){
            $items = Cart::where('user_id', $loggedUser->id)->update([
                'status'     => $status,
                'updated_at' => date('Y-m-d h:i:s')
            ]);

            return $items;
        }else{
            return [];
        }
    }

    //get last order ID
    public function getLastOrderId($prefix){
        if(!empty($prefix)){
            // $order_no = Order::where('order_no', 'LIKE', '%'.$prefix.'%')
            //     ->orderBy('order_no', 'DESC')
            //     ->orderBy('id', 'DESC')
            //     ->first();

            $order_no = Order::orderBy('id', 'DESC')->first();
            
            return $order_no;
        }else{
            throw new \Exception("Order prefix must me provide for get last order ID!.");
        }
    }

    public function saveCountryOrder($data, $action, $default_status = null)
    {
        
        $nextOrderNo          = null;
        $cartLogic            = new CartLogic();
        $cart                 = $data['cart'];
        $loggedUser           = $cartLogic->getLoggedUser();
        $delivery_address     = null;

        $tmp = $data['cart']->items;

        $countr_order = [];

        $_tmp = '';

        foreach($tmp as $item){
            $country_order[$item['item']->country_id][] = $item;
        }

        if(count($cart) > 0){
            if(count($loggedUser) > 0){
                DB::beginTransaction();

                $nextOrderNo = $cartLogic->getNextOrderNo();
    
                if(empty($nextOrderNo)){
                    $this->removeOrderInSession($loggedUser);
                    throw new \Exception("New Order no couln't created!.");
                }

                foreach ($country_order as $key => $_order) {

                    $order                        = new Order();
                    $order->country_id            = $key;
                    $order->order_no              = $nextOrderNo;
                    $order->amount                = 0;
                    $order->customer_id           = $loggedUser->id;
                    $order->user_id               = $loggedUser->id;
                    $order->delivery_type_id      = 0;
                    $order->voucher_id            = 0;
                    $order->order_date            = date('Y-m-d h:i:s');
                    $order->status                = $default_status?: NEWLY_CREATED;

                    $billing_address  = '';
                    $delivery_address = '';

                    $order->billing_address  = $billing_address;
                    $order->delivery_address = $delivery_address;
                    $order->delivery_date    = null;
                    $order->created_at       = date('Y-m-d h:i:s');
                    $order->updated_at       = date('Y-m-d h:i:s');
                    $order->save();

                    $_amount = 0;

                    if(count($order)>0){
                        foreach ($_order as $item) {
                            if(isset($item['item']) && count($item['item']) > 0){
                                
                                $stock_type = MAIN_STOCK;

                                $orderDetail                   = new OrderDetail();
                                $orderDetail->order_id         = $order->id;
                                $orderDetail->product_id       = $item['item']['id'];
                                $orderDetail->qty              = $item['qty'];
                                $orderDetail->price            = $item['price'];
                                $orderDetail->discounted_price = $item['item']['discount'];
                                $orderDetail->stock_type       = $stock_type;
                                $orderDetail->status           = DEFAULT_STATUS;
                                $orderDetail->created_at       = date('Y-m-d h:i:s');
                                $orderDetail->updated_at       = date('Y-m-d h:i:s');
                                $orderDetail->save();

                                $_amount += $item['qty']*$item['price'];

                                if(count($orderDetail) > 0){
                                    DB::commit();
                                }else{
                                    DB::rollBack();
                                    throw new \Exception("Couldn't saved order details!.");    
                                }
                            }else{
                                throw new \Exception("Cannot save order details, cart has no items.");    
                            }
                        }

                        $order->amount=$_amount;
                        $order->save();

                        $orderTransaction             = new OrderTransaction();
                        $orderTransaction->order_id   = $order->id;
                        $orderTransaction->status     = NEWLY_CREATED;
                        $orderTransaction->reference  = PENDING_TEXT;
                        $orderTransaction->action_by  = $loggedUser->id;
                        $orderTransaction->created_at = date('Y-m-d h:i:s');
                        $orderTransaction->updated_at = date('Y-m-d h:i:s');
                        $orderTransaction->save();
        
                        if(count($orderTransaction) > 0){
                            DB::commit();
                            //save order in session
                            //$this->saveOrderInSession($loggedUser, $order);
                            $_tmp = $order;                    
                        }else{
                            DB::rollBack();
                            throw new \Exception("Order transaction couldn't be saved!.");        
                        }
                    }else{
                        throw new \Exception("Order couldn't be saved!.");    
                    }
                }
                return $_tmp;
            }else{
                throw new \Exception("Cannot created new order no!.");
            }
        }else{
            throw new \Exception("Items not found in shopping cart!.");
        }
    }

    //save order
    public function saveOrder($data, $action, $default_status = null){
        $nextOrderNo          = null;
        $cartLogic            = new CartLogic();
        $cart                 = $data['cart'];
        $loggedUser           = $cartLogic->getLoggedUser();
        $delivery_address     = null;
        
        if(count($cart) > 0){
            if(count($loggedUser) > 0){
                
                DB::beginTransaction();
    
                //get order no
                $nextOrderNo = $cartLogic->getNextOrderNo();
    
                if(empty($nextOrderNo)){
                    $this->removeOrderInSession($loggedUser);
                    throw new \Exception("New Order no couln't created!.");
                }
                
                //order create or update according to the $action
                if($action == 'create'){
                    $order                        = new Order();
                    $order->order_no              = $nextOrderNo;
                    $order->amount                = $cart->subTotal?:0;
                    $order->customer_id           = $loggedUser->id;
                    $order->user_id               = $loggedUser->id;
                    $order->delivery_type_id      = 0;
                    $order->voucher_id            = 0;
                    $order->order_date            = date('Y-m-d h:i:s');
                    $order->status                = $default_status?: NEWLY_CREATED;
                }else{
                    throw new \Exception("Something went wrong!.");
                }

                $billing_address  = '';
                $delivery_address = '';

                $order->billing_address  = $billing_address;
                $order->delivery_address = $delivery_address;
                $order->delivery_date    = null;
                $order->created_at       = date('Y-m-d h:i:s');
                $order->updated_at       = date('Y-m-d h:i:s');
                $order->save();

        
                if(count($order) > 0){
                    if(count($cart) > 0 && count($cart->items)){
                        foreach($cart->items as $item){
                            if(isset($item['item']) && count($item['item']) > 0 && isset($item['availableQty']) && $item['availableQty'] !== '0'){
                                $isInStock = $this->getAvailableQtyByProductId($item['id']);
    
                                if(isset($isInStock)){
                                    if($isInStock['status'] == MAIN_STOCK){
                                        $stock_type = MAIN_STOCK;
                                    }else if($isInStock['status'] == PROMO_STOCK){
                                        $stock_type = PROMO_STOCK;
                                    }else{
                                        $stock_type = MAIN_STOCK;
                                    }
    
                                    $orderDetail                   = new OrderDetail();
                                    $orderDetail->order_id         = $order->id;
                                    $orderDetail->product_id       = $item['item']['id'];
                                    $orderDetail->qty              = $item['qty'];
                                    $orderDetail->price            = $item['price'];
                                    $orderDetail->discounted_price = $item['item']['discount'];
                                    $orderDetail->stock_type       = $stock_type;
                                    $orderDetail->status           = DEFAULT_STATUS;
                                    $orderDetail->created_at       = date('Y-m-d h:i:s');
                                    $orderDetail->updated_at       = date('Y-m-d h:i:s');
                                    $orderDetail->save();
    
                                    if(count($orderDetail) > 0){
                                        DB::commit();
                                    }else{
                                        DB::rollBack();
                                        throw new \Exception("Couldn't saved order details!.");    
                                    }
                                }else{
                                    throw new \Exception("Item not in stock!.");
                                }
                            }
                        }
                    }else{
                        throw new \Exception("Cannot save order details, cart has no items.");    
                    }
    
                    //save order transaction table
                    $orderTransaction             = new OrderTransaction();
                    $orderTransaction->order_id   = $order->id;
                    $orderTransaction->status     = NEWLY_CREATED;
                    $orderTransaction->reference  = PENDING_TEXT;
                    $orderTransaction->action_by  = $loggedUser->id;
                    $orderTransaction->created_at = date('Y-m-d h:i:s');
                    $orderTransaction->updated_at = date('Y-m-d h:i:s');
                    $orderTransaction->save();
    
                    if(count($orderTransaction) > 0){
                        DB::commit();
                        //save order in session
                        //$this->saveOrderInSession($loggedUser, $order);
                        return $order;                    
                    }else{
                        DB::rollBack();
                        throw new \Exception("Order transaction couldn't be saved!.");        
                    }
                }else{
                    throw new \Exception("Order couldn't be saved!.");    
                }
            }else{
                throw new \Exception("Cannot created new order no!.");
            }
        }else{
            throw new \Exception("Items not found in shopping cart!.");
        }
    }

    //deleted order tables
    //params: order record
    public function deleteOrder($order){
        if(count($order) > 0){
            DB::beginTransaction();

            $deleted_order = Order::where('id', $order['id'])->forceDelete();
            
            if($deleted_order){
                $deleted_order_detail = OrderDetail::where('order_id', $order['id'])->forceDelete();

                if($deleted_order_detail){
                    $deleted_order_transaction = OrderTransaction::where('order_id', $order['id'])->forceDelete();

                    if($deleted_order_transaction){
                        DB::commit();
                        return $order;
                    }else{
                        DB::rollBack();
                        throw new \Exception("Order transaction could'n be deleted!.");
                    }
                }else{
                    DB::rollBack();
                    throw new \Exception("Order detail could'n be deleted!.");
                }
            }
        }else{
            DB::rollBack();
            throw new \Exception("Order instant not found!.");
        }
    }

    //save order in session
    //params: logged user, order record
    public function saveOrderInSession($user, $order){
        if(count($user) > 0 && count($order) > 0){
            Session::put('user_'.$user->id, $order);
            Session::save();
        }else{
            throw new \Exception("Invalid parameters passed!.");
        }
    }

    //get order in session
    //params: logged user
    public function getOrderFromSession($user, $attr = null){
        if(count($user) > 0){
            if(isset($attr)){
                return Session::get('user_'.$user->id)[$attr];
            }else{
                return Session::get('user_'.$user->id);
            }
        }else{
            throw new \Exception("Invalid parameters passed!.");
        }
    }

    //remove order in session
    //params: logged user
    public function removeOrderInSession($user){
        if(count($user) > 0){
            Session::forget('user_'.$user->id);
            //Session::save();
        }else{
            throw new \Exception("user must be provide to remove order in session!.");
        }
    }

    //save payment in payment table
    //params: data array
    public function savePayment($data){
        if(count($data) > 0){   
            DB::beginTransaction();
            $payment = null;

            if(count($data) && isset($data['order_id'])){
                $payment = $this->getPaymentByOrderId($data['order_id']);

                if(count($payment) > 0){
                    $payment = $payment;
                }else{
                    $payment = new Payment();
                }
            }else{
                throw new \Exception("Order id not found!.");
            }

            if($payment == null){
                throw new \Exception("Payment variable cannot be null!.");
            }

            $payment->invoice_id      = !empty($data['invoice_id'])? $data['invoice_id'] : null;
            $payment->order_id        = $data['order_id']?:'';
            $payment->payment_type_id = $data['payment_type_id']?:'';
            $payment->payment_status  = $data['payment_status'];
            $payment->amount          = $data['payment_amount']?:'';
            $payment->created_at      = $data['current_date']?:'';
            $payment->updated_at      = $data['current_date']?:'';
            $payment->save();

            if(count($payment) > 0){
                DB::commit();
                return $payment;
            }else{
                DB::rollBack();
                throw new \Exception("Something went wrong!, payment couldn't saved!.");    
            }
        }else{
            throw new \Exception("Invalid data passed for save payment!.");
        }
    }

    //update payment
    public function updatePayment($order, $paidAmount){
        if(count($order) > 0 && isset($order['id']) && !empty($order['id'])){   
            if(isset($paidAmount)){
                DB::beginTransaction();

                $payment                 = Payment::where('order_id', $order['id'])->orderBy('id', 'DESC')->first();
                $payment->amount         = $paidAmount;
                $payment->payment_status = APPROVED;
                $payment->save();

                if(count($payment) > 0){
                    DB::commit();
                    return $payment;
                }else{
                    DB::rollBack();
                    throw new \Exception("Something went wrong!, payment couldn't updated!.");    
                }   
            }else{
                throw new \Exception("Invalid paid amount passed!.");
            }
        }else{
            throw new \Exception("Order not found!.");
        }
    }

    public function updatePaymenTransaction($payment_id, $paidAmount){
        if(isset($payment_id) && !empty($payment_id)){   
            if(isset($paidAmount)){
                DB::beginTransaction();

                $paymentTransaction              = PaymentTransaction::where('payment_id', $payment_id)->orderBy('id', 'DESC')->first();
                $paymentTransaction->paid_amount = $paidAmount;
                $paymentTransaction->updated_at  = date('Y-m-d h:i:s');
                $paymentTransaction->save();

                if(count($paymentTransaction) > 0){
                    DB::commit();
                    return $paymentTransaction;
                }else{
                    DB::rollBack();
                    throw new \Exception("Something went wrong!, payment transaction couldn't updated!.");    
                }   
            }else{
                throw new \Exception("Invalid paid amount passed!.");
            }
        }else{
            throw new \Exception("Invalid payment id!.");
        }
    }

    //deleted items in cart by status
    public function removeItemInCartByStatus($status, $user = null){
        if(isset($status)){
            $cartLogic  = new CartLogic();

            if($user){
                $loggedUser = $cartLogic->getLoggedUser($user);
            }else{
                $loggedUser = $cartLogic->getLoggedUser();
            }

            if(isset($loggedUser) && count($loggedUser) > 0 && !empty($loggedUser->id)){
                //delete items from cart which item was successfully bought 
                $deleted    = Cart::where('user_id', $loggedUser->id)->where('status', $status)->forceDelete();

                if(count($loggedUser) > 0 && !empty($loggedUser->id)){
                    //first use coupon code not shout auto filled after secound order
                    $updateCartDetails = CartDetail::where('user_id', $loggedUser->id)->update([
                        'voucher_id' => NULL,
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                }

                return $deleted;
            }else{
                throw new \Exception("User was not logged into the system");
            }
        }else{
            throw new \Exception("Status not provied!.");
        }
    }

    //get order by no
    //prams: order no
    public function getOrderByNo($orderNo){
        return $orderNo;
        if(!empty($orderNo)){
            $order = Order::select(
                    'orders.*',
                    'orders.created_at as order_created_at',
                    'payment.order_id as payment_order_id',
                    'payment.amount as payment_amount',
                    'payment.payment_type_id',
                    'payment.payment_status',
                    'payment.created_at as payment_created_at',
                    'payment.updated_at as payment_updated_at'
                )
                ->join('payment', function($query){
                    $query->on('orders.id', '=', 'payment.order_id')
                        ->where('payment.status', DEFAULT_STATUS)
                        ->whereNull('payment.deleted_at')
                        ->whereNull('orders.deleted_at');
                })
                ->where('orders.order_no', $orderNo)
                ->orderBy('orders.id', 'DESC')
                ->first();
                
            if(count($order) > 0 && isset($order->id)){
                $orderDetail = OrderDetail::where('order_id', $order->id)
                    ->where('status', DEFAULT_STATUS)
                    ->whereNull('deleted_at')
                    ->get();

                if(count($orderDetail) > 0){
                    return [
                        'order'         => $order,
                        'order_details' => $orderDetail
                    ];
                }else{
                    throw new \Exception("Order_detail not found!.");
                }
            }else{
                return [];
            }
        }else{
            throw new \Exception("Order_no not found!.");
        }
    }

    //change order status
    public function updateOrder($order_id, $status){
        
        if(!empty($order_id) && !empty($status)){
            DB::beginTransaction();

            $order         = Order::where('id', $order_id)->first();
            $order->status = APPROVED;
            $order->save();

            if(count($order) > 0){
                //get logged user
                $cartLogic  = new CartLogic();
                $loggedUser = $cartLogic->getLoggedUser($order->user_id);

                if(count($loggedUser) > 0 && isset($loggedUser->id)){

                    //save order transation
                    $orderTransaction             = new OrderTransaction();
                    $orderTransaction->order_id   = $order_id;
                    $orderTransaction->status     = APPROVED;
                    $orderTransaction->reference  = APPROVED_TEXT;
                    $orderTransaction->action_by  = $loggedUser->id;
                    $orderTransaction->created_at = date('Y-m-d h:i:s');
                    $orderTransaction->updated_at = date('Y-m-d h:i:s');
                    $orderTransaction->save();

                    if(count($orderTransaction) > 0){
                        DB::commit();
                        return $order;
                    }else{
                        DB::rollBack();
                        throw new \Exception("Order transaction couldn't be updated!.");        
                    }
                }else{
                    DB::rollBack();
                    throw new \Exception("User not must be logged in!.");        
                }
            }else{
                DB::rollBack();
                throw new \Exception("Order status couldn't be changed!.");    
            }
        }else{
            DB::rollBack();
            throw new \Exception("Order id or status not found!.");
        }
    }

    //get payment by order id
    //params: order id
    public function getPaymentByOrderId($order_id){
        if(isset($order_id) && !empty($order_id)){
            $payment = Payment::where('order_id', $order_id)
                ->where('status', DEFAULT_STATUS)
                ->whereNull('deleted_at')
                ->orderBy('id', 'DESC')
                ->first();
            
            return $payment;
        }else{
            throw new \Exception("Order id not found!.");
        }
    }

    //get delivery types
    public function getDeliveryType($status = null){
        $deliveryType = DeliveryType::whereNull('deleted_at');

        if(isset($status) && $status !== null){
            $deliveryType = $deliveryType->where('status', $status);
        }

        return $deliveryType = $deliveryType->orderBy('id', 'ASC')->get();
    }

    //get order details
    public function getOrderDetails($orderId){
        if(isset($orderId)){
            $orderDetails = OrderDetail::where('order_id', $orderId)
                ->where('status', DEFAULT_STATUS)
                ->whereNull('deleted_at')
                ->get();
            
                return $orderDetails;
        }else{
            throw new \Exception("Invalid Order ID passed 'getOrderDetail' function.");
        }
    }

    //reduce item stock
    //params: product id, reduce qty
    public function reduceItemStock($product_id, $qty, $warehouse_id = MAIN_WAREHOUSE){
        if(isset($product_id) && isset($qty) && !empty($qty)){
            DB::beginTransaction();

            $productStock = Stock::where('product_id', $product_id)->where('warehouse_id', $warehouse_id)->orderBy('id', 'DESC')->first();

            if(count($productStock) > 0 && isset($productStock->qty) && $productStock->qty >= $qty){
                $updatedQty        = ($productStock->qty - $qty);
                $productStock->qty = $updatedQty;
            }else{
                throw new \Exception("Something went wrong, Request quantity is greater than stock quantity.");        
            }

            $productStock->updated_at = date('Y-m-d h:i:s');
            $productStock->save();

            if(count($productStock)){
                $promotionStock = ProductPromotion::select('product_promotion.*', 'promotion.promotion_type', 'product_promotion.allocate_qty')
                    ->join('promotion', 'product_promotion.promotion_id', '=', 'promotion.id')
                    ->where('product_promotion.product_id', $product_id)
                    ->where('promotion.promotion_type', DEAL_PROMOTION)
                    ->where('promotion.status', DEFAULT_STATUS)
                    ->where('product_promotion.status', DEFAULT_STATUS)
                    ->where('product_promotion.promotion_status', DEFAULT_STATUS)
                    ->whereNull('promotion.deleted_at')
                    ->whereNull('product_promotion.deleted_at')
                    ->first();
                    
                if(count($promotionStock) > 0 && 
                    isset($promotionStock->promotion) && 
                    count($promotionStock->promotion) > 0 && 
                    $promotionStock->promotion->promotion_type == DEAL_PROMOTION){
                    
                    $promotionStock->sold_qty = ($promotionStock->sold_qty + $qty);
                    $promotionStock->updated_at   = date('Y-m-d h:i:s');
                    $promotionStock->save();

                    if(count($promotionStock) > 0){
                        //save stock transaction
                        $stockTransaction                   = new StockTransaction();
                        $stockTransaction->stock_id         = $productStock->id;
                        $stockTransaction->remark           = $qty.' of quantity has been reduced from stock and promotion stock.';
                        $stockTransaction->qty              = $qty;
                        $stockTransaction->type             = 1;
                        $stockTransaction->transaction_date = date('Y-m-d h:i:s');
                        $stockTransaction->created_at       = date('Y-m-d h:i:s');
                        $stockTransaction->save();

                        if(count($stockTransaction) > 0){
                            DB::commit();
                            return $productStock;
                        }else{
                            DB::rollBack();
                            throw new \Exception("Stock transaction couldn't be saved!.");        
                        }
                    }else{
                        DB::rollBack();
                        throw new \Exception("Couldn;t updated qty in product_promotion tb.");        
                    }
                }else{
                    //save stock transaction
                    $stockTransaction                   = new StockTransaction();
                    $stockTransaction->stock_id         = $productStock->id;
                    $stockTransaction->remark           = $qty.' of quantity has been reduced from stock.';
                    $stockTransaction->qty              = $qty;
                    $stockTransaction->type             = 1;
                    $stockTransaction->transaction_date = date('Y-m-d h:i:s');
                    $stockTransaction->created_at       = date('Y-m-d h:i:s');
                    $stockTransaction->save();

                    if(count($stockTransaction) > 0){
                        DB::commit();
                        return $productStock;
                    }else{
                        DB::rollBack();
                        throw new \Exception("Stock transaction couldn't be saved!.");        
                    }
                }
            }else{
                DB::rollBack();
                throw new \Exception("Stock couldn't be changed!.");    
            }
        }else{
            throw new \Exception("Passed invalid parameters to 'reduceItemStock' function!.");    
        }
    }

    //reduce the stock
    //params: order_id
    public function reductStock($orderId){
        if(isset($orderId) && !empty($orderId)){
            $orderDetails = $this->getOrderDetails($orderId);
            $reduced      = null;
            if(isset($orderDetails) && count($orderDetails)){
                foreach($orderDetails as $detail){
                    if(count($detail) && isset($detail->product_id) && !empty($detail->product_id) && isset($detail->qty)){
                        //reduce item stock
                        //params: product id, reduce qty
                        $reduced = $this->reduceItemStock($detail->product_id, $detail->qty);
                    }else{
                        throw new \Exception("Order detail not found!.");    
                    }
                }

                if($reduced !== null){
                    return $reduced;
                }else{
                    throw new \Exception("Something went wrong!, stock couldn't reduced!.");            
                }
            }else{
                throw new \Exception("Order details not found!.");    
            }
        }else{
            throw new \Exception("Invalid Order ID passed 'reduceStock' function.");
        }
    }

    //add item to wishlist
    public function addItemToWishlist($user_id, $product_id){
        if(isset($user_id) && isset($product_id) && !empty($user_id) && !empty($product_id)){
            DB::beginTransaction();
            
            $wishlist              = new Wishlist();
            $wishlist->user_id     = $user_id;
            $wishlist->product_id  = $product_id;
            $wishlist->date        = date('y-m-d H:i:s');
            $wishlist->save();

            if(count($wishlist) > 0){
                DB::commit();
                return $wishlist;
            }else{
                DB::rollBack();
            }
        }else{
            throw new \Exception("Invalid params passed for 'addItemToWishlist'.");
        }
    }

    //update regional center
    public function updateCartRegionalCenter($regional_center_id){
        if(isset($regional_center_id) && !empty($regional_center_id)){
            $cartLogic  = new CartLogic();
            $loggedUser =  $cartLogic->getLoggedUser();

            if(isset($loggedUser)){
                $cart_detail = CartDetail::where('user_id', $loggedUser->id)
                                ->where('status', DEFAULT_STATUS)
                                ->whereNull('deleted_at')
                                ->orderBy('id', 'DESC')
                                ->first();

                $cart_detail->location_id = $regional_center_id;
                $cart_detail->updated_at  = date('Y-m-d h:i:s');
                $cart_detail->save();

                return $cart_detail;
            }else{
                throw new \Exception('logged user not found!.');
            }
        }else{
            throw new \Exception('Regional Center not found!.');
        }
    }

    public function getCartCustomer(){
        $lg =  Auth::getUser()->id;
        $customer = Customer::where('user_id', $lg)
                ->where('status', 1)
                ->whereNull('deleted_at')
                ->first();

        return $customer;
    }

}