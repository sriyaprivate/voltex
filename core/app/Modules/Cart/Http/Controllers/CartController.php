<?php
/**
 * SHOPPING CART
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-28
 */

namespace App\Modules\Cart\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Classes\Functions;
use App\Classes\DeliveryChargesCalculator;

use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Modules\Cart\Repositories\CartRepository;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Account\Http\Requests\UserAccountUpdateRequest;

//payment gateway
use App\Classes\Payment\HnbPaymentGateway;
use App\Classes\Payment\AmexPaymentGateway;
use App\Classes\Payment\Invoice;

use App\Models\Order;
use App\Models\OrderDetails;
use Illuminate\Support\Collection;

use DB;
use URL;
use Auth;
use Route;
use Session;

class CartController extends Controller
{
    //variable declaration
    private $common      = null;
    private $cartRepo    = null;
    private $productRepo = null;
    private $cartLogic   = null;

    public function __construct(Functions $common, CartRepository $cartRepo, CartLogic $cartLogic, ProductRepository $productRepo){
        $this->common      = $common;
        $this->cartRepo    = $cartRepo;
        $this->cartLogic   = $cartLogic;
        $this->productRepo = $productRepo;
    }

    /**
     * Display a Cart.
     * @return View
     */
    public function index()
    {
        try{
            $qtyData     = null;
            $sessionCart = null;

            //reset status of carts item
            
            //get session cart
            $temp_data = $this->cartLogic->hasTemporaryCart();

            if(count($temp_data) > 0){
                $qtyData = $temp_data;
            }
            
            //get logged user
            $loggedUser = $this->cartLogic->getLoggedUser();

            //reset carts item's status
            //param: logged user , status
            $changed    = $this->cartRepo->resetCartItemsStatus($loggedUser, DEFAULT_STATUS);

            //get him cart data
            $cart       = $this->cartLogic->getCart($loggedUser, null, 'user');

            $country_cart = [];

            foreach ($cart->items as $key => $value) {
                $country_cart[$value['item']->country_name]['data'][] = $value;
                $country_cart[$value['item']->country_name]['flag'] = $value['item']->country_flag;
            }

            // return $country_cart;
            
            //cart main breadcrumb
            $breadcrumbs = [
                ['route_name' => 'cart.summary', 'status'        => 'active', 'name' => 'Cart Summary'],
                ['route_name' => 'cart.delivery', 'status'       => '', 'name'       => 'Delivery & Pickup'],
                ['route_name' => 'cart.payment.method', 'status' => '', 'name'       => 'Payment Options'],
                ['route_name' => 'cart.confirmation', 'status'   => '', 'name'       => 'Confirmation']
            ];

            
            $currentRoute = Route::currentRouteName();

            return view('cart::index-test', compact('cart', 'qtyData', 'breadcrumbs', 'currentRoute', 'country_cart'));
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Display a Cart.
     * @return View
     */
    public function testCart()
    {
        try{
            $qtyData     = null;
            $sessionCart = null;

            //reset status of carts item
            
            //get session cart
            $temp_data = $this->cartLogic->hasTemporaryCart();

            if(count($temp_data) > 0){
                $qtyData = $temp_data;
            }
            
            //get logged user
            $loggedUser = $this->cartLogic->getLoggedUser();

            //reset carts item's status
            //param: logged user , status
            $changed    = $this->cartRepo->resetCartItemsStatus($loggedUser, DEFAULT_STATUS);

            //get him cart data
            $cart       = $this->cartLogic->getCart($loggedUser, null, 'user'); 
            
            //cart main breadcrumb
            $breadcrumbs = [
                ['route_name' => 'cart.summary', 'status'        => 'active', 'name' => 'Cart Summary'],
                ['route_name' => 'cart.delivery', 'status'       => '', 'name'       => 'Delivery & Pickup'],
                ['route_name' => 'cart.payment.method', 'status' => '', 'name'       => 'Payment Options'],
                ['route_name' => 'cart.confirmation', 'status'   => '', 'name'       => 'Confirmation']
            ];
            
            $currentRoute = Route::currentRouteName();

            return view('cart::test', compact('cart', 'qtyData', 'breadcrumbs', 'currentRoute'));
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the deivery page.
     * @return View
     */
    public function delivery(Request $request)
    {
        try{
            $loggedUser    = $this->cartLogic->getLoggedUser();
            $product       = $this->cartRepo->getCartItemListByUser($loggedUser);
            $deliveryTypes = $this->cartRepo->getDeliveryType();
            $disabled      = [];

            //get logged user city id
            /*if(count($loggedUser) > 0 && count($loggedUser->customer) > 0 && isset($loggedUser->customer->city_id)){*/

                //reset carts item's status
                //param: logged user , status
                $changed         = $this->cartRepo->resetCartItemsStatus($loggedUser, DEFAULT_STATUS);
                $cart            = $this->cartLogic->getCart($loggedUser); 
                $delivery_type   = $request->input('type');
                $district_id     = (int) $request->input('district');
                $city_id         = (int) $request->input('location');
                $nearest_outlet  = [];
                $districts       = [];
                $cities          = [];
                $delivery_charge = 0;

                if(count($product) == 1 && isset($product[0]) && isset($product[0]['delivery_options']) && count($product[0]['delivery_options'])){
                    $delivery_types = $this->cartLogic->stringExplode($product[0]['delivery_options'], ',');

                    $disabled = [
                        'status'     => true,
                        'array'      => $delivery_types,
                        'item_count' => count($product)
                    ];
                }else{
                    $disabled = [
                        'status'     => false,
                        'array'      => [],
                        'item_count' => count($product)
                    ];
                }
                
                if($request->input('type') !== null && !empty($request->input('type')) && $request->input('type') == 'pickup'){
                    //params: optional district_id , return type
                    $districts   = $this->common->getDistricts('', 'get');
                }else if($request->input('type') !== null && !empty($request->input('type')) && $request->input('type') == 'direct'){
                    $cities      = $this->common->getCities('', 'get');
                }
                
                if(isset($delivery_type) && isset($district_id) && !empty($delivery_type) && !empty($district_id)){
                    //params: district id
                    $nearest_outlet = $this->common->getNearestOutletByDistrictId($district_id);
                }
                
                if(count($cart) > 0){
                    if(isset($cart->totalWeight) && isset($delivery_type) && $delivery_type !== 'pickup'){
                        $city = null;

                        if(!empty($city_id)){
                            $city = $city_id;
                        }else if(count($loggedUser) > 0 && count($loggedUser->customer) > 0 && isset($loggedUser->customer->city_id)){
                            $city = $loggedUser->customer->city_id;
                        }
                        
                        //calculate total weight of items in cart whitch can be delivered
                        $delivery_charge = $this->getDeliveryCharges($delivery_type, $city);
                    }
                }
                else{
                    return redirect()->route('home.index');
                    // throw new \Exception("Someting went wrong!, cart couldn't be empty!.");
                }
                
                //main breadcrumb
                $breadcrumbs = [
                    ['route_name' => 'cart.summary', 'status'        => 'active', 'name' => 'Cart Summary'],
                    ['route_name' => 'cart.delivery', 'status'       => 'active', 'name' => 'Delivery & Pickup'],
                    ['route_name' => 'cart.payment.method', 'status' => '', 'name'       => 'Payment Options'],
                    ['route_name' => 'cart.confirmation', 'status'   => '', 'name'       => 'Confirmation']
                ];

                //get current page route name
                $currentRoute = Route::currentRouteName();

                return view('cart::delivery', compact('cart', 'breadcrumbs', 'currentRoute', 'loggedUser', 'districts', 'cities', 'nearest_outlet', 'delivery_charge', 'disabled', 'deliveryTypes'));

            /*}else{
                return Functions::redirectTo(route('user.account.info.view').'?next_url='.route('cart.delivery'), 'Warning!.', 'Please updated your informations!.', 'warning');
            }*/
        }catch(\Exception $e){
            // return $e->getMessage();
            return $this->common->redirectWithAlert(
                'cart.summary', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * get delivery charge
     * @param Request
     * @return Json delivery charge
     */
    public function loadDeliveryCharges(Request $request)
    {
        $delivery_charge    = 0;
        $city_id            = $request->input('city_id');
        $delivery_type_name = $request->input('delivery_type_name');

        $delivery_charge = $this->getDeliveryCharges($delivery_type_name, $city_id);
        if(count($delivery_charge) > 0 && isset($delivery_charge['data'])){
            $delivery_charge['data'] = number_format($delivery_charge['data'], 2);
        }

        return $delivery_charge;
    }

    /**
     * calculate delivery charge
     * @param city_id
     * @param totalWeight
     * @return Fload delivery charge
     */
    public function calculateDeliveryCharges($city_id, $totalWeight)
    {
        $delivery        = new DeliveryChargesCalculator();
        $delivery_charge = $delivery->calculateDeliveryCharges($city_id, $totalWeight);
        return $delivery_charge;
    }

    /**
     * payment
     * @return Response
     */
    public function payment_method(UserAccountUpdateRequest $request)
    {   
        try{
            //cart detail include payment option and delivery option 
            $cartDetails      = [];

            $delivery_type   = $request->input('type'); 
            //get all payment methods
            $paymentMethods  = $this->cartLogic->getPaymentMethods();
            //get logged user
            $loggedUser      = $this->cartLogic->getLoggedUser();

            //cannot ids     = cannotProceedPaymentItemIds
            $cannotIds       = $request->input('cannotProceedPaymentItemIds');
            
            if(isset($cannotIds) && !empty($cannotIds)){
                //change status of cart items
                //param: ids of items, status
                $changeStatus = $this->cartRepo->changeCartItemStatus($cannotIds, CANNOT_PAYMENT);
            }

            // params: delivery form request
            if(count($request->all()) > 0){
                $this->cartRepo->saveCustomerAddressesDetails($request);
            }

            //get him cart data
            $cart            = $this->cartLogic->getCart($loggedUser, CAN_PAYMENT);

            if(count($cart) == 0){
                throw new \Exception("Item(s) not found in cart!.");
            }

            $delivery_charge = 0;

            //get user city
            $city = '';

            if($request->input('delivery_form.city') !== null && !empty($request->input('delivery_form.city'))){
                $city = $request->input('delivery_form.city');
            }elseif(count($loggedUser) > 0 && count($loggedUser->customer) > 0 && isset($loggedUser->customer->city_id)){
                $city = $loggedUser->customer->city_id;
            }else{
                $city = '';
            }

            //if post request and have delivery type in it run code
            //get payment type , delivery type
            $cartDetails = $this->cartRepo->getCartDetails($loggedUser->id);
            
            if(count($cart) > 0 && isset($cart->totalWeight)){
                if(asset($delivery_type) && !empty($delivery_type)){
                    if($delivery_type !== PICKUP){
                        $delivery_charge = $this->getDeliveryCharges($delivery_type, $city);                
                    }else{
                        $regional_center_id = $request->input('pickup.location_id');

                        if(isset($regional_center_id) && !empty($regional_center_id)){
                            $updated = $this->cartRepo->updateCartRegionalCenter($regional_center_id);

                            if(!isset($updated) && empty($updated)){
                                throw new \Exception("Regional center couldn't save!.");
                            }
                        }else{
                            throw new \Exception('Regional Center not found!.');
                        }
                    }
                }else{                    
                    if(count($cartDetails) > 0 && isset($cartDetails['delivery_method_name'])){
                        if(count($cartDetails) && isset($cartDetails['delivery_method_name']) && strtolower($cartDetails['delivery_method_name']) !== PICKUP){
                            $delivery_type   = $this->cartRepo->getDeliveryTypeByName($cartDetails['delivery_method_name']);
                            
                            if(count($delivery_type) > 0){
                                $delivery_charge = $this->getDeliveryCharges($delivery_type->name, $city);
                            }
                        }           
                    }
                }
            }
            

            $breadcrumbs = [
                ['route_name' => 'cart.summary', 'status'        => 'active', 'name' => 'Cart Summary'],
                ['route_name' => 'cart.delivery', 'status'       => 'active', 'name' => 'Delivery & Pickup'],
                ['route_name' => 'cart.payment.method', 'status' => 'active', 'name' => 'Payment Options'],
                ['route_name' => 'cart.confirmation', 'status'   => '', 'name'       => 'Confirmation']
            ];
            
            return view('cart::payment.payment-option', compact('breadcrumbs', 'cart', 'delivery_charge', 'paymentMethods', 'cartDetails'));
        }catch(\Exception $e){
            //dd($e->getMessage());
            $route = 'user.account.info.view';

            if(isset($cart) && count($cart) == 0){
                $route = 'cart.summary';
            }

            return $this->common->redirectWithAlert(
                $route, 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * show order summary
     * @return view
     * @param $paymentMethodRequest
     */
    public function order_confirmation(Request $paymentMethodRequest)
    {
        try{
            // return $paymentMethodRequest->all();
            $isSaved           = 0;
            $deliveryCharges   = 0;   
            $cartDetails       = [];
            //get selected payment method
            $payment_method_id = $paymentMethodRequest->input('payment_method');

            //update cart_detail table's payment method column
            if(!empty($payment_method_id)){
                $isSaved = $this->cartRepo->saveOrderPaymentMethod($payment_method_id);
            }

            if(count($isSaved) > 0){

                //copy cart summart code
                $data                 = $this->copyCartSummaryCode();
                $cart                 = $data['cart'];
                $cartDetails          = $data['cartDetails'];
                $delivery_charge      = $data['delivery_charge'];
                $delivery_information = $data['delivery_information'];
                
                if(count($cart) == 0){
                    throw new \Exception("Session expired!.");
                }

                if(count($cartDetails) && isset($cartDetails['delivery_method_name']) && strtolower($cartDetails['delivery_method_name']) !== PICKUP){
                    $delivery_type   = $this->cartRepo->getDeliveryTypeByName($cartDetails['delivery_method_name']);

                    if(count($delivery_type) > 0 && count($delivery_charge) > 0 && isset($delivery_charge['data'])){
                        $deliveryCharges = $delivery_charge['data'];
                    }
                }    
                
                $breadcrumbs = [
                    ['route_name' => 'cart.summary', 'status'        => 'active', 'name' => 'Cart Summary'],
                    ['route_name' => 'cart.delivery', 'status'       => 'active', 'name' => 'Delivery & Pickup'],
                    ['route_name' => 'cart.payment.method', 'status' => 'active', 'name' => 'Payment Options'],
                    ['route_name' => 'cart.confirmation', 'status'   => 'active', 'name' => 'Confirmation']
                ];
                
                return view('cart::payment.payment-confirmation', compact('breadcrumbs', 'cart', 'delivery_charge', 'cartDetails', 'delivery_information'));
            }else{
                throw new \Exception("Couldn't updated cart_detail table's payment_method column!.");
            }
        }catch(\Exception $e){
            //dd($e->getMessage());
            return $this->common->redirectWithAlert(
                'cart.payment.method', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * order confirmation
     * @return Response
     */
    public function confirmation()
    {
        $cart = [];
        return view('cart::delivery', compact('cart'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('cart::create');
    }

    /**
     * add items to the cart
     * @param  prduct_id,Request
     * @return Redirect
     */
    public function store($product_id, Request $request)
    {
        try{
            $prev_url = $request->input('prev_url');
            $action   = trim($request->input('action'));
            $qty      = (int) $request->input('qty')?:1;

            if(count($action) > 0 && !empty($action) && $action == 'cart' || $action == 'buy'){
                //get logged user data
                $loggedUser  = $this->cartLogic->getLoggedUser();
                //get avaible qty
                //$product     = $this->cartRepo->getAvailableQtyByProductId($product_id);
                //get qty of item in cart                
                $itemCart    = $this->cartRepo->getItemQtyByProduct($product_id, $loggedUser);

                if(count($itemCart) > 0 && !empty($itemCart->qty)){
                    //get old cart data
                    $oldCart = $this->cartLogic->getCart($loggedUser);
                    $cart    = new CartLogic($oldCart);

                    if($itemCart->qty){
                        //session cart qty + request qty
                        $totalQty = $itemCart->qty + $qty;
                        
                        if($totalQty){
                            //get product according to the ID
                            $item = $this->cartRepo->getProductById($product_id);
                            
                            if(count($item) > 0){
                                //params: product_id, qty, old cart data if has
                                //return: cart data
                                $cart->addCart($product_id, $qty, $item);
                                
                                if($action == 'cart'){
                                    //redirect with previous page query params
                                    return Functions::redirectTo(url()->previous());
                                }else if($action == 'buy'){
                                    //redirect with previous page query params
                                    return Functions::redirectTo('/cart/summary');
                                }
                            }else{  
                                throw new \Exception("Product not found for ID : ".$product_id);
                            }       
                        }else{
                            return Redirect()->back()->with([
                                'notification' => 'Something went wrong!.', 
                                'option' => [
                                    'timeOut'  => '10000', 
                                    'type'     => 'warning',
                                    'position' => 'toast-bottom-right'
                                ]
                            ]);   
                        }
                    }else{
                        return Redirect()->back()->with([
                            'notification' => 'Sorry, The order limited has been exists!.', 
                            'option' => [
                                'timeOut'  => '10000', 
                                'type'     => 'warning',
                                'position' => 'toast-bottom-right'
                            ]
                        ]);    
                    }
                }else{
                    if($qty){
                        //get product according to the ID
                        $item = $this->cartRepo->getProductById($product_id);
                        
                        if(count($item) > 0){
                            $oldCart = $this->cartLogic->getCart($loggedUser);
                            
                            $cart    = new CartLogic($oldCart);
                            //params: product_id, qty, old cart data if has
                            //return: cart data
                            $cart->addCart($product_id, $qty, $item);
    
                            if($action == 'cart'){
                                return Functions::redirectTo(url()->previous());
                            }else if($action == 'buy'){
                                return Functions::redirectTo('/cart/summary');
                            }
                        }else{  
                            throw new \Exception("Product not found for ID : ".$product_id);
                        }       
                    }else{
                        throw new \Exception("Invalid product or Stock not available!.");
                    }
                }
            }else if(count($action) > 0 && !empty($action) && $action == 'wishlist'){

                $user = $this->cartLogic->getLoggedUser();

                if(count($user)){

                    if(!isset($product_id) && empty($product_id)){
                        throw new \Exception("Invalid product_id passed to wishlist!.");
                    }

                    //add item to wishlist
                    //params: user_id, product_id
                    $createdWishlist = $this->cartRepo->addItemToWishlist($user->id, $product_id);

                    if(count($createdWishlist) > 0){
                        return redirect()->back()->with([
                            'success.title'   => 'Done!',
                            'success.message' => 'Item has been successfully added to wishlist!.'
                        ]);
                    }else{
                        throw new \Exception("Item couldn't be add to wishlist!.");
                    }
                }else{
                    return $this->common->redirectWithAlert(
                        '/login', 
                        'info', 
                        'Info!.', 
                        'Please login before add item to wishlist!.',
                        'url'
                    ); 
                }

            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('cart::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('cart::edit');
    }

    /**
     * Update the cart
     * @param  Request $request
     * @return Redirect
     */
    public function update(Request $request)
    {
        try{
            //get each qty
            $items          =  $request->input('qty');
            $customer_id    =  $this->cartLogic->getLoggedUser();
            $next_url       =  $request->input('next_url');
            $options        =  $request->input('options');
            $isChange       =  $request->input('updateStatus');
            $customize_cost =  $request->input('c_c')?:0;
            
            if(count($items) > 0){
                foreach($items as $cart_id => $qty){
                    //Session::forget('cart');
                    $loggedUser = $this->cartLogic->getLoggedUser();
                    $oldCart    = $this->cartLogic->getCart($loggedUser);
                    $cart       = new CartLogic($oldCart);
                    //params: product_id, qty, old cart data if has
                    //return: cart data
                    $cart->updateCart($cart_id, $qty);

                }

                if(!empty($next_url)){
                    if(count($options) > 0){
                        return redirect($next_url.'?options='.implode(',', $options));
                    }else{
                        return redirect($next_url);
                    }
                }else{
                    return redirect()->route('cart.summary');
                }
            }
        }catch(\Exception $e){
            return $e->getMessage();
            return $this->common->redirectWithAlert(
                'cart.summary', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $product_id, $cart_id)
    {
        try{
            if(count($cart_id) > 0 && isset($cart_id)){
                $isExist = $this->productRepo->isProductExist($product_id);
                
                if($isExist){
                    //Session::forget('cart');
                    $next_url   = $request->input('next_url');
                    $loggedUser = $this->cartLogic->getLoggedUser();
                    $oldCart    = $this->cartLogic->getCart($loggedUser);
                    $cart       = new CartLogic($oldCart);
                    //params: product_id, qty, old cart data if has
                    //return: cart data
                    $cart       = $cart->removeCart($cart_id, $cart);

                    
                     if(!empty($next_url)){
                        return redirect($next_url);
                    }else{
                        return redirect()->route('cart.summary');
                    }
                }else{
                    throw new \Exception("Product not found!.");
                }
            }else{
                throw new \Exception("Product not found for ID :".$product_id);
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'cart.summary', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    /**
     * merge session cart data with logged user DB cart when user login
     * @return Redirect
     */
    public function cartMerge()
    {
        try{
            //call logic of cart merge
            $this->cartLogic->mergeSessionCartWithDB();
            return redirect()->route('cart.summary');
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    /**
     * Remove user session
     * @return Response
     */
    public function forgetSessionCartOfUser()
    {
        try{
            //call logic of cart merge
            $forget = $this->cartLogic->forgetUserSessionCart();
            return redirect()->route('cart.summary');
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'bigstore.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    public function getEachItemDeliveryOption(Request $request)
    {
        //user selected delivery option
        //params: request and return type html or array
        $html = $this->cartLogic->getEachItemDeliveryOption($request, 'html');
        return $html;
    }

    //get delivery charges
    //params: delivery type name, city id
    public function getDeliveryCharges($delivery_type, $city)
    {
        //get total weight of items whitch can deliver
        $totalWeight     = $this->cartLogic->getCartItemsTotalWeight($delivery_type);
        //calculate total weight of items in cart whitch can be delivered
        $delivery_charge = $this->calculateDeliveryCharges($city, $totalWeight);
        return $delivery_charge;
    }

    //make payment
    public function makePayment(Request $request)
    {
        try{
            //get cart summary code
            $data        = $this->copyCartSummaryCode($request);
            
            $order       = null;
            $customer_id = $this->cartLogic->getLoggedUser()->id;
            //$loggedUser  = $this->cartLogic->getLoggedUser($customer_id);
            
            $order_no       = $request->input('order_no')?: '';
            $order          = [];
            
            // DB::beginTransaction();
            //save order
            if(isset($order_no) && !empty($order_no)){
                //$order = $this->cartRepo->saveOrder($data, 'update', $order_no);
                //get order by order no
                $order = $this->cartRepo->getOrderByNo($order_no);

                if(isset($order) && count($order['order']) > 0){
                    $order = $order['order'];
                }else{
                    $order = [];
                }
            }else{
                //create order
                $order = $this->cartRepo->saveCountryOrder($data, 'create', null, $request->input('options'));

                $deleted = $this->cartRepo->removeItemInCartByStatus(DEFAULT_STATUS);
                return redirect("account/order-list");
            }
            
        }catch(\Exception $e){
            return $e->getMessage();
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    //return total, check free delivery or not. if not delviery charges not add.
    public function getTotal($total, $deliveryCharge, $cartDetails = [])
    {
        if(isset($cartDetails) && count($cartDetails) > 0 && $cartDetails != null){
            $coupon_discount = 0;
            
            if(isset($cartDetails['voucher_type']) == VOUCHER_PERCENTAGE && isset($cartDetails['voucher_percentage']) && !empty($cartDetails['voucher_percentage']) && $cartDetails['voucher_status'] == 1 && $cartDetails['voucher_active'] == 1 && $cartDetails['coupon_is_expired'] !== 1){
                $coupon_discount = calculatePercentage($total, $cartDetails['voucher_percentage']);
            }
            // elseif(isset($cartDetails['voucher_type']) == VOUCHER_VALUE && isset($cartDetails['voucher_value']) && !empty($cartDetails['voucher_value'])){
            //     $coupon_discount = $cartDetails['voucher_value'];
            // }

            if(FREE_DELIVERY_ENABLED == true && checkRule($total, FREE_DELIVERY_RULE, FREE_DELIVERY_OPARETOR)){
                //delivery charges not addition
                $total = ($total - $coupon_discount);
            }else{
                //if not free deliver delivery charges is add
                $total = ($total - $coupon_discount) + $deliveryCharge;
            }

            return $total;
        }else{
            return $total;
        }
    }

    public function copyCartSummaryCode($request = [])
    {
        //get logged user
        $loggedUser = $this->cartLogic->getLoggedUser();
        $customer   = [];

        if(count($request) > 0 && $request->input('customer_id') && !empty($request->input('customer_id'))){
            $customer  = $this->cartLogic->getCustomer($request->input('customer_id'));
        }
        //get him cart data
        $cart                 = $this->cartLogic->getCart($loggedUser, CAN_PAYMENT, 'user', $loggedUser);

        $delivery_charge      = 0;
        $delivery_information = [];

        return [
            'cart'       => $cart,
            'loggedUser' => $loggedUser,
            'customer'   => $customer
        ];
    }

    private function isValid($data, $error)
    {
        return Functions::isValid($data, $error);
    }

    public function configCustomerToCart(Request $request)
    {
        try{
            $customer_code = $request->input('customer_id');
            $cart_items_id = $request->input('cart_items_id');

            if(!empty($customer_code)){   
                $customer = $this->cartRepo->getCustomerByCode($customer_code);

                if(count($customer) > 0){
                    if(count($cart_items_id) > 0){
                        $cart = $this->cartRepo->addCustomerToCart($cart_items_id, $customer->id);

                        if(count($cart) > 0){
                            return [
                                'success'  => true,
                                'customer' => $customer,
                                'msg'      => 'Cart has been successfully allocate to the customer.'  
                            ];
                        }else{
                            return [
                                'success'  => false,
                                'customer' => [],
                                'msg'      => "Shopping cart couldn't connect to the customer."
                            ];
                        }
                    }else{
                        return [
                            'success'  => false,
                            'customer' => [],
                            'msg'      => "Invalid cart data."
                        ];
                    }
                }else{
                    return [
                        'success'  => false,
                        'customer' => [],
                        'msg'      => "Customer couldn't found!." 
                    ];
                }
            }else{
                return [
                    'success'  => false,
                    'customer' => [],
                    'msg'      => "Customer id cannot be empty!."
                ];
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    public function removeCustomerFromCart($customer_id = null)
    {
        try{
            if(!empty($customer_id)){
                $delete = $this->cartRepo->removeCartFromCustomer($customer_id);

                if($delete){
                    return redirect()->back();
                }else{
                    throw new \Exception("Customer couldn't be delete!");
                }
            }else{
                throw new \Exception("Customer id cannot be empty!");
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }
}
