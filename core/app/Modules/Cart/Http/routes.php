<?php
/**
 * SHOPPING CART
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-28
 */

Route::group(['prefix' => 'cart', 'namespace' => 'App\\Modules\Cart\Http\Controllers'], function()
{
    Route::group(['middleware' => ['web']], function(){
        /****************************************
                        GET Routes
        ****************************************/
        Route::get('/summary', [
            'uses'  => 'CartController@index',
            'as'    => 'cart.summary'
        ]);

        Route::get('/test', [
            'uses'  => 'CartController@testCart',
            'as'    => 'cart.test'
        ]);
        // Route::get('check-out', [
        //     'uses' => 'BigStoreController@checkoutView',
        //     'as'   => 'cart.summary'
        // ]);
        
        Route::get('/payment', [
            'uses'  => 'CartController@payment_method',
            'as'    => 'cart.payment.method'
        ]);
        Route::get('/confirmation', [
            'uses'  => 'CartController@confirmation',
            'as'    => 'cart.confirmation'
        ]);
        Route::get('/add/{product_id}', [
            'uses'  => 'CartController@store',
            'as'    => 'cart.add'
        ]);
        Route::get('/remove/{product_id}/{cart_id}', [
            'uses'  => 'CartController@destroy',
            'as'    => 'cart.remove'
        ]);
        Route::get('/merge', [
            'uses'  => 'CartController@cartMerge',
            'as'    => 'cart.merge'
        ]);
        Route::get('/forget/user/session', [
            'uses'  => 'CartController@forgetSessionCartOfUser',
            'as'    => 'cart.forget.user.session'
        ]);
        Route::get('/payment/confirmation', [
            'uses'  => 'CartController@order_confirmation',
            'as'    => 'cart.payment.confirmation'
        ]);
        Route::get('/make/payment', [
            'uses'  => 'CartController@makePayment',
            'as'    => 'cart.get.make.payment'
        ]);
        Route::get('/remove/customer/{customer_id}/cart', [
            'uses'  => 'CartController@removeCustomerFromCart',
            'as'    => 'remove.customer.cart'
        ]);
        Route::get('/invoice', [
            'uses'  => 'CartController@makePayment',
            'as'    => 'cart.invoice'
        ]);
        Route::get('/product/customize', [
            'uses'  => 'CartController@setProductCustomizeOption',
            'as'    => 'product.customize.option'
        ]);
        Route::get('/customize/option', [
            'uses'  => 'CartController@removeCustomizeOption',
            'as'    => 'remove.cart.customize.option'
        ]);
        /****************************************
                        POST Routes
        ****************************************/
        Route::post('/update', [
            'uses'  => 'CartController@update',
            'as'    => 'cart.update'
        ]);

        Route::post('/config/customer', [
            'uses'  => 'CartController@configCustomerToCart',
            'as'    => 'config.customer'
        ]);

        Route::group(['middleware' => ['guestUser']], function(){
            Route::get('/delivery', [
                'uses'  => 'CartController@delivery',
                'as'    => 'cart.delivery'
            ]);
            Route::post('/delivery/charge', [
                'uses'  => 'CartController@loadDeliveryCharges',
                'as'    => 'cart.delivery.charge'
            ]);
        });

        /****************************************
                    Auth midileware
        ****************************************/
        Route::group(['middleware' => ['auth']], function(){
            Route::get('/payment/view', [
                'uses'  => 'CartController@viewPayment',
                'as'    => 'cart.payment.view'
            ]);
            Route::post('/payment/confirmation', [
                'uses'  => 'CartController@order_confirmation',
                'as'    => 'cart.payment.confirmation'
            ]);
            Route::post('/payment', [
                'uses'  => 'CartController@payment_method',
                'as'    => 'cart.payment.method'
            ]);
            Route::post('/item/delivery/option', [
                'uses'  => 'CartController@getEachItemDeliveryOption',
                'as'    => 'cart.item.delivery.methods'
            ]);
        });

    });

    Route::get('order/test/{order_id}', 'CartController@getOrderById');

    Route::post('customer/measurement', 'CartController@saveCustomerMeasurement');
});
