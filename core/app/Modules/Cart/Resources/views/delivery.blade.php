@extends('layouts.master') @section('title','Delivery & Pickup')

@section('links')
  <!-- select2 -->
  <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
  
  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="{{ route('cart.summary') }}">Cart Summary</a></li>
        <li class="breadcrumb-item active">Delivery & Pickup</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="cart-summary mb-4">
    <div class="container">

      <div class="row mb-4">
        <div class="col-md-12">
          @include('cart::includes.breadcrumb')
        </div>
      </div>

      <div class="row">
        <div class="col-lg-9 col-md-12 col-sm-9 col-xs-12">
          <div class="card product-list position-relative">
            <div class="panel-refresh" name="panel-refresh"></div>
            <div class="card-header border-bottom-0 bg-gray">
              <h6 class="h6 text-uppercase mt-2 font-weight-bold">Delivery & Pickup</h6>
            </div>
            <!-- <form action="{{ route('cart.delivery') }}" method="GET" id="delivery_form"> -->
              
              <div class="card-body pl-4 pr-4">
                <div class="row mb-4 mt--3 {{ isDisabled($disabled, 'pickup', 'false') && isDisabled($disabled, 'direct', 'false') && isDisabled($disabled, 'express', 'false')? 'd-block' : 'd-none' }}">
                    <div class="col">
                      <div class="alert alert-dark mt-4" role="alert">
                        <strong>Sorry, This item has not any delivery option. Please contact hotline.</strong>
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="panel-refresh" name="panel-refresh"></div>
                  @if(isValid($deliveryTypes))
                    @foreach($deliveryTypes as $type)
                      @if($type->status == 1)
                        <div class="col-4 text-center">
                          <form>
                            <label class="custom-control custom-radio xs-p-10 {{ isDisabled($disabled, strtolower($type->name), 'cursor-not-allowed') }}" data-toggle="tooltip" title="{{ isValid($type->long_description, '') }}">
                              <input type="radio" id="delivery_{{ strtolower($type->name) }}" name="type" {{ Request::get('type') == strtolower($type->name)? ' checked=checked' : '' }} onchange="this.form.submit()" value="{{ strtolower($type->name) }}" class="custom-control-input {{ isDisabled($disabled, strtolower($type->name), 'cursor-not-allowed') }}" {{ isDisabled($disabled, strtolower($type->name), 'disabled') }}>
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description d-flex align-items-center text-uppercase">{{ strtolower($type->name) }}</span>
                            </label>
                          </form>
                          {{ isValid($type->long_description, '') }}
                        </div>
                      @else
                        <div class="col-4 text-center">
                          <form>
                            <label class="custom-control custom-radio cursor-not-allowed xs-p-10" data-toggle="tooltip" title="{{ isValid($type->long_description, '') }}">
                              <input type="radio" id="delivery_{{ strtolower($type->name) }}" name="type" {{ Request::get('type') == strtolower($type->name)? ' checked=checked' : '' }} onchange="this.form.submit()" value="{{ strtolower($type->name) }}" class="custom-control-input cursor-not-allowed" disabled>
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description d-flex align-items-center text-uppercase">{{ strtolower($type->name) }}</span>
                            </label>
                          </form>
                          {{ isValid($type->long_description, '') }}
                        </div>
                      @endif
                    @endforeach
                  @else
                    <h3>There no delivery types.</h3>
                  @endif
                </div>
                
                <div class="card mt-4 rounded-0{{ Request::get('type') == 'pickup'? ' d-block' : ' d-none' }}" id="pickup_block">
                  <div class="panel-refresh" name="panel-refresh"></div>
                  <form action="{{ route('cart.delivery') }}" method="GET" id="pickup_form">
                    <input type="hidden" name="type" value="pickup">
                    <div class="card-header">
                      <h6 class="h6 text-uppercase m-0 font-weight-bold">Pickup</h6>
                    </div>
                    <div class="card-body">
                      <p class="card-text">Select your district.</p>
                      <div class="form-group">
                        <label for="distric" class="required">District</label>
                        <select class="js-example-basic-single" name="district" id="district" onchange="this.form.submit()">
                          <option value="">Please Select a district</option>
                          @if(count($districts) > 0)
                            @foreach($districts as $district)
                              <option value="{{ $district->id }}" {{ Request::get('district') == $district->id? 'selected=selected': '' }}>{{ $district->name }}</option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                      @if(count($nearest_outlet) > 0)
                        @if(!empty($nearest_outlet['message']))
                          <div class="h4 mt-4 text-center text-secondary">Sorry, nearest outlet not found in "{{ $nearest_outlet['district'] }}"</div>
                          <div class="h6 text-center text-secondary">{{ $nearest_outlet['message'] }}</div>
                        @else
                          <div class="form-group{{ !empty(Request::get('district'))? 'd-block': 'd-none' }}" id="location">
                            <label for="dealer outlet" class="required">Regional center</label>
                            <select class="js-example-basic-single" name="location" id="location" {{ !empty(Request::get('district'))? '': 'disabled' }}>
                              <option value="">Please Select a regional center</option>
                              @if(count($nearest_outlet) > 0 && isset($nearest_outlet['outlets']))
                                @foreach($nearest_outlet['outlets'] as $outlet)
                                  <option value="{{ $outlet->id }}" {{ Request::get('outlet') == $outlet->id? 'selected=selected': '' }}>{{ $outlet->name }}</option>
                                @endforeach
                              @endif
                            </select>
                          </div>
                        @endif
                      @endif
                    </div>
                  </form>
                </div>

                <div class="card mt-4 rounded-0{{ Request::get('type') == 'express'? ' d-block' : ' d-none' }}" id="express_block">
                  <div class="card-header">
                    <h6 class="h6 text-uppercase m-0 font-weight-bold">Express</h6>
                  </div>
                  <div class="card-body">
                    <p class="card-text">The item should be delivered to the customer's shipping address.</p>
                    <p class="card-text"><b>Note:</b> An additional charge will be added for the total order.</p>
                  </div>
                </div>
                <div class="card mt-4 rounded-0{{ Request::get('type') == 'direct'? ' d-block' : ' d-none' }}" id="direct_block">
                  <div class="card-body">
                    <div class="card-text mb-4">If your delivery address is different than billing address click on "Different address".</div>
                    <form action="{{ route('cart.payment.method') }}" method="POST" id="delivery_form">
                      {!! csrf_field() !!}
                      <div id="injected-user-selected"></div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 order-lg-1 order-2">
                          <div class="panel-refresh" name="panel-refresh"></div>
                          <div class="" name="form-panel-refresh"></div>
                          
                          <div class="btn btn-sm btn-light btn-block rounded-0 custome-tgl-btn">
                            <span class="togg-name float-left font-weight-bold">Billing Address</span>
                          </div>
                          <div id="collapseBilling">
                            <div class="card card-body rounded-0 border-0 pl-0 pr-0 pb-0">
                                <input type="hidden" name="type" value="{{ Request::get('type') }}">
                                @if(count($loggedUser))
                                  <div class="row">
                                    <div class="col-md-6 mb-3">
                                      <label class="required">First Name</label>
                                      <input type="text" name="first_name" class="form-control form-control-sm{{ $errors->has('first_name')? ' is-invalid': '' }}" placeholder="First name" value="{{ Request::old('first_name')?:isValid($loggedUser->customer->first_name) }}">
                                      <div class="invalid-feedback">{{ $errors->first('first_name') }}</div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                      <label class="required">Last Name</label>
                                      <input type="text" name="last_name" class="form-control form-control-sm" placeholder="Last name" value="{{ Request::old('last_name')?:isValid($loggedUser->customer->last_name) }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6 mb-3">
                                      <label for="validationCustom04" class="required">Phone No</label>
                                      <input type="number" name="phone_no" class="form-control form-control-sm{{ $errors->has('phone_no')? ' is-invalid': '' }}" placeholder="Ex: 0710000000" value="{{ Request::old('phone_no')?:isValid($loggedUser->customer->phone_no) }}"  maxlength="10" minlength="10">
                                      <div class="invalid-feedback">{{ $errors->first('phone_no') }}</div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                      <label for="validationCustom05">Mobile No</label>
                                      <input type="number" name="mobile_no" class="form-control form-control-sm" placeholder="Ex: 0710000000" value="{{ Request::old('mobile_no')?:isValid($loggedUser->customer->mobile_no) }}"  maxlength="10" minlength="10">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 mb-3">
                                      <label class="required">Address Line 1</label>
                                      <input type="text" name="address_line_1" class="form-control form-control-sm{{ $errors->has('address_line_1')? ' is-invalid': '' }}" placeholder="Enter Address Line 1" value="{{ Request::old('address_line_1')?:isValid($loggedUser->customer->address_line_1) }}">
                                      <div class="invalid-feedback">{{ $errors->first('address_line_1') }}</div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 mb-3">
                                      <label>Address Line 2</label>
                                      <input type="text" name="address_line_2" class="form-control form-control-sm" placeholder="Enter Address Line 2" value="{{ Request::old('address_line_2')?:isValid($loggedUser->customer->address_line_2) }}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 mb-3">
                                      <label class="required">Email</label>
                                      <input type="email" name="email" class="form-control form-control-sm{{ $errors->has('email')? ' is-invalid': '' }}" placeholder="Enter Email address" value="{{ Request::old('email')?:isValid($loggedUser->customer->email) }}">
                                      <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 mb-3">
                                      <label class="required">City</label>
                                      <select name="city" class="js-example-basic-single select2-hidden-accessible city-selector{{ $errors->has('city')? ' is-invalid': '' }}">
                                        <option value="">select your city</option>
                                        @if(count($cities) > 0)
                                          @foreach($cities as $city)
                                            <option value="{{ $city->id }}" data-postal-code="{{ $city->postal_code }}" {{ Request::old('city') == $city->id?: isValid($loggedUser->customer->city_id) == $city->id? 'selected': '' }}>{{ $city->name }}</option>
                                          @endforeach
                                        @endif
                                      </select>
                                      <div class="invalid-feedback{{ $errors->has('city')? ' d-block': '' }}">{{ $errors->first('city') }}</div>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                      <label class="required">ZIP/Postal Code</label>
                                      <input type="text" name="postal_code" class="form-control form-control-sm postal_code cursor-not-allowed{{ $errors->has('postal_code')? ' is-invalid': '' }}" placeholder="Your ZIP/Postal Code" value="{{ Request::old('postal_code')?:isValid($loggedUser->customer->city? $loggedUser->customer->city->postal_code?:'' : '') }}" readonly>
                                      <div class="invalid-feedback">{{ $errors->first('postal_code') }}</div>
                                    </div>
                                  </div>
                                  <button type="submit" id="btn-save" class="btn btn-sm btn-primary float-right cursor-not-allowed  d-none" disabled><i class="fa fa-save" aria-hidden="true"></i> Save changes</button>
                                @endif
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 order-lg-2 order-1">
                          <a class="btn btn-sm btn-light btn-block rounded-0 custome-tgl-btn" id="collapseS" aria-expanded="false" data-toggle="collapse" href="#collapseShipping" aria-controls="collapseShipping">
                            <span class="togg-name float-left font-weight-bold">Different address</span>
                            <span class="togg float-right"><i id="toggle_btn" class="fa fa-toggle-{{ $errors->first('delivery_form.*')? 'on':'off' }}" aria-hidden="true"></i></span>
                          </a>

                          <div class="collapse{{ $errors->first('delivery_form.*')? ' show':'' }}" id="collapseShipping">
                            <div class="card card-body rounded-0 border-0 pl-0 pr-0 pb-0">
                                <!-- this is for load other data of user selected e.g delivery option -->
                                <div name="user-selected-data"></div>
                                <div class="row">
                                  <div class="col-md-6 mb-3">
                                    <label class="required">First Name</label>
                                    <input type="text" name="delivery_form[first_name]" class="form-control form-control-sm{{ $errors->has('delivery_form.first_name')? ' is-invalid': '' }}" placeholder="First name" value="{{ Request::old('delivery_form.first_name') }}">
                                    <div class="invalid-feedback">{{ $errors->first('delivery_form.first_name') }}</div>
                                  </div>
                                  <div class="col-md-6 mb-3">
                                    <label class="required">Last Name</label>
                                    <input type="text" name="delivery_form[last_name]" class="form-control form-control-sm{{ $errors->has('delivery_form.last_name')? ' is-invalid': '' }}" placeholder="Last name" value="{{ Request::old('delivery_form.last_name') }}">
                                    <div class="invalid-feedback">{{ $errors->first('delivery_form.last_name') }}</div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6 mb-3">
                                    <label  class="required" for="validationCustom04">Phone No</label>
                                    <input type="number" name="delivery_form[phone_no]" class="form-control form-control-sm{{ $errors->has('delivery_form.phone_no')? ' is-invalid': '' }}" placeholder="Enter Your Phone No" max="10" min="10" value="{{ Request::old('delivery_form.phone_no') }}">
                                    <div class="invalid-feedback">{{ $errors->first('delivery_form.phone_no') }}</div>
                                  </div>
                                  <div class="col-md-6 mb-3">
                                    <label for="validationCustom05">Mobile No</label>
                                    <input type="number" name="delivery_form[mobile_no]" class="form-control form-control-sm{{ $errors->has('delivery_form.mobile_no')? ' is-invalid': '' }}" placeholder="Enter Your Mobile No" max="10" min="10" value="{{ Request::old('delivery_form.mobile_no') }}">
                                    <div class="invalid-feedback">{{ $errors->first('delivery_form.mobile_no') }}</div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12 mb-3">
                                    <label class="required">Address Line 1</label>
                                    <input type="text" name="delivery_form[address_line_1]" class="form-control form-control-sm{{ $errors->has('delivery_form.address_line_1')? ' is-invalid': '' }}" placeholder="Enter Address Line 1" value="{{ Request::old('delivery_form.address_line_1') }}">
                                    <div class="invalid-feedback">{{ $errors->first('delivery_form.address_line_1') }}</div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12 mb-3">
                                    <label>Address Line 2</label>
                                    <input type="text" name="delivery_form[address_line_2]" class="form-control form-control-sm{{ $errors->has('delivery_form.address_line_2')? ' is-invalid': '' }}" placeholder="Enter Address Line 2" value="{{ Request::old('delivery_form.address_line_2') }}">
                                    <div class="invalid-feedback">{{ $errors->first('delivery_form.address_line_2') }}</div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6 mb-3">
                                    <label class="required">City</label>
                                    <select name="delivery_form[city]" class="js-example-basic-single select2-hidden-accessible city-selector{{ $errors->has('delivery_form.city')? ' is-invalid': '' }}">
                                      <option value="">select your city</option>
                                      @if(count($cities) > 0)
                                        @foreach($cities as $city)
                                          <option value="{{ $city->id }}" {{ Request::old('delivery_form.city') == $city->id? 'selected':'' }} data-postal-code="{{ $city->postal_code }}">{{ $city->name }}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                    <div class="invalid-feedback">{{ $errors->first('delivery_form.city') }}</div>
                                  </div>
                                  <div class="col-md-6 mb-3">
                                    <label class="required">ZIP/Postal Code</label>
                                    <input type="text" name="delivery_form[postal_code]" class="form-control form-control-sm cursor-not-allowed postal_code{{ $errors->has('delivery_form.postal_code')? ' is-invalid': '' }}" placeholder="Your ZIP/Postal Code" value="{{ Request::old('delivery_form.postal_code') }}" readonly>
                                    <div class="invalid-feedback">{{ $errors->first('delivery_form.postal_code') }}</div>
                                  </div>
                                </div>
                                <!-- <button class="btn btn-sm btn-primary float-right ml-1" type="submit"><i class="fa fa-pencil" aria-hidden="true"></i> Save</button>
                                <button class="btn btn-sm btn-primary float-right" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button> -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

              </div>
            <!-- </form> -->
          </div>
        </div>
        <div class="col-lg-3 col-md-12 col-sm-3 col-xs-12">
          <div class="position-relative">
            <!-- selected form -->
            <div id="user-selected">
              <input type="hidden" name="pickup[location_id]" id="regional_center" value="">
            </div>
            @if(isValid($loggedUser->customer->city_id) && Request::get('type') == 'direct' || Request::get('type') == 'express')
              @if(count($delivery_charge) > 0 && isset($delivery_charge['data']))
                @include('cart::includes.cart_summary', ['btn_text' => 'Next', 'showDeliveryCharges' => true, 'delivery_charge' => $delivery_charge, 'disabled' => false, 'show_modal' => true])
              @else
                @include('cart::includes.cart_summary', ['btn_text' => 'Next', 'showDeliveryCharges' => true, 'delivery_charge' => $delivery_charge, 'disabled' => true, 'show_modal' => false])
              @endif
            @else
              @include('cart::includes.cart_summary', ['btn_text' => 'Next', 'showDeliveryCharges' => true, 'delivery_charge' => $delivery_charge, 'disabled' => true, 'show_modal' => true])
            @endif

            @if(empty(Request::get('type')))   
              <div class="alert alert-secondary alert-dismissable mt-4">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Information!.</strong>
                  <br>
                  Please select the delivery method
              </div>
            @endif
            
            <div class="alert alert-secondary alert-dismissable mt-4 {{ count($delivery_charge) > 0 && !isset($delivery_charge['data']) && !empty(Request::get('type')) && Request::get('type') !== PICKUP? 'd-block' : 'd-none' }}" id="not-delivery-to">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Information!.</strong>
                <br>
                We are not delivery to selected city.
            </div>
          </div>
        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->

  <!-- The Modal -->
  <div class="modal fade" id="delivery_summary_modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title small-font-mobile">Delivery Summary</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div id="each-item-delivery-methods" class="align-middle text-center"><h2>Loading...</h2></div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" id="btn-proceed-to-payment" class="btn btn-sm btn-info">Proceed to payment</button>
          <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@stop

@section('js')
  <!-- select2 -->
  <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>

  <!-- toastr notification -->
  <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

  <script src="{{ asset('assets/js/main.js') }}"></script>
@stop 

@section('scripts')
<script>
  $(document).ready(function(){
    // In your Javascript (external .js resource or <script> tag)
    $('.js-example-basic-single').select2();

    $('#collapseB').on('click', function(e){
      $('#collapseB span.togg i').toggleClass('fa-toggle-off fa-toggle-on', 200);
    });
    $('#collapseS').on('click', function(e){
      $('#collapseS span.togg i').toggleClass('fa-toggle-off fa-toggle-on', 200);
    });

    // $('input[name=type]').on('click', function(e){
    //   var delivery_type = $(this).val();
    //   if(delivery_type == 'pickup'){
    //     $('#pickup_block').removeClass('d-none');
    //     $('#direct_block').addClass('d-none');
    //     $('#direct_block').removeClass('d-block');
    //   }if(delivery_type == 'direct'){
    //     $('#pickup_block').addClass('d-none');
    //     $('#pickup_block').removeClass('d-block');
    //     $('#direct_block').removeClass('d-none');
    //     $('#district').prop('disabled', true);
    //     $('#dealer_outlet').prop('disabled', true);
    //   }
    // });

    //remove panel refresh after load page
    removePanelRefresh();
    //add panel refresh class
    $('form').on('submit', function(){
      addPanelRefresh();
    });
  });
  
  //show message when user forget to updated there cart.
  @if(Request::get('cart-changes') == 'saved')
    notification("toast-bottom-right", "success", "Your cart has been updated!.", "10000");
  @endif

  var city = $('select[name=city]').val();
  
  if(city.length == 0){
    $('#not-delivery-to').addClass('d-none');
    $('#not-delivery-to').removeClass('d-block');
  }

  //get and set postal code by city
  $('.city-selector').on('change', function(e){
      // addPanelRefresh();
      //onchange set postal code to postal_code field
      var city = $('option:selected', this);

      $(this).closest('.row').find('.postal_code').val(city.data('postal-code'));

      var data = new FormData();
      data.append('city_id', city.val());
      data.append('delivery_type_name', $('input[name=type]:checked').val());

      $.ajax({
        method: 'POST',
        url: '{{ route('cart.delivery.charge') }}',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response){
          
          if(response.result > 0){
            $('#delivey_cost').html('Rs '+response.data);
            var hidden_subtotal = $('#hidden_sub_total').val();
            var total = (parseFloat(hidden_subtotal) + parseFloat(response.data)).toFixed(2);
            console.log(total);
            $('#sub_total').html('Rs '+total);

            $('#btn-next').prop('disabled', false);
            $('#btn-next').removeClass('cursor-not-allowed');
            $('#not-delivery-to').addClass('d-none');
            $('#not-delivery-to').removeClass('d-block');
          }
          
          if(response.result == 0){
            if(response.message !== ''){
              $('#delivey_cost').html('Rs 0.00');
              $('#btn-next').prop('disabled', true);
              $('#btn-next').addClass('cursor-not-allowed');
              $('#not-delivery-to').removeClass('d-none');
              $('#not-delivery-to').addClass('d-block');

              $.alert({
                theme: 'material',
                title: 'Warning!',
                type: 'orange',
                content: response.message
              });
            }
          }
        },
        error: function(xhr){
          if(xhr.responseJSON.message){
            $.alert({
              theme: 'material',
              title: 'Warning!',
              type: 'red',
              content: xhr.responseJSON.message
            });
          }else{
            $.alert({
              theme: 'material',
              title: 'Warning!',
              type: 'red',
              content: 'Someting went wrong!.'
            });
          }
        }
      });

      removePanelRefresh('panel-refresh');
  });

  $('[data-toggle="tooltip"]').tooltip();  
  
  $('#collapseBilling').on('change keyup keydown', 'input, select', function(e){
    if($(this).val().trim().length > 0){
      $('#delivery_form').prop('action', '{{ route("user.account.update.info") }}');
      $('#btn-save').prop('disabled', false);
      $('#btn-save').removeClass('d-none');
      $('#btn-save').removeClass('cursor-not-allowed');
      $('#btn-next').prop('disabled', true);
      $('#btn-next').addClass('cursor-not-allowed');
    }
  });

  $('#collapseShipping').on('change keyup keydown', 'input, select', function(e){
    var pettern = new RegExp('([0-9]{10})');

    if($('input[name="delivery_form[first_name]"]').val().trim().length > 0 &&
        $('input[name="delivery_form[last_name]"]').val().trim().length > 0 &&
        $('input[name="delivery_form[phone_no]"]').val().trim().length > 0 && pettern.test($('input[name="delivery_form[phone_no]"]').val()) &&
        $('input[name="delivery_form[address_line_1]"]').val().trim().length > 0 &&
        $('select[name="delivery_form[city]"] option:selected').val().trim().length > 0
      ){
        $('#btn-next').prop('disabled', false);
        $('#btn-next').removeClass('cursor-not-allowed');
    }else{
      $('#btn-next').prop('disabled', true);
      $('#btn-next').addClass('cursor-not-allowed');
    }
  });

  $('#collapseS').on('click', function(){
    var status = $(this).attr("aria-expanded");
    
    if(status == 'false'){
      addPanelRefresh('div[name=form-panel-refresh]');

      $('#btn-next').prop('disabled', true);
      $('#btn-next').addClass('cursor-not-allowed');

      if($('input[name="delivery_form[first_name]"]').val().length > 0 &&
        $('input[name="delivery_form[last_name]"]').val().length > 0 && 
        $('input[name="delivery_form[phone_no]"]').val().length > 0 && 
        $('input[name="delivery_form[address_line_1]"]').val().length > 0 && 
        $('select[name="delivery_form[city]"] option:selected').val().length > 0 && 
        $('input[name="delivery_form[postal_code]"]').val().length > 0){
          $('#btn-next').prop('disabled', false);
          $('#btn-next').removeClass('cursor-not-allowed');
      }
    }else{
      removePanelRefresh();
      $('#btn-next').prop('disabled', false);
      $('#btn-next').removeClass('cursor-not-allowed');
    }
  }); 

  $('#btn-next').on('click', function(){
    $('#injected-user-selected').html($('#user-selected').html());

    //load each item delivery methods
    var data = new FormData();
    data.append('selected_delivery_option', $('input[name=type]:checked').val());
    data.append('selected_regional_center', $('#location :selected').text());

    $.ajax({
      method: 'POST',
      url: '{{ route('cart.item.delivery.methods') }}',
      processData: false,
      contentType: false,
      cache: false,
      data: data,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(response){
        console.log(response);
        if(response){
          if(response.html){
            if(response.item_count == response.error_count){
              $('#btn-proceed-to-payment').addClass('disabled cursor-not-allowed');
              $('#btn-proceed-to-payment').attr('disabled', true);
            }

            $('#delivery_form').append('<input type="hidden" name="cannotProceedPaymentItemIds" value="'+response.cannotProceedArray+'">');
            $('#each-item-delivery-methods').html(response.html);
          }
        }
      },
      error: function(xhr){
        console.log(xhr);
      }
    });

    $('#btn-proceed-to-payment').on("click", function(){
      $('#delivery_form').submit();
    });
  });
  

  $('#location').on('change', function(){
    if($('option:selected', this).val() !== ''){
      $('#btn-next').prop('disabled', false);
      $('#btn-next').removeClass('cursor-not-allowed');
    }else{
      $('#btn-next').prop('disabled', true);
      $('#btn-next').addClass('cursor-not-allowed');
    }
    $('#regional_center').val($('option:selected', this).val());
  });
</script>
@stop