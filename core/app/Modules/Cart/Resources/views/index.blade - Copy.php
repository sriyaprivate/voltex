@extends('layouts.master') @section('title','Cart Summary')

@section('links')
<!-- toastr notification -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style type="text/css">
    
    .mtb1{
        font-family: "Lato", sans-serif;
        font-size: 14px;
        font-weight: 300;
        letter-spacing: 0.15em;
        text-transform: uppercase;
        text-decoration: none;
        text-align: center;
        padding: 1em 1.15em 1em 1.3em;
        border: 2px solid transparent;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border-radius: 0;
        color: white;
        cursor: pointer;
        /* height: 40px; */
        display: -webkit-inline-box;
        display: -webkit-inline-flex;
        display: -moz-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-box-direction: normal;
        -webkit-box-orient: horizontal;
        -webkit-flex-direction: row;
        -moz-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: baseline;
        -ms-flex-align: baseline;
        -webkit-align-items: baseline;
        -moz-align-items: baseline;
        align-items: baseline;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        -moz-justify-content: center;
        justify-content: center;
    }

    .bread-style{
        font-size: 15px;
        color: #000;
        font-weight: 300;
    }

    .item-title-name{
        font-family: Merriweather, Times, Times New Roman, serif !important;
        font-size: 28px !important;
        margin: 0.3em 0 !important;
        text-transform: none !important;
        letter-spacing: 0.03em !important;
    }

    @media (max-width: 500px) {
        .zoomContainer{
            height: 230px !important;
        }
    }

    .cart-row-container {
        overflow: hidden;
        border-bottom: 1px dashed #e3e3e3;
        border-top: 1px dashed #e3e3e3;
        margin-top: -1px;
        width: -webkit-fill-available;
    }

    .cart-row {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        padding: 10px 0px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        -moz-align-items: center;
        align-items: center;
        clear: both;
        overflow: hidden;
    }

    .c-24{
        position: relative;
        float: left;
        width: 100%;
        padding-left: 15px;
        padding-right: 15px;
    }

    .cart-row .product-image-placeholder {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -moz-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        min-width: 0;
        min-height: 0;
        position: relative;
        display: inline-block;
        vertical-align: middle;
        text-align: center;
        background-color: white;
        margin: 0em 0em 0 5em;
        width: 150px;
    }

    .cart-row .product-qty {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -moz-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        min-width: 0;
        min-height: 0;
        position: relative;
        vertical-align: middle;
        display: inline-block;
        margin-left: 0em;
    }

    .cart-row .product-qty .buttons {
        position: relative;
        display: inline-block;
        vertical-align: middle;
    }

    .cart-row .product-qty .count {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        margin-left: 0.3em;
        font-size: 4em;
        color: #555;
    }

    .cart-row .product-qty .buttons .plusminus {
        float: left;
        clear: both;
    }

    .cart-row .product-qty .buttons .plusminus.minus {
        margin-top: 0.3em;
    }

    .cart-row .product-qty .buttons .plusminus {
        float: left;
        clear: both;
    }

    .cart-row .product-info {
        -webkit-box-flex: 1;
        -webkit-flex: 1 1 auto;
        -moz-box-flex: 1;
        -moz-flex: 1 1 auto;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        min-width: 0;
        min-height: 0;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-direction: normal;
        -webkit-box-orient: horizontal;
        -webkit-flex-direction: row;
        -moz-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        -moz-align-items: center;
        align-items: center;
        cursor: pointer;
        margin-left: 5em;
        min-width: 120px;
    }

    .cart-row .product-info .essentials {
        -webkit-box-flex: 1;
        -webkit-flex: 1 1 auto;
        -moz-box-flex: 1;
        -moz-flex: 1 1 auto;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        min-width: 0;
        min-height: 0;
        height: 10em;
    }

    .cart-row .product-info .essentials h2 {
        display: inline-block;
        font-size: 2.7em;
        margin: 0;
        font-family: "Merriweather", Times, "Times New Roman", serif;
        text-transform: none;
        letter-spacing: 0.03em;
    }

    .cart-row .product-info .essentials .subtitle {
        display: block;
    }

    .cart-row .product-info .essentials .actions {
        margin-top: 1em;
        font-size: 0.8em;
    }

    .cart-row .product-info .essentials .actions a {
        border-right: 1px solid #ababab;
        padding: 0em 0.6em 0em 0.4em;
    }

    .cart-row .product-info .message.measurements {
        color: #c10006;
    }

    .cart-row .product-info .message {
        font-size: 1em;
        font-weight: 600;
        color: #c10006;
    }
    .m-1-v {
        margin-top: 1em !important;
        margin-bottom: 1em !important;
    }

    .cart-row-container a {
        font-weight: 400;
    }
    a {
        color: #081e33;
    }

    @media (min-width: 1152px)
    .responsive .cart-row .product-info .more {
        min-width: 280px;
        max-width: 280px;
    }

    .cart-row .product-info .more {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -moz-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        min-width: 0;
        min-height: 0;
        min-width: 296px;
        max-width: 296px;
        margin: 0px 0px 0px 0px;
        display: inline-block;
        vertical-align: middle;
        text-align: center;
        height: 9em;
    }

    .cart-row .product-price {
        width: 120px;
        height: 7.5em;
        max-width: 120px;
        display: inline-block;
        vertical-align: middle;
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 auto;
        -moz-box-flex: 0;
        -moz-flex: 0 0 auto;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        min-width: 0;
        min-height: 0;
        font-size: 1.35em;
        color: #333;
        text-align: right;
    }

    .img-row {
        vertical-align: middle;
        border-style: none;
    }

    .cart-row .product-price span {
        position: relative;
        display: inline-block;
        vertical-align: middle;
    }

    .price {
        font-weight: 300;
    }

    .beauty-heading {
        text-align: center;
        display: flex;
        flex-direction: row;
        justify-content: center;
        line-height: 1.5;
        width: 100%;
        color: #555;
        border-bottom: 1px solid #aaa;
        padding: 0.5em 0 0.3em;
        margin: 0 0 1em;
    }

    .measurement-value {
        width: 100px;
        font-size: 1.5em;
        margin-right: 5px;
        padding: 0.3em;
        text-align: center;
    }

    .unit {
        font-size: 1.5em;
    }

    .choice-image{
        border: 1px solid;
        margin-top: 15px;
    }

    .choice-image:hover{
        background-color: darkcyan;
        cursor: pointer;
    }

    .option-choice-image{
        border: 1px solid;
    }

    .option-row{
        font-weight: bolder;
        color: #000;
    }

    .option-row:hover{
        background-color: darkcyan;
        cursor: pointer;
        color: #fff;
    }

    .active-row{
        background-color: darkcyan;
        cursor: pointer;
        color: #fff;
    }

    .fabric-row:hover{
        background-color: darkcyan;
        cursor: pointer;
        color: #fff;
    }

    .fabric-active-row{
        background-color: darkcyan;
        cursor: pointer;
        color: #fff;
    }

    .breadcrumbs{
        display: block;
        overflow: hidden;
        padding: 10px 0;
        margin: 0;
        font-size: 13px;
        color: #999;
    }

    .breadcrumbs ul {
        padding: 0;
        margin: 0;
        list-style: none;
    }

    .breadcrumbs ul li {
        float: left;
        margin: 0 8px 0 0;
        font-size: 13px;
        line-height: 22px;
    }

    .white-block {
        display: block;
        padding: 24px 12px 30px;
        margin-bottom: 0px;
        min-height: 150px;
        background-color: #fff;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -moz-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        -webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        box-shadow: 0 1px 5px rgba(0,0,0,0.10);
    }

    .content-title{
        font-size: 26px!important;
        line-height: 30px;
        color: #eb232a;
        padding: 15px;
        margin: 0!important;
        font-family: 'geogrotesque_semibold';
    }

</style>
@stop

@section('content')
  <!-- breadcrumb -->
    <div class="breadcrumbs">
        <div class="container-fluid">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="c-272-outlets-switches.aspx">Shopping Cart</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Check-Out</li>
            </ul>
        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->

  
  <!-- item view -->
    <div class="item-view" style="margin-bottom: 10px">
        <div class="container-fluid">
            <div class="row white-block">
              <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12">
                @if(isset($cart) && count($cart) > 0)
                    <div id="row" style="padding-top: 0px;padding-bottom: 15px">
                        <h3 class="content-title">My Shopping Cart</h3>
                        @if(isset(collect($cart)->get('items')->first()['first_name']))
                            <div class="bg-light user-name" id="d-load" style="position: absolute;right: 0;margin-top: -41px;right: 200px;padding: 6px 15px;background-color: #dddddd91 !important;">
                                <img src="{{ asset('assets\images\default-user-profile.png') }}" style="width: 30px;position: absolute;margin-top: -6px;margin-left: -45px;"> 
                                <span id="customer_profile_pic">{{ collect($cart)->get('items')->first()['first_name'] }} {{ collect($cart)->get('items')->first()['last_name'] }}</span>
                                <a href="{{ route('remove.customer.cart', ['customer_id' => collect($cart)->get('items')->first()['customer_id']]) }}"><i class="fa fa-times" ></i></a>
                            </div>
                        @endif
                        @if(Auth::check())
                            <button type="button" class="pull-right btn btn-sm btn-primary float-right" style="position: absolute;right: 0;margin-top: -41px;right: 15px;" data-toggle="modal" data-target="#modalCustomerScan"><i class="fa fa-qrcode" aria-hidden="true"></i> Scan Customer</button>
                        @else
                            <a href="{{ url('login?next_url=cart/summary') }}" class="pull-right btn btn-sm btn-primary float-right" style="position: absolute;right: 0;margin-top: -41px;right: 15px;"><i class="fa fa-qrcode" aria-hidden="true"></i> Scan Customer</a>
                        @endif
                    </div>
                    
                    <form method="post" id="form_customer_scan">
                        <!-- The Modal -->
                        <div class="modal" id="modalCustomerScan">
                          <div class="modal-dialog">
                            <div class="modal-content">

                              <!-- Modal Header -->
                              <div class="modal-header">
                                <h4 class="modal-title"><i class="fa fa-qrcode" aria-hidden="true"></i> Scan Customer</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>

                              <!-- Modal body -->
                              <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label for="email" class="required">Customer ID</label>
                                        <input type="text" class="form-control" id="customer_id" placeholder="Please scan customer id or type">
                                    </div>
                                </form>
                              </div>

                              <!-- Modal footer -->
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-sm rounded-0" id="btn_customer_scan_done" disabled>Done</button>
                                <button type="button" class="btn btn-secondary btn-sm rounded-0" data-dismiss="modal">Close</button>
                              </div>

                            </div>
                          </div>
                        </div>
                    </form>

                    <form action="{{ route('cart.update') }}" method="post" class="form-cart-summary position-relative" id="cart-form">
                        <!-- cart summary -->
                        <!-- panel refresh -->
                        <div class="panel-refresh" name="panel-refresh"></div>
                        {{ csrf_field() }}
                        @if(isset(collect($cart)->get('items')->first()['customer_id']))
                            <input type="hidden" name="customer_id" value="{{ collect($cart)->get('items')->first()['customer_id'] }}">
                        @endif
                        <button type="submit" id="btn-update-cart" class="btn btn-sm btn-primary float-right invisible" style="position: absolute;right: 0;margin-top: -41px;"><i class="fa fa-pencil" aria-hidden="true"></i> Update Cart</button>
                        
                        <style type="text/css">
                            .price{
                                font-size: 19px !important;
                            }
                        </style>
                        @if(isset($cart) && count($cart) > 0 && isset($cart->items))
                            <?php
                                $customizeCost = 0;
                            ?>
                            @foreach($cart->items as $key => $data)
                                @if(count($data) > 0)
                                <div class="border pt-4 mb-4 pb-4">
                                    <div class="row" style="margin: 0px auto">
                                        @if(isset($data['availableQty']) && $data['availableQty'] == 0)
                                        <div class="item-disabled">
                                            <div class="ribbon ribbon-top-left"><span>Out of stock</span></div>
                                        </div>
                                        @endif
                                        <div class="col-2">
                                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                <span class="badge badge-danger rounded-badge">
                                                    {{ number_format($data['item']->discount_rate)?:'-' }}%<br>OFF
                                                </span>
                                            @endif
                                            <a href="{{ route('bigstore.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'item_name' => str_slug($data['item']->display_name?:$data['item']->name, '-'), 'item_id' => $key]) }}">
                                                <img src="{{ url('data/product_thumb/images/product/'.(isValid($data['item']['image'])? : DEFAULT_PRODUCT_IMG)) }}" class="img-fluid img-row" alt="{{ isValid($data['item']['display_name'])?: '-' }}" style="width:130px;">
                                            </a>
                                        </div>
                                        <div class="col-1">
                                            <div class="product-qty" style="padding-left: 40px;">
                                                <div class="buttons">
                                                    <div class="discreteButton mini symbolonly plusminus plus" style="cursor: pointer;">
                                                        <i class="fa fa-plus" aria-hidden="true" style="font-size: 1.8em"></i>
                                                    </div>
                                                    <div class="discreteButton mini symbolonly plusminus minus" style="cursor: pointer;">
                                                        <i class="fa fa-minus" aria-hidden="true" style="font-size: 1.8em"></i>
                                                    </div>
                                                </div>

                                                <div class="count">
                                                    <span class="display_qty" style="font-size: 28px;">{{ isValid($data['qty'])?: '0' }}</span>
                                                    <input type="hidden" min="1" max="{{ $data['availableQty']?:0 }}" class="form-control form-control-sm" name="qty[{{ (isset($data) && isset($data['cart_id']))? $data['cart_id'] : 0 }}]" value="{{ isValid($data['qty'])?: '0' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="essentials">
                                                <h5>
                                                    <a href="{{ route(isValid($data['item']->status) && $data['item']->status == DEAL_PROMOTION? 'saving-center.item.view' : 'bigstore.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'item_name' => str_slug($data['item']->display_name?:$data['item']->name, '-'), 'item_id' => $key]) }}">
                                                    {{ isValid($data['item']['display_name'])?: '-' }}</a>
                                                    <span class="el-icon-info-sign"></span>
                                                </h5>                

                                                <div class="messages"></div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="product-price" style="min-width: 200px;text-align: right;">      
                                                @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                    <span><strong class="pr-4 price" style="color: #868e96 !important;">{{ CURRENCY_SIGN }} {{ number_format((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100), 2)?: '-' }}</strong></span>
                                                    <br>
                                                    <span class="pr-4  text-secondary">
                                                        <del class="pr-1">{{ CURRENCY_SIGN }} {{ number_format(isValid($data['item']['price']), 2)?: '-' }}</del>
                                                    </span>
                                                @else
                                                    <span><strong class="pr-4 price" style="color: #868e96 !important;">{{ CURRENCY_SIGN }} {{ number_format(isValid($data['item']['price']), 2)?: '-' }}</strong></span>
                                                @endif  
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="pull-left" style="text-align: right;margin-left: 50px;">
                                                @if(strlen($data['item']->cutomize_price) !== 0)
                                                    @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                        <span><strong class="pr-4  price">{{ CURRENCY_SIGN }} {{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100)) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                    @else
                                                        <span><strong class="pr-4 price">{{ CURRENCY_SIGN }} {{ number_format(((isValid($data['item']['price']) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                    @endif
                                                @else
                                                    @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                        <span><strong class="pr-4  price">{{ CURRENCY_SIGN }} {{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                    @else
                                                        <span><strong class="pr-4 price">{{ CURRENCY_SIGN }} {{ number_format(((isValid($data['item']['price']) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0 , 2)?: '-' }}</strong></span>
                                                    @endif
                                                @endif

                                                <?php 
                                                    if(isValid($data['item']->cutomize_price)){
                                                        $customizeCost += ((collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0) * $data['qty']?:0);
                                                    }                            
                                                ?>
                                            </div>
                                            <a href="{{ url('cart/remove/'.((isset($data['item']) && !empty($data['item']->id))? $data['item']->id : 0)).'/'.((isset($data['cart_id']) && !empty($data['cart_id']))? $data['cart_id'] : 0) }}?next_url={{ route('cart.summary') }}" name="action" class="btn btn-light pull-right rounded-0 btn-sm" style="margin-left:-4px;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>

                                    <div class="row additional-data-{{ $key }}" style="display: block">
                                        <div class="col-md-12">
                                            @if(strlen($data['item']->choice_option) !== 0) 
                                                <?php
                                                    $options    = strExplode(isValid($data['item']->choice_option), ',', null);
                                                    $cart_ids   = strExplode(isValid($data['item']->cart_product_choices), ',', null);
                                                    $images     = strExplode(isValid($data['item']->option_image), ',', null);
                                                    $prices     = strExplode(isValid($data['item']->cutomize_price), ',', null);
                                                ?>
                                                @if(isset($options) && count($options) > 0)
                                                    @foreach($options as $key => $option)
                                                        <div class="row" style="margin-bottom: 5px">                                        
                                                            <div class="col-3"></div>
                                                            <div class="col-1">
                                                                <img src="{{ url(UPLOAD_PATH).'/uploads/images/choices/options/'.$images[$key] }}" class="border">
                                                            </div>
                                                            <div class="col-3">
                                                                <h6 style="padding-top: 10%">{{ isValid($options[$key]) }}</h6>
                                                            </div>
                                                            <div class="col-2">
                                                                <span>
                                                                    <strong class="pr-4  price text-right" style="font-size: 15px;padding-top: 14%;color: #868e96 !important;">
                                                                        Rs {{ isValid(number_format($prices[$key], 2)) }}
                                                                    </strong>
                                                                </span>
                                                            </div>           
                                                            <div class="col-3">
                                                                <a href="{{ route('remove.cart.customize.option', ['cart_id' => $cart_ids[$key]]) }}" name="action" class="btn btn-light pull-right rounded-0 btn-sm mr-3 mt-4" style="margin-left:-4px;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                            </div>                     
                                                        </div>

                                                        <input type="hidden" name="options[]" value="{{ (isset($data['cart_id']) && !empty($data['cart_id']))? $data['cart_id'] : 0 }}_{{ $cart_ids[$key] }}">
                                                    @endforeach                                                        
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                            <input type="hidden" name="c_c" value="{{ $customizeCost }}">
                        @else
                            <div class="bordered m-4">
                            <div class="text-center text-secondary">
                                <h4>Your shopping cart is empty</h4>
                                <span class="mt--4">You have not items in your shopping cart, add item <a class="text-primary" href="{{ route('bigstore.index') }}">here</a></span>
                            </div>
                            </div>
                        @endif

                        <script>
                            var tm = 0;
                            function showAdditional(no){
                                $('.additional-data-'+no).toggle();
                            }
                        </script>
                    </form>
                @endif
              </div>
                @if(isset($cart) && count($cart))
                  <div class="col-lg-4 col-md-4 col-sm-4 col-12 offset-lg-8 pr-0 position-relative">
                    @include('cart::includes.cart_summary', ['btn_text' => 'checkout'])
                  </div>
                  <input type="hidden" id="c_data" value="{{ collect($cart)->get('items')->pluck('id')?:'' }}">
                @else
                    <div style="margin:0 auto;">
                        <div class="bordered m-4">
                        <div class="text-center text-secondary">
                            <h4>Your shopping cart is empty</h4>
                            <span class="mt--4">You have not items in your shopping cart, add item <a class="text-primary" href="{{ route('store.index') }}?type_id=1">here</a></span>
                        </div>
                        </div>
                    </div>
                @endif
            </div>        
        </div>
    </div> <!-- /.item specification -->

@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>

  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#fixed-menu').hide();

            $('.fabric-option-row').click(function(){
                console.log('jnsdfjjkdf');
                $(this).addClass('fabric-active-row');
            });

        });

        $('#customer_id').on('keyup', function(e){
            var customer_id = $(this).val();

            if(customer_id.length > 0){
                $('#btn_customer_scan_done').attr('disabled', false);
            }else{
                $('#btn_customer_scan_done').attr('disabled', true);
            }
        });

        $('#modalCustomerScan').on('shown.bs.modal', function() {
            $("#customer_id").focus();
        })

        $(document).on('submit','#form_customer_scan',function(e){
            e.preventDefault();

            var customer_id   = $('#customer_id').val();
            var cart_items_id = $('#c_data').val();
            
            $.ajax({
                url: '{{ route("config.customer") }}',
                type: 'POST',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'), 
                    customer_id: customer_id,
                    cart_items_id: cart_items_id
                },
                dataType: 'JSON',
                success: function (response) {
                    $('#modalCustomerScan').modal('hide');
                    removePanelRefresh();

                    if(response.success == true){
                        $('.user-name').removeClass('d-none');

                        $.confirm({
                            theme: 'material',
                            title: 'Done!', 
                            type: 'green',
                            content: response.msg,
                            buttons: {
                                ok: function(){
                                    location.reload();
                                }
                            }
                        });
                    }else{
                        $.confirm({
                            theme: 'material',
                            title: 'Warning!', 
                            type: 'red',
                            content: response.msg,
                            buttons: {
                                ok: function(){
                                    location.reload();
                                }
                            }
                        });
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });

            $('.option-row').click(function(){
                $(this).addClass('active-row');
            });

        });

    </script>

    <!-- load cart js in extranal useage -->
    @include('cart::includes.cart-js')
@stop