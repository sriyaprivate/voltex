@extends('layouts.master') @section('title','Payment option')

@section('links')
  <!-- select2 -->
  <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style>
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #ccc;
            box-shadow:  0px 0px 0px 0px #ccc;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:auto;
    padding:0 10px;
    border-bottom:none;
}
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="{{ route('cart.summary') }}">Cart Summary</a></li>
        <li class="breadcrumb-item"><a href="{{ route('cart.delivery') }}">Delivery & Pickup</a></li>
        <li class="breadcrumb-item active">Payment Confirmation</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="cart-summary mb-4">
    <div class="container">

      <div class="row mb-4">
        <div class="col-md-12">
          @include('cart::includes.breadcrumb')
        </div>
      </div>

      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 position-relative">
          <!-- panel refresh -->
          <div class="panel-refresh" name="panel-refresh"></div>
          <div class="card product-list mb-4">
            <div class="card-header border-bottom-0 bg-gray">
                <div class="row">
                    <div class="col">
                      <h6 class="h6 text-uppercase mt-2 font-weight-bold">Order Information</h6>
                    </div>
                </div>
            </div>
            <div class="card-body pl-4 pr-4 mb-1">
              <div class="row">
                <div class="col-12">
                <div id="collapseBilling">
                  <div class="card card-body rounded-0 border-0 pl-0 pr-0 pb-0">
                      <input type="hidden" name="type" value="{{ Request::get('type') }}">
                      @if(count($delivery_information))
                        <!-- <fieldset class="scheduler-border">
                          <legend class="scheduler-border">Delivery information</legend>
                          
                        </fieldset> -->
                        <table class="table-sm table table-layout-fixed">
                          <tr>
                            <td colspan="3" class="border-top-0">
                              <div class="pb-1 pull-left small-font-14-mobile" style="font-size: 15px;">Delivery & payment method</div>
                              <a href="{{ route('cart.delivery') }}{{ isValid($cartDetails) && isValid($cartDetails->delivery_method_name)? '?type='.strtolower($cartDetails->delivery_method_name) : '' }}" class="btn border pt-1 bg-gray pull-right"><span class="fa fa-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Delivery Type</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>
                              @if(isset($cartDetails) && isset($cartDetails->delivery_method_name) && strtolower($cartDetails->delivery_method_name) == PICKUP)
                                <table class="mt-4 mb-4 table">
                                  <tr>
                                    <td>Selected type</td>
                                    <td><strong class="pr-3 pl-3">:</strong></td>
                                    <td>{{ isValid($cartDetails) && isValid($cartDetails->delivery_method_name)? $cartDetails->delivery_method_name :'-' }}</td>
                                  </tr>
                                  <tr>
                                    <td>Regional Center</td>
                                    <td><strong class="pr-3 pl-3">:</strong></td>
                                    <td>{{ $cartDetails->regional_center_name?:'-' }}</td>
                                  </tr>
                                  <tr>
                                    <td>Address</td>
                                    <td><strong class="pr-3 pl-3">:</strong></td>
                                    <td>{{ $cartDetails->regional_center_address?:'-' }}</td>
                                  </tr>
                                  <tr>
                                    <td>Contact no</td>
                                    <td><strong class="pr-3 pl-3">:</strong></td>
                                    <td>{{ $cartDetails->regional_center_contact_no?:'-' }}</td>
                                  </tr>
                                </table>
                              @else
                                {{ isValid($cartDetails) && isValid($cartDetails->delivery_method_name)? $cartDetails->delivery_method_name :'-' }}
                              @endif
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Payment Method</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>
                              {{ isValid($cartDetails) && isValid($cartDetails->payment_method_name)? $cartDetails->payment_method_name :'-' }}
                              </br>
                              <img class="text-center" src="{{ url('data/bank_card/images/cards') }}/{{ isValid($cartDetails) && isValid($cartDetails->image_path)? $cartDetails->image_path :'-' }}" alt="{{ isValid($cartDetails) && isValid($cartDetails->payment_method_name)? $cartDetails->payment_method_name :'-' }}"/>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="3">&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="3" class="border-top-0">
                              <div class="pb-1 pull-left pb-3" style="font-size: 15px;">Delivery Information</div>
                              <!-- <a href="{{ route('user.account.info.view') }}" class="btn pt-1 bg-gray pull-right"><span class="fa fa-pencil"></span></a> -->
                            </td>
                          </tr>
                          <tr>
                            <td><strong>First Name</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->first_name)?:'-' }}</td>
                          </tr>
                          <tr>
                            <td><strong>Last Name</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->last_name)?:'-' }}</td>
                          </tr>
                          <tr>
                            <td><strong>Phone no</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->phone_no)?:'-' }}</td>
                          </tr>
                          <tr>
                            <td><strong>Mobile no</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->mobile_no)?:'-' }}</td>
                          </tr>
                          <tr>
                            <td><strong>Address line 1</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->address_line_1)?:'-' }}</td>
                          </tr>
                          <tr>
                            <td><strong>Address line 2</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->address_line_2)?:'-' }}</td>
                          </tr>
                          <tr>
                            <td><strong>Email</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->email)?:'-' }}</td>
                          </tr>
                          <tr>
                            <td><strong>City</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->city)?:'-' }}</td>
                          </tr>
                          <tr>
                            <td><strong>ZIP/Postal Code</strong></td>
                            <td><strong class="pr-3 pl-3">:</strong></td>
                            <td>{{ isValid($delivery_information->postal_code)?:'-' }}</td>
                          </tr>
                        </table>
                      @endif
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>

          <form action="{{ route('cart.update') }}" method="post" class="form-cart-summary position-relative" id="cart-form">
            <!-- cart summary -->
            <input type="hidden" name="next_url" value="{{ route('cart.payment.confirmation') }}">
            <!-- panel refresh -->
            <div class="panel-refresh" name="panel-refresh"></div>
            {{ csrf_field() }}
            <div class="card product-list">
              <div class="card-header border-bottom-0 bg-gray">
                  <div class="row">
                      <div class="col">
                      <h6 class="h6 text-uppercase mt-2 font-weight-bold">Product Items</h6>
                      </div>
                      <div class="col">
                      <button type="submit" id="btn-update-cart" class="btn btn-sm btn-primary float-right" disabled><i class="fa fa-pencil" aria-hidden="true"></i> Update Cart</button>
                      </div>
                  </div>
              </div>
              @include('cart::includes.cart', ['next_url' => route('cart.payment.confirmation'), 'hide_out_of_stock' => true])
            </div>
          </form>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
          <div class="position-relative">
            <div id="user-selected">
              <input type="hidden" name="pickup[location_id]" id="regional_center" value="">
            </div>
            @if(isValid($cartDetails) && isset($cartDetails['delivery_method_name']) && strtolower($cartDetails['delivery_method_name']) == PICKUP)
              @include('cart::includes.cart_summary', ['btn_text' => 'Confirm', 'showDeliveryCharges' => false, 'delivery_charge' => 0, 'disabled' => false, 'show_modal' => false, 'btn_id' => 'btn-confirm'])
            @elseif(isValid($cartDetails) && isset($cartDetails['delivery_method_name']) && strtolower($cartDetails['delivery_method_name']) !== PICKUP)
              @include('cart::includes.cart_summary', ['btn_text' => 'Confirm', 'showDeliveryCharges' => true, 'delivery_charge' => $delivery_charge, 'disabled' => false, 'show_modal' => false, 'btn_id' => 'btn-confirm'])
            @else
              @include('cart::includes.cart_summary', ['btn_text' => 'Confirm', 'showDeliveryCharges' => false, 'delivery_charge' => 0, 'disabled' => false, 'show_modal' => false, 'btn_id' => 'btn-confirm'])
            @endif
          </div>
        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
  <!-- select2 -->
  <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
  <!-- toastr notification -->
  <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>
  <!-- custome js file -->
  <script src="{{ asset('assets/js/main.js') }}"></script>
@stop 

@section('scripts')
  <!-- load cart js in extranal useage -->
  @include('cart::includes.cart-js')
  
  <script>
    $('#btn-confirm').click(function(){
      window.open("{{ url('cart/make/payment') }}", "_self");
    });
  </script>
@stop