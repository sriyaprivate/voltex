@extends('layouts.master') @section('title','Payment gateway response')

@section('links')
  <!-- select2 -->
  <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style>
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #ccc;
            box-shadow:  0px 0px 0px 0px #ccc;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:auto;
    padding:0 10px;
    border-bottom:none;
}
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item active">Payment gateway response</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="cart-summary mb-4">
    <div class="container">

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 position-relative">
          <!-- panel refresh -->
          <div class="panel-refresh" name="panel-refresh"></div>
          <div class="card product-list mb-4">
            <div class="card-header border-bottom-0 bg-gray">
                <div class="row">
                    <div class="col">
                      <h6 class="h6 text-uppercase mt-2 font-weight-bold d-flex justify-content-center-mobile">Payment gateway response</h6>
                    </div>
                </div>
            </div>
            <div class="card-body pl-4 pr-4 mb-1"  style="height: auto !important;">
              <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-12 mx-auto">
                    @if(isset($gatewayResponse->ResponseCode) && $gatewayResponse->ResponseCode == 1)
                        <div class="h3 small-font-21-mobile text-center text-center" style="margin-top: 20px;">Thank you for your purchase.</div>
                        <div class="h6 text-center" style="font-size: 15px;">Your order has been received.</div>
                    @else
                        <div class="h4 small-font-21-mobile text-center" style="margin-top: 20px;">Something went wrong!.</div>
                        <div class="h6 text-center small-font-10-mobile" style="font-size: 15px;">make sure provide valid details.</div>
                    @endif
                    <table class="table" style="margin-top: 60px; margin-bottom: 45px;">
                        @if(count($gatewayResponse) > 0)
                            @if(strExplode(strExplode($gatewayResponse->OrderID, '_', 0) !== ''))
                                <tr>
                                    <td style="width: 20%;"><div class="h6 text-right">Order no</div></td>
                                    <td><strong> : </strong></td>
                                    <td><div class="h5 small-font-10-mobile {{ $gatewayResponse->ResponseCode == 1? 'text-success' : 'text-danger' }}">{{ strExplode($gatewayResponse->OrderID, '_', 0) }}</div></td>
                                </tr>
                            @endif
                            @if(isset($gatewayResponse->ReasonCodeDesc))
                                <tr>
                                    <td><div class="h6 text-right">Message</div></td>
                                    <td><strong> : </strong></td>
                                    <td><div class="h5 small-font-10-mobile {{ $gatewayResponse->ResponseCode == 1? 'text-success' : 'text-danger' }}">{{ $gatewayResponse->ReasonCodeDesc }}</div></td>
                                </tr>
                            @endif

                            @if(isset($gatewayResponse->ResponseCode) && $gatewayResponse->ResponseCode != 1)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><strong>&nbsp;</strong></td>
                                    <td>
                                        <form action="{{ route('cart.get.make.payment') }}" method="GET">
                                            <input type="hidden" name="order_no" value="{{ strExplode($gatewayResponse->OrderID, '_', 0) }}">
                                            <button class="btn btn-primary btn-sm small-font-10-mobile" type="submit" style="padding:0px 20px !important"><span class="fa fa-repeat"></span> Re-try</button>
                                        </form>
                                    </td>
                                </tr>
                            @endif
                        @else
                            <td colspan="3"><div class="h4 text-center">Gateway Response not found!.</div></td>
                        @endif
                    </table>
                    @if(isset($gatewayResponse->ResponseCode) && $gatewayResponse->ResponseCode == 1)
                        <div class="h5 text-center pb-4" style="font-size: 15px;">You will receive an e-mail confirmation with order details shortly.</div>
                        <a href="{{ route('bigstore.index') }}" class="btn btn-sm btn-block btn-primary mb-0">Continue Shopping</a>
                    @endif
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
  <!-- select2 -->
  <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
  <!-- toastr notification -->
  <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>
  <!-- custome js file -->
  <script src="{{ asset('assets/js/main.js') }}"></script>
@stop 

@section('scripts')
  <!-- load cart js in extranal useage -->
  @include('cart::includes.cart-js')
  
  <script>
    
  </script>
@stop