<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="lahiru dilshan">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - payment gateway</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- ecommerce styles -->
    <link href="{{asset('assets/css/ecommerce.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/media.css')}}" rel="stylesheet">
    <style>
        @keyframes spinner {
            to {transform: rotate(360deg);}
        }
        
        .spinner:before {
            content: '';
            box-sizing: border-box;
            position: absolute;
            top: 50%;
            left: 50%;
            width: 40px;
            height: 40px;
            margin-top: -10px;
            margin-left: -10px;
            border-radius: 50%;
            border: 2px solid #ccc;
            border-top-color: #ed1a3d;
            animation: spinner .6s linear infinite;
        }
        .big-text{
            font-size: 75px;
        }
        .small-text{
            font-size: 25px;
        }
        .m-top{
            margin-top:200px
        }
        
        @media screen and (max-width: 680px) {
            .big-text{
                font-size: 35px;
            }
            .small-text{
                font-size: 15px;
            }
            .m-top{
                margin-top:100px
            }
        }    
    </style>
  </head>
  <body onload="submitForm()">    
    <div class="container-fluid">
        <div class="row h-100">
            <div class="col-12"  style="margin-top:20%">
                <div class="text-center big-text">Please wait</div>
                <div class="text-center small-text">Payment gateway is beging processing.</div>
                <br>
                <div class="spinner m-top"></div>
                <div class="d-none">
                    {!! $paymentForm !!}
                </div>
            </div>
        </div>
    </div>
  </body>

  <script>
    function submitForm(){
        document.getElementById("gatewayForm").submit();
    }
  </script>
</html>
