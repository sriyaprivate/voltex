@extends('layouts.master') @section('title','Payment option')

@section('links')
  <!-- select2 -->
  <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style>
.panel-heading.collapsed .fa-chevron-down,
.panel-heading .fa-chevron-right {
  display: none;
}

.panel-heading.collapsed .fa-chevron-right,
.panel-heading .fa-chevron-down {
  display: inline-block;
}

i.fa {
  cursor: pointer;
  margin-right: 5px;
}

.collapsed ~ .panel-body {
  padding: 0;
}
</style>
@stop

@section('content')
  <!-- breadcrumb -->
  <div class="custome-breadcrumb">
    <div class="container">

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home.index') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li class="breadcrumb-item"><a href="{{ route('cart.summary') }}">Cart Summary</a></li>
        <li class="breadcrumb-item"><a href="{{ route('cart.delivery') }}">Delivery & Pickup</a></li>
        <li class="breadcrumb-item active">Payment option</li>
      </ol>

    </div> <!-- /.container -->
  </div> <!-- /.breadcrumb -->


  <!-- account-section -->
  <div class="cart-summary mb-4">
    <div class="container">

      <div class="row mb-4">
        <div class="col-md-12">
          @include('cart::includes.breadcrumb')
        </div>
      </div>

      <div class="row">
        <div class="col-lg-9 col-md-12 col-sm-9 col-12">
          <div class="card product-list position-relative mb-4">
            <div class="panel-refresh" name="panel-refresh"></div>
            <div class="card-header bg-gray border-bottom-0">
              <h6 class="h6 text-uppercase mt-2 font-weight-bold">Coupon</h6>
            </div>
            <div class="card-body pl-4 pr-4">
              <div class = "container">
                <a class="btn btn-sm btn-light btn-block rounded-0 custome-tgl-btn collapsed accordion-toggle" id="collapseS" aria-expanded="false" data-toggle="collapse" href="#couponRedeem" aria-controls="couponRedeem">
                  <span class="togg-name float-left font-weight-bold">Coupon Redeem</span>
                  <span class="togg float-right"><i id="toggle_btn" class="fa fa-toggle-off" aria-hidden="true"></i></span>
                </a>
                <div class="panel-collapse collapse in  mb-4 mt-3" id="couponRedeem">
                  <div class="ml-4">
                    <div class="row">
                      <div class="col-10">
                        <div class="form-group">
                          <!-- <label for="coupon">Coupon</label> -->
                          <input type="text" class="form-control" id="coupon" aria-describedby="emailHelp" placeholder="Enter coupon code." style="height: 33px;">
                          <small id="emailHelp" class="form-text text-muted">Please enter valid coupon code to redeem..</small>
                        </div>
                      </div>
                      <div class="col-2">
                        <div class="form-group">
                          <!-- <label for="coupon">Coupon</label> -->
                          <button type="submit" class="btn btn-sm btn-block btn-primary" id="coupon_btn">redeem</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card product-list position-relative">
            <div class="panel-refresh" name="panel-refresh"></div>
            <div class="card-header bg-gray border-bottom-0">
              <h6 class="h6 text-uppercase mt-2 font-weight-bold">Payment Options</h6>
            </div>
            <div class="card-body pl-4 pr-4">
              <form action="{{ route('cart.payment.confirmation') }}" method="POST" id="form-payment-method">
                {!! csrf_field()  !!}
                <!-- payment method accordings -->
                @if(count($paymentMethods) > 0)
                  <div class = "container">
                    <div class="panel-group mb-4 mt-3" id="accordion">
                      <form action="{{ route('cart.payment.confirmation') }}" method="POST">
                        @foreach($paymentMethods as $key => $method)
                          <div class="panel panel-default">
                            <div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#heading_{{ $key }}">
                              <div class="panel-heading">
                                <h4 class="panel-title">
                                  <img src="{{ url('data/payment_methods/images/cards/'.$method->image) }}" class="float-left mr-4"/>
                                  <span class="h6 align-middle">{{ isValid($method->card_name, '-') }}</span>
                                  <div class="pull-right">
                                    @if(isset($method->cart_installment) && count($method->cart_installment) > 0 && isset($method->cart_installment[0]->rule) && isValid(hideIfNotSetToRule($cart, $method->cart_installment[0]->rule), false))
                                      <i id="arrow-icon" class="fa fa-angle-right text-secondary" style="font-size: 18px;margin-right: 10px;"></i>
                                    @else
                                      <input type="radio" name="payment_method" value="{{ isValid($method->id, '-') }}" {{ isValid($method->status, false)? '':'disabled' }}>
                                    @endif
                                  </div>
                                </h4>
                              </div>
                            </div>
                            <div id="heading_{{ $key }}" class="panel-collapse collapse in">
                              <div class="panel-body">
                                </br>
                                  <div class="text-light-gray indent-75">{{ isValid($method->description, '-') }}</div>

                                  <div class="installment_block">
                                    @if(isset($method->cart_installment) && count($method->cart_installment) > 0 && isset($method->cart_installment[0]->rule) && isValid(hideIfNotSetToRule($cart, $method->cart_installment[0]->rule), false))
                                      <div class="mt-4 overflow-x-scroll indent-75">
                                        @if(isset($method->cart_installment) && count($method->cart_installment) > 0)
                                          <div class="f15u">Normal Payment</div>
                                            <label class="default-payment-method panel-title"><input type="radio" name="payment_method"  value="{{ isValid($method->id, '-') }}" {{ isValid($method->status, false)? '':'disabled' }}> Pay with {{ isValid($method->card_name, '-') }}
                                            </label>
                                          <div class="f15u mt-4">Easy installment plans</div>
                                          @if(isset($method->cart_installment) && isset($method->cart_installment[0]) && !empty($method->cart_installment[0]))
                                            <label>*{{ getOparetorText($method->cart_installment[0]->operator) }} total for installments is Rs. {{ number_format($method->cart_installment[0]->rule) }}</label>
                                          @endif
                                          <div class="position-relative">
                                            <table class="table installment-plan-table">
                                              <thead class="thead-light">
                                                <tr>
                                                  <th>Action</th>
                                                  <th>Plan (Months)</th>
                                                  <th>Installment</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($method->cart_installment->sortBy('installment.installment_plan') as $easyPayment)
                                                  @if(isset($easyPayment->installment) && count($easyPayment->installment) > 0)
                                                    <tr class="row-select panel-title">
                                                      <td><input type="radio" name="payment_method" class="ml-3" value="{{ $easyPayment->installment->id }}"/></td>
                                                      <td>{{ $easyPayment->installment->installment_plan }}</td>
                                                      <td>
                                                        Rs. {{ number_format(isValid(getInstallmentAmount($easyPayment->installment->installment_plan, $easyPayment->interest_rate,  $cart), 0)) }}</td>
                                                    </tr>
                                                  @endif
                                                @endforeach
                                              </tbody>
                                            </table>
                                          </div>
                                        @endif
                                      </div>
                                    @endif
                                  </div>
                                </br>
                              </div>
                            </div>
                          </div>
                        @endforeach
                      </form>
                    </div>  
                  </div>
                @endif
                <!-- end container -->
              </form>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-12 col-sm-3 col-12">
          <div class="position-relative">
            <div class="panel-refresh"></div>
            @if(count($cartDetails) && isset($cartDetails['delivery_method_name']))
              @if(strtolower($cartDetails['delivery_method_name']) == PICKUP)
                @include('cart::includes.cart_summary', ['btn_text' => 'Next', 'showDeliveryCharges' => false, 'delivery_charge' => 0, 'disabled' => true, 'show_modal' => false])
              @elseif(strtolower($cartDetails['delivery_method_name']) !== PICKUP)
                @include('cart::includes.cart_summary', ['btn_text' => 'Next', 'showDeliveryCharges' => true, 'delivery_charge' => $delivery_charge, 'disabled' => true, 'show_modal' => false])
              @else
                @include('cart::includes.cart_summary', ['btn_text' => 'Next', 'showDeliveryCharges' => false, 'delivery_charge' => 0, 'disabled' => true, 'show_modal' => false])
              @endif
            @endif
          </div>
        </div>
      </div>

    </div> <!-- /.container -->
  </div> <!-- /.account-section -->
@stop

@section('js')
  <!-- select2 -->
  <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
  <!-- toastr notification -->
  <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

  <script src="{{ asset('assets/js/main.js') }}"></script>
@stop 

@section('scripts')
<script>
  $(document).ready(function(){
    removePanelRefresh();
    //add panel refresh class
    $('form').on('submit', function(){
      addPanelRefresh();
    });

    $(".row-select").click(function() {
        var checkbox = $(this).find("input[type='radio']");
        checkbox.prop('checked', true);
    });
  });

  $('.panel-title').on('click', function (e) {    
    if($(this).find('#arrow-icon').length){
      $('#arrow-icon').toggleClass('fa-angle-down fa-angle-right');
      $('.default-payment-method').find('input[name=payment_method]').prop('checked', true);
    }else{
      $('#arrow-icon').removeClass('fa-angle-down');
      $('#arrow-icon').addClass('fa-angle-right');
    }

    if($(this).find('input[name=payment_method]').prop('disabled') == false){
      $(this).find('input[type=radio]').prop('checked', true);

      if($(this).find('input[type=radio]').prop('checked') == true){
        $('#btn-next').prop('disabled', false);
      }else{
        $('#btn-next').prop('disabled', true);
      }
    }else{
      $('input[name=payment_method]').prop('checked', false)
    }
  });

  $('input[type=radio]').click(function(e){
    $(this).prop('checked', true);
  });

  $('#btn-next').on('click', function(){
    $('#form-payment-method').submit();
  });

  $('#collapseS').on('click', function(e){
    $('#collapseS span.togg i').toggleClass('fa-toggle-off fa-toggle-on', 200);
  });
</script>
@stop