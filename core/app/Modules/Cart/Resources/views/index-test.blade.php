@extends('layouts.master') @section('title','Cart Summary')

@section('links')
<!-- toastr notification -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
<link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">
@stop

@section('css')
    <style type="text/css">
        .cart{
            margin: 90px -25px 0px 0 !important;
        }

        .performa-invoice-cart{
            margin: 90px -25px 0px 0 !important;
        }

        .cart-summery-card {
            background: white;
            width: 100%;
            border-radius: 3px;
            padding: 20px;
            margin-bottom: 5%;
            z-index: 9999;
            box-shadow: 0 1px 30px rgba(1, 1, 1, 0.2);
            -webkit-box-shadow: 0 1px 30px rgba(1, 1, 1, 0.2);
        }

        .cart-summery-card-header {
            border-bottom: 1px solid #E8E8E8;
            padding-bottom: 15px;    
        }
        .cart-summery-card-total {
          float: right;
          font-weight: bold;
        }

        .cart-summery-card-items {
            padding-top: 20px;
        }

        .cart-summery-card-items li {
            list-style-type: none;
            border-bottom: 1px solid #E8E8E8;
            padding-bottom: 10px;
            cursor: pointer;
            margin-bottom: 8px;
        }

        .cart-summery-card-items li:hover {
            background-color: whitesmoke;
        }

        .cart-summery-card-clearfix:after {
          content: "";
          display: table;
          clear: both;
        }

        .white-box-whole ul li{
            box-shadow: 0 0px 10px rgba(1, 1, 1, 0.1);
            -webkit-box-shadow: 0 0px 10px rgba(1, 1, 1, 0.1);
        }

        .short_name{
            display: inline-block;
            width: 100%;
            overflow: visible;
            text-overflow: ellipsis;
        }


    .qty {
        text-align: center;
        color: #495057; 
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }

    .summery-text{
        font-size: 15px;
        color: #000;
        font-weight: bold;
        padding: 10px;
    }
    </style>
@stop

@section('content')

    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="c-272-outlets-switches.aspx">Cart View</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Save Order</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <form action="{{ route('cart.update') }}" method="post" class="form-cart-summary position-relative" id="cart-form">
                {{ csrf_field() }}
                <button type="submit" id="btn-update-cart" class="btn btn-sm btn-primary float-right invisible" style="position: absolute;right: 0;margin-top: -41px;"><i class="fa fa-pencil" aria-hidden="true"></i> Update Cart</button>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                        <ul class="cart-summery-cart-items" style="padding-top: 0px !important">
                        <?php $i=1; ?>
                        @foreach($cart->items as $key => $data)
                            @if(count($data) > 0)
                                <li class="clearfix">
                                    <div class="row">
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                            <img style="width: 150px" src="{{ url('data/product_thumb/images/product/'.$data['item']->image)}}" alt="item1"/>
                                        </div>
                                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="short_name" style="padding-top: 20px;font-size: 12px">{{$data['item']->name}}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3" style="padding-right: 0px">
                                                    <img class="pull-right" src="{{url(UPLOAD_PATH.'uploads/images/flags/'.$data['item']->country_flag)}}" style="width: 30px;height: 20px;float: left" title="{{$data['item']->country_name}}">
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px">
                                                    <p class="short_name">{{$data['item']->code}}</p>
                                                </div>
                                            

                                                <div class="col-md-5">
                                                    <p class="">{{'$'}}{{$data['item']->price}}</p>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                            <div class="row">
                                                <div class="col-md-12" style="text-align: right;padding-top: 22px">
                                                    <input type="button" value="-" class="qtyminus" field="qty_{{$data['item']->id}}" />
                                                    <input type="text" name="qty[{{ (isset($data) && isset($data['cart_id']))? $data['cart_id'] : 0 }}]" id="qty_{{$data['item']->id}}" value="{{ isValid($data['qty'])?: '0' }}" class="qty" style="width: 100px;height: 25px;" />
                                                    <input type="button" value="+" class="qtyplus" field="qty_{{$data['item']->id}}" style="margin-right: 10%" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="pull-right" style="margin-bottom: 0px;padding-right: 10%;padding-top: 5%;color: #000;font-weight: bold">{{'$'}}{{$data['item']->price*$data['qty']}}</p>        
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="{{ url('cart/remove/'.((isset($data['item']) && !empty($data['item']->id))? $data['item']->id : 0)).'/'.((isset($data['cart_id']) && !empty($data['cart_id']))? $data['cart_id'] : 0) }}?next_url={{ route('cart.summary') }}" class="pull-right" name="action" style="padding-right: 10%;font-size: 10px;text-decoration: underline;color: crimson;border-bottom: 0px"><p>Remove</p></a>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                            <?php $i++; ?>
                        @endforeach
                        </ul>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                        @if(isset($country_cart))
                            @foreach($country_cart as $key=>$value)
                                <div class="cart-summery-card">
                                    <div class="cart-summery-card-header">
                                        {{$key}} <img class="pull-right" src="{{url(UPLOAD_PATH.'uploads/images/flags/'.$value['flag'])}}" style="width: 30px;height: 20px;float: left;margin-right: 5px" title="{{$data['item']->country_name}}"> - Cart Summery
                                    </div>

                                    <div class="row summery-text">
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">Item Code</div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Unit Price</div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right;">Quantity</div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right;">Amount</div>
                                    </div>

                                    <?php $total_amount = 0; ?>
                                    @foreach($value['data'] as $index=>$data)
                                        <div class="row summery-text">
                                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">{{$data['item']->code}}</div>
                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">${{$data['item']->price}}</div>
                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right;">{{$data['qty']}}</div>
                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right;">${{$data['item']->price*$data['qty']}}</div>
                                        </div>
                                        <?php $total_amount += $data['item']->price*$data['qty']; ?>
                                    @endforeach

                                    <div class="row summery-text">
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right;">Total</div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: right;">${{$total_amount}}</div>
                                    </div>                                    
                                </div>  
                            @endforeach
                        @endif

                        <div class="cart-summery-card">
                            <div class="cart-summery-card-header">
                                @if(isset($cart) && count($cart))
                                    @include('cart::includes.cart_summary2', ['btn_text' => 'checkout'])
                                    <input type="hidden" id="c_data" value="{{ collect($cart)->get('items')->pluck('id')?:'' }}">
                                @else
                                    <div style="margin:0 auto;">
                                        <div class="bordered m-4">
                                            <div class="text-center text-secondary">
                                                <h4>Your cart is empty</h4>
                                                <span class="mt--4">You have not items in your cart, add item <a class="text-primary" href="{{ route('store.index') }}?type_id=1">here</a></span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>

@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>

  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#fixed-menu').hide();


            $('.qtyplus').click(function(e){
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('#'+fieldName).val());
                // If is not undefined
                if (!isNaN(currentVal)) {
                    // Increment
                    $('#'+fieldName).val(currentVal + 1);
                } else {
                    // Otherwise put a 0 there
                    $('#'+fieldName).val(0);
                }

                $('#btn-update-cart').click();

            });
            // This button will decrement the value till 0
            $(".qtyminus").click(function(e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('#'+fieldName).val());
                // If is not undefined
                if (!isNaN(currentVal)) {
                    // Increment
                    $('#'+fieldName).val(currentVal - 1);
                } else {
                    // Otherwise put a 0 there
                    $('#'+fieldName).val(0);
                }

                $('#btn-update-cart').click();

            });

            $('#btn-next').click(function(){
                $('#cart-form').prop('action', "{{ route('cart.update').'?next_url=cart/invoice' }}");
              
                $('<input>').attr({
                    type: 'hidden',
                    name: 'updateStatus',
                    value: '1'
                }).appendTo($('#cart-form'));
                $('#cart-form').submit();
            });

        });

    </script>
@stop