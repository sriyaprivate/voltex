@extends('layouts.master') @section('title','Cart Summary')

@section('links')
<!-- toastr notification -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style type="text/css">
    
    
</style>
@stop

@section('content')

    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="c-272-outlets-switches.aspx">Cart View</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Save Order</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12">
                @if(isset($cart) && count($cart) > 0)
                    <div id="row" style="padding-top: 0px;padding-bottom: 15px">
                        <h3 class="content-title">My Shopping Cart</h3>
                    </div>
                    @if(isset($cart) && count($cart) > 0 && isset($cart->items))
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card-group">
                                    
                                    <div class="card col-md-9" style="padding: 20px">
                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-md-5" style="text-align: center"> Product </div>
                                            <div class="col-md-3" style="text-align: center"> Quantity </div>
                                            <div class="col-md-2" style="text-align: right"> Unit Price(USD) </div>
                                            <div class="col-md-2" style="text-align: right"> Amount(USD) </div>
                                        </div>
                                        @foreach($cart->items as $key => $data)
                                            @if(count($data) > 0)
                                                <div class="row" style="">
                                                    <div class="col-md-2">
                                                        @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                            <span class="badge badge-danger rounded-badge">
                                                                {{ number_format($data['item']->discount_rate)?:'-' }}%
                                                            </span>
                                                        @endif
                                                        <img src="{{ url('data/product_thumb/images/product/'.(isValid($data['item']['image'])? : DEFAULT_PRODUCT_IMG)) }}" class="img-responsive" alt="{{ isValid($data['item']['display_name'])?: '-' }}">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h5 style="font-size: 12px;color: crimson" class="mt-4">
                                                            {{ isValid($data['item']['display_name'])?: '-' }}
                                                            <span class="el-icon-info-sign"></span>
                                                        </h5>
                                                        {{$data['item']->code}}<br>
                                                        <a href="{{ url('cart/remove/'.((isset($data['item']) && !empty($data['item']->id))? $data['item']->id : 0)).'/'.((isset($data['cart_id']) && !empty($data['cart_id']))? $data['cart_id'] : 0) }}?next_url={{ route('cart.summary') }}" name="action" class="btn btn-danger pull-left rounded-0 btn-sm"><i class="fa fa-trash" style="color: #fff" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="input-group mt-4">
                                                            <div class="input-group-prepend">
                                                                <button class="btn btn-outline-secondary plus" type="button"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                            <input type="number" min="1" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" name="qty[{{ (isset($data) && isset($data['cart_id']))? $data['cart_id'] : 0 }}]" value="{{ isValid($data['qty'])?: '0' }}" style="height: 38px !important">
                                                            <div class="input-group-prepend">
                                                                <button class="btn btn-outline-secondary minus" type="button"><i class="fa fa-minus"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="product-price mt-4">      
                                                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                                <span><strong class="pr-4 price" style="color: #868e96 !important;font-size: 13px!important">{{ number_format((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100), 2)?: '-' }}</strong></span>
                                                                
                                                                <span class="pr-4  text-secondary" style="color: red!important;font-size: 11px!important">
                                                                    <del class="pr-1">{{ number_format(isValid($data['item']['price']), 2)?: '-' }}</del>
                                                                </span>
                                                            @else
                                                                <span><strong class="pr-4 price" style="color: #868e96 !important;">{{ number_format(isValid($data['item']['price']), 2)?: '-' }}</strong></span>
                                                            @endif  
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="pull-right mt-4">
                                                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                                <span><strong class="pr-4  price" style="font-size: 15px!important">{{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100)?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                            @else
                                                                <span><strong class="pr-4 price" style="font-size: 15px!important">{{ number_format(((isValid($data['item']['price'])?:0)) * $data['qty']?:0 , 2)?: '-' }}</strong></span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>

                                    <div class="card col-md-3" style="padding: 0px">
                                        <div class="card-header">Summery</div>
                                        <div class="card-body text-dark">
                                            <h5 class="card-title">Sub Total</h5>                                        
                                            <h5 class="card-title">Discount</h5>                                        
                                        </div>
                                        <div class="card-footer">
                                            <h5 class="card-title">Total</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="bordered m-4">
                            <div class="text-center text-secondary">
                                <h4>Your cart is empty</h4>
                                <span class="mt--4">You have not items in your cart, add item <a class="text-primary" href="{{ route('bigstore.index') }}">here</a></span>
                            </div>
                        </div>
                    @endif
                @endif
                </div>

            </div>
        </div>

    </div>

@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>

  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#fixed-menu').hide();

        });

    </script>

    <!-- load cart js in extranal useage -->
    @include('cart::includes.cart-js')
@stop