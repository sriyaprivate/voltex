@extends('layouts.master') @section('title','Cart Summary')

@section('links')
<!-- toastr notification -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style type="text/css">
    
    
</style>
@stop

@section('content')

    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="c-272-outlets-switches.aspx">Cart View</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Save Order</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12">
                @if(isset($cart) && count($cart) > 0)
                    <div id="row" style="padding-top: 0px;padding-bottom: 15px">
                        <h3 class="content-title">My Shopping Cart</h3>
                        @if(isset(collect($cart)->get('items')->first()['first_name']))
                            <div class="bg-light user-name" id="d-load" style="position: absolute;right: 0;margin-top: -41px;right: 200px;padding: 6px 15px;background-color: #dddddd91 !important;">
                                <img src="{{ asset('assets\images\default-user-profile.png') }}" style="width: 30px;position: absolute;margin-top: -6px;margin-left: -45px;"> 
                                <span id="customer_profile_pic">{{ collect($cart)->get('items')->first()['first_name'] }} {{ collect($cart)->get('items')->first()['last_name'] }}</span>
                                <a href="{{ route('remove.customer.cart', ['customer_id' => collect($cart)->get('items')->first()['customer_id']]) }}"><i class="fa fa-times" ></i></a>
                            </div>
                        @endif
                        @if(Auth::check())
                            <button type="button" class="pull-right btn btn-sm btn-primary float-right" style="position: absolute;right: 0;margin-top: -41px;right: 15px;" data-toggle="modal" data-target="#modalCustomerScan"><i class="fa fa-qrcode" aria-hidden="true"></i> Scan Customer</button>
                        @else
                            <a href="{{ url('login?next_url=cart/summary') }}" class="pull-right btn btn-sm btn-primary float-right" style="position: absolute;right: 0;margin-top: -41px;right: 15px;"><i class="fa fa-qrcode" aria-hidden="true"></i> Scan Customer</a>
                        @endif
                    </div>
                    
                    <form method="post" id="form_customer_scan">
                        <!-- The Modal -->
                        <div class="modal" id="modalCustomerScan" style="padding-top: 13%">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title"><i class="fa fa-qrcode" aria-hidden="true"></i> Scan Customer</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="email" class="required">Customer ID</label>
                                                <input type="text" class="form-control" id="customer_id" placeholder="Please scan customer id or type">
                                            </div>
                                        </form>
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-info btn-sm rounded-0" id="btn_customer_scan_done" disabled>Done</button>
                                        <button type="button" class="btn btn-secondary btn-sm rounded-0" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>

                    <form action="{{ route('cart.update') }}" method="post" class="form-cart-summary position-relative" id="cart-form">
                        <!-- cart summary -->
                        <div class="panel-refresh" name="panel-refresh"></div>
                        {{ csrf_field() }}
                        @if(isset(collect($cart)->get('items')->first()['customer_id']))
                            <input type="hidden" name="customer_id" value="{{ collect($cart)->get('items')->first()['customer_id'] }}">
                        @endif
                        <button type="submit" id="btn-update-cart" class="btn btn-sm btn-primary float-right invisible" style="position: absolute;right: 0;margin-top: -41px;"><i class="fa fa-pencil" aria-hidden="true"></i> Update Cart</button>
                        
                        <style type="text/css">
                            .price{
                                font-size: 19px !important;
                            }
                        </style>
                        @if(isset($cart) && count($cart) > 0 && isset($cart->items))
                            <?php
                                $customizeCost = 0;
                            ?>

                            <table class="table table-sm table-bordered fs-11">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th width="10%">Product</th>
                                        <th class="text-center">Name</th>
                                        <th width="15%" class="text-center">Quantity</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Amount</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach($cart->items as $key => $data)
                                        @if(count($data) > 0)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td class="text-center">
                                                    @if(isset($data['availableQty']) && $data['availableQty'] == 0)
                                                    <div class="item-disabled">
                                                        <div class="ribbon ribbon-top-left"><span>Out of stock</span></div>
                                                    </div>
                                                    @endif
                                                    <div class="col-10" style="padding: 10px">
                                                        @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                            <span class="badge badge-danger rounded-badge">
                                                                {{ number_format($data['item']->discount_rate)?:'-' }}%
                                                            </span>
                                                        @endif
                                                        <a href="{{ route('bigstore.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'item_name' => str_slug($data['item']->display_name?:$data['item']->name, '-'), 'item_id' => $key]) }}">
                                                        <img src="{{ url('data/product_thumb/images/product/'.(isValid($data['item']['image'])? : DEFAULT_PRODUCT_IMG)) }}" class="img-responsive" alt="{{ isValid($data['item']['display_name'])?: '-' }}" style="width:50px;">
                                                        </a>
                                                    </div>
                                                    {{$data['item']->code}}
                                                </td>
                                                <td style="text-align: center">
                                                    <h5 style="font-size: 12px;padding-top: 25px">
                                                        <a href="{{ route(isValid($data['item']->status) && $data['item']->status == DEAL_PROMOTION? 'saving-center.item.view' : 'bigstore.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'item_name' => str_slug($data['item']->display_name?:$data['item']->name, '-'), 'item_id' => $key]) }}">
                                                        {{ isValid($data['item']['display_name'])?: '-' }}</a>
                                                        <span class="el-icon-info-sign"></span>
                                                    </h5>
                                                </td>
                                                <td>
                                                    <div class="product-qty" style="padding-left: 10px">
                                                        <div class="buttons" style="float: left;padding-top: 15px;padding-right: 15px">
                                                            <div class="discreteButton mini symbolonly plusminus plus" style="cursor: pointer;">
                                                                <i class="fa fa-plus" aria-hidden="true" style="font-size: 1.8em"></i>
                                                            </div>
                                                        </div>
                                                        <div class="count" style="float: left;width: 60%">
                                                            <input class="light-style-input display_qty qty_text" type="number" class="form-control" value="{{$data['qty']}}" style="text-align: center">
                                                            <input type="hidden" min="1" max="{{ $data['availableQty']?:0 }}" class="form-control form-control-sm" name="qty[{{ (isset($data) && isset($data['cart_id']))? $data['cart_id'] : 0 }}]" value="{{ isValid($data['qty'])?: '0' }}">
                                                        </div>
                                                        <div class="buttons" style="padding-top: 15px;padding-left: 140px">
                                                            <div class="discreteButton mini symbolonly plusminus minus" style="cursor: pointer;">
                                                                <i class="fa fa-minus" aria-hidden="true" style="font-size: 1.8em"></i>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="product-price" style="min-width: 200px;text-align: right;padding-top: 20px;">      
                                                        @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                            <span><strong class="pr-4 price" style="color: #868e96 !important;font-size: 13px!important">{{ CURRENCY_SIGN }} {{ number_format((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100), 2)?: '-' }}</strong></span>
                                                            
                                                            <span class="pr-4  text-secondary" style="color: red!important;font-size: 11px!important">
                                                                <del class="pr-1">{{ CURRENCY_SIGN }} {{ number_format(isValid($data['item']['price']), 2)?: '-' }}</del>
                                                            </span>
                                                        @else
                                                            <span><strong class="pr-4 price" style="color: #868e96 !important;">{{ CURRENCY_SIGN }} {{ number_format(isValid($data['item']['price']), 2)?: '-' }}</strong></span>
                                                        @endif  
                                                    </div>
                                                </td>
                                                <td style="text-align: right">
                                                    <div class="pull-right" style="padding-top: 20px">
                                                        @if(strlen($data['item']->cutomize_price) !== 0)
                                                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                                <span><strong class="pr-4  price" style="font-size: 13px!important">{{ CURRENCY_SIGN }} {{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100)) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                            @else
                                                                <span><strong class="pr-4 price" style="font-size: 13px!important">{{ CURRENCY_SIGN }} {{ number_format(((isValid($data['item']['price']) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                            @endif
                                                        @else
                                                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                                <span><strong class="pr-4  price" style="font-size: 15px!important">{{ CURRENCY_SIGN }} {{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                            @else
                                                                <span><strong class="pr-4 price" style="font-size: 15px!important">{{ CURRENCY_SIGN }} {{ number_format(((isValid($data['item']['price']) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0 , 2)?: '-' }}</strong></span>
                                                            @endif
                                                        @endif

                                                        <?php 
                                                            if(isValid($data['item']->cutomize_price)){
                                                                $customizeCost += ((collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0) * $data['qty']?:0);
                                                            }                            
                                                        ?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="{{ url('cart/remove/'.((isset($data['item']) && !empty($data['item']->id))? $data['item']->id : 0)).'/'.((isset($data['cart_id']) && !empty($data['cart_id']))? $data['cart_id'] : 0) }}?next_url={{ route('cart.summary') }}" name="action" class="btn btn-light pull-right rounded-0 btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endif
                                        <?php $i++; ?>
                                    @endforeach

                                </tbody>

                                <tfoot>
                                    @if(isset($cart) && count($cart))
                                        @include('cart::includes.cart_summary1', ['btn_text' => 'checkout'])
                                        <input type="hidden" id="c_data" value="{{ collect($cart)->get('items')->pluck('id')?:'' }}">
                                    @else
                                        <div style="margin:0 auto;">
                                            <div class="bordered m-4">
                                                <div class="text-center text-secondary">
                                                    <h4>Your cart is empty</h4>
                                                    <span class="mt--4">You have not items in your cart, add item <a class="text-primary" href="{{ route('store.index') }}?type_id=1">here</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </tfoot>
                            </table>

                        @else
                            <div class="bordered m-4">
                                <div class="text-center text-secondary">
                                    <h4>Your cart is empty</h4>
                                    <span class="mt--4">You have not items in your cart, add item <a class="text-primary" href="{{ route('bigstore.index') }}">here</a></span>
                                </div>
                            </div>
                        @endif

                    </form>
                @endif
                </div>

            </div>
        </div>

    </div>

@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>

  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#fixed-menu').hide();

            $('.fabric-option-row').click(function(){
                console.log('jnsdfjjkdf');
                $(this).addClass('fabric-active-row');
            });

        });

        $('#customer_id').on('keyup', function(e){
            var customer_id = $(this).val();

            if(customer_id.length > 0){
                $('#btn_customer_scan_done').attr('disabled', false);
            }else{
                $('#btn_customer_scan_done').attr('disabled', true);
            }
        });

        $('#modalCustomerScan').on('shown.bs.modal', function() {
            $("#customer_id").focus();
        })

        $(document).on('submit','#form_customer_scan',function(e){
            e.preventDefault();

            var customer_id   = $('#customer_id').val();
            var cart_items_id = $('#c_data').val();
            
            $.ajax({
                url: '{{ route("config.customer") }}',
                type: 'POST',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'), 
                    customer_id: customer_id,
                    cart_items_id: cart_items_id
                },
                dataType: 'JSON',
                success: function (response) {
                    $('#modalCustomerScan').modal('hide');
                    removePanelRefresh();

                    if(response.success == true){
                        $('.user-name').removeClass('d-none');

                        $.confirm({
                            theme: 'material',
                            title: 'Done!', 
                            type: 'green',
                            content: response.msg,
                            buttons: {
                                ok: function(){
                                    location.reload();
                                }
                            }
                        });
                    }else{
                        $.confirm({
                            theme: 'material',
                            title: 'Warning!', 
                            type: 'red',
                            content: response.msg,
                            buttons: {
                                ok: function(){
                                    location.reload();
                                }
                            }
                        });
                    }
                },
                error: function(xhr){
                    console.log(xhr);
                }
            });

            $('.option-row').click(function(){
                $(this).addClass('active-row');
            });

        });

    </script>

    <!-- load cart js in extranal useage -->
    @include('cart::includes.cart-js')
@stop