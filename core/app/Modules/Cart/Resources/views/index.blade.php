@extends('layouts.master') @section('title','Cart Summary')

@section('links')
<!-- toastr notification -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
<style type="text/css">
    
    
</style>
@stop

@section('content')

    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="c-272-outlets-switches.aspx">Cart View</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Save Order</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12">
                @if(isset($cart) && count($cart) > 0)
                    <div id="row" style="padding-top: 0px;padding-bottom: 15px">
                        <h3 class="content-title">My Shopping Cart</h3>
                    </div>

                    <form action="{{ route('cart.update') }}" method="post" class="form-cart-summary position-relative" id="cart-form">
                        <!-- cart summary -->
                        <div class="panel-refresh" name="panel-refresh"></div>
                        {{ csrf_field() }}
                        <button type="submit" id="btn-update-cart" class="btn btn-sm btn-primary float-right invisible" style="position: absolute;right: 0;margin-top: -41px;"><i class="fa fa-pencil" aria-hidden="true"></i> Update Cart</button>
                        <style type="text/css">
                            .price{
                                font-size: 19px !important;
                            }
                        </style>
                        @if(isset($cart) && count($cart) > 0 && isset($cart->items))

                            <table class="table table-sm table-bordered fs-11">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th width="10%">Product</th>
                                        <th class="text-center">Name</th>
                                        <th width="15%" class="text-center">Quantity</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Amount</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach($cart->items as $key => $data)
                                        @if(count($data) > 0)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td class="text-center">
                                                    @if(isset($data['availableQty']) && $data['availableQty'] == 0)
                                                    <div class="item-disabled">
                                                        <div class="ribbon ribbon-top-left"><span>Out of stock</span></div>
                                                    </div>
                                                    @endif
                                                    <div class="col-10" style="padding: 10px">
                                                        @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                            <span class="badge badge-danger rounded-badge">
                                                                {{ number_format($data['item']->discount_rate)?:'-' }}%
                                                            </span>
                                                        @endif
                                                        <a href="{{ route('bigstore.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'item_name' => str_slug($data['item']->display_name?:$data['item']->name, '-'), 'item_id' => $key]) }}">
                                                        <img src="{{ url('data/product_thumb/images/product/'.(isValid($data['item']['image'])? : DEFAULT_PRODUCT_IMG)) }}" class="img-responsive" alt="{{ isValid($data['item']['display_name'])?: '-' }}" style="width:50px;">
                                                        </a>
                                                    </div>
                                                    {{$data['item']->code}}
                                                </td>
                                                <td style="text-align: center">
                                                    <h5 style="font-size: 12px;padding-top: 25px">
                                                        <a href="{{ route(isValid($data['item']->status) && $data['item']->status == DEAL_PROMOTION? 'saving-center.item.view' : 'bigstore.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'item_name' => str_slug($data['item']->display_name?:$data['item']->name, '-'), 'item_id' => $key]) }}">
                                                        {{ isValid($data['item']['display_name'])?: '-' }}</a>
                                                        <span class="el-icon-info-sign"></span>
                                                    </h5>
                                                </td>
                                                <td>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <button class="btn btn-outline-secondary plus" type="button"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                        <input type="number" min="1" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" name="qty[{{ (isset($data) && isset($data['cart_id']))? $data['cart_id'] : 0 }}]" value="{{ isValid($data['qty'])?: '0' }}" style="height: 38px !important">
                                                        <div class="input-group-prepend">
                                                            <button class="btn btn-outline-secondary minus" type="button"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="product-price" style="min-width: 200px;text-align: right;padding-top: 20px;">      
                                                        @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                            <span><strong class="pr-4 price" style="color: #868e96 !important;font-size: 13px!important">{{ CURRENCY_SIGN }} {{ number_format((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100), 2)?: '-' }}</strong></span>
                                                            
                                                            <span class="pr-4  text-secondary" style="color: red!important;font-size: 11px!important">
                                                                <del class="pr-1">{{ CURRENCY_SIGN }} {{ number_format(isValid($data['item']['price']), 2)?: '-' }}</del>
                                                            </span>
                                                        @else
                                                            <span><strong class="pr-4 price" style="color: #868e96 !important;">{{ CURRENCY_SIGN }} {{ number_format(isValid($data['item']['price']), 2)?: '-' }}</strong></span>
                                                        @endif  
                                                    </div>
                                                </td>
                                                <td style="text-align: right">
                                                    <div class="pull-right" style="padding-top: 20px">
                                                        @if(strlen($data['item']->cutomize_price) !== 0)
                                                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                                <span><strong class="pr-4  price" style="font-size: 13px!important">{{ CURRENCY_SIGN }} {{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100)) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                            @else
                                                                <span><strong class="pr-4 price" style="font-size: 13px!important">{{ CURRENCY_SIGN }} {{ number_format(((isValid($data['item']['price']) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                            @endif
                                                        @else
                                                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                                                <span><strong class="pr-4  price" style="font-size: 15px!important">{{ CURRENCY_SIGN }} {{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                                                            @else
                                                                <span><strong class="pr-4 price" style="font-size: 15px!important">{{ CURRENCY_SIGN }} {{ number_format(((isValid($data['item']['price']) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0 , 2)?: '-' }}</strong></span>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="{{ url('cart/remove/'.((isset($data['item']) && !empty($data['item']->id))? $data['item']->id : 0)).'/'.((isset($data['cart_id']) && !empty($data['cart_id']))? $data['cart_id'] : 0) }}?next_url={{ route('cart.summary') }}" name="action" class="btn btn-light pull-right rounded-0 btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endif
                                        <?php $i++; ?>
                                    @endforeach

                                </tbody>

                                <tfoot>
                                    @if(isset($cart) && count($cart))
                                        @include('cart::includes.cart_summary1', ['btn_text' => 'checkout'])
                                        <input type="hidden" id="c_data" value="{{ collect($cart)->get('items')->pluck('id')?:'' }}">
                                    @else
                                        <div style="margin:0 auto;">
                                            <div class="bordered m-4">
                                                <div class="text-center text-secondary">
                                                    <h4>Your cart is empty</h4>
                                                    <span class="mt--4">You have not items in your cart, add item <a class="text-primary" href="{{ route('store.index') }}?type_id=1">here</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </tfoot>
                            </table>

                        @else
                            <div class="bordered m-4">
                                <div class="text-center text-secondary">
                                    <h4>Your cart is empty</h4>
                                    <span class="mt--4">You have not items in your cart, add item <a class="text-primary" href="{{ route('store.index') }}">here</a></span>
                                </div>
                            </div>
                        @endif

                    </form>
                @endif
                </div>

            </div>
        </div>

    </div>

@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>

  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#fixed-menu').hide();

        });

    </script>

    <!-- load cart js in extranal useage -->
    @include('cart::includes.cart-js')
@stop