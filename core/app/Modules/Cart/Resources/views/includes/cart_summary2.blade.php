<!-- panel refresh -->
<?php
    $currentRouteName = Request::route()->getName();
?>

<div class="row summery-text">
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-6" style="text-align: right">Sub Total</div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-6">
        <p class="text-right {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="total_price">
        @if(count($cart) > 0 && isset($cart->totalPrice))
            {{ CURRENCY_SIGN }} {{ number_format($cart->totalPrice, 2) }}
            <input type="hidden" name="hidden_totalPrice" id="hidden_totalPrice" value="{{ number_format($cart->totalPrice, 2) }}">
        @else
            {{ CURRENCY_SIGN }} 0.00
            <input type="hidden" name="hidden_totalPrice" id="hidden_totalPrice" value="0.00">
        @endif
        </p>
    </div>
</div>

<div class="row summery-text">
    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-6" style="text-align: right;">Total</div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-6">
        @if(count($cart) > 0 && isset($cart->subTotal))
            @if(isset($showDeliveryCharges) && $showDeliveryCharges && count($delivery_charge) > 0 && isset($delivery_charge['data']) && !empty($delivery_charge['data']))
                <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total">
                    {{ CURRENCY_SIGN }} {{ number_format((($cart->subTotal + $delivery_charge['data']) ), 2) }}
                </p>
                <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="{{ (($cart->subTotal + $delivery_charge['data']) ) }}">
            @else
                <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total">
                    {{ CURRENCY_SIGN }} {{ number_format($cart->subTotal , 2) }}
                </p>
                <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="{{ $cart->subTotal  }}">
            @endif
        @else
            <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total" >
                {{ CURRENCY_SIGN }} 0.00
            </p>
            <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="0.00">
        @endif
    </div>
</div>

<div class="row summery-text">
    <div class="col-md-6">
        <a href="{{ url('/') }}" class="btn btn-sm btn-block btn-secondary" style="padding: 5px 2px !important;color: #fff !important;background-color: crimson;border-color: crimson">Continue Ordering</a>
    </div>
    <div class="col-md-6">
        <button style="padding: 5px 2px !important;background-color: crimson;border-color: crimson" type="button" id="btn-next" class="btn btn-sm btn-block btn-secondary mb-0">Save Order <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
    </div>
</div>