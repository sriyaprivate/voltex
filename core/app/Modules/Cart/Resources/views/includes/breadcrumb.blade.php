<div class="cart-breadcrumb">
    @if(count($breadcrumbs) > 0)
        @foreach($breadcrumbs as $breadcrumb)
            @if(isset($breadcrumb['status']) && $breadcrumb['status'] != "")
                <a {{ $currentRoute = $breadcrumb['route_name']? 'class=active' : ''}} href="{{ route($breadcrumb['route_name']) }}">
                    <span class="breadcrumb__inner">
                        <span class="breadcrumb__title">{{ $breadcrumb['name'] }}</span>
                    </span>
                </a>
            @else
                <a>
                    <span class="breadcrumb__inner">
                        <span class="breadcrumb__title">{{ $breadcrumb['name'] }}</span>
                    </span>            
                </a>
            @endif
        @endforeach
    @endif
</div>