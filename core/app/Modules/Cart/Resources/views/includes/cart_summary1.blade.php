<!-- panel refresh -->
<?php
    $currentRouteName = Request::route()->getName();
?>

<tr>
    <td colspan="5" style="text-align: right">
        <span style="font-size: 15px!important;padding: 15px">Sub Total</span>
    </td>
    <td>
        <div class="col">
            <p style="font-size: 15px!important;padding: 10px" class="text-right {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="total_price">
            @if(count($cart) > 0 && isset($cart->totalPrice))
                {{ CURRENCY_SIGN }} {{ number_format($cart->totalPrice, 2) }}
                <input type="hidden" name="hidden_totalPrice" id="hidden_totalPrice" value="{{ number_format($cart->totalPrice, 2) }}">
            @else
                {{ CURRENCY_SIGN }} 0.00
                <input type="hidden" name="hidden_totalPrice" id="hidden_totalPrice" value="0.00">
            @endif
            </p>
        </div>
    </td>
    <td></td>
</tr>
<tr>
    <td colspan="5" style="text-align: right">
        <span style="font-size: 15px!important;padding: 15px">Discount</span>
    </td>
    <td>
        <div class="col">
            <p style="font-size: 15px!important;padding: 10px" class="text-right {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="total_discount">
                @if(count($cart) > 0 && isset($cart->totalDiscount))
                    {{ CURRENCY_SIGN }} {{ number_format($cart->totalDiscount, 2) }}
                    <input type="hidden" name="hidden_total_discount" id="hidden_total_discount" value="{{ number_format($cart->totalDiscount, 2) }}">
                @else
                    {{ CURRENCY_SIGN }} 0.00
                    <input type="hidden" name="hidden_total_discount" id="hidden_total_discount" value="0.00">
                @endif
            </p>
        </div>
    </td>
    <td></td>
</tr>
<tr>
    <td colspan="5" style="text-align: right">
        <span style="font-size: 15px!important;padding: 15px">Total</span>
    </td>
    <td>
        <div class="col">
            @if(count($cart) > 0 && isset($cart->subTotal))
                @if(isset($showDeliveryCharges) && $showDeliveryCharges && count($delivery_charge) > 0 && isset($delivery_charge['data']) && !empty($delivery_charge['data']))
                    <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total">
                        {{ CURRENCY_SIGN }} {{ number_format((($cart->subTotal + $delivery_charge['data']) ), 2) }}
                    </p>
                    <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="{{ (($cart->subTotal + $delivery_charge['data']) ) }}">
                @else
                    <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total" >
                        {{ CURRENCY_SIGN }} {{ number_format($cart->subTotal , 2) }}
                    </p>
                    <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="{{ $cart->subTotal  }}">
                @endif
            @else
                <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total" >
                    {{ CURRENCY_SIGN }} 0.00
                </p>
                <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="0.00">
            @endif
        </div>
    </td>
    <td></td>
</tr>
<tr>
    <td colspan="5" style="border-bottom:hidden;border-left:hidden;;border-right:hidden;">
    <td colspan="2" style="border-bottom:hidden;border-left:hidden;;border-right:hidden;">
        <a href="{{ url('/') }}" class="btn btn-sm btn-block btn-secondary" style="padding: 5px 2px !important;margin-top: 20px;color: #fff !important">Continue Ordering</a>
            <button style="padding: 5px 2px !important" type="button" id="btn-next" class="btn btn-sm btn-block btn-secondary mb-0">Save Order <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
    </td>
</tr>