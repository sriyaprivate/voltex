<!-- <style type="text/css">
    .price{
        font-size: 19px !important;
    }
</style>
@if(isset($cart) && count($cart) > 0 && isset($cart->items))
    <?php
        $customizeCost = 0;
    ?>
    @foreach($cart->items as $key => $data)
        @if(count($data) > 0)
        <div class="border pt-4 mb-4 pb-4">
            <div class="row" style="margin: 0px auto">
                @if(isset($data['availableQty']) && $data['availableQty'] == 0)
                <div class="item-disabled">
                    <div class="ribbon ribbon-top-left"><span>Out of stock</span></div>
                </div>
                @endif
                <div class="col-2">
                    @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                        <span class="badge badge-danger rounded-badge">
                            {{ number_format($data['item']->discount_rate)?:'-' }}%<br>OFF
                        </span>
                    @endif
                    <a href="{{ route('bigstore.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'item_name' => str_slug($data['item']->display_name?:$data['item']->name, '-'), 'item_id' => $key]) }}">
                        <img src="{{ url('data/product_thumb/images/product/'.(isValid($data['item']['image'])? : DEFAULT_PRODUCT_IMG)) }}" class="img-fluid img-row" alt="{{ isValid($data['item']['display_name'])?: '-' }}" style="width:130px;">
                    </a>
                </div>
                <div class="col-1">
                    <div class="product-qty" style="padding-left: 40px;">
                        <div class="buttons">
                            <div class="discreteButton mini symbolonly plusminus plus" style="cursor: pointer;">
                                <i class="fa fa-plus" aria-hidden="true" style="font-size: 1.8em"></i>
                            </div>
                            <div class="discreteButton mini symbolonly plusminus minus" style="cursor: pointer;">
                                <i class="fa fa-minus" aria-hidden="true" style="font-size: 1.8em"></i>
                            </div>
                        </div>
                        <div class="count">
                            <span class="display_qty" style="font-size: 28px;">{{ isValid($data['qty'])?: '0' }}</span>
                            <input type="hidden" min="1" max="{{ $data['availableQty']?:0 }}" class="form-control form-control-sm" name="qty[{{ (isset($data['item']) && !empty($data['item']->id))? $data['item']->id : 0 }}]" value="{{ isValid($data['qty'])?: '0' }}">
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="essentials">
                        <h2>
                            <a href="{{ route(isValid($data['item']->status) && $data['item']->status == DEAL_PROMOTION? 'saving-center.item.view' : 'bigstore.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN, 'item_name' => str_slug($data['item']->display_name?:$data['item']->name, '-'), 'item_id' => $key]) }}">
                            {{ isValid($data['item']['display_name'])?: '-' }}</a>
                            <span class="el-icon-info-sign"></span>
                        </h2>                           
                        <div class="actions">
                            <a href="#" class="customized-item" data-pro-id="{{$data['item']->id}}" data-cart-id="{{$data['cart_id']}}" data-price="{{ $data['item']->discounted_amount }}">Designs |</a>
                            <a href="#" onclick="showAdditional({{ $key }})">Hide |</a>
                            <a href="#" class="copyItem">Customized</a>
                        </div>
                        <div class="messages"></div>
                    </div>
                    <div class="select-measurements">
                        @if(isset(collect($cart)->get('items')->first()['customer_id']))
                            <button type="button" class="btn btn-measure" data-pro-id="{{$data['item']->id}}" style="background-color: #081e33;color: white;height: 30px;padding: 0 0.3rem;" data-id="1">Choose measurements / size</button>
                        @else
                            <button type="button" class="btn btn-measure" style="background-color: #081e33;color: white;height: 30px;padding: 0 0.3rem;" data-id="1" disabled>Choose measurements / size</button>
                        @endif
                    </div>
                </div>
                <div class="col-2">
                    <div class="product-price" style="min-width: 200px;text-align: right;">      
                        @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                            <span><strong class="pr-4 price" style="color: #868e96 !important;">{{ CURRENCY_SIGN }} {{ number_format((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100), 2)?: '-' }}</strong></span>
                            <br>
                            <span class="pr-4  text-secondary">
                                <del class="pr-1">{{ CURRENCY_SIGN }} {{ number_format(isValid($data['item']['price']), 2)?: '-' }}</del>
                            </span>
                        @else
                            <span><strong class="pr-4 price" style="color: #868e96 !important;">{{ CURRENCY_SIGN }} {{ number_format(isValid($data['item']['price']), 2)?: '-' }}</strong></span>
                        @endif  
                    </div>
                </div>
                <div class="col-3">
                    <div class="pull-left" style="text-align: right;margin-left: 50px;">
                        @if(strlen($data['item']->cutomize_price) !== 0)
                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                <span><strong class="pr-4  price">{{ CURRENCY_SIGN }} {{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100)) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                            @else
                                <span><strong class="pr-4 price">{{ CURRENCY_SIGN }} {{ number_format(((isValid($data['item']['price']) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                            @endif
                        @else
                            @if(count($data['item']) > 0 && isset($data['item']->discount_rate))
                                <span><strong class="pr-4  price">{{ CURRENCY_SIGN }} {{ number_format((((isValid($data['item']['price']) * (100 - isValid($data['item']->discount_rate)) / 100) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0, 2)?: 0 }}</strong></span>
                            @else
                                <span><strong class="pr-4 price">{{ CURRENCY_SIGN }} {{ number_format(((isValid($data['item']['price']) + collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0)) * $data['qty']?:0 , 2)?: '-' }}</strong></span>
                            @endif
                        @endif

                        <?php 
                            if(isValid($data['item']->cutomize_price)){
                                $customizeCost += ((collect(strExplode(isValid($data['item']->cutomize_price), ',', null))->sum()?:0) * $data['qty']?:0);
                            }                            
                        ?>
                    </div>
                    <a href="{{ url('cart/remove/'.((isset($data['item']) && !empty($data['item']->id))? $data['item']->id : 0)).'/'.((isset($data['cart_id']) && !empty($data['cart_id']))? $data['cart_id'] : 0) }}?next_url={{ $next_url }}" name="action" class="btn btn-light pull-right rounded-0 btn-sm" style="margin-left:-4px;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="row additional-data-{{ $key }}" style="display: block">
                <div class="col-md-12">
                    @if(strlen($data['item']->choice_option) !== 0) 
                        <?php
                            $options    = strExplode(isValid($data['item']->choice_option), ',', null);
                            $options_id = strExplode(isValid($data['item']->choice_option_id), ',', null);
                            $images     = strExplode(isValid($data['item']->option_image), ',', null);
                            $prices     = strExplode(isValid($data['item']->cutomize_price), ',', null);
                        ?>
                        @if(isset($options) && count($options) > 0)
                            @foreach($options as $key => $option)
                                <div class="row" style="margin-bottom: 5px">                                        
                                    <div class="col-3"></div>
                                    <div class="col-1">
                                        <img src="{{ url(UPLOAD_PATH).'/uploads/images/choices/options/'.$images[$key] }}" class="border">
                                    </div>
                                    <div class="col-3">
                                        <h6 style="padding-top: 10%">{{ isValid($options[$key]) }}</h6>
                                    </div>
                                    <div class="col-2">
                                        <span>
                                            <strong class="pr-4  price text-right" style="font-size: 15px;padding-top: 14%;color: #868e96 !important;">
                                                Rs {{ isValid(number_format($prices[$key], 2)) }}
                                            </strong>
                                        </span>
                                    </div>           
                                    <div class="col-3">
                                        <a href="{{ route('remove.cart.customize.option', ['option_id' => $options_id[$key]]) }}" name="action" class="btn btn-light pull-right rounded-0 btn-sm mr-3 mt-4" style="margin-left:-4px;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </div>                     
                                </div>
                            @endforeach                                                        
                        @endif
                    @endif
                </div>
            </div>
        </div>
        @endif
    @endforeach
@else
    <div class="bordered m-4">
    <div class="text-center text-secondary">
        <h4>Your shopping cart is empty</h4>
        <span class="mt--4">You have not items in your shopping cart, add item <a class="text-primary" href="{{ route('bigstore.index') }}">here</a></span>
    </div>
    </div>
@endif

<script>
    var tm = 0;
    function showAdditional(no){
        $('.additional-data-'+no).toggle();
    }
</script> -->