<!-- panel refresh -->
<?php
    $currentRouteName = Request::route()->getName();
?>
<div class="panel-refresh" name="panel-refresh"></div>
<div class="order-summary mt-mobile">
    <div class="h-100 p-4">
        <div class="row mb-3">
            <div class="col-5">
                <p class="text-uppercase fs-11">Subtotal</p>
            </div>
            <div class="col">
                <p class="text-right {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="total_price">
                @if(count($cart) > 0 && isset($cart->totalPrice))
                    {{ CURRENCY_SIGN }} {{ number_format($cart->totalPrice, 2) }}
                    <input type="hidden" name="hidden_totalPrice" id="hidden_totalPrice" value="{{ number_format($cart->totalPrice, 2) }}">
                @else
                    {{ CURRENCY_SIGN }} 0.00
                    <input type="hidden" name="hidden_totalPrice" id="hidden_totalPrice" value="0.00">
                @endif
                </p>
            </div>
        </div>
        @if(isset($showDeliveryCharges) && $showDeliveryCharges)
            <div class="row">
                <div class="col">
                    <p class="text-uppercase fs-11">Delivery Cost</p>
                </div>
                <div class="col">
                    <p class="text-right {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="delivey_cost">
                        @if(count($delivery_charge) > 0 && isset($delivery_charge['data']))
                            {{ CURRENCY_SIGN }} {{ number_format($delivery_charge['data'], 2) }}
                            <input type="hidden" name="hidden_delivery_charge" id="hidden_delivery_charge" value="{{ number_format($delivery_charge['data'], 2) }}">
                        @else
                            {{ CURRENCY_SIGN }} 0.00
                            <input type="hidden" name="hidden_delivery_charge" id="hidden_delivery_charge" value="0.00">
                        @endif
                    </p>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col">
                <p class="text-uppercase fs-11">Discount</p>
            </div>
            <div class="col">
                <p class="text-right {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="total_discount">
                    @if(count($cart) > 0 && isset($cart->totalDiscount))
                        {{ CURRENCY_SIGN }} {{ number_format($cart->totalDiscount, 2) }}
                        <input type="hidden" name="hidden_total_discount" id="hidden_total_discount" value="{{ number_format($cart->totalDiscount, 2) }}">
                    @else
                        {{ CURRENCY_SIGN }} 0.00
                        <input type="hidden" name="hidden_total_discount" id="hidden_total_discount" value="0.00">
                    @endif
                </p>
            </div>
        </div>

    </div>
    <div class="p-4">
        <div class="row">
            <div class="col-5">
                <p class="text-uppercase m-0 fs-11">Total</p>
            </div>
            <div class="col">
                @if(count($cart) > 0 && isset($cart->subTotal))
                    @if(isset($showDeliveryCharges) && $showDeliveryCharges && count($delivery_charge) > 0 && isset($delivery_charge['data']) && !empty($delivery_charge['data']))
                        <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total">
                            {{ CURRENCY_SIGN }} {{ number_format((($cart->subTotal + $delivery_charge['data']) ), 2) }}
                        </p>
                        <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="{{ (($cart->subTotal + $delivery_charge['data']) ) }}">
                    @else
                        <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total" >
                            {{ CURRENCY_SIGN }} {{ number_format($cart->subTotal , 2) }}
                        </p>
                        <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="{{ $cart->subTotal  }}">
                    @endif
                @else
                    <p class="text-right m-0 {{ $currentRouteName !== 'cart.summary' ?:'cart-summary-l-font' }}" id="sub_total" >
                        {{ CURRENCY_SIGN }} 0.00
                    </p>
                    <input type="hidden" name="hidden_sub_total" id="hidden_sub_total" value="0.00">
                @endif
            </div>
        </div>
        <hr>
        <a href="{{ url('/bigstore') }}" class="btn btn-sm btn-block btn-secondary">Continue Shopping</a>
        @if(isset(collect($cart)->get('items')->first()['customer_id']))
            <button type="button" id="{{ !empty($btn_id)? $btn_id : 'btn-next' }}" class="btn btn-sm btn-block btn-secondary mb-0{{ count($cart) > 0 && isset($cart->items)?'' : 'cursor-not-allowed disabled' }}" {{ count($cart) > 0 && isset($cart->items)? '' : 'disabled' }} {{ isset($disabled) && $disabled? 'disabled':'' }} {{ isset($show_modal)? 'data-toggle=modal data-target=#delivery_summary_modal':'' }} >{{ $btn_text?:'-' }} <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
        @else
            <button type="button" class="btn btn-sm btn-block btn-secondary cursor-not-allowed mb-0{{ count($cart) > 0 && isset($cart->items)?'' : 'cursor-not-allowed disabled' }}" disabled>{{ $btn_text?:'-' }} <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
            <br>
            <div class="alert alert-secondary alert-dismissible fade show" role="alert">
              please assign shopping cart to the customer.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
    </div>
</div>