<script>
    //remove item from cart
    function removeItem(url){
        window.open(url, '_self');
    }

    $('input[type=number]').bind('change keyup', function(e){
        //validate qty
        var isValid = 0;
        $('input[type=number]').each(function(){
            var pattern = /^([0-9]*[1-9][0-9]*)$/;
      
            if($(this).val().trim().length > 0 && pattern.test($(this).val().trim())){
                isValid++;
            }else{
                isValid--;
            }
        });

        if(isValid > 0){
            $('#btn-update-cart').prop('disabled', false);
            $('#btn-update-cart').click();
        }else{
            $('#btn-update-cart').prop('disabled', true);
        }
    });

    //qty validation
    var lastCurrentQty = null;
    $('input[type=number]').bind('change keyup', function(e){
        //validate qty
        var pattern = /^([1-9]*[1-9][0-9]*)$/;

        if($(this).val().length > 0 && pattern.test($(this).val())){
            $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', false);
            lastCurrentQty = $(this).val();
            $('#btn-update-cart').click();
        }else{
            //show notification
            replaceQty = 1;

            if(lastCurrentQty !== null){ 
                if($(this).val().length == 0){
                    replaceQty = '';
                    $('#btn-update-cart').prop('disabled', false);
                    $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', true);
                    $('#btn-update-cart').prop('disabled', true);
                }else{
                    replaceQty = lastCurrentQty;
                    notification("toast-bottom-right", "warning", "Limited stock available!.", "5000");
                    $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', false);
                    $('#btn-update-cart').prop('disabled', false);
                } 
            }else{ 
                if($(this).val().length == 0){
                    replaceQty = '';
                    $('#btn-update-cart').prop('disabled', true);
                    $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', true);
                }else{
                    replaceQty = replaceQty;
                    $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', false);
                    $('#btn-update-cart').prop('disabled', false);
                    //notification("toast-bottom-right", "warning", "Limited stock available!.", "5000");
                }
            }
            $(this).val(replaceQty);
        }
    });

    @if(isset(Auth::user()->id) && Session::has('sessionCart_'.Auth::user()->id) && isset($qtyData))
        //@prams : click ok link, click cancel link, qty data "json encode", title, message
        confirm_alert("{{ route('cart.merge') }}", "{{ route('cart.forget.user.session') }}", '{!! json_encode($qtyData) !!}', 'btn-red', 'Are you sure?', "Do you want to merge tempary shopping cart with account?");
    @endif

    //remove panel refresh after load page.
    removePanelRefresh();

    //add panel refresh class
    $('form').on('submit', function(){
        addPanelRefresh('div[name="panel-refresh"]');
    });

    var isInputChanged = false;

    $("input[type='number']").on('keyup', function(){
        isInputChanged = true;
    });

    $('#btn-next').click(function(){
        $('#cart-form').prop('action', "{{ route('cart.update').'?next_url=cart/invoice' }}");
      
        $('<input>').attr({
            type: 'hidden',
            name: 'updateStatus',
            value: '1'
        }).appendTo($('#cart-form'));
        $('#cart-form').submit();
    });

    $('.plus').click(function(){
        var oldQty     = parseInt($(this).closest("div.product-qty").find("input[type='hidden']").val());
        var oldQtyText = $(this).closest("div.product-qty").find(".display_qty").html();

        if(typeof(oldQty) != undefined){
            if(oldQty != 0){
                var newQty = (oldQty += 1);
                $(this).closest("div.product-qty").find(".display_qty").text(newQty);
                $(this).closest("div.product-qty").find("input[type='hidden']").val(newQty);
                $('#btn-update-cart').click();
            }
        }
    });

    $('.minus').click(function(){
        var oldQty     = parseInt($(this).closest("div.product-qty").find("input[type='hidden']").val());
        var oldQtyText = $(this).closest("div.product-qty").find(".display_qty").html();

        if(typeof(oldQty) != undefined){
            if(oldQty > 1){
                var newQty = (oldQty -= 1);
                $(this).closest("div.product-qty").find(".display_qty").text(newQty);
                $(this).closest("div.product-qty").find("input[type='hidden']").val(newQty);
                $('#btn-update-cart').click();
            }
        }
    });

    $('.qty_text').change(function(){
        var oldQty     = parseInt($(this).val());
        
        if(typeof(oldQty) != undefined){
            if(oldQty > 1){
                var newQty = oldQty;
                $(this).closest("div.product-qty").find(".display_qty").text(newQty);
                $(this).closest("div.product-qty").find("input[type='hidden']").val(newQty);
                $('#btn-update-cart').click();
            }
        }
    });
</script>