<?php
/**
 * SHOPPING CART
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-12-04
 */

namespace App\Modules\Cart\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $timestamp = true;

    protected $fillable = [
        'user_id', 
        'product_id', 
        'qty', 
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
