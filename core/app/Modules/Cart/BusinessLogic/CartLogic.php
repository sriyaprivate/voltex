<?php
/**
 * SHOPPING CART
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-28
 */

Namespace App\Modules\Cart\BusinessLogic;

use DB;
use Session;
use App\Models\BankCard;
use App\Modules\Cart\Entities\Cart;
use App\Modules\Cart\Repositories\CartRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\PerformaInvoice;
use App\Models\User;
use App\Models\Customer;

class CartLogic {
    public $cartRepo;
    //variable declaration
    public $items            = null;
    public $totalQty         = 0;
    public $totalPrice       = 0;
    public $totalDiscount    = 0;
    public $subTotal         = 0;
    public $totalWeight      = 0;
    public $currentProductId = null;

    public function __construct($oldCart = null){
        $this->cartRepo = new CartRepository();

        //first of all checked cart has items then it's store the variable for update cart
        if($oldCart){
            $this->items         = $oldCart->items;
            $this->totalQty      = $oldCart->totalQty;
            $this->totalPrice    = $oldCart->totalPrice;
            $this->totalDiscount = $oldCart->totalDiscount;
            $this->subTotal      = $oldCart->subTotal;
            $this->totalWeight   = $oldCart->totalWeight;
        }
    }

    //validate string
    public function isValid($str, $notFound = '-'){
        if(count($str) && isset($str) && !empty($str)){
            return $str;
        }else{
            return $notFound;
        }
    }

    public function stringExplode($dataArray, $separator = ','){
        if(count($dataArray) > 0){
            return explode($separator, str_replace(' ', '', strtolower($dataArray)));
        }else{
            throw new \Exception("Invalid array passed to explode!.");
        }
    }

    //transaction commit or rollBack
    public function commitOrRollBack($allOk, $msg){
        if(count($allOk) > 0){
            DB::commit();
        }else{
            DB::rollBack();
            throw new \Exception($msg);
        }
    }

    /**
     * get cart data
     */
    public function getCart($user = null, $status = null, $userType = 'user', $customer = null){
        if(count($user) > 0 && isset($user->id) && $user->id !== null){
            if($status == null){
                if($_SERVER['QUERY_STRING'] !== '' && 
                    count(explode('=', $_SERVER['QUERY_STRING'])) > 0 &&
                    count(explode('=', $_SERVER['QUERY_STRING'])[1]) > 0 &&
                    explode('=', $_SERVER['QUERY_STRING'])[1] == 'direct'){
                        $products = Cart::where('cart.user_id', $user->id);
                            
                            $products = $products->where('cart.status', CAN_PAYMENT)
                            ->whereNull('cart.deleted_at')
                            ->get(['product_id', 'qty', 'id']);
                }else{
                    $products = Cart::where('cart.user_id', $user->id);

                    $products = $products->whereIn('cart.status', CART_STATUS)
                        ->whereNull('cart.deleted_at')
                        ->get(['cart.product_id', 'cart.qty', 'cart.id']);
                }
            }else{
                $products = Cart::where('cart.user_id', $user->id);
                    
                $products = $products->where('cart.status', $status)
                ->whereNull('cart.deleted_at')
                ->get(['product_id', 'qty', 'id']);
            }

            if(count($products) > 0){
                foreach($products as $key => $product){
                    if(count($product) > 0 && isset($product->product_id)){
                        $product_id = $product->product_id;
                        $cart_id = $product->id;
                        $item       = $this->cartRepo->getProductById($product_id,$cart_id);

                        if(count($item) > 0){
                            $storedItem         = ['qty' => $product['qty'], 'price' => $item->price, 'item' => $item, 'id' => $product_id, 'first_name' => $product['first_name'], 'last_name' => $product['last_name'], 'customer_id' => $product['customer_id'], 'cart_id' => $product['id']];
                            //get total weight of item (g)
                            $weight             = $storedItem['qty'] * $item->weight;
                            //total weight
                            $this->totalWeight += $weight;

                            $storedItem['availableQty'] = 1;

                            $this->items = collect($this->items);
                            
                            //each item weight * by qty
                            $storedItem['totalWeight'] = $weight;
                            $this->items[$product->id] = $storedItem;
                            $this->refreshCart();
                        }else{
                            throw new \Exception("Product not found for this ID: ".$product_id);
                        }
                    }else{
                        throw new \Exception("Product not found!.");
                    }
                }
                //calculate each product total price with there qty
                $storedItem['eachTotalPrice'] = ($item->price * $storedItem['qty']);
                //update items array with new cart data
                // $this->items[$product_id] = $storedItem;
                $this->items              = collect($this->items);
                //re calculate cart
                //$this->refreshCart();
                $cart = $this;
                $this->totalWeight = ($this->totalWeight/1000); 
            }else{
                $cart = [];
            }
        }else{
            $cart = Session::has('cart')? Session::get('cart') : null;
            
            if($cart){
                //check stock if it is available or not.
                $cart = $this->checkedItemInStock($cart);
                //update cart without out of stock items.
                $cart = new CartLogic($cart);
                $cart->refreshCart('update');
            }
        }

        return $cart;
    }

    //check stock has or not
    //params: instant of getCart();
    public function checkedItemInStock($cartData){
        if(count($cartData) > 0 && isset($cartData->items)){
            foreach($cartData->items as $item){
                if(count($item) > 0 && isset($item['id'])){
                    //get session cart data
                    $cartItem    = $this->cartRepo->getProductById($item['id']);
                    //get cart data from db.
                    $sessionItem = $cartData->items[$item['id']]['item'];

                    //if current db data different with session cart data, session data update to current data.
                    if(count($cartItem) > 0 && isset($cartItem['discount_rate'])){
                        $sessionItem['discount_rate']     = $cartItem['discount_rate'];
                        $sessionItem['discounted_amount'] = $cartItem['discounted_amount'];
                        $sessionItem['discount']          = $cartItem['discount'];

                        $cartData->items[$item['id']]     = $sessionItem;
                    }else{
                        //current discount not found unset session data
                        unset($sessionItem['discount_rate']);
                        unset($sessionItem['discounted_amount']);
                        unset($sessionItem['discount']);

                        $cartData->items[$item['id']] = $sessionItem;
                    }
                    
                    //replace now available qty to cart available qty
                    $item['availableQty']         = 1;
                    $cartData->items[$item['id']] = $item;
                }else{
                    throw new \Exception("Empty element found in cart instance.");
                }
            }
        }else{
            throw new \Exception("Invalid cart instance passed!.");
        }

        return $cartData;
    }

    
    /**
     * Add product to shopping cart
     * @return data
     * @param item_id
     * @param qty
     * @param item product data according to the item_id (product_id)
     */
    public function addCart($item_id, $qty = 0, $item){
        if(count($item) > 0 && isset($item)){
            if(isset($item_id) && !empty($item_id)){
                if(isset($qty) && !empty($qty) && $qty > 0){
                    //created and store data to array
                    $storedItem = ['qty' => $qty, 'price' => $item->price, 'item' => $item, 'id' => $item_id];

                    //calculate each product total price with there qty
                    $storedItem['eachTotalPrice'] = ($item->price * $storedItem['qty']);
                    $storedItem['availableQty']   = $this->cartRepo->getAvailableQtyByProductId($item_id)['availableQty']?:0;
                    //update items array with new cart data
                    $this->items[$item_id]        = $storedItem;
                    $this->items                  = collect($this->items);

                    //re calculate cart
                    $optionalData = ['product_id' => $item_id, 'newQty' => $storedItem['qty']];
                    return $this->refreshCart('create', $optionalData);

                }else{
                    throw new \Exception('Quantity must be at least 1.');
                }
            }else{
                throw new \Exception('Invalid product id!.');
            }
        }else{
            throw new \Exception('Invalid Item object passed!.');
        }
    }

    /**
     * remove product from shopping cart
     * @return boolean
     * @param item_id
     * @param cart_object
     */
    public function removeCart($item_id, $cart){
        if(count($item_id) && !empty($item_id) && count($cart) > 0){

            if(count($this->items) > 0 && isset($this->items[$item_id])){
                //remove item from cart.
                unset($cart->items[$item_id]);

                $optionalData = ['cart_id' => $item_id];
                $this->refreshCart('remove', $optionalData);
            }
        }else{
            throw new \Exception('Invalid product ID!.');
        }
    }

    //re calculate cart discount, total, subtotal
    public function refreshCart($action = null, $optionalData = null){
        //each item discount stored the totalDiscount variable.
        $this->totalDiscount = $this->items->sum(function($item){
            if(!isset($item['item']['discount'])){
                return 0;
            }
            
            return $item['item']['discount'] * $item['qty'];
        });

        //get number of items have in cart.
        $this->totalQty   = $this->items->count('qty');
        //get total
        $this->totalPrice = $this->items->sum(function($item){
            return $item['price'] * $item['qty'];
        });

        //get subtotal with subtraction with total discount
        $this->subTotal = ($this->totalPrice - $this->totalDiscount);
        //save new cart in session if has items in cart.
        if(count($this->items) > 0){
            if(isset($action) && $action == 'create'){
                if(count($this->items) > 0){
                    if(count($this->getLoggedUser()) > 0 && !empty($this->getLoggedUser()->id)){
                        if(count($optionalData) > 0 && !empty($optionalData['product_id'])){
                            // return 'KLLL';
                            DB::beginTransaction();
                            //checked product already in cart.
                            $isExists = $this->cartRepo->isProductExistInCart($optionalData['product_id'], $this->getLoggedUser());
                            
                            $customer = $this->getLoggedUser();

                            $updatedCart = Cart::insert([
                                'user_id'     => $this->getLoggedUser()->id,
                                'product_id'  => $optionalData['product_id'],
                                'customer_id' => count($customer) && !empty($customer->id)? $customer->id : NULL,
                                'qty'         => $optionalData['newQty'],
                                'created_at'  => date('Y-m-d h:i:s')
                            ]);

                            //rollBack or commit if all ok
                            $this->commitOrRollBack($updatedCart, "Shopping cart couldn't be updated!.");
                        }else{
                            throw new \Exception("Something went wrong!.");
                        }
                    }else{
                        Session::put('cart', $this);
                        Session::save();
                    }
                }else{
                    Session::forget('cart');
                }
            }else if(isset($action) && $action == 'update'){
                if(count($this->getLoggedUser()) > 0 && !empty($this->getLoggedUser()->id)){
                    if(count($optionalData) > 0 && !empty($optionalData['cart_id'])){
                        DB::beginTransaction();

                        $updatedCart = Cart::where('user_id', $this->getLoggedUser()->id)->where('id', $optionalData['cart_id'])->update([
                            'qty' => $optionalData['newQty'],
                            'updated_at' => date('Y-m-d h:i:s')
                        ]);
                            
                        $this->commitOrRollBack($updatedCart, "Shopping cart couldn't be updated!.");
                    }else{
                        throw new \Exception("Something went wrong!.");
                    }
                }else{
                    Session::put('cart', $this);
                    Session::save();
                }
            }else if(isset($action) && $action == 'remove'){
                if(count($this->items) > 0){
                    if(count($this->getLoggedUser()) > 0 && !empty($this->getLoggedUser()->id)){
                        if(count($optionalData) > 0 && !empty($optionalData['cart_id'])){
                            DB::beginTransaction();

                            $deletedItem = Cart::where('user_id', $this->getLoggedUser()->id)
                                ->where('id', $optionalData['cart_id'])
                                ->where('status', 1)
                                ->whereNull('deleted_at')
                                ->delete();
                                
                            $this->commitOrRollBack($deletedItem, "Shopping cart couldn't be updated!.");
                        }else{
                            throw new \Exception("Something went wrong!.");
                        }
                    }else{
                        Session::put('cart', $this);
                        Session::save();
                    }
                }else{
                    Session::forget('cart');
                }
            }
        }else{
            if(count($this->getLoggedUser()) > 0 && !empty($this->getLoggedUser()->id)){
                DB::beginTransaction();

                $deletedItem = Cart::where('user_id', $this->getLoggedUser()->id)
                    ->where('status', 1)
                    ->whereNull('deleted_at')
                    ->delete();

                $this->commitOrRollBack($deletedItem, "Shopping cart couldn't be updated!.");
            }else{
                Session::forget('cart');
            }
        }
    }

    /**
     * update shopping cart
     * @return redirect
     * @param product_id
     * @param update_qty
     */
    public function updateCart($cart_id, $newQty){
        if(!empty($cart_id) && !empty($newQty)){
            //checked item has in cart
            if(isset($this->items) && count($this->items) > 0){
                $cartItem        = $this->items[$cart_id];
                //replace old qty with new qty
                $cartItem['qty'] =  $newQty;
                //update each product total with new qty
                $cartItem['eachTotalPrice'] = $cartItem['price'] * $cartItem['qty'];

                $this->items[$cart_id]   = $cartItem;
                //re calculate discount, total and subtotal
                $optionalData = ['cart_id' => $cart_id, 'newQty' => $newQty];

                $this->refreshCart('update', $optionalData);
                //save updated cart
            }else{
                throw new \Exception("Invalid product id or product not found!.");
            }
        }else{
            throw new \Exception("Invalid product id or Invalid Quantity.");
        }
    }

    /**
     * get logged user details
     */
    public function getLoggedUser($user_id = null){

        if(!isset($user_id)){              
            $user = Auth::user();

            if(count($user) > 0){
                return $user;
            }else{
                return [];
            }
        }else{
            if($user_id){
                $user = User::where('id', $user_id)
                        ->where('status', 1)
                        ->whereNull('deleted_at')
                        ->first();

                if($user){
                    return $user;
                }else{
                    return [];    
                }
            }else{
                return [];
            }
        }    
    }

    /**
     * merge session data with db cart data
     */
    public function mergeSessionCartWithDB(){
        $loggedUser = $this->getLoggedUser();

        if(count($loggedUser) > 0 && isset($loggedUser->id)){
            $sessionCart = Session::get('sessionCart_'.$loggedUser->id);

            if(count($sessionCart) > 0){
                $payload = unserialize(base64_decode($sessionCart->payload));
                
                if(count($payload) > 0 && !empty($payload['cart'])){
                    if(!empty($payload['cart']->items)){                    
                        
                        if(count($loggedUser) > 0){
                            $updatedCart = null;
                            
                            foreach($payload['cart']->items as $key => $item){
                                if(count($item) > 0 && !empty($item['id'])){
                                    //checked product already in cart.
                                    $cartRepo   = new CartRepository();
                                    $isExists   = $cartRepo->isProductExistInCart($item['id'], $loggedUser);
                                    $proceedQty = $item['qty'];
                                    
                                    if($isExists){
                                        //get item qty in cart table or Session
                                        $itemInDB     = $cartRepo->getItemQtyByProduct($item['id'], $loggedUser);
                                        $product      = $cartRepo->getAvailableQtyByProductId($item['id']);

                                        if(count($itemInDB) > 0 && count($product) && !empty($product->availableQty)){
                                            //cart qty in db + new qty from form
                                            $totalQty   = ($itemInDB->qty + $item['qty']);

                                            if($product->availableQty >= $totalQty){
                                                $proceedQty = $totalQty;
                                            }else if($product->availableQty <= $totalQty){
                                                $proceedQty = $product->availableQty;
                                            }else{
                                                
                                            }
                                        }
                                        
                                        DB::beginTransaction();

                                        //if item already in cart, updated item qty of cart
                                        $updatedCart = Cart::where('user_id', $loggedUser->id)
                                            ->where('product_id', $item['id'])
                                            ->update([
                                                'qty'         => $proceedQty,
                                                'updated_at'  => date('Y-m-d h:i:s')
                                            ]);
                                        
                                        //commit if all ok and rollBack if not
                                        $this->commitOrRollBack($updatedCart, "Shopping cart couln't be updated!.");
                                    }else{
                                        //get item qty in cart table or Session
                                        $itemInDB     = $cartRepo->getItemQtyByProduct($item['id'], $loggedUser);
                                        $product      = $cartRepo->getAvailableQtyByProductId($item['id']);

                                        if(count($itemInDB) > 0 && count($product) && !empty($product->availableQty)){
                                            //cart qty in db + new qty from form
                                            $totalQty   = ($itemInDB->qty + $item['qty']);

                                            if($product->availableQty >= $totalQty){
                                                $proceedQty = $totalQty;
                                            }else if($product->availableQty <= $totalQty){
                                                $proceedQty = $product->availableQty;
                                            }else{
                                                
                                            }
                                        }
                                        
                                        DB::beginTransaction();
                                        //item not in cart, insert item to cart table.
                                        $updatedCart = Cart::insert([
                                            'user_id'     => $loggedUser->id,
                                            'product_id'  => $item['id'],
                                            'qty'         => $proceedQty,
                                            'created_at'  => date('Y-m-d h:i:s')
                                        ]);

                                        //commit if all ok and rollBack if not
                                        $this->commitOrRollBack($updatedCart, "Shopping cart couln't be inserted!.");
                                    }
                                }
                            }
    
                            if(count($updatedCart) > 0 && $updatedCart !== null){
                                //after merge forget the cart session of user
                                Session::forget('sessionCart_'.$loggedUser->id);
                            }
                        }
                    }
                } 
            }else{
                throw new \Exception("Session not found!.");
            }
        }else{
            throw new \Exception("Logged user not found!.");
        }
    }

    /**
     * merge cart data with db
     */
    public function mergeCartWithDB($sessionCart){
        $loggedUser = $this->getLoggedUser();

        if(count($sessionCart) > 0){
            $payload = unserialize(base64_decode($sessionCart->payload));
            
            if(count($payload) > 0 && !empty($payload['cart'])){
                if(!empty($payload['cart']->items)){                    
                    
                    if(count($loggedUser) > 0){
                        $updatedCart = null;
                        
                        foreach($payload['cart']->items as $key => $item){
                            if(count($item) > 0 && !empty($item['id'])){
                                //checked product already in cart.
                                $cartRepo   = new CartRepository();
                                $isExists   = $cartRepo->isProductExistInCart($item['id'], $loggedUser);
                                $proceedQty = $item['qty'];
                                
                                if($isExists){
                                    //get item qty in cart table or Session
                                    $itemInDB     = $cartRepo->getItemQtyByProduct($item['id'], $loggedUser);
                                    $product      = $cartRepo->getAvailableQtyByProductId($item['id']);

                                    if(count($itemInDB) > 0 && count($product) && !empty($product->availableQty)){
                                        //cart qty in db + new qty from form
                                        $totalQty   = ($itemInDB->qty + $item['qty']);

                                        if($product->availableQty >= $totalQty){
                                            $proceedQty = $totalQty;
                                        }else if($product->availableQty <= $totalQty){
                                            $proceedQty = $product->availableQty;
                                        }else{
                                            
                                        }
                                    }
                                    
                                    DB::beginTransaction();

                                    //if item already in cart, updated item qty of cart
                                    $updatedCart = Cart::where('user_id', $loggedUser->id)
                                        ->where('product_id', $item['id'])
                                        ->update([
                                            'qty'         => $proceedQty,
                                            'updated_at'  => date('Y-m-d h:i:s')
                                        ]);
                                    
                                    //commit if all ok and rollBack if not
                                    $this->commitOrRollBack($updatedCart, "Shopping cart couln't be updated!.");
                                }else{
                                    //get item qty in cart table or Session
                                    $itemInDB     = $cartRepo->getItemQtyByProduct($item['id'], $loggedUser);
                                    $product      = $cartRepo->getAvailableQtyByProductId($item['id']);

                                    if(count($itemInDB) > 0 && count($product) && !empty($product->availableQty)){
                                        //cart qty in db + new qty from form
                                        $totalQty   = ($itemInDB->qty + $item['qty']);

                                        if($product->availableQty >= $totalQty){
                                            $proceedQty = $totalQty;
                                        }else if($product->availableQty <= $totalQty){
                                            $proceedQty = $product->availableQty;
                                        }else{
                                            
                                        }
                                    }
                                    
                                    DB::beginTransaction();
                                    //item not in cart, insert item to cart table.
                                    $updatedCart = Cart::insert([
                                        'user_id'     => $loggedUser->id,
                                        'product_id'  => $item['id'],
                                        'qty'         => $proceedQty,
                                        'created_at'  => date('Y-m-d h:i:s')
                                    ]);

                                    //commit if all ok and rollBack if not
                                    $this->commitOrRollBack($updatedCart, "Shopping cart couln't be inserted!.");
                                }
                            }
                        }

                        if(count($updatedCart) > 0 && $updatedCart !== null){
                            //after merge forget the cart session of user
                            Session::forget('sessionCart_'.$loggedUser->id);
                        }
                    }
                }
            } 
        }else{
            throw new \Exception("Session not found!.");
        }
    }

    /**
     * forget user session cart if click cancel in cart merge confirm message
     */
    public function forgetUserSessionCart(){
        $loggedUser = $this->getLoggedUser();

        if(count($loggedUser) > 0 && isset($loggedUser->id)){
            if(Session::has('sessionCart_'.$loggedUser->id)){
                return Session::forget('sessionCart_'.$loggedUser->id);
            }else{
                throw new \Exception("Session cart data not found for forget this user!.");
            }
        }else{
            throw new \Exception("Cannot forget Session cart data, because user not logged in!.");
        }
    }

    /**
     * get session cart
     */
    public function hasTemporaryCart(){
        if(isset(Auth::user()->id) && Session::has('sessionCart_'.Auth::user()->id)){
            $sessionCart = Session::get('sessionCart_'.Auth::user()->id);
            $qtyData     = [];

            if(count($sessionCart) > 0 && !empty($sessionCart->payload)){
                $payload = unserialize(base64_decode($sessionCart->payload));
                
                if(count($payload) > 0 && !empty($payload['cart'])){
                    if(!empty($payload['cart']->items)){  
                        //get logged user
                        $loggedUser = $this->getLoggedUser();
                        
                        if(count($loggedUser) > 0){
                            $updatedCart = null;
                            
                            foreach($payload['cart']->items as $key => $item){
                                if(count($item) > 0 && !empty($item['id'])){
                                    //checked product already in cart.
                                    $cartRepo = new CartRepository();
    
                                    $isExists = $cartRepo->isProductExistInCart($item['id'], $loggedUser);
                                    $totalQty = $item['qty'];
    
                                    if($isExists){
                                        $itemInDB = $cartRepo->getItemQtyByProduct($item['id'], $loggedUser);
    
                                        if(count($itemInDB) > 0){
                                            //get availableQty by product id
                                            $product = $this->cartRepo->getAvailableQtyByProductId($item['id']);
                                            
                                            //cart qty in db + new qty from session
                                            $totalQty = ($itemInDB->qty + $item['qty']);

                                            $canAddToCart = $product->availableQty >= $totalQty;

                                            $qtyData['canAddToCart'] = $canAddToCart;
                                            $qtyData['availableQty'] = $product->availableQty;
                                            $qtyData['totalQty']     = $totalQty;
                                        }
                                    }else{
                                        //get availableQty by product id
                                        $product = $this->cartRepo->getAvailableQtyByProductId($item['id']);
                                        
                                        //cart qty in db + new qty from session
                                        $totalQty = $item['qty'];

                                        $canAddToCart = $product->availableQty >= $totalQty;

                                        $qtyData['canAddToCart'] = $canAddToCart;
                                        $qtyData['availableQty'] = $product->availableQty;
                                        $qtyData['totalQty']     = $totalQty;
                                    }
                                }
                            }
                        }else{
                            throw new \Exception("Logged user not found!.");
                        }
                    }else{
                        throw new \Exception("Item not found in Session payload!.");
                    }
                }else{
                    throw new \Exception("Empty Session payload!.");
                }
            }else{
                throw new \Exception("Session doesn't exists!.");
            }

            return $qtyData;
        }
    }

    public function getCartItemList($user){
        $item_list = $this->cartRepo->getCartItemListByUser($user);
        return $item_list;
    }

    /**
     * get each item's delivery option
     * @param user selected delivery option
     * @return html
     */
    public function getEachItemDeliveryOption($request){
        $selected_delivery_option = $request->input('selected_delivery_option');
        $selected_regional_center = $request->input('selected_regional_center');
        $loggedUser               = $this->getLoggedUser();
        $item_count               = 0;
        $error_count              = 0;
        $cannotHtml               = null;
        $error_msg                = '<div class="pt-4 text-left text-danger">NOTE: We can see that you have picked "<strong>'.$selected_delivery_option.'</strong>" option to deliver your order.';
        $error_msg2               = 'item(s) in the cart are not available for the chosen delivery type. checkout will proceed with only the item(s) which status is (<span class="fa fa-check text-success"></span>).';
        
        if(count($loggedUser) > 0){
            $items              = $this->getCartItemList($loggedUser); 
            $item_count         = count($items?: 0);
            //store item id which cannot be proceed to payment
            $cannotProceedArray = [];

            if(count($items) > 0){
                $html = '
                <table>
                    <tr>
                        <td class="pb-2"><div class="text-left" style="font-size: 13px;">Selected delivery option</div></td>
                        <td class="pb-2 pl-3 pr-3">:</td>
                        <td class="pb-2 text-left"><strong>'.ucfirst(isValid($selected_delivery_option, '-')).'</strong></td>
                    </tr>';

                    if(!empty($selected_regional_center) > 0 && !empty($selected_regional_center)){
                        $html .= '
                        <tr>
                            <td class="pb-2"><div class="text-left" style="font-size: 13px;">Selected Regional center</div></td>
                            <td class="pb-2 pl-3 pr-3">:</td>
                            <td class="pb-2 text-left"><strong>'.isValid($selected_regional_center, '-').'</strong></td>
                        </tr>';
                    }

                    $html .='
                    <tr>
                        <td class="pb-2"><div class="text-left" style="font-size: 13px;">Can deliver</div></td>
                        <td class="pb-2 pl-3 pr-3">:</td>
                        <td class="pb-2 text-left"><strong><span class="fa fa-check text-success"></span></strong></td>
                    </tr>
                    <tr>
                        <td class="pb-2"><div class="text-left" style="font-size: 13px;">Can not deliver</div></td>
                        <td class="pb-2 pl-3 pr-3">:</td>
                        <td class="pb-2 text-left"><strong><span class="fa fa-times text-danger"></span></strong></td>
                    </tr>
                </table>';
                
                $html      .= '
                <div class="overflow-x-scroll">
                <table class="table table-sm mt-4">
                    <thead>
                        <tr class="bg-secondary text-light">
                            <th scope="col" class="text-center">#</th>
                            <th scope="col" class="text-left">Item Code</th>
                            <th scope="col" class="text-left" width="40%">Item name</th>
                            <th scope="col" class="text-left">Available Delivery Option</th>
                            <th scope="col" class="text-left">Status</th>
                        </tr>
                    </thead>
                <tbody>';
                
                foreach($items as $key => $item){
                    $canDelivery = '';

                    if(count($item->delivery_options) && isset($item->delivery_options) && !empty($item->delivery_options)){
                        $delivery_options = $this->stringExplode($item->delivery_options, ',');
                        
                        if(count($delivery_options) > 0 && !empty($selected_delivery_option)){
                            $canDelivery = in_array(strtolower($selected_delivery_option), $delivery_options);
                        }
                    }

                    $html .= '
                    <tr>
                        <th scope="row" class="text-center">'.(++$key).'</th>
                        <td class="text-left">'.isValid($item->code, '-').'</td>
                        <td class="text-left">'.isValid($item->name, '-').'</td>
                        <td class="text-left">'.isValid($item->delivery_options, '-').'</td>';

                        $class      = '';
                        $status     = '';
                        
                        if($canDelivery){
                            $class  = 'fa fa-check text-success';
                            $status = isValid(ucfirst($selected_delivery_option), '-').' available';
                        }else{
                            $error_count++;
                            $class  = 'fa fa-times text-danger';
                            array_push($cannotProceedArray, $item->id);
                            $status = isValid(ucfirst($selected_delivery_option), '-').' not available';

                            // $cannotHtml  = '<div class="pt-4 text-left text-danger">NOTE: ';
                            $cannotHtml .= '<strong>'.$item->code.' - '.$item->name.'</strong>, ';
                            // $cannotHtml .= 'item cannot be deliver by <strong>'.$selected_delivery_option.'</strong>, Anyway do you want to proceed to payment, click proceed to payment button.</div>';
                        }

                        $html .='
                        <td class="text-left"><span class="'.$class.'"></span> '.$status.'</td>
                    </tr>';
                }

                if($cannotHtml !== null){
                    $cannotHtml = $error_msg.' '.$cannotHtml.' '.$error_msg2;   
                }

                $html .= "
                    </tbody>
                </table>
                </div>".$cannotHtml;
                
                return response()->json([
                    'error_count'        => $error_count,
                    'html'               => $html,
                    'item_count'         => $item_count,
                    'cannotProceedArray' => $cannotProceedArray
                ]);
            }else{
                throw new \Exception("Cart has not items!.");
            }
        }else{
            throw new \Exception("Logged user not found!.");
        }
    }

    /**
     * get each item's delivery status array
     * @param user selected delivery option
     * @return array
     */
    public function getItemDeliveryStatus($selected_delivery_option = null){
        $loggedUser  = $this->getLoggedUser();
        $item_count  = 0;
        $error_count = 0;
        $statusArray = [];
        
        if(count($loggedUser) > 0){
            $items       = $this->getCartItemList($loggedUser); 
            $item_count  = count($items?: 0);

            if(count($items) > 0){               
                foreach($items as $key => $item){
                    $canDelivery = '';

                    if(count($item->delivery_options) && isset($item->delivery_options) && !empty($item->delivery_options)){
                        $delivery_options = $this->stringExplode($item->delivery_options, ',');

                        if(count($delivery_options) > 0 && !empty($selected_delivery_option)){
                            $canDelivery = in_array(strtolower($selected_delivery_option), $delivery_options);
                            
                            if($canDelivery){
                                $statusArray[$item->id] = ['canDelivery' => true, 'item' => $item];
                            }else{
                                $statusArray[$item->id] = ['canDelivery' => false, 'item' => $item];
                            }
                        }else{
                            throw new \Exception("Delivery status not found or selected delivery status not found!.");
                        }
                    }
                }
                
                return [
                    'selected_delivery_option' => $selected_delivery_option,
                    'error_count'              => $error_count,
                    'statusArray'              => $statusArray,
                    'item_count'               => $item_count
                ];
            }else{
                throw new \Exception("Cart has not items!.");
            }
        }else{
            throw new \Exception("Logged user not found!.");
        }
    }

    //get total weight of items whitch can deliver
    public function getCartItemsTotalWeight($delivery_type){
        if(isset($delivery_type) && !empty($delivery_type)){

            $itemDeliveryStatus = $this->getItemDeliveryStatus($delivery_type);
            $totalWeight        = 0;

            if(isset($itemDeliveryStatus['statusArray']) && count($itemDeliveryStatus['statusArray']) > 0){
                $totalWeightCollect = collect($itemDeliveryStatus['statusArray']);

                if(count($totalWeight) > 0){
                    $totalWeight = $totalWeightCollect->where('canDelivery', true)->sum(function($item){
                        return $item['item']['cart_qty'] * $item['item']['weight'];
                    });
                }
            }
            
            if(isset($totalWeight) && count($totalWeight) && is_int($totalWeight)){
                return ($totalWeight / 1000); // g to kg
            }else{
                throw new \Exception("Invalid weight supplyed to calculation!.");
            }
        }else{
            throw new \Exception("Delivery type not provided!.");
        }
    }

    //get session cart by browwer id
    public function getTemparyCartSessionByBrowserSessionId(){
        $session_id = Session::getId();
		$sessionData = DB::table('sessions')
			->where('id', $session_id)
            ->first();  

        return $sessionData;
    }

    public function setTemparyCartWithLoggedUser($sessionData){
        $cartLogic   = new CartLogic();
		$loggedUser  = $cartLogic->getLoggedUser();
        
		if(count($loggedUser) > 0 && isset($loggedUser->id)){
		    if(count($sessionData) > 0 && !empty($sessionData->payload)){
                $payload = unserialize(base64_decode($sessionData->payload));
                
                if(count($payload) > 0 && !empty($payload['cart'])){
                    if(!empty($payload['cart']->items)){
                        Session::put('sessionCart_'.$loggedUser->id, $sessionData);
                    }
                }
		    }
		}else{
            throw new \Exception("User not logged in!.");
        }
    }

    //get payment methods
    public function getPaymentMethods(){
        $methods = BankCard::with(['bank', 'cart_installment.installment'])
                    ->whereNull('deleted_at')
                    ->get();

        return $methods;
    }

    //get next order No
    public function getNextOrderNo(){
        $nextOrderNo     = null;
        $LastorderRecord = $this->cartRepo->getLastOrderId(ORDER_PREFIX);
        
        if(count($LastorderRecord) > 0 && isset($LastorderRecord->order_no)){
            $lastOrderNumber = explode('-', $LastorderRecord->order_no);

            if(count($lastOrderNumber) > 0 && isset($lastOrderNumber[1])){
                $nextOrderNo = ($lastOrderNumber[1] + 1);
            }else{
                throw new \Exception("Something went wrong!.");   
            }
        }

        if($nextOrderNo !== null){
            return ORDER_PREFIX.str_pad($nextOrderNo, 6, 0, STR_PAD_LEFT);
        }else{
            return ORDER_PREFIX.str_pad(1, 6, 0, STR_PAD_LEFT);
        }
    }

    //get next order No
    public function getNextSupplierCode(){
        $nextOrderNo     = null;
        
        $LastorderRecord = $order_no = Order::where('order_no', 'LIKE', '%'.SUPPLIER_PREFIX.'%')
            ->orderBy('order_no', 'DESC')
            ->orderBy('id', 'DESC')
            ->first();
        
        if(count($LastorderRecord) > 0 && isset($LastorderRecord->order_no)){
            $lastOrderNumber = explode('-', $LastorderRecord->order_no);

            if(count($lastOrderNumber) > 0 && isset($lastOrderNumber[1])){
                $nextOrderNo = ($lastOrderNumber[1] + 1);
            }else{
                throw new \Exception("Something went wrong!.");   
            }
        }

        if($nextOrderNo !== null){
            return ORDER_PREFIX.str_pad($nextOrderNo, 6, 0, STR_PAD_LEFT);
        }else{
            return ORDER_PREFIX.str_pad(1, 6, 0, STR_PAD_LEFT);
        }
    }

    //get order by id
    public function getOrderById($order_id){
        try{
            if(isset($order_id)){
                $order = Order::where('id', $order_id)->with([
                    'user.customer', 
                    'deliveryType', 
                    'payment.transaction', 
                    'location',
                    'details.product' => function($product){
                        $product->select(
                            'product.id',
                            'product.name',
                            'product.display_name',
                            'product.weight'
                        );
                    }
                ])->first();
                
                //params: data, if invalid return param
                if($this->isValid($order, false)){
                    if($this->isValid($order->details, false)){
                        $details = [];
                        $data = [
                            'order_id' => $order->id,
                            'order_no' => $order->order_no,
                            'order_date' => $order->order_date,
                            'order_id' => $order->payment->payment_type_id,
                            'total_amount' => $order->payment->amount,
                            'total_discount' => $order->payment->amount,
                            //'total_discount' => $order->details->discounted_price * $order->details->qty,
                            'unit_price' => $order->price,
                        ];
                        
                       return $order;
                    }
                }else{
                    throw new \Exception('Invalid order id.');
                }
            }else{
                throw new \Exception('Order id not found!.');
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    //get customer by customer id
    public function getCustomer($customer_id = null){

        if($customer_id !== null){ 
            $customer = Customer::where('id', $customer_id)
                ->where('status', 1)
                ->whereNull('deleted_at')
                ->first();

            return $customer;
        }else{
            throw new \Exception('Customer id not found!.');
        }
    }

    public function getPendingPIConfirmations($customer = null){
        $loggedUser = $this->getLoggedUser();

        if($loggedUser){
            $res = PerformaInvoice::where('customer_id',$loggedUser->id)->where('status',1)->with(['order'])->get();
        }else{
            $res = [];
        }
        
        return $res;

    }
}