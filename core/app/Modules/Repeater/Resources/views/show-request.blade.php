@extends('layouts.master') @section('title','Repeater Request')

@section('links')
    <link href="{{asset('assets/libraries/Gijgo/gijgo.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
    <style type="text/css">
        
    </style>
@stop

@section('content')
    
    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="#">Repeater Request</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Create Repeater Request</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <div class="row profile">
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">
                        <div class="">
                            <ul class="nav flex-column" style="list-style-type: none;">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('repeater/create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Create Request</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('repeater/list')}}"><i class="fa fa-history" aria-hidden="true"></i> List Requets</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        
                <div class="col-md-9 mb-3" style="font-size: 12px">
                                
                    <h3 class="box-title">
                        Repeater Request - {{ $form->form->category->name }} {{$form?"#(".$form->request_no.")":""}}
                    </h3>
                
                    <h4 style="margin-top: 0">Request From</h4>
                    <table class="table table-bordered" style="margin-bottom: 20px;">
                        <tbody>
                            <tr>
                                <td width="20%">Initiated By</td>
                                <td width="80%">
                                    <strong>
                                        {{$form->initiator->first_name}} {{$form->initiator->last_name}}<br>
                                        {{$form->initiator->username}}
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">Contact</td>
                                <td width="80%">
                                    {{$form->initiator->mobile?$form->initiator->mobile:"no contact number found "}}
                                </td>            
                            </tr>
                            <tr>
                                <td width="20%">Email</td>
                                <td width="80%">
                                    <a href="mailto: {{$form->initiator->email?$form->initiator->email:''}}"> {{$form->initiator?$form->initiator->email:" "}}</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <h4>Request Details</h4>
                    <table class="table table-bordered">
                        <tbody>
                            <?php $steps = json_decode($json)->count ?>
                            @for($i=1 ; $i <= $steps ; $i++)
                                <?php $aa = 'step'.$i?>
                                <?php $array = json_decode($json,true)?>
                                {{-- Getting json repeater form details --}}
                                @foreach($array[$aa]['fields'] as $fields)
                                    <tr>
                                        @if(isset($fields['hint']) && ($fields['hint']) != null)
                                            <td width="20%">{{$fields['hint']}}</td>
                                        @elseif(isset($fields['label']) && ($fields['label']) != null)
                                            <td width="20%">{{$fields['label']}}</td>

                                            {{-- Getting values if form input type equal Checkbox --}}
                                            @if((($fields['type']) == "check_box") && (isset($fields['options'])))
                                                @foreach($fields['options'] as $option)
                                                    @if(($option['value']) == "true")
                                                      <td>{{$option['text']}}</td>
                                                    @elseif(($option['value']) == "false")
                                                      <td>{{$option['text'] = "-"}}</td>
                                                    @endif
                          
                                                @endforeach
                                            @endif
                                        @endif

                                        {{-- Getting values if form input type not equal Checkbox --}}
                                        <td colspan="4">{{isset($fields['value'])?$fields['value']:'-'}}</td>
                                    </tr>
                                @endforeach
                            @endfor
                        </tbody>
                    </table>
                      
                    <div class="row">
                        @foreach($form->attachments as $key=>$attach)
                        <div class="col-md-3">
                            <div class="card mb-3 box-shadow">
                                <div class="card-img-top" style="height: 155px; width: 100%; display: block;">
                                    <img class="img-responsive" src="{{url('core/storage/'.$attach->file)}}" width="100%" style="margin: 10% auto;">
                                    <hr style="color: #ddd">
                                </div>
                                <div class="card-body"> 
                                    <p class="card-text">Uploaded By
                                    <br><small class="text-muted">{{$attach->created_at}}</small></p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <a target="_blank" href="{{url('core/storage/'.$attach->file)}}" class="btn btn-sm btn-outline-secondary">View</a>
                                        </div>                  
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/libraries/Gijgo/gijgo.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'yyyy-mm-dd'
            });

            // In your Javascript (external .js resource or <script> tag)
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        });
</script>
@stop