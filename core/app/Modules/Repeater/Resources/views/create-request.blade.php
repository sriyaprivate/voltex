@extends('layouts.master') @section('title','Repeater Request')

@section('links')
    <link href="{{asset('assets/libraries/Gijgo/gijgo.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
    <style type="text/css">
        .footer-wrap {
            bottom: -32.5% !important;
        }

        .cart{
            margin: 90px -25px 0px 0 !important;
        }

        .performa-invoice-cart{
            margin: 90px -25px 0px 0 !important;
        }
    </style>
@stop

@section('content')
    
    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="#">Repeater Request</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Create Repeater Request</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <div class="row profile">
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">
                        <div class="">
                            <ul class="nav flex-column" style="list-style-type: none;">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('repeater/create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Create Request</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('repeater/list')}}"><i class="fa fa-history" aria-hidden="true"></i> List Requets</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        
                <div class="col-md-9 mb-3" style="font-size: 12px">
                    <form  class="form-horizontal" action="{{url('repeater/new/request')}}" role="form" method="POST" enctype="multipart/form-data" name="form_json">
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($forms as $key=>$form)
                                <li style="padding: 10px;background-color: #F3F3F3"><a class="@if($key==0) active @endif" style="border-bottom: none;font-size: small;font-weight: bold" href="#{{$form->id}}" role="tab" data-toggle="tab">{{$form->category->name}}</a></li>
                            @endforeach
                            <li style="padding: 10px;background-color: #F3F3F3"><a style="border-bottom: none;font-size: small;font-weight: bold" href="#attachments" role="tab" data-toggle="tab">Attachments</a></li>
                        </ul>

                        <div class="tab-content">
                            
                            <div class="tab-pane fade" id="attachments">
                                <div class="box-body">
                                    <div class="row" style="padding-top: 15px;padding-left: 10px;">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <input id="input-b1" name="file[]" type="file" class="file" multiple 
                                            value="">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @foreach($forms as $key=>$form)
                                <input type="hidden" name="forms[]" value="{{$form->id}}">
                                <div class="tab-pane fade @if($key==0) in active show @endif" id="{{$form->id}}">
                                    <div class="box-body">
                                        {!! csrf_field() !!}
                                        <div class="row" style="padding-top: 15px">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="form-group @if($errors->has('form_json')) has-error @endif">
                                                            <?php $steps = json_decode($form->form)->count?>
                                                            @for($i=1 ; $i <= $steps ; $i++)
                                                                <?php $cc = 'step' . $i ?>
                                                                <?php $array = json_decode($form, true)?>
                                                                <?php $fields = json_decode($array['form'], true)[$cc]['fields']?>

                                                                <input type="hidden" name="{{$form->id}}[count]" value="{{$i}}">
                                                                <div class="col-md-12 col-lg-12">
                                                                    <div class="row">
                                                                    @foreach($fields as $key=>$field)
                                                                            @if($field['type']=='edit_text')<!-- EDIT TEXTS -->
                                                                            <div class="col-md-4">
                                                                                <label class="control-label {{isset($field['v_required'])?$field['v_required']['value']?"required":"":""}} ">
                                                                                <strong>{{$field['hint']}}</strong></label>
                                                                                <input placeholder="{{$field['hint']}}" class="form-control" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][value]{{$field['key']}}"
                                                                                {{isset($field['v_required'])?$field['v_required']['value']?"required":"":""}} />
                                                                                <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][key]" value="{{$field['key']}}">
                                                                                <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][type]" value="{{$field['type']}}">
                                                                                <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][hint]" value="{{$field['hint']}}">
                                                                            </div>
                                                                            @elseif($field['type']=='radio')<!-- RADIO BUTTONS -->
                                                                            <div class="col-md-4">

                                                                                <label class="control-label{{isset($field['v_required'])?$field['v_required']['value']?"required":"":""}} ">
                                                                                <strong>{{$field['label']}}</strong></label><br>
                                                                                @foreach($field['options'] as $option )
                                                                                    <div class="radio">
                                                                                        <label><input type="radio" value="{{$option['text']}}" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][value]{{$field['key']}}">{{$option['text']}}</label>
                                                                                    </div>
                                                                                    <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][key]" value="{{$field['key']}}">
                                                                                    <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][type]" value="{{$field['type']}}">
                                                                                    <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][label]" value="{{$field['label']}}">
                                                                                @endforeach
                                                                            </div>
                                                                            @elseif($field['type']=='check_box')<!-- CHECKBOXES -->
                                                                            <div class="col-md-4">
                                                                                <label class="control-label{{isset($field['v_required'])?$field['v_required']['value']?"required":"":""}} ">
                                                                                <strong>{{$field['label']}}</strong></label>
                                                                                @foreach($field['options'] as $option )
                                                                                    <div class="checkbox" style="margin-top: 10px">
                                                                                        <label><input type="checkbox" value="{{$option['text']}}" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][value]{{$field['key']}}">{{$option['text']}}</label>
                                                                                    </div>
                                                                                    <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][key]" value="{{$field['key']}}">
                                                                                    <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][type]" value="{{$field['type']}}">
                                                                                    <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][label]" value="{{$field['label']}}">
                                                                                @endforeach
                                                                            </div>
                                                                            @elseif($field['type']=='spinner')<!-- SPINNER -->
                                                                            <div class="col-md-4">
                                                                                <label class="control-label{{isset($field['v_required'])?$field['v_required']['value']?"required":"":""}} ">
                                                                                <strong>{{$field['hint']}}</strong></label>
                                                                                <select class="form-control" id="sel1">
                                                                                    @foreach($field['values'] as $option )
                                                                                        <option name="{{$form->id}}[{{$cc}}][fields][{{$key}}][value]{{$field['key']}}" value="{{$option}}">{{$option}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][key]" value="{{$field['key']}}">
                                                                                <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][type]" value="{{$field['type']}}">
                                                                                <input type="hidden" name="{{$form->id}}[{{$cc}}][fields][{{$key}}][hint]" value="{{$field['hint']}}">
                                                                            </div>
                                                                            @endif
                                                                    @endforeach
                                                                    </div>
                                                                </div>
                                                                @if($errors->has('form_json'))
                                                                    <span class="help-block">{{$errors->first('form_json')}}</span>
                                                                @endif
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="btn pull-right" style="color: #fff;background: crimson">
                            Save Request
                        </button>
                    </form>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="{{asset('assets/libraries/Gijgo/gijgo.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'yyyy-mm-dd'
            });

            // In your Javascript (external .js resource or <script> tag)
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        });
</script>
@stop