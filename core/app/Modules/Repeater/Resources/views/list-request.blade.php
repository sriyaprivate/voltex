@extends('layouts.master') @section('title','Repeater Request')

@section('links')
    <link href="{{asset('assets/libraries/Gijgo/gijgo.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
    <style type="text/css">
        .footer-wrap {
            bottom: -32.5% !important;
        }

        .cart{
            margin: 90px -25px 0px 0 !important;
        }

        .performa-invoice-cart{
            margin: 90px -25px 0px 0 !important;
        }
    </style>
@stop

@section('content')
    
    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="#">Repeater Request</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Create Repeater Request</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole" style="min-height: 365px">
            <div class="row profile">
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">
                        <div class="">
                            <ul class="nav flex-column" style="list-style-type: none;">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('repeater/create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Create Request</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('repeater/list')}}"><i class="fa fa-history" aria-hidden="true"></i> List Requets</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        
                <div class="col-md-9 mb-3" style="font-size: 12px">
                    
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th>Request No</th>
                                <th>Repeater Product</th>
                                <th>Initiator</th>
                                <th>Created Date</th>
                                <th width="10%" class="text-center" colspan="3">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($_lists)>0)
                                <?php $i=1; ?>
                                @foreach($_lists as $_list)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>
                                            <a target="_blank" href="{{url('repeater/show/')}}/{{$_list->id}}" style="margin:0">
                                                {{$_list->request_no}}
                                            </a>
                                        </td>
                                        <td>{{$_list['form']['category']->name}}</td>
                                        <td>
                                            {{$_list['initiator']->username}}
                                            <br>
                                            {{$_list['initiator']->first_name}} {{$_list['initiator']->last_name}}
                                        </td>
                                        <td>{{$_list->created_at}}</td>
                                        <td style="text-align: center;">
                                            <a target="_blank" href="{{url('repeater/show/')}}/{{$_list->id}}" style="margin:0">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5" style="text-align: center">No Forms</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="{{asset('assets/libraries/Gijgo/gijgo.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'yyyy-mm-dd'
            });

            // In your Javascript (external .js resource or <script> tag)
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        });
</script>
@stop