@extends('layouts.master') @section('title','Repeater Request')

@section('links')
    <link href="{{asset('assets/libraries/Gijgo/gijgo.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libraries/select2-4.0.4/dist/css/select2.min.css')}}" rel="stylesheet" />
@stop

@section('css')
    <style type="text/css">
        .footer-wrap {
            bottom: -32.5% !important;
        }

        .cart{
            margin: 90px -25px 0px 0 !important;
        }

        .performa-invoice-cart{
            margin: 90px -25px 0px 0 !important;
        }
    </style>
@stop

@section('content')
    
    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="#">Repeater Request</a>
                </li>
                <li class="arrow" style="float: left"> → </li><li>Create Repeater Request</li>
            </ul>
        </div> <!-- /.breadcrumb -->

        <div class="white-box-whole">
            <div class="row profile">
                <div class="col-md-3 mb-3">
                    <div class="profile-sidebar">
                        <div class="">
                            <ul class="nav flex-column" style="list-style-type: none;">
                                <li class="active nav-item">
                                    <a class="nav-link" href="{{url('repeater/create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Create Request</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('repeater/list')}}"><i class="fa fa-history" aria-hidden="true"></i> List Requets</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        
                <div class="col-md-9 mb-3" style="font-size: 12px">
                    <form method="GET" role="form" action="{{url('repeater/new/request')}}">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th>Form Type</th>
                                    <th>Remark</th>
                                    <th width="10%" class="text-center" colspan="3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($repeater_forms)>0)
                                    <?php $i=1; ?>
                                    @foreach($repeater_forms as $repeater_form)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$repeater_form->name}}</td>
                                            <td>{{$repeater_form->desc}}</td>
                                            <td style="text-align: center">
                                                <input value="{{$repeater_form->id}}" type="checkbox" name="selcted_form_types[]">
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" style="text-align: center">No Forms</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>                    
                        <div class="pull-right">
                            <button type="submit" class="btn" style="background-color: crimson;color: #fff">Create Request</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="{{asset('assets/libraries/Gijgo/gijgo.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/libraries/select2-4.0.4/dist/js/select2.min.js')}}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'yyyy-mm-dd'
            });

            // In your Javascript (external .js resource or <script> tag)
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        });
</script>
@stop