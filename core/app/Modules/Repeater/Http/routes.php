<?php

Route::group(['prefix' => 'repeater', 'middleware' => ['web', 'auth'], 'namespace' => 'App\\Modules\Repeater\Http\Controllers'], function()
{
    Route::get('/create', [
        'uses'  => 'RepeaterController@createRepeaterRequestView',
        'as'    => 'repeater.new.request'
    ]);

    Route::get('/new/request', [
        'uses'  => 'RepeaterController@newRepeaterRequestView',
        'as'    => 'repeater.new.request'
    ]);

    Route::get('/list', [
        'uses'  => 'RepeaterController@viewRepeaterRequestList',
        'as'    => 'repeater.list.request'
    ]);

    Route::get('/show/{id}', [
        'uses'  => 'RepeaterController@showRequestDetails',
        'as'    => 'repeater.show.request'
    ]);

    Route::post('/new/request', [
        'uses'  => 'RepeaterController@saveRepeatForm',
        'as'    => 'repeater.save.request'
    ]);

});