<?php

namespace App\Modules\Repeater\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use App\Classes\Functions;;

use App\Models\Activations;
use App\Models\User;
use App\Models\RepeaterFormCategories;
use App\Models\RepeaterForm;
use App\Models\RepeaterFormRequests;
use App\Models\RepeaterFormAttachment;
use App\Modules\Repeater\Repositories\RepeaterRepo;
use App\Modules\Cart\BusinessLogic\CartLogic;

use Auth;
use PDF;
use QrCode;

use DB;
use Sentinel;
use File;

class RepeaterController extends Controller
{
    protected $common    = null;
    private $repeaterRepo = null;

    function __construct(Functions $common, RepeaterRepo $repeaterRepo, CartLogic $cartLogic){
        $this->common      = $common;
        $this->repeaterRepo = $repeaterRepo;
        $this->cartLogic     = $cartLogic;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function createRepeaterRequestView()
    {
        $repeater_forms = RepeaterFormCategories::all();
        return view('repeater::create-request-view',compact('repeater_forms'));
    }

    public function newRepeaterRequestView(Request $request)
    {
        $selectedforms = $request->selcted_form_types;
        if($selectedforms==null){
             return redirect('repeater/create')->with([
                'error' => true,
                'error.message' => 'Please select atleast one request type!',
                'error.title'   => 'Error!'
            ]);
        }
        $forms = RepeaterForm::with(['category'])->whereIn('repeater_form_category_id',$selectedforms)->get();
        return view("repeater::create-request",compact('forms'));
    }

    public function saveRepeatForm(Request $request)
    {

        DB::transaction(function () use ($request) {
            $loggedUser = $this->cartLogic->getLoggedUser();            

            $array = json_decode(json_encode($request->all()), true);
            $aa = [];   
                
            foreach ($request->forms as $key => $form_id) {
                array_push($aa, $array[$form_id]);
                //SAVE FORM
                $repeaterForm = RepeaterFormRequests::create([
                    'form_id'      =>  $form_id,
                    'form_details' =>  json_encode( json_encode($array[$form_id])),
                    'status'       => STATUS_TODO,
                    'user_id'      => $loggedUser->id,
                    'request_by'   => $loggedUser->id

                ]);

                if($repeaterForm){
                    $tmp="REQ/".str_pad($repeaterForm->id, 6, '0', STR_PAD_LEFT);
                    $repeaterForm->request_no = $tmp;
                    $repeaterForm->save();
                }

            }

            if($request->hasfile('file')){
                foreach($request->file('file') as $key=>$file){
                    $file_ext = $file->getClientOriginalExtension();
                    $destinationPath = storage_path('uploads/repeater/request_attachments/');
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath, 0777, true);
                    }

                    $name = date("YmdHis").'_'.$repeaterForm->id.'_'.$key.'.'.$file_ext;
                    if($file->move(storage_path().'/uploads/repeater/request_attachments', $name)){
                        $fil         = RepeaterFormAttachment::create([
                            'file'       => 'uploads/repeater/request_attachments/'.$name,
                            'request_id' => $repeaterForm->id
                        ]);                              
                    }
                }
            }

        });

        return redirect('repeater/create')->with([
            'success' => true,
            'success.message' => 'Request created successfully!',
            'success.title'   => 'Request Created!'
        ]);
    }

    public function viewRepeaterRequestList(Request $request)
    {   
        $_lists = RepeaterFormRequests::with(['form.category','attachments','initiator']);

        $_lists = $_lists->orderBy('timestamp','desc')->paginate(10);

        return view("repeater::list-request",compact('_lists'));
    }

    public function showRequestDetails($id)
    {   
        $form = RepeaterFormRequests::with(['form.category','initiator'])->find($id);
        $json = json_decode($form->form_details);
        $files =  RepeaterFormAttachment::where('request_id',$id)->get();

        // return view("RepeaterFormRequests::show",compact('json','form','files'));

        return view("repeater::show-request",compact('json','form','files'));
    }
}
