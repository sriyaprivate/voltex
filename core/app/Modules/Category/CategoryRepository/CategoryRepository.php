<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */

namespace App\Modules\Category\CategoryRepository;

use App\Modules\Category\Models\Category;
use App\Modules\Category\Models\CategoryType;
use App\Modules\Category\Models\CategoryFilter;
use App\Modules\Product\Models\Product;
use App\Models\ProductCategory;
use App\Classes\Functions;
use App\Models\Filter;
use App\Models\FilterValue;
use Response;
use DB;

class CategoryRepository {

	//get all default filters
	public function getDefaultFilters(){
		try {
			$filter = Filter::
				select(
					DB::raw('DISTINCT filter_values.value as value'),
					DB::raw('GROUP_CONCAT(filter_values.id) as value_ids'),
					'filter.*'
				)->join('category_filter as category_filters', function($category_filter_join){
	            	$category_filter_join->on('filter.id', '=', 'category_filters.filter_id');
	            })->join('filter_value as filter_values', function($join){
	            	$join->on('category_filters.id', '=', 'filter_values.category_filter_id');
	            })
	            ->where('filter.is_default', 1)
	            ->groupBy('filter_values.value')
	            ->get();
			
			if(count($filter) > 0)
			{
				return $filter;
			}	
			else
			{
				return [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//     these function used for the home page implementation.
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//get main categories by page name.
	public function getMainCategoriesByPage($page, $type = 'parents', $category_id = null){
		if(isset($page)){
			if($type == 'parents'){
				$categories = Category::select('category.*')
					->join('category_type as ct', 'category.category_type_id', '=', 'ct.id')
					->where('category.status', '1')
					->where('category.deleted_at', null)
					->where('category.parent', null)
					->where('ct.name', 'LIKE', '%'.$page.'%')
					->orderBy('category.id', 'ASC')
					->get();
			}elseif($type == 'tree'){
				if($category_id == null){
					$categories = Category::select('category.*')
						->join('category_type as ct', 'category.category_type_id', '=', 'ct.id')
						->where('category.status', '1')
						->where('category.deleted_at', null)
						->where('ct.name', 'LIKE', '%'.$page.'%')
						->orderBy('category.id', 'ASC')
						->get()
						->toHierarchy();
				}else{
					$categories = Category::select('category.*')
						->join('category_type as ct', 'category.category_type_id', '=', 'ct.id')
						->where('category.status', '1')
						->where('category.deleted_at', null)
						->where('category.id', $category_id)
						->orderBy('category.id', 'ASC')
						->get()
						->toHierarchy();
				}
			}

			return $categories;
		}else{
			throw new \Exception('Page parameter cannot be empty on "getMainCategoriesByPage" function.');
		}
	}

	public function getFilterForCategory($category = null){
		if($category !== null){
			return $filters = CategoryFilter::with('filter', 'filter_values')
				->where('category_id', $category)
				->get();
		}else{
			throw new \Exception('Category ID is invalid or not provided!.');
		}
	}

	public function getCategoryType(){
		$categoryTypes = CategoryType::where('status', DEFAULT_STATUS)
			->whereNull('deleted_at')
			->get();

		if(count($categoryTypes) > 0){
			return $categoryTypes;
		}else{
			throw new \Exception('Catgeory type not found!.');
		}		
	}

	public function getCategoryTypeById($id = null){
		$categoryType = CategoryType::where('status', DEFAULT_STATUS)
			->where('id',$id)
			->whereNull('deleted_at')
			->first();

		if(count($categoryType) > 0){
			return $categoryType;
		}else{
			throw new \Exception('Catgeory type not found!.');
		}		
	}

	public function getCategoryById($type_id = null, $cat_id = null){
		$category = Category::where('category.status', '1')
				->where('category.deleted_at', null)
				->where('category.category_type_id', $type_id)
				->where('category.id', $cat_id)
				->first();

		return $category->name;
	}

	public static function getCategories($type_id = null){
		// return $type_id;
		if($type_id == null || $type_id == ''){
			$categories = Category::select('category.*')
				->join('category_type as ct', 'category.category_type_id', '=', 'ct.id')
				->where('category.status', '1')
				->where('category.deleted_at', null)
				->orderBy('category.id', 'ASC')
				->get()
				->toHierarchy();
		}else{
			$categories = Category::select('category.*')
				->join('category_type as ct', 'category.category_type_id', '=', 'ct.id')
				->where('category.status', '1')
				->where('category.deleted_at', null)
				->where('category.category_type_id', $type_id)
				->orderBy('category.id', 'ASC')
				->get()
				->toHierarchy();
		}

		return $categories;
	}

	public static function getCategoryTypes(){
		$cat_types = CategoryType::all();
		return $cat_types;
	}

	public static function getMostSellingProducts(){
		// $data = ProductCategory::whereNull('deleted_at')
		// 	->groupBy('product_category.category_id')
		// 	->orderBy('product_category.id','ASC')
		// 	->with(['product.image'])
		// 	->get();

		$data = Product::whereNull('deleted_at')->with(['product_category.category','image'])->limit(9)->get();

		return $data;
	}
}