<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */
 
namespace App\Modules\Category\CategoryLogic;
use App\Modules\Category\CategoryRepository\CategoryRepository;
use App\Classes\Functions;

class CategoryLogic {
	//variable declaration.
	public $comman       = null;
	public $categoryRepo = null;

	//assign dependancy injection.
	public function __construct(Functions $comman, CategoryRepository $categoryRepo){
		$this->comman       = $comman;
		$this->categoryRepo = $categoryRepo;
	}
}