<?php
/**
 * BANNER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */

namespace App\Modules\Banner\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class section_banner extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'section_banner';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $timestamp = true;

    protected $fillable = [
        'banner_id', 
        'banner_section_id', 
        'banner_page_id', 
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function page(){
        return $this->belongsTo('App\Modules\Banner\Models\banner_page', 'banner_page_id', 'id')
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC');
    }

    public function section(){
        return $this->belongsTo('App\Modules\Banner\Models\banner_section', 'banner_section_id', 'id')
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC');
    }

    public function image(){
        return $this->hasMany('App\Modules\Banner\Models\section_banner_image', 'section_banner_id', 'id')
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC');
    }
}
