<?php
/**
 * BANNER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */

namespace App\Modules\Banner\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class section_banner_image extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'section_banner_image';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $timestamp = true;

    protected $fillable = [
        'banner_section_id',
        'banner_id',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function url(){
        return $this->belongsTo('App\Modules\Banner\Models\banners', 'banner_id', 'id')
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC');
    }

    
}
