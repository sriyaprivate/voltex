@extends('layouts.master') @section('title','Home')
@section('links')
    <!-- slick.css -->
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">

    <!-- hover effects -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/hover-effect-ideas/css/set2.css')}}" />

    <!-- owl.carousel.js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />
@stop

@section('css')
@stop

@section('content')
    <!-- left main category -->
    <div class="main-slider-section">
        <div class="container-fluid">
            <div class="carousel slide" id="slider" data-ride="carousel">
              @if(count($banners) > 0 && isset($banners[1]) && count($banners[1]->image))
                @if(count($banners[1]->image) > 1)
                  <ol class="carousel-indicators">
                    @foreach($banners[1]->image as $key => $value)
                      <li data-target="#slider" data-slide-to="{{ $key }}" class="{{ $key == '0'?  'active':'' }}"></li>
                    @endforeach
                  </ol>
                @endif
                <div class="carousel-inner" role="listbox" style="max-height: 400px">
                  @foreach($banners[1]->image as $key => $banner)
                    <div class="carousel-item {{ $key == 0? 'active' : '' }}">
                      @if(isValid($banner->url) && isValid($banner->url->link))
                        <a href="{{ $banner->url->link }}" target="_blank">
                          <img src="{{ url('data/original/'.$banner->url->img_path) }}">
                        </a>
                      @else
                        <img src="{{ url('data/original/'.$banner->url->img_path) }}">
                      @endif
                      <!-- <div class="carousel-caption d-md-block">
                        <h1 class="text-uppercase font-weight-bold">save upto 50%</h1>
                        <p class="font-weight-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                      </div> -->
                    </div>
                  @endforeach
                  @if(count($banners[1]->image) > 1)
                    <a class="left carousel-control carousel-control-prev" href="#slider" role="button" data-slide="prev">‹</a>
                    <a class="right carousel-control carousel-control-next" href="#slider" role="button" data-slide="next">›</a>
                  @endif
                </div>
              @else
                <div class="carousel-item active">
                  <img src="{{ asset('assets/images/slider/default/default-600x375.png')}}">
                </div>
              @endif
            </div> <!-- /slider -->
        </div> <!-- /container -->
    </div> <!-- /header search panel -->   


    <!-- buy latest section -->
    <div class="buy-latest-section">
        <div class="container-fluid">
            <div class="card border-0 rounded-0">
                <div class="card-block">

                    <!-- heading -->
                    <div class="header">
                        <p class="h6 font-weight-bold text-uppercase">Hottest Deals</p>
                        <hr class="line-rose-left">
                        <hr class="line-normal">
                    </div> <!-- /heading -->

                    <div class="row">
              
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                @if(count($latestProduct) > 0)
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="item-sm">
                                        <div class="promotion">
                                            <span class="dis ng-binding">6%</span>
                                            <span class="save">OFF</span>
                                        </div>
                                                                                                    
                                        <img class="item-img mt-0" src="http://localhost/olm-store/data/original/images/product/item-020180221122737.jpg" alt="watch">
                                        <div class="info">
                                            <h6 class="text-underline-onHover">
                                                <a href="http://localhost/olm-store/bigstore/item/remax-rb-s10-best-wireless-41-sports-earphones-black/181">Remax RB-S10 Best Wireless 4.1 Sports Earphones  Black</a>
                                            </h6>
                                            <p class="h6 price mb-1"><small><del>Rs 4,690.00</del></small> Rs 4,399.00</p>
                                            <!-- BUY NOW & ADD TO CART -->
                                            <form action="http://localhost/olm-store/cart/add/181" method="get" id="form_181" class="product_form">
                                                <input type="hidden" name="prev_url" value="">
                                                <div class="cd-customization">
                                                    <div class="qty mr-1">
                                                        <input type="number" name="qty" min="1" max="5" class="form-control form-control-sm" value="1">
                                                    </div>
                                               
                                                    <div class="add-cart">
                                                        <button type="submit" name="action" value="buy" class="btn btn-sm btn-secondary btn-block"><span>Buy Now</span></button>
                                                    </div>
                                                    
                                                    <button type="submit" name="action" value="cart" class="btn btn-link cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                                                    
                                                    <a href="http://localhost/olm-store/cart/add/181?action=wishlist" class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                                </div>
                                            </form>
                                                      <!-- BUY NOW & ADD TO CART -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="item-sm">
                                        <div class="promotion">
                                            <span class="dis ng-binding">6%</span>
                                            <span class="save">OFF</span>
                                        </div>
                                                                                                    
                                        <img class="item-img mt-0" src="http://localhost/olm-store/data/original/images/product/item-020180221122737.jpg" alt="watch">
                                        <div class="info">
                                            <h6 class="text-underline-onHover">
                                                <a href="http://localhost/olm-store/bigstore/item/remax-rb-s10-best-wireless-41-sports-earphones-black/181">Remax RB-S10 Best Wireless 4.1 Sports Earphones  Black</a>
                                            </h6>
                                            <p class="h6 price mb-1"><small><del>Rs 4,690.00</del></small> Rs 4,399.00</p>
                                            <!-- BUY NOW & ADD TO CART -->
                                            <form action="http://localhost/olm-store/cart/add/181" method="get" id="form_181" class="product_form">
                                                <input type="hidden" name="prev_url" value="">
                                                <div class="cd-customization">
                                                    <div class="qty mr-1">
                                                        <input type="number" name="qty" min="1" max="5" class="form-control form-control-sm" value="1">
                                                    </div>
                                               
                                                    <div class="add-cart">
                                                        <button type="submit" name="action" value="buy" class="btn btn-sm btn-secondary btn-block"><span>Buy Now</span></button>
                                                    </div>
                                                    
                                                    <button type="submit" name="action" value="cart" class="btn btn-link cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                                                    
                                                    <a href="http://localhost/olm-store/cart/add/181?action=wishlist" class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                                </div>
                                            </form>
                                                      <!-- BUY NOW & ADD TO CART -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="item-sm">
                                        <div class="promotion">
                                            <span class="dis ng-binding">6%</span>
                                            <span class="save">OFF</span>
                                        </div>
                                                                                                    
                                        <img class="item-img mt-0" src="http://localhost/olm-store/data/original/images/product/item-020180221122737.jpg" alt="watch">
                                        <div class="info">
                                            <h6 class="text-underline-onHover">
                                                <a href="http://localhost/olm-store/bigstore/item/remax-rb-s10-best-wireless-41-sports-earphones-black/181">Remax RB-S10 Best Wireless 4.1 Sports Earphones  Black</a>
                                            </h6>
                                            <p class="h6 price mb-1"><small><del>Rs 4,690.00</del></small> Rs 4,399.00</p>
                                            <!-- BUY NOW & ADD TO CART -->
                                            <form action="http://localhost/olm-store/cart/add/181" method="get" id="form_181" class="product_form">
                                                <input type="hidden" name="prev_url" value="">
                                                <div class="cd-customization">
                                                    <div class="qty mr-1">
                                                        <input type="number" name="qty" min="1" max="5" class="form-control form-control-sm" value="1">
                                                    </div>
                                               
                                                    <div class="add-cart">
                                                        <button type="submit" name="action" value="buy" class="btn btn-sm btn-secondary btn-block"><span>Buy Now</span></button>
                                                    </div>
                                                    
                                                    <button type="submit" name="action" value="cart" class="btn btn-link cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                                                    
                                                    <a href="http://localhost/olm-store/cart/add/181?action=wishlist" class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                                </div>
                                            </form>
                                                      <!-- BUY NOW & ADD TO CART -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="item-sm">
                                        <div class="promotion">
                                            <span class="dis ng-binding">6%</span>
                                            <span class="save">OFF</span>
                                        </div>
                                                                                                    
                                        <img class="item-img mt-0" src="http://localhost/olm-store/data/original/images/product/item-020180221122737.jpg" alt="watch">
                                        <div class="info">
                                            <h6 class="text-underline-onHover">
                                                <a href="http://localhost/olm-store/bigstore/item/remax-rb-s10-best-wireless-41-sports-earphones-black/181">Remax RB-S10 Best Wireless 4.1 Sports Earphones  Black</a>
                                            </h6>
                                            <p class="h6 price mb-1"><small><del>Rs 4,690.00</del></small> Rs 4,399.00</p>
                                            <!-- BUY NOW & ADD TO CART -->
                                            <form action="http://localhost/olm-store/cart/add/181" method="get" id="form_181" class="product_form">
                                                <input type="hidden" name="prev_url" value="">
                                                <div class="cd-customization">
                                                    <div class="qty mr-1">
                                                        <input type="number" name="qty" min="1" max="5" class="form-control form-control-sm" value="1">
                                                    </div>
                                               
                                                    <div class="add-cart">
                                                        <button type="submit" name="action" value="buy" class="btn btn-sm btn-secondary btn-block"><span>Buy Now</span></button>
                                                    </div>
                                                    
                                                    <button type="submit" name="action" value="cart" class="btn btn-link cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                                                    
                                                    <a href="http://localhost/olm-store/cart/add/181?action=wishlist" class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                                </div>
                                            </form>
                                                      <!-- BUY NOW & ADD TO CART -->
                                        </div>
                                    </div>
                                </div>
                                @else
                                
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="text-center mb-4 text-secondary">
                                        <div class="h2">Buy Latest products not found.</div>
                                    </div>    
                                </div>

                                @endif
                            </div>
                        </div>
              
                    </div>
                </div>
            </div>
        </div> <!-- /container -->
    </div> <!-- /buy latest section -->


    <!-- banner section -->
    <section class="mt-4">
        <div class="container-fluid">
            @if(count($banners) > 0 && isset($banners[4]) && count($banners[4]->image))
                @if($banners[4]->image[0]->url->link !== "")
                    <a href="{{ $banners[4]->image[0]->url->link }}" target="_blank">
                        <img class="site-banner mb-4" src="{{ url('data/original/'.$banners[4]->image[0]->url->img_path) }}">
                    </a>
                @else
                    <img class="site-banner mb-4" src="{{ url('data/original/'.$banners[4]->image[0]->url->img_path) }}">
                @endif
            @else
                <img class="site-banner mb-4" src="{{ asset('assets/images/banner/default/default-1170-185.png') }}">
            @endif
        </div> <!-- /container -->
    </section> <!-- /banner section -->  
    
@stop

@section('js')
    <!-- slick.js -->
    <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>
    <!-- owl.carousel.js -->
    <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){

        });
    </script>
    @include('cart::includes.cart-js')
@stop