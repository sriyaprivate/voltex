@extends('layouts.master') @section('title','Home')
@section('links')
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('assets/css/front-slider.css')}}">
    <style type="text/css">
        .textsss {
            z-index: 1;
            transition: all 0s ease 0s;
            min-height: 0px;
            min-width: 0px;
            line-height: 76px;
            border-width: 0px;
            margin: 0px;
            padding: 0px;
            letter-spacing: 0px;
            font-size: 50px;
            left: 793.5px;
            top: 130px;
            visibility: visible;
            opacity: 1;
            transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.0025, 0, 0, 0, 1);
        }

        .slider > li > div {
            text-align: center;
        }

        .slider > li > div > figure {
            display: inline-block;
            width: 32.3333333%;
        }


    </style>
@stop

@section('content')
    
    <div class="slideshow hidden-sm hidden-xs">
             
        <input id="button-1" type="radio" name="radio-set" class="selector-1" checked="checked" />
        <label for="button-1" class="button-label-1"></label>
     
        <input id="button-2" type="radio" name="radio-set" class="selector-2" />
        <label for="button-2" class="button-label-2"></label>
         
        <input id="button-3" type="radio" name="radio-set" class="selector-3" />
        <label for="button-3" class="button-label-3"></label>
         
        <input id="button-4" type="radio" name="radio-set" class="selector-4" />
        <label for="button-4" class="button-label-4"></label>

        <label for="button-1" class="arrow a1"></label>
        <label for="button-2" class="arrow a2"></label>
        <label for="button-3" class="arrow a3"></label>
        <label for="button-4" class="arrow a4"></label>
 
        <div class="content">
            <div class="parallax-bg"></div>
            <ul class="slider">
                <li style="background-image: url('{{asset('assets/images/slider/image1.jpg') }}');min-height: 500px">
                    
                </li>
                <li style="background-image: url('{{asset('assets/images/slider/image2.jpg') }}');min-height: 500px">
                    
                </li>
                <li style="background-image: url('{{asset('assets/images/slider/image3.jpg') }}');min-height: 500px">
                    
                </li>
                <li style="background-image: url('{{asset('assets/images/slider/image4.jpg') }}');min-height: 500px">
                    
                </li>
            </ul>
        </div><!-- content -->
 
    </div><!-- slideshow -->

    <div class="homepage-products-wrap">
        <div id="sli_homeau_new">
            <div class="homepage-products" style="margin-bottom: 5% !important">
                <div class="title">Great Accessories from <strong style="color: crimson">Orange Electric</strong></div>

                @if(isset($m_s_pro) && count($m_s_pro)>0)
                    @foreach($m_s_pro as $value)
                        <div class="row">
                        @foreach($value as $product)
                            <div class="col-md-4">
                                <div class="imagebox" style="text-align: -webkit-center;margin-bottom: -35px;margin-top: -20px">
                                    <a href="{{ url('store/view-item')}}?type_id={{$product['product_category']['category']->category_type_id}}&cat_id={{$product['product_category']->category_id}}&item_id={{$product->id}}">
                                        <img src="{{ url('data/product_thumb/images/product/') }}/{{$product['image']['image']}}">
                                    </a>
                                </div>
                                <div class="product-name">
                                    <a href="">{{$product->display_name}}</a>
                                </div>
                                <div class="prod-code-price">
                                    <span class="code">{{$product->code}}</span>
                                </div><!--//prod-code-price--> 
                            </div>    
                        @endforeach
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
@stop

@section('js')
    
@stop

@section('scripts')
    <script src="{{ asset('assets/js/front-slider.js') }}"></script>
    <script>
        $(document).ready(function(){

        });
    </script>
    @include('cart::includes.cart-js')
@stop