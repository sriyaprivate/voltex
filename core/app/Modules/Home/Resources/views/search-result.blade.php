@extends('layouts.master') @section('title','search result')

@section('links')
    <!-- slick.css -->
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">

    <!-- hover effects -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/hover-effect-ideas/css/set2.css')}}" />

    <!-- owl.carousel.js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/mtreeJs/mtree.css')}}" />

    <!-- ion.rangeSlider.skinFlat.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.skinFlat.css')}}" />

    <!-- jquery confirm alert -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />

    <!-- toastr notification -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop

@section('css')
@stop

@section('content')
    <!-- breadcrumb -->
    <div class="custome-breadcrumb">
        <div class="container-fluid">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item active">Search</li>
            </ol>

        </div> <!-- /.container -->
    </div> <!-- /.breadcrumb -->

  <!-- sign in/up panel -->
  <div class="market big-store pb-4">
    <div class="container-fluid">
      <!-- wrapper -->
      <!-- <div class="inside-wrapper"> -->
    
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pr-4 sidebar">
                
                <!-- sidebar navigation -->
                <div class="side-menu mb-4">
                    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>
                    <!-- mtree menu -->

                    <ul class="mtree transit">
                        <li {{ Request::route('category_id') == null? 'class=mtree-active':'' }}>
                            <a href="{{ route('bigstore.index') }}">All</a>
                        </li>
                        @foreach($categories as $node)
                            {!! renderNode($node) !!}
                        @endforeach
                    </ul><!-- /.mtree menu -->
                </div> <!-- /.sidebar navigation -->

                <!-- /.sidebar-filter -->
                <form name="side-filter-form" class="form-horizontal" method="GET" accept-charset="UTF-8">
                    <div class="sidebar-module-container">
                        <h3 class="section-title mt-5">shop by</h3>
                        <button type="submit" class="btn btn-secoundary btn-sm pull-right mt--5" id="btn-filter"><span class="fa fa-filter"></span> Filter</button>
                        <!-- sidebar-widget -->
                        <div class="sidebar-widget">
                            <div class="widget-header mt-4">
                                <h4 class="widget-title text-uppercase">Price</h4>
                            </div>
                            <div class="sidebar-widget-body mt-4">
                                <input type="text" id="price_range" name="price_range" value="" />
                            </div>
                        </div> <!-- /.sidebar-widget -->

                        <!-- sidebar-widget -->
                        <div class="sidebar-widget">
                        @if(count($filters) > 0)
                            @foreach($filters as $key => $data)
                            <div class="widget-header mt-4">
                                <h4 class="widget-title text-uppercase">{{ $data->filter->name?:'-' }}</h4>
                            </div>
                            @if(count($data->filter_values) > 0)
                            <div class="sidebar-widget-body mt-3 filter-block">
                                <input type="hidden" class="filter_value" name="{{ strtolower(str_slug($data->filter->name, '_'))?:'-' }}" value="">
                                @foreach($data->filter_values as $value)
                                <div class="custom-controls-stacked">
                                    <label class="custom-control custom-checkbox">
                                    <?php $isTrue = false; ?>
                                    @foreach(explode(',', Request::get(strtolower(str_slug($data->filter->name, '_')))) as $val)
                                        @if(strtolower($val) == strtolower($value->value) )
                                            <?php $isTrue = true; ?>
                                        @endif
                                    @endforeach
                                        <input type="checkbox" class="custom-control-input filters" value="{{ strtolower($value->value)?:'-' }}" {{ ($isTrue)? 'checked' : '' }}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">{{ ucfirst($value->value)?:'-' }}</span>
                                    </label>
                                </div>
                                @endforeach
                            </div>
                            @endif
                            @endforeach
                        @endif
                        </div> <!-- /.sidebar-widget -->
                    </div> <!-- /.sidebar-filter -->
                    <div id="input-hidden"></div>
                    <div id="input-hidden-block"></div>              
                    <div id="input-hidden-block-q"></div>              
                </form>
                <!-- current view-type | gird or list  -->
                <input type="hidden" name="view" value="{{ Request::get('view')?: 'grid' }}"/>
            </div> <!-- /.col-lg-12 col-md-12 col-sm-12 -->

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pl-0 right position-relative">
            <!-- banner top -->
                <div name="panel-refresh"></div>
                <!-- filters-container -->
                <div class="clearfix filters-container pt-4 mb-5 pb-4" style="margin-top: -24px;">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4">
                            <div class="btn-group">
                                <a id="list" title="item list view mode" class="btn btn-light btn-sm view-type{{ isValid(Request::get('view')) && Request::get('view') == 'list'? ' text-danger' :'' }}" data-view-type="list"><i class="fa fa-th-large" aria-hidden="true"></i> List</a>
                                <a id="grid" title="item grid view mode" class="btn btn-light btn-sm view-type{{ isValid(Request::get('view')) && Request::get('view') == 'grid'? ' text-danger' :'' }}" data-view-type="grid"><i class="fa fa-th" aria-hidden="true"></i> Grid</a>
                            </div>
                        </div>                        
                        <div class="col-lg-{{ ($products->total() > $products->perPage())? '6' : '10' }}  col-sm-8">
                            <form class="form-inline pull-right">
                                <label class="mr-2" for="inlineFormCustomSelect">Sort by</label>
                                <select name="sort" class="custom-select mb-2 mr-3 mb-sm-0" id="inlineFormCustomSelect">
                                    <option value="{{ SORT_OPTION[5] }}" {{ Request::get('sort') == SORT_OPTION[5]? 'selected':''}}>Category - Ascending</option>
                                    <option value="{{ SORT_OPTION[6] }}" {{ Request::get('sort') == SORT_OPTION[6]? 'selected':''}}>Category - Descending</option>
                                    <option value="{{ SORT_OPTION[1] }}" {{ Request::get('sort') == SORT_OPTION[1]? 'selected':''}}>Date - Latest on top</option>
                                    <option value="{{ SORT_OPTION[2] }}" {{ Request::get('sort') == SORT_OPTION[2]? 'selected':''}}>Date - Oldest on top</option>
                                    <option value="{{ SORT_OPTION[3] }}" {{ Request::get('sort') == SORT_OPTION[3]? 'selected':''}}>Price - High to law</option>
                                    <option value="{{ SORT_OPTION[4] }}" {{ Request::get('sort') == SORT_OPTION[4]? 'selected':''}}>Price - Low to high</option>
                                </select>
                                <label class="mr-2" for="inlineFormCustomSelect">Show</label>
                                <select name="show" class="custom-select mb-2 mb-sm-0" id="inlineFormCustomSelect">
                                    @if(!empty(Request::get('show')))
                                        <option value="{{ SHOW_OPTION[1] }}" {{ Request::get('show') == SHOW_OPTION[1]? 'selected':''}}>{{ SHOW_OPTION[1] }}</option>
                                        <option value="{{ SHOW_OPTION[2] }}" {{ Request::get('show') == SHOW_OPTION[2]? 'selected':''}}>{{ SHOW_OPTION[2] }}</option>
                                        <option value="{{ SHOW_OPTION[3] }}" {{ Request::get('show') == SHOW_OPTION[3]? 'selected':''}}>{{ SHOW_OPTION[3] }}</option>
                                        <option value="{{ SHOW_OPTION[4] }}" {{ Request::get('show') == SHOW_OPTION[4]? 'selected':''}}>{{ SHOW_OPTION[4] }}</option>
                                    @else
                                        <option value="{{ SHOW_OPTION[1] }}">{{ SHOW_OPTION[1] }}</option>
                                        <option value="{{ SHOW_OPTION[2] }}" selected>{{ SHOW_OPTION[2] }}</option>
                                        <option value="{{ SHOW_OPTION[3] }}">{{ SHOW_OPTION[3] }}</option>
                                        <option value="{{ SHOW_OPTION[4] }}">{{ SHOW_OPTION[4] }}</option>
                                    @endif
                                </select>
                            </form>
                        </div>
                        @if($products->total() > $products->perPage())
                            <div class="col-lg-4 col-sm-12">
                                <nav aria-label="Page navigation example">
                                    @include('includes.pagination', ['paginator' => $products])
                                </nav>
                            </div>
                        @endif
                    </div>
                </div> <!-- /.filters-container -->
                <!-- search-result -->
                <div class="search-result">We found <strong>{{ isValid($products->total())?:'0' }}</strong> results for <strong>{{ isValid(Request::get('q'))?:'-' }}</strong></div>

                <!-- get max price -->
                <?php $maxPrice = 0;?>
                
                <!-- grid view -->
                @if(Request::get('view') == null || Request::get('view') == 'grid')
                <div id="products" class="row grid-view">
                    @if(count($products) > 0)
                        @foreach($products as $product)
                        <?php 
                            if($maxPrice < $product->price){
                                $maxPrice = $product->price;
                            }
                        ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">                
                            <div class="card text-center mb-4 clearfix">
                                @if(!empty($product->discount_rate))
                                <div class="promotion">
                                    <span class="dis ng-binding">{{ number_format($product->discount_rate?:'0') }}%</span>
                                    <span class="save">OFF</span>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-12">
                                    @if(isset($product->images) && count($product->images) > 0)
                                        @if(count($product->images) == 1)
                                            @if(isset($product->images[0]->image))
                                                <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.$product->images[0]->image) }}" alt="{{ $product->name?:'-' }}">
                                            @else
                                                <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="{{ $product->name?:'-' }}">
                                            @endif  
                                        @else
                                        <div class="cart-item-slider">
                                        @foreach($product->images as $image)
                                            <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.$image->image) }}" alt="{{ $product->name?:'-' }}">
                                        @endforeach
                                        </div>
                                        @endif
                                    @else
                                        <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="{{ $product->name?:'-' }}">                      
                                    @endif
                                    </div>
                                    
                                    <div class="col-12">
                                        <div class="details arrow_box_top top">
                                            <h6 class="mb-3 text-underline-onHover product-name"><a href="{{ route(isValid($product->status) && $product->status == DEAL_PROMOTION? 'saving-center.item.view' : 'bigstore.item.view', ['item_name' => str_slug($product->display_name?: $product->name, '-'), 'item_id' => $product->id]) }}">{{ isValid($product->display_name)?: 'Unknown' }}</a></h6>
                                            <p class="mb-2 price">
                                            @if(!empty($product->discounted_amount))
                                                <small><del>{{ CURRENCY_SIGN }} {{ number_format($product->price?: '-', 2) }}</del></small>
                                            @endif
                                            <br>
                                            {{ CURRENCY_SIGN }} {{ number_format($product->discounted_amount?: $product->price, 2) }}
                                            </p>
                                            @if(!empty($product->availableQty) && $product->availableQty > 0)
                                                <form action="{{ route('cart.add', ['product_id' => isValid($product->id)]) }}" method="get" id="form_{{ isValid($product->id) }}"  class="product_form">
                                                    <!-- <input type="hidden" name="prev_url" value="{{ Request::getQueryString() }}"> -->
                                                    <div class="cd-customization">
                                                        <div class="qty">
                                                            <input type="number" name="qty" min="1" max="{{ $product->availableQty }}" class="form-control form-control-sm" value="1">
                                                        </div>
                                                        <div class="add-cart">
                                                            <button type="submit" name="action" title="Click to buy item" value="buy" class="btn btn-sm btn-secondary btn-block"><span>Buy Now</span></button>
                                                        </div>
                                                        <button type="submit" name="action" value="cart" title="Add item to shopping cart" class="btn btn-link cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                                                        @if(count($loggedInUser) != 0 && isValid($product->hasInWishlist))
                                                            <button class="btn text-secondary btn-link wishlist" title="Item Already In Wishlist" disabled><i class="fa fa-heart" aria-hidden="true"></i></button>
                                                        @else
                                                            <a href="{{ url('cart/add') }}/{{ isValid($product->id) }}?action=wishlist"  title="click to add wishlist." class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                                        @endif
                                                    </div>
                                                </form>
                                            @else
                                                <div class="text-uppercase message-box">
                                                    Out of stock
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                
                        <div class="text-center mb-4 text-secondary">
                            <div class="h5">Sorry!, Your favorite stores will be available soon.</div>
                        </div>
                    </div>
                    @endif
                </div> <!-- /.grid view -->
                <!-- list view -->
                @elseif(Request::get('view') !== null && Request::get('view') == 'list')
                <div id="products" class="row list-view">
                    @if(count($products) > 0)
                        @foreach($products as $product)
                            @if(count($product) > 0)
                                <?php 
                                    if($maxPrice < $product->price){
                                        $maxPrice = $product->price;
                                    }
                                ?>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">                
                                    <div class="card mb-4 clearfix">
                                    @if(!empty($product->discount_rate))
                                        <div class="promotion">
                                            <span class="dis ng-binding">{{ number_format($product->discount_rate)?:'0' }}%</span>
                                            <span class="save">OFF</span>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-5 pr-0">
                                            @if(isset($product->images) && count($product->images) > 0)
                                                @if(count($product->images) == 1)
                                                    @if(isset($product->images[0]->image))
                                                        <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.$product->images[0]->image) }}" alt="{{ $product->name?:'-' }}">
                                                    @else
                                                        <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="{{ $product->name?:'-' }}">
                                                    @endif  
                                                @else
                                                <div class="cart-item-slider">
                                                    @foreach($product->images as $image)
                                                      <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.$image->image) }}" alt="{{ $product->name?:'-' }}">
                                                    @endforeach
                                                </div>
                                                @endif
                                            @else
                                                <img class="img-fluid" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="{{ $product->name?:'-' }}">                      
                                            @endif
                                        </div>
                              
                                        <div class="col-7 pl-0">
                                            <div class="details top">
                                                <h6 class="mb-3 text-underline-onHover product-name"><a href="{{ route(isValid($product->status) && $product->status == DEAL_PROMOTION? 'saving-center.item.view' : 'bigstore.item.view', ['item_name' => str_slug($product->display_name?: $product->name, '-'), 'item_id' => $product->id]) }}">{{ isValid($product->display_name)?: 'Unknown name' }}</a></h6>
                                                <p class="mb-2 price">
                                                    @if(!empty($product->discounted_amount))
                                                        <small><del>{{ CURRENCY_SIGN }} {{ number_format($product->price?: '-', 2) }}</del></small>
                                                    @endif
                                                    <br>
                                                    {{ CURRENCY_SIGN }} {{ number_format($product->discounted_amount?: $product->price, 2) }}
                                                </p>

                                                @if(!empty($product->availableQty) && $product->availableQty > 0)
                                    
                                                <form action="{{ route('cart.add', ['product_id' => isValid($product->id)]) }}" method="get" id="form_{{ isValid($product->id) }}" class="product_form">
                                                    <!-- <input type="hidden" name="prev_url" value="{{ Request::getQueryString() }}"> -->
                                                    <div class="cd-customization">
                                                        <div class="qty">
                                                            <input type="number" name="qty" min="1" max="{{ $product->availableQty }}" class="form-control form-control-sm" value="1">
                                                        </div>
                                                        <div class="add-cart">
                                                            <button type="submit" name="action" value="buy" class="btn btn-sm btn-secondary btn-block"><span>Buy Now</span></button>
                                                        </div>
                                                        <button type="submit" name="action" value="cart" class="btn btn-link cart"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                                                        @if(isValid($product->hasInWishlist))
                                                            <button class="btn text-secondary btn-link wishlist" title="Item Already In Wishlist" disabled><i class="fa fa-heart" aria-hidden="true"></i></button>
                                                        @else
                                                            <a href="{{ url('cart/add') }}/{{ isValid($product->id) }}?action=wishlist"  title="click to add wishlist." class="btn btn-link wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                                        @endif
                                                    </div>
                                                </form>
                                                @else
                                                <div class="text-uppercase message-box">
                                                        Out of stock
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">  
                                <h3 class="text-center">Data not found!.</h3>
                            </div>
                            @endif
                        @endforeach
                    @else
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                
                        <div class="text-center mb-4 text-secondary">
                            <div class="h1 text-uppercase"><i class="fa fa-meh-o" aria-hidden="true"></i></div>
                                <div class="h5">Sorry!, Your favorite stores will be available soon.</div>
                            </div>
                        </div>
                    @endif
                        </div> <!-- /.list/grid view -->
                    @endif
                    <!-- /.list view -->
            
                    <!-- list/grid view bottom pagination -->
                    <div class="search-result-container mt-4 pt-4">
                        @if($products->total() > $products->perPage())
                        <nav aria-label="Page navigation example">
                            @include('includes.pagination', ['paginator' => $products])
                        </nav>
                        @endif
                    </div> <!-- /.list/grid view bottom pagination -->

                </div> <!-- /.col-lg-12 col-md-12 col-sm-12 -->
            </div> <!-- /.row -->

            <!-- </div> --> <!-- /.wrapper -->

        </div> <!-- /.container -->
    </div> <!-- /.sign in/up panel -->
@stop

@section('js')
    <!-- slick.js -->
    <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>

    <!-- owl.carousel.js -->
    <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <script src="{{asset('assets/libraries/mtreeJs/jquery.velocity.min.js')}}"></script>
    <script src="{{asset('assets/libraries/mtreeJs/mtree.js')}}"></script>

    <!-- ion.rangeSlider-2.2.0 -->
    <script src="{{asset('assets/libraries/ion.rangeSlider-2.2.0/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>

    <!-- jquery confirm alert -->
    <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>

    <!-- toastr notification -->
    <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
@include('cart::includes.cart-js')
<script>
    $(document).ready(function() {
      // $("#price_range").ionRangeSlider({
      //   type: "double",
      //   grid: true,
      //   min: 0,
      //   max: {{ isset($maxPrices['maxPrice'])? $maxPrices['maxPrice'] : '1000' }},
      //   from: {{ Request::get('price_range')? explode('-', Request::get('price_range'))[0] : 0 }},
      //   to: {{ Request::get('price_range')? explode('-', Request::get('price_range'))[1] : (isset($maxPrices['maxPrice'])? $maxPrices['maxPrice'] : '1000') / 2 }},
      //   prefix: "Rs ",
      //   hide_min_max: true,
      //   input_values_separator: "-"
      // });        
    });

    //set multiple filters separete by commar
    $('.filters').change(function(){
      var tempValue='';
      tempValue = $(this).parents('.filter-block').find('input:checkbox').map(function(n){
          if(this.checked){
            return  this.value;
          };
        }).get().join(',');
        
      $(this).parents('.filter-block').find('.filter_value').attr('value', tempValue);
    })

    //set hidden values of select boxes
    function load_select_filter(){
      var sort = $('select[name=sort] option:selected').val();
      var show = $('select[name=show] option:selected').val();
      $('#input-hidden').html('<input type="hidden" name="sort" value="'+sort+'"><input type="hidden" name="show" value="'+show+'">');
    }

    //set hidden value of view type
    function load_view_type_filter(viewType){
      $('#input-hidden-block').html('<input type="hidden" name="view" value="'+viewType+'">');
    }

    //current view type
    var viewType = $("input[name=view]").val();

    //set all hidden values and submit form
    function trigger_load(viewType){
      $('.filters').trigger('change');
      load_select_filter();
      load_view_type_filter(viewType);
      $('#input-hidden-block-q').html('<input type="hidden" name="q" value="{{ Request::get('q') }}">');
      var onlyUrl = window.location.href.replace(window.location.search,'')
      $('form[name="side-filter-form"]').submit();
    }

    //filter button
    $('#btn-filter').click(function(){
      trigger_load(viewType);
    });

    //view type
    $('.view-type').click(function(){
      viewType = $(this).data('view-type');
      trigger_load(viewType);
    });

    //select boxes
    $('select').on('change', function(){
      trigger_load(viewType);

    });

    //qty validation
    // var lastCurrentQty = null; 
    // $('input[type=number]').bind('change keyup', function(e){
    //   //validate qty
    //   var pattern = /^([0-9]*[1-9][0-9]*)$/;
    //   var maxQty  = $(this).prop('max');

    //   if($(this).val().length > 0 && pattern.test($(this).val()) && parseInt($(this).val()) <= parseInt(maxQty)){
    //     $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', false);
    //     lastCurrentQty = $(this).val();
    //   }else{
    //     $(this).parents('.cd-customization').find("button[value != 'wishlist']").prop('disabled', true);
        
    //     //show notification
    //     replaceQty = 1;

    //     if(lastCurrentQty !== null){ 
    //       replaceQty = lastCurrentQty;
    //     }else{ 
    //       replaceQty = maxQty;
    //     }

    //     $(this).val(replaceQty);        
    //     notification("toast-bottom-right", "warning", "Limited stock available!.", "5000");
    //   }
    // });

    $('.product_form').on('submit', function(e){
      panelRefresh('div[name=panel-refresh]');
    });
  </script>
<script>
$(document).ready(function(){
    $('.cart-item-slider').slick({
        dots: true,
        arrows: false,
        setPosition: true
    });

    $("#price_range").ionRangeSlider({
        type: "double",
        grid: true,
        min: 0,
        max: {{ Request::get('price_range')? explode('-', Request::get('price_range'))[1] : (isset($maxPrice) && !empty($maxPrice)? $maxPrice : '1000') }},
        from: {{ Request::get('price_range')? explode('-', Request::get('price_range'))[0] : 0 }},
        to: {{ Request::get('price_range')? explode('-', Request::get('price_range'))[1] : (isset($maxPrice) && !empty($maxPrice)? $maxPrice : '1000') }},
        prefix: "Rs ",
        hide_min_max: true,
        input_values_separator: "-"
    });
});
</script>
@stop