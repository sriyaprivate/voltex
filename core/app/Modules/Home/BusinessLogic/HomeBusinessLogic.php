<?php 
namespace App\Modules\Home\BusinessLogic;

use App\Modules\Banner\BannerRepository\BannerRepository;
use App\Modules\Category\CategoryRepository\CategoryRepository;

class HomeBusinessLogic {
	//global variable declaration
	private $bannerRepo;
	private $categoryRepo;

	//dependancy injection
	public function __construct(BannerRepository $bannerRepo, CategoryRepository $categoryRepo){
		$this->bannerRepo   = $bannerRepo;
		$this->categoryRepo = $categoryRepo;
	}	

	/**
     * get all banner according to the page.
     * @param page name
     * @return banner object
     */
	public function getBanners($page = null){
		$banners     = $this->bannerRepo->getBannersByPage($page);
		$bannerArray = [];

		if(count($banners) > 0){
			foreach($banners->banner_section as $key => $banner){
				$bannerArray[$banner->section->id] = $banner;
			}
		}

		return $bannerArray;
	}

	/**
     * get main categories
     * @param page name
     * @return category object
     */
	public function getMainCategories($page, $type){
		$categories = $this->categoryRepo->getMainCategoriesByPage($page, $type);
		return $categories;
	}



}
