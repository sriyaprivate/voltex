<?php

namespace App\Modules\Home\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Modules\Home\BusinessLogic\HomeBusinessLogic;
use App\Modules\SavingsCenter\Repositories\SavingsCenterRepository;
use App\Modules\Home\Repositories\HomeRepository;
use App\Modules\Product\Models\Product;
use App\Modules\Category\CategoryRepository\CategoryRepository;
use App\Modules\Product\Repositories\ProductRepository;
use App\Classes\Functions;
use File;

class HomeController extends Controller
{
    //variable declaration
    protected $homeLogic        = null;
    protected $savingCenterRepo = null;
    protected $homeRepo         = null;
    protected $categoryRepo     = null;
    protected $productRepo      = null;
    protected $common           = null;

    public function __construct(HomeBusinessLogic $homeLogic,SavingsCenterRepository $savingCenterRepo,HomeRepository $homeRepo, CategoryRepository $categoryRepo, ProductRepository $productRepo, Functions $common){
        $this->homeLogic        = $homeLogic;
        $this->savingCenterRepo = $savingCenterRepo;
        $this->homeRepo         = $homeRepo;
        $this->categoryRepo     = $categoryRepo;
        $this->productRepo      = $productRepo;
        $this->common           = $common;
    }

    /**
     * Home page.
     * @return view
     */
    public function index()
    {
        try {
            $mainCategories = $this->homeLogic->getMainCategories(BIG_STORE, 'parents'); //BIG_STORE
            //latest product
            $folderPath = storage_path('brands');

            $logoImages = File::files($folderPath);

            return view('home::index', compact('mainCategories','logoImages'));   
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('home::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('home::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('home::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * global ajax search
     * @param $request
     * @return array
     */
    public function ajaxSearch(Request $request)
    {   
        $keyword = $request->input('q');
        $limit   = $request->input('limit')?:'10';

        if(isset($keyword) && !empty($keyword)){
            $products     = Product::select('product.name', 'product.display_name', 'product.code', 'product.status', 'product.id')
                ->with(['images', 'price', 'pro_product.promo', 'pro_product' => function($promo){
                    $promo->whereNull('deleted_at')
                    ->orderBy('promotion_id', 'DESC');
                }])
                ->leftjoin('product_tag', 'product.id', '=', 'product_tag.product_id')
                ->leftjoin('tag', function($join){
                    $join->on('product_tag.tag_id', '=','tag.id')
                        ->on('product_tag.product_id', '=','product.id');
                })
                ->where(function($query) use($keyword){
                    $query->where('product.display_name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('product.name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('tag.name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('product.code', 'LIKE', '%'.$keyword.'%');
                })
                ->whereIn('product.status', PRODUCT_CHECK_STATUS)
                ->whereNull('product.deleted_at')
                ->groupBy('product.id')
                ->orderBy('product.name', 'ASC')
                ->orderBy('product.display_name', 'ASC')
                ->limit($limit)
                ->get();

            return $products;
        }else{
            return [];
        }
    }

    /**
     * global ajax search
     * @param $request
     * @return array
     */
    public function search(Request $request, $slug = null, $category_id = null)
    {   
        try{
            $category_id = (int) trim($category_id);
            /**
             * @param : page name, return type tree or parents
             * @return array
             */
            $categories = $this->categoryRepo->getMainCategoriesByPage(BIG_STORE, 'tree');
            /**
             * get product according to the category id and filters
             * @param category_id
             * @param request for filters
             * @return array
             */
            $show         = SHOW_OPTION[1]; //SHOW_OPTION[1] = 10
            $input_show   = (int) $request->input('show');		
            $show         = ($input_show !== 0)? $input_show : $show; 
            $products     = $this->productRepo->getProductByCategory($category_id, $request);
            $filters      = $this->categoryRepo->getFilterForCategory($category_id);
            
            return view('home::search-result', compact('categories', 'products', 'filters'));

        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }
}
