<?php

Route::group(['prefix' => '', 'middleware' => 'web',  'namespace' => 'App\\Modules\Home\Http\Controllers'], function()
{
    Route::get('/', [
    	'uses' => 'HomeController@index',
    	'as'   => 'home.index'		
    ]);

    // Route::get('/', function(){
    //     return "<img style='    display: block;
    //         margin-left: auto;
    //         margin-right: auto;
    //         width: 40%;
    //         margin-top: 10%;
    //         ' src='https://www.gannett-cdn.com/-mm-/438112d08852a5cf64fb668899b62a1c6abcfadb/c=0-104-5312-3105&r=x1683&c=3200x1680/local/-/media/2017/05/23/WIGroup/Appleton/636311326049773956-UC.jpg'/>";
    // });

    Route::get('/ajax/search', [
    	'uses' => 'HomeController@ajaxSearch',
    	'as'   => 'home.ajax.search'		
    ]);

    //for global search
    Route::get('/search', [
        'uses' => 'HomeController@search',
        'as'   => 'home.search'
    ]);
});
