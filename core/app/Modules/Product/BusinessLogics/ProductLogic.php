<?php namespace App\Modules\Product\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use DB;
use Exception;
use App\Models\Tag;
use App\Modules\Product\Models\Product;
use App\Models\ProductTag;
use App\Models\Warrenty;
use App\Models\ReturnPolicy;
use App\Modules\Product\Models\Image;
use App\Models\DeliveryType;
use App\Models\DeliveryCity;
use App\Models\Specification;
use App\Models\ProductCategory;

class ProductLogic{

	/** 
	 * This function is used to get all tags
	 * @param String $tag
	 * @return array
	 */

	public function searchTag($tag){
		return $tag = Tag::where('name','LIKE','%'.$tag.'%')->lists('name','id');
	}

	/** 
	 * This function is used to add product master data
	 * @param String $product_array
	 * @return $last_insert_id
	 */

	public function addProduct($product_array){
		DB::beginTransaction();
		$product = Product::create($product_array);
		if(!$product){
			DB::rollback();
			throw new Exception("Error occured while adding new product");
		}
		DB::commit();
		return $product->id;
	}
	/** 
	 * This function is used to add product category
	 * @param Array $product_category
	 * @return 
	 */
	public function addProductCategory($product_category){
		//DB::beginTransaction();
		$pro_cat = ProductCategory::insert($product_category);
		if(!$pro_cat){
			DB::rollback();
			throw new Exception("Error occured while adding product category");
		}
		DB::commit();
	}

	/** 
	 * This function is used to add tag
	 * @param String $tag_array Integer $product_id
	 * @return $last_insert_id
	 */

	public function addTag($product_id,$tag_array){
		DB::beginTransaction();
		foreach ($tag_array as $value) {
			$tag = Tag::create(['name' => $value]);
			if(!$tag){
				DB::rollback();
				throw new Exception("Error occured while adding tag");
			}
			DB::commit();
			$product_tag = ProductTag::create([
				'product_id' => $product_id,
				'tag_id' 	 => $tag->id]);
			if(!$product_tag){
				DB::rollback();
				throw new Exception("Error occured while adding product tag");
			}
			DB::commit();
		}
	}

	/** 
	 * This function is used to add warrenty
	 * @param String $warrenty_array
	 * @return
	 */

	public function addWarrenty($warrenty_array){
		DB::beginTransaction();
		$warrenty = Warrenty::create($warrenty_array);
		if(!$warrenty){
			DB::rollback();
			throw new Exception("Error occured while adding warrenty");
		}
		DB::commit();
		return $warrenty->id;
	}

	/** 
	 * This function is used to add return
	 * @param String $return_array
	 * @return
	 */

	public function addReturn($return_array){
		DB::beginTransaction();
		$return = ReturnPolicy::create($return_array);
		if(!$return){
			DB::rollback();
			throw new Exception("Error occured while adding return");
		}
		DB::commit();
		return $return->id;
	}

	/**
     * This function is used to add images in item
     * @param array $images_array Array of data that needed to add images
     * @return object Item object if success, exception otherwise
     */

    public function addImages($images_array){
    	DB::beginTransaction();
        $image = Image::insert($images_array);
        if(!$image){
            DB::rollback();
            throw new Exception("Error occured while adding images");
        }
        DB::commit();
       
    }

    /**
     * This function is used to add delivery types
     * @param array $type_array Array of data that needed to add delivery types
     * @return 
     */

    public function addDeliveryType($product_id,$type_array){
        $type = [];
        try{
        	DB::beginTransaction();
            foreach ($type_array as $value) {
				$delivery_type = DeliveryType::create([
					'product_id' 		=> $product_id,
					'delivery_type_id'	=> $value
				]);
				if(!$delivery_type){
					DB::rollback();
				}
				DB::commit();
				array_push($type,$delivery_type->id);
			}
			return $type;
        }catch(Exception $e){
        	 DB::commit();
        }
    }

    /**
     * This function is used to add product specification
     * @param array $specification_array Array of data that needed to add specification
     * @return 
     */

    public function addSpecification($product_id,$specification_array){
        $spec_arr = [];
        foreach ($specification_array as $spec => $value) {
        	array_push($spec_arr,[
        		'product_id' 	=> $product_id,
				'specification'	=> $spec,
				'value'			=> $value
			]);
		}
		DB::beginTransaction();
		$product_spec = Specification::insert($spec_arr);
		if(!$product_spec){
			DB::rollback();
			throw new Exception("Error occured while adding product specification");
		}
		DB::commit();
    }

    /**
     * This function is used to get product list
     * @param 
     * @return 
     */

    public function getProductList(){
    	return $product_list = Product::with(['product_category.category.type','product_images'])->paginate(20);
    }

    /** 
     * This function is used to get produt details
     * @param Integer $id
     * @return 
     */

    public function getProductDetails($id){
    	return $product_details = Product::with('product_category.category.type')->where('id',$id)->get();
    }

    /** 
	 * This function is used to edit product master data
	 * @param String $product_array
	 * @return $last_insert_id
	 */

	public function editProduct($product_id,$product_array){
		DB::beginTransaction();
		$product = Product::where('id',$product_id)->update($product_array);
		if(!$product){
			DB::rollback();
			throw new Exception("Error occured while updating product");
		}
		DB::commit();
		return $product_id;
	}

	/** 
	 * This function is used to edit product category
	 * @param Array $product_category
	 * @return 
	 */
	public function editProductCategory($product_id,$product_category){
		DB::beginTransaction();
		$product_category_remove = ProductCategory::where('product_id',$product_id);
		$product_category_remove->delete();
		if(!$product_category_remove){
			DB::rollback();
			throw new Exception("Error occured while deleting product category");
		}
		DB::commit();
		$pro_cat = ProductCategory::insert($product_category);
		if(!$pro_cat){
			DB::rollback();
			throw new Exception("Error occured while adding product category");
		}
		DB::commit();
	}

	/** 
	 * This function is used to edit tag
	 * @param String $tag_array Integer $product_id
	 * @return $last_insert_id
	 */

	public function editTag($product_id,$tag_array){
		DB::beginTransaction();
		$product_tag_remove = ProductTag::where('product_id',$product_id);
		$product_tag_remove->delete();
		if(!$product_tag_remove){
			DB::rollback();
			throw new Exception("Error occured while deleting product tag");
		}
		DB::commit();
		foreach ($tag_array as $value) {
			$tag = Tag::create(['name' => $value]);
			if(!$tag){
				DB::rollback();
				throw new Exception("Error occured while adding tag");
			}
			DB::commit();
			$product_tag = ProductTag::create([
				'product_id' => $product_id,
				'tag_id' 	 => $tag->id]);
			if(!$product_tag){
				DB::rollback();
				throw new Exception("Error occured while adding product tag");
			}
			DB::commit();
		}
	}


	/** 
	 * This function is used to edit warrenty
	 * @param String $warrenty_array
	 * @return
	 */

	public function editWarrenty($product_id,$warrenty_array){
		DB::beginTransaction();
		$product_warrenty_remove = Warrenty::where('product_id',$product_id);
		$product_warrenty_remove->delete();
		if(!$product_warrenty_remove){
			DB::rollback();
			throw new Exception("Error occured while deleting product warrenty");
		}
		DB::commit();
		if(count($warrenty_array) > 0){
			$warrenty = Warrenty::create($warrenty_array);
			if(!$warrenty){
				DB::rollback();
				throw new Exception("Error occured while adding warrenty");
			}
			DB::commit();
		}
	}

	/** 
	 * This function is used to edit return
	 * @param String $return_array
	 * @return
	 */

	public function editReturn($product_id,$return_array){
		DB::beginTransaction();
		$product_return_remove = ReturnPolicy::where('product_id',$product_id);
		$product_return_remove->delete();
		if(!$product_return_remove){
			DB::rollback();
			throw new Exception("Error occured while deleting product return");
		}
		DB::commit();
		if(count($return_array) > 0){
			$return = ReturnPolicy::create($return_array);
			if(!$return){
				DB::rollback();
				throw new Exception("Error occured while adding return");
			}
			DB::commit();
		}
	}

	/**
     * This function is used to edit product specification
     * @param array $specification_array Array of data that needed to edit specification
     * @return 
     */

    public function editSpecification($product_id,$specification_array){
        $spec_arr = [];
        DB::beginTransaction();
        $product_specification_remove = Specification::where('product_id',$product_id);
		$product_specification_remove->delete();
		if(!$product_specification_remove){
			DB::rollback();
			throw new Exception("Error occured while deleting product specification");
		}
		DB::commit();
        if(count($specification_array) > 0){
	        foreach ($specification_array as $spec => $value) {
	        	array_push($spec_arr,[
	        		'product_id' 	=> $product_id,
					'specification'	=> $spec,
					'value'			=> $value
				]);
			}
			$product_spec = Specification::insert($spec_arr);
			if(!$product_spec){
				DB::rollback();
				throw new Exception("Error occured while adding product specification");
			}
			DB::commit();
		}
    }

    /**
     * This function is used to get only produt details
     * @param Integer $product_id
     * @return product_list object
     */

    public function get_product($product_id){
    	try{
    		$product = Product::where('id',$product_id)->where('status',1)->get();
    		if(count($product) > 0){
    			return $product;
    		}else{
    			return [];
    		}
    	}catch(Exception $e){
    		throw new Exception($e->getMessage());
    	}
	}
}
