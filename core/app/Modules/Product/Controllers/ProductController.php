<?php namespace App\Modules\Product\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Intervention\Image\Facades\Image;
use Exception;

use App\Modules\supplier\Controllers\SupplierController as Supplier;
use App\Modules\Product\BusinessLogics\ProductLogic;
use App\Classes\Functions;
use App\Modules\Category\CategoryRepository\CategoryRepository;



class ProductController extends Controller {


	protected $supplier;
	protected $product;
	protected $category;

    public function __construct(Supplier $supplier,ProductLogic $product,Functions $function,CategoryRepository $category_repository){
        $this->supplier  = $supplier;
        $this->product   = $product;
        $this->funcation = $function;
        $this->category  = $category_repository;
    }


	/**
	 * Display a listing of the product.
	 *
	 * @return Response
	 */
	public function index()
	{
		$product_list = $this->product->getProductList();
		return view('Product::list')->with(['product_list' => $product_list]);
	}

	/**
	 * Show the form for creating a new product.
	 *
	 * @return Response
	 */
	public function create()
	{
		$all_supplier = $this->supplier->getSupplier();
		$big_store 	  = $this->category->getAllCategory(1, '', 'get');
		$bulk_market  = $this->category->getAllCategory(2, '', 'get');
		$savings 	  = $this->category->getAllCategory(3, '', 'get');
		return view("Product::add")->with([
			'all_supplier' 		  => $all_supplier,
			'supplier' 			  => '',
			'big_store_category'  => $big_store,
			'bulk_market_category'=> $bulk_market,
			'savings'			  => $savings
		]);
	}

	/**
	 * This function is used to all tag
	 *
	 * @return Response
	 */
	public function tag(Request $request)
	{
		$all_tag = $this->product->searchTag($request->get('term'));
		return Response::json($all_tag);
		
	}


	/**
	 * Store a newly created product in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{	
		//validate form request data
		$this->validate($request,[
			'item_name'				=> 'required|unique:product,name',
            'item_code' 			=> 'required|unique:product,code',
            'display_name'			=> 'unique:product,name|unique:product,display_name',
            'warrenty_period'		=> 'required_if:warranty_available,==,1|numeric',
            'weight'				=> 'numeric',
            'width'					=> 'numeric',
            'height'				=> 'numeric',
            'return'				=> 'required_if:return_available,==,1|numeric',
            'big_store_category'	=> 'required_without_all:bulk_market_category,savings',
            'bulk_market_category'  => 'required_without_all:big_store_category,savings',
            'savings' 				=> 'required_without_all:big_store_category,bulk_market_category'
        ]);

		try{
			//product master data add
			$last_insert_id = $this->product->addProduct([
				'supplier_id' 	=> $request->get('supplier'),
				'code'	   		=> $request->get('item_code'),
				'name'	   		=> $request->get('item_name'),
				'display_name'	=> $request->get('display_name'),
				'description'	=> $request->get('description'),
				'weight'		=> $request->get('weight'),
				'width'			=> $request->get('width'),
				'height'		=> $request->get('height'),
			]);
			//product category
			$product_category = [];
			if(!empty($request->get('big_store_category'))){
				array_push($product_category,[
					'product_id'  => $last_insert_id,
					'category_id' => $request->get('big_store_category')
				]);
			}

			if(!empty($request->get('bulk_market_category'))){
				array_push($product_category,[
					'product_id'  => $last_insert_id,
					'category_id' => $request->get('bulk_market_category')
				]);
			}

			if(!empty($request->get('savings'))){
				array_push($product_category,[
					'product_id'  => $last_insert_id,
					'category_id' => $request->get('savings')
				]);
			}

			$product_cat = $this->product->addProductCategory($product_category);
			//tag add
			$tag_arr = explode(',',$request->get('tag'));
			$tag = $this->product->addTag($last_insert_id,$tag_arr);
			//warrenty add
			if($request->get('warranty_available') > 0){
				$warenty = $this->product->addWarrenty([
					'product_id' 	  => $last_insert_id,
					'warrenty_period' => $request->get('warrenty_period'),
					'period_type'	  => $request->get('period_type')
				]);
			}
			//Return add
			if($request->get('return_available') > 0){
				$return = $this->product->addReturn([
					'product_id' 	  => $last_insert_id,
					'return_period'   => $request->get('return'),
					'period_type'	  => $request->get('return_period')
				]);
			}
			//product images
			//make product folder
			if(!is_dir(storage_path('product'))){
				mkdir(storage_path('product'), 0777);
			}
			try {
	            //check image is browse
	            if($request->hasFile('images')){
	                $files = $request->file('images'); //get file
	                $img_arr = array();
	                $i = 0;
	                foreach($files as $file){
	                    $extn = $file->getClientOriginalExtension(); //get extension
	                    $fileName = 'item-'.$i.date('YmdHis').'.'.$extn; //set file name
	                    $destinationPath = storage_path('product'); // destination
	                    $file->move($destinationPath, $fileName);//move to file
	                    //resize image
	                    // $img = Image::make($destinationPath . '/' . $fileName)->fit(400,300, function ($c) {
	                    //     $c->upsize();
	                    // },"top");
	                    // $img->save($destinationPath . '/' . $fileName);
	                    array_push($img_arr,['product_id' => $last_insert_id,'image' => $fileName]);
	                    $i++;
	                }
	                $images = $this->product->addImages($img_arr);
	            }
	        }catch(Exception $e){
	            return redirect('product/add')->with([
	                'error' 	   => true,
	                'error.title'  => 'Error..!',
	                'error.message'=> $e->getMessage()
	        	])->withInput();
	        }

	        //delivery
	        //$delivery_type = $this->product->addDeliveryType($last_insert_id,$request->get('delivery_type'));
	        
	        //product specification
	        $specification = [];
	        if(!empty($request->get('specification')) && !empty($request->get('value'))){
	        	foreach ($request->get('specification') as $key => $value) {
	        		$specification[$value] = $request->get('value')[$key];
	        	}
	        	$product_spec = $this->product->addSpecification($last_insert_id,$specification);
	        }
	        
	        return redirect( 'product/add' )->with([
	            'success' 		 => true,
	            'success.title'  => 'Success..!',
	            'success.message'=> 'Product added successfully'
	        ]);
	    }catch(Exception $e){
	    	return redirect('product/add')->with([
	                'error' 	   => true,
	                'error.title'  => 'Error..!',
	                'error.message'=> $e->getMessage()
	        ])->withInput(); 
	    }
        
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$all_supplier 		  = $this->supplier->getSupplier();
		$big_store_category   = $this->category->getAllCategory(1, '', 'get');
		$savings_cat 	  	  = $this->category->getAllCategory(2, '', 'get');
		$bulk_market_category = $this->category->getAllCategory(3, '', 'get');
		$product_detail		  = $this->product->getProductDetails($id);
		$tags				  = [];

		if(isset($product_detail[0]->product_tag))
		{
			foreach ($product_detail[0]->product_tag as $key => $value) {
				array_push($tags, $value->tag->name);
			}

			$tags_string = join(",",$tags);
		}
		$big_store   = '';
		$savings     = '';
		$bulk_market = '';
		if(isset($product_detail[0]->product_category))
		{
			foreach ($product_detail[0]->product_category as $key => $value) {
				if($value->category->type->id == '1'){
					$big_store = $value->category_id;
				}
				if($value->category->type->id == '2'){
					$savings = $value->category_id;
				}
				if($value->category->type->id == '3'){
					$bulk_market = $value->category_id;
				}
				
			}
		}

		return view('Product::edit')->with([
			'product_detail' 		=> $product_detail,
			'all_supplier' 			=> $all_supplier,
			'big_store_category' 	=> $big_store_category,
			'bulk_market_category'  => $bulk_market_category,
			'product_tag'			=> $tags_string,
			'savings_category'		=> $savings_cat,
			'big_store'				=> $big_store,
			'savings'               => $savings,
			'bulk_market'			=> $bulk_market
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		//validate form request data
		$this->validate($request,[
			'item_name'				=> 'required|unique:product,name,'.$id,
            'display_name'			=> 'unique:product,name|unique:product,display_name,'.$id,
            'warrenty_period'		=> 'required_if:warranty_available,==,1|numeric',
            'weight'				=> 'numeric',
            'width'					=> 'numeric',
            'height'				=> 'numeric',
            'return'				=> 'required_if:return_available,==,1|numeric',
            'big_store_category'	=> 'required_without_all:bulk_market_category,savings',
            'bulk_market_category'  => 'required_without_all:big_store_category,savings',
            'savings' 				=> 'required_without_all:big_store_category,bulk_market_category'
        ]);

        try{
        	//product master data edit

			$update_product = $this->product->editProduct($id,[
				'supplier_id' 	=> $request->get('supplier'),
				'name'	   		=> $request->get('item_name'),
				'display_name'	=> $request->get('display_name'),
				'description'	=> $request->get('description'),
				'weight'		=> $request->get('weight'),
				'width'			=> $request->get('width'),
				'height'		=> $request->get('height'),
			]);

			//product category edit
			$product_category = [];
			if(!empty($request->get('big_store_category'))){
				array_push($product_category,[
					'product_id'  => $id,
					'category_id' => $request->get('big_store_category')
				]);
			}

			if(!empty($request->get('bulk_market_category'))){
				array_push($product_category,[
					'product_id'  => $id,
					'category_id' => $request->get('bulk_market_category')
				]);
			}

			if(!empty($request->get('savings'))){
				array_push($product_category,[
					'product_id'  => $id,
					'category_id' => $request->get('savings')
				]);
			}

			$product_cat = $this->product->editProductCategory($id,$product_category);

			//tag add
			$tag_arr = explode(',',$request->get('tag'));
			$tag = $this->product->editTag($id,$tag_arr);

			//warrenty add
			if($request->get('warranty_available') > 0){
				$warenty = $this->product->editWarrenty($id,[
					'product_id' 	  => $id,
					'warrenty_period' => $request->get('warrenty_period'),
					'period_type'	  => $request->get('period_type')
				]);
			}else{
				$warenty = $this->product->editWarrenty($id,[]);
			}
			//Return add
			if($request->get('warranty_available') > 0){
				$return = $this->product->editReturn($id,[
					'product_id' 	  => $id,
					'return_period'   => $request->get('return'),
					'period_type'	  => $request->get('return_period')
				]);
			}else{
				$return = $this->product->editReturn($id,[]);
			}
			 //product specification
	        $specification = [];
	        if(!empty($request->get('specification'))){
	        	foreach ($request->get('specification') as $key => $value) {
	        		$specification[$value] = $request->get('value')[$key];
	        	}
	        }else{
	        	$specification = [];
	        }
        	$product_spec = $this->product->editSpecification($id,$specification);
	      

			return redirect( 'product/list' )->with([
	            'success' 		 => true,
	            'success.title'  => 'Success..!',
	            'success.message'=> 'Product updaded successfully'
	        ]);
        }catch(Exception $e){
        	return back()->with([
	                'error' 	   => true,
	                'error.title'  => 'Error..!',
	                'error.message'=> $e->getMessage()
	        ])->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
