<?php namespace App\Modules\Product\Models;

/**
*
* Product Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class ProductFilter extends Model {

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_filter';
     /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
    * Create relationship with location.
    */



}
