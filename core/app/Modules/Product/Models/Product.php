<?php namespace App\Modules\Product\Models;

/**
*
* Product Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model {

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product';
     /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
    * Create relationship with location.
    */

    //FOR GLOBAL SEARCH
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'product.name'         => 10,
            'product.display_name' => 9,
            'product.code'         => 8
        ]
    ];
    //FOR GLOBAL SEARCH

    public function product_category(){
        return $this->hasOne('App\Models\ProductCategory','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function images(){
        return $this->hasMany('App\Modules\Product\Models\Image','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function product_tag(){
        return $this->hasMany('App\Models\ProductTag','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function warrenty(){
        return $this->hasOne('App\Models\Warrenty','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function return_product(){
        return $this->hasOne('App\Models\ReturnPolicy','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function product_specification(){
        return $this->hasMany('App\Models\Specification','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('specification', 'ASC');
    }

    public function stock(){
        return $this->hasOne('App\Modules\Stock\Models\Stock','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function product_pricebook(){
        return $this->hasMany('App\Modules\Pricebook\Models\ProductPricebook', 'product_id', 'id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function specification_title(){
        return $this->hasMany('App\Models\SpecificationTitle','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('title', 'ASC');
    }

    public function active_bank_promotions(){
        return $this->belongsToMany('App\Models\Promotion', 'product_promotion', 'product_id', 'promotion_id')
            ->withPivot('allocate_qty', 'promotion_price', 'promotion_status')
            //->where('product_promotion.allocate_qty', '>', 0)
            ->where('promotion.promotion_type', BANK_PROMOTION)
            ->where('promotion.status', DEFAULT_STATUS)
            ->where('promotion.period_from', '<=', date('Y-m-d'))
            ->where('promotion.period_to', '>=', date('Y-m-d'));
    }
    /* ------------------ Made by Lahiru ----------------------------------------------------------*/

    public function product_promotion(){
        return $this->hasMany('App\Models\ProductPromotion', 'product_id', 'id')
            ->orderBy('updated_at','DESC')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function product_stock(){
        return $this->hasOne('App\Models\Stock', 'product_id', 'id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function image(){
        return $this->hasOne('App\Modules\Product\Models\Image','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function datasheets(){
        return $this->hasMany('App\Modules\Product\Models\Datasheet','product_id','id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function product_price(){
        return $this->hasOne('App\Modules\Pricebook\Models\ProductPricebook', 'product_id', 'id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    public function price(){
        return $this->hasOne('App\Modules\Pricebook\Models\ProductPricebook', 'product_id', 'id')
            ->where('pricebook_channel.status', 1)
            ->where('product_pricebook.status', 1)
            ->join('pricebook_channel', 'pricebook_channel.id', '=', 'product_pricebook.pricebook_channel_id')
            ->where('pricebook_channel.pricebook_id', 1)
            ->where('pricebook_channel.channel_id', CHANNEL_ONLINE_ID)
            ->whereNull('product_pricebook.deleted_at')
            ->whereNull('pricebook_channel.deleted_at');
    }

    public function pro_product(){
        return $this->hasOne('App\Models\ProductPromotion', 'product_id', 'id')
            ->where('status', 1)
            ->whereNull('deleted_at');
    }

    //product has country
    public function country(){
        return $this->belongsTo('App\Models\Country','country_id');
    }


}
