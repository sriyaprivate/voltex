<?php namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_image';

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
