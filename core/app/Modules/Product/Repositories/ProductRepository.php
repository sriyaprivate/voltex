<?php
/**
 * PRODUCT MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-03
 */

namespace App\Modules\Product\Repositories;

use App\Modules\Product\Models\Image;
use App\Modules\Product\Models\Product;
use App\Models\ProductCategory;
use App\Models\Filter;
use App\Models\FilterValue;
use App\Modules\Category\Models\CategoryFilter;
use App\Modules\Product\Models\ProductFilter;
use App\Models\ProductChoice;
use App\Models\ProductChoiceOption;
use App\Models\ProductMeasurement;
use App\Models\Fabric;
use App\Models\DeliveryType;
use App\Modules\Pricebook\Models\ProductPricebook;
use App\Modules\Category\Models\Category;
use App\Classes\Functions;
use Illuminate\Support\Collection;

use DB;
use Response;

class ProductRepository{
	
	/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	+ get product by category id                   	
	+ param: category id, request data, 
	+ return type mean get query without get(), first(), paginate()	it maybe help to add more where class in controller +
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	
	public function getProductByCategory($category_id, $request = null, $main_category_id = null){
		$show       = DEFAULT_SELECTED_SHOW_OPTION; //SHOW ITEMS COUNT PER PAGE
		$toDay      = date('Y-m-d');
		$common     = new Functions();
		$loggedUser = $common->getLoggedUser(); 
		$keyword    = '';
		$status     = 1;

		if($request !== null){
			$sort       = $request->input('sort')? explode('_', $request->input('sort')): '';
			$input_show = (int) $request->input('show');	
			$show       = ($input_show !== 0)? $input_show : $show; 
			$filters    = $this->getFilters($request);
			$keyword    = $request->input('q');

			//if set some keyword status will not be check
			$status     = !empty($keyword)? '' : 1; 

			if($request->input('price_range') !== null){
				$minPrice   = explode('-', $request->input('price_range'))[0]?:'0'; 
				$maxPrice   = explode('-', $request->input('price_range'))[1]?:'0'; 
			}
		}

		$cat_type_id = $request->type_id;
		
		$products = Product::select(
				'product.id',
				'product.country_id',
				'product.supplier_id',
				'product.tax_type_id',
				'product.code',
				'product.name',
				'product.selling_price',
				'product.display_name',
				'product.status',
				'product.created_at',
				'product.updated_at',
				'product.moq',
				'product.lead_time',
				'stock.qty',
			DB::raw('category.lft orderByCategory'),
			'wishlist.product_id as hasInWishlist')
			->with([
				'product_category.category.type',
				'images',
				'country'
			])->orderBy('product.code','asc')
			->join('product_category', function($query){
				$query->on('product.id', '=', 'product_category.product_id')
					->where('product_category.status', 1)
					->whereNull('product_category.deleted_at');
			})
			->join('category', function($query)use($cat_type_id){
				$query->on('product_category.category_id', '=', 'category.id')
					->where('category.status', 1)
					->whereNull('category.deleted_at')
					->where('category.category_type_id',$cat_type_id);
			})
			->leftjoin('product_promotion', function($query){
				$query->on('product.id', '=', 'product_promotion.product_id')
					->where('product_promotion.status', 1)
					->whereNull('product_promotion.deleted_at');
			})
			->leftjoin('wishlist', function($query) use($loggedUser){
				$query->on('product.id', '=', 'wishlist.product_id')
					->where('wishlist.status', 1)
					->where('wishlist.user_id', $loggedUser? $loggedUser->id : '')
					->whereNull('wishlist.deleted_at');
			})
			->leftjoin('promotion', function($query) use($toDay, $keyword){
				$query = $query->on('product_promotion.promotion_id', '=', 'promotion.id')
					->where('product_promotion.status', 1)
					->where('promotion.status', 1)
					->where('promotion.period_from', '<=', $toDay)
					->where('promotion.period_to', '>=', $toDay)
					->whereNull('product_promotion.deleted_at')
					->whereNull('promotion.deleted_at');
				
				//load all item without check promotion type
				if(empty($keyword)){
					$query = $query->where('promotion.promotion_type', DISCOUNT);
				}else{
					$query = $query->whereIn('promotion.promotion_type', CART_PROMOTION_TYPE);
				}
			})
			->leftjoin('stock', function($query) use($status){
				$query->on('product.id', '=', 'stock.product_id')
					->where('stock.status', 1)
					// ->where('product.status', 1)
					->whereNull('stock.deleted_at')
					->whereNull('product.deleted_at');
			});

		if($category_id !== null && $category_id !== 0 && $category_id !== ''){
			$category_ids = $this->getCategoryChilders($category_id, $main_category_id);

			if(count($category_ids)){
				$products = $products->whereIn('product_category.category_id', array_unique($category_ids));
			}else{
				throw new \Exception("Category ids are not found.");
			}
		}else{
			$category_ids = $this->getCategoryChilders(null, $main_category_id);

			if(count($category_ids) > 0){
				$products = $products->whereIn('product_category.category_id', array_unique($category_ids));
			}else{
				return [];
			}
		}
		
		if(isset($keyword) && !empty($keyword)){
			$products     = $products->leftjoin('product_tag', 'product.id', '=', 'product_tag.product_id')
                ->leftjoin('tag', function($join){
                    $join->on('product_tag.tag_id', '=','tag.id')
                        ->on('product_tag.product_id', '=','product.id');
                })
                ->where(function($query) use($keyword){
                    $query->where('product.display_name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('product.name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('tag.name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('product.code', 'LIKE', '%'.$keyword.'%');
				})
				->whereIn('product.status', PRODUCT_CHECK_STATUS)
				->paginate($show);
		}else{
			$products =	$products->whereIn('product.status', PRODUCT_CHECK_STATUS)
				->paginate($show);
		}

		return $products;
	}

	public function _getProductByCategory($category_id, $request = null, $main_category_id = null){

		$cat_type_id = $request->type_id;
		$show       = ALL_SHOW_OPTION; //SHOW ITEMS COUNT PER PAGE

		$products = Product::select(
				'product.id',
				'product.country_id',
				'product.supplier_id',
				'product.tax_type_id',
				'product.code',
				'product.name',
				'product.selling_price',
				'product.display_name',
				'product.status',
				'product.created_at',
				'product.updated_at',
				'product.moq',
				'product.lead_time')
			->join('product_category', function($query){
				$query->on('product.id', '=', 'product_category.product_id')
					->where('product_category.status', 1)
					->whereNull('product_category.deleted_at');
			})
			->join('category', function($query)use($cat_type_id){
				$query->on('product_category.category_id', '=', 'category.id')
					->where('category.status', 1)
					->whereNull('category.deleted_at')
					->where('category.category_type_id',$cat_type_id);
			})
			->with([
				'product_category.category.type',
				'images',
				'country'
			])->orderBy('product.created_at','asc');

		if($category_id !== null && $category_id !== 0 && $category_id !== ''){
			// return 1;
			$category_ids = $this->getCategoryChilders($category_id, $main_category_id);

			if(count($category_ids)){
				$products = $products->whereIn('product_category.category_id', array_unique($category_ids));
			}else{
				throw new \Exception("Category ids are not found.");
			}
		}

		$products =	$products->whereIn('product.status', PRODUCT_CHECK_STATUS)
				->paginate($show);

		return $products;

	}

	//get childer of category
	public function getCategoryChilders($category_id = null, $main_category_id = null){
		if($category_id !== null){
			$category = Category::where('id', $category_id)
				->first();
			if(count($category) > 0){
				$category = $category->getDescendantsAndSelf()->toArray();

				if(count($category) > 0){
					$category = collect($category)
						->pluck('id')
						->toArray();

					if(isset($category) && is_array($category)){
						return $category;
					}
				}
			}
		}elseif($main_category_id !== null){
			$category = Category::select('id')->where('category_type_id', $main_category_id)
				->get();

			if(count($category) > 0){
				if(count($category) > 0){
					$category = collect($category)
						->pluck('id')
						->toArray();

					if(isset($category) && count($category) > 0 && is_array($category)){
						return $category;
					}
				}
			}
		}else{

		}
	}

	public function getFilters($request){
		$filter = [];
		$data   = array_filter($request->all());
		
		if(count($data)){
			foreach($data as $key => $filter1){
				$getFilter = Filter::where('name',str_replace('_',' ',$key))->first();
	
				if($getFilter){
					$filter[$getFilter->id] = explode(',',$filter1);
				}
			}
	
			return $filter;	
		}else{
			return [];
		}
	}

	//get max price of pricebook
	public function getMaxPrice(){
		$maxPrice = ProductPricebook::orderBy('mrp', 'DESC')->first();

		if(isset($maxPrice->mrp)){
			return ['maxPrice' => $maxPrice->mrp];
		}
	}

	//get product details
	public function getProductById($product_id){
		

		$products = Product::with([				
			'product_category.category.type',
			'images',
			'datasheets',
			'country'
		]);

		if(is_array($product_id)){
			$products = $products->whereIn('product.id', $product_id)
				->groupBy('product.id')
				->get();	
		}else{
			$products = $products->where('product.id', $product_id)
				->groupBy('product.id')
				->first();	
		}


		return $products;
	}

	//get related products
	public function getRelatedProducts($item_id = null){
		$relatedProducts = [];

		if(!empty($item_id) && $item_id !== null && $item_id !== 0){
			
			$products = Product::select('tag.*', 'product.display_name', 'product_category.category_id')
				->join('product_tag', 'product_tag.product_id', '=', 'product.id')
				->join('product_category', 'product.id', '=', 'product_category.product_id')
				->join('tag', 'product_tag.tag_id', '=', 'tag.id')
				->where('product.id', $item_id)
				->whereNull('product.deleted_at')
				->whereNull('tag.deleted_at')
				->whereNull('product_tag.deleted_at')
				->get();

			if(isset($products) && count($products)){
				//tags id
				$products = collect($products);
				$tag_ids   = $products->pluck('id');
				$tag_names = $products->pluck('name');
				
				if(count($tag_ids) > 0){
					$product_ids = Product::select('product.id')
						->join('product_tag', 'product_tag.product_id', '=', 'product.id')
						->join('tag', 'product_tag.tag_id', '=', 'tag.id')
						->join('product_category', 'product.id', '=', 'product_category.product_id')
						->join('category', 'product_category.category_id', '=', 'category.id')
						// ->whereIn('tag.id', $tag_ids)
						->where('product.id', '!=', $item_id)
						->where('product_category.category_id', $products->first()->category_id)
						->where('category.category_type_id', BIGSTORE_CATEGORY)
						->where('product.status', PRODUCT_CHECK_STATUS)
						->where(function($query) use($tag_names){
							if(count($tag_names) > 0){
								foreach($tag_names as $tag){
									if(!empty($tag)){
										$query->orWhere('tag.name', 'LIKE', '%'.$tag);
									}
								}
							}
						})
						// ->where(function($query) use($products){
						// 	if(count($products) > 0){
						// 		$product_name = $products->first();

						// 		if(count($product_name) > 0){
						// 			$keywords = explode(' ', $product_name->display_name);
									
						// 			if(count($keywords) > 0){
						// 				foreach($keywords as $keyword){
						// 					if(count($keyword) > 0){
						// 						$query->orWhere('product.display_name', 'LIKE', '%'.$keyword.'%')
						// 							->orWhere('product.name', 'LIKE', '%'.$keyword.'%');
						// 					}
						// 				}
						// 			}
						// 		}
						// 	}
						// })
						->whereNull('product.deleted_at')
						->groupBy('product.id')
						->get()
						->toArray();

					if(count($product_ids) > 0){
						$relatedProducts = $this->getProductById($product_ids);	
					}else{
						$relatedProducts = [];
					}
				}else{
					$relatedProducts = [];
				}
				
				return $relatedProducts;
			}
		}else{
			throw new \Exception("Invalid Product ID!.");
		}
	}

	//check product is exist or not
	public function isProductExist($product_id){
		if(count($product_id) > 0 && !empty($product_id)){
			$product = Product::where('id', $product_id)
				->whereIn('status', PRODUCT_CHECK_STATUS)
				->whereNull('deleted_at')
				->get();

			if(count($product) > 0){
				return true;
			}else{
				return false;
			}
		}else{
			throw new \Exception("Invalid product ID!.");
		}
	}

	//get delivery types
	public function getDeliveryType(){
		$deliveryType = DeliveryType::where('status', DEFAULT_STATUS)
			->whereNull('deleted_at')
			->get();

		if(count($deliveryType) > 0){
			return $deliveryType;
		}else{
			throw new \Exception("Delivery type not found!.");
		}
	}

	//get saving center product in view
	//get product details
	public function savingProduct($product_id){
		$products = Product::select('product.*', 
			'product_promotion.allocate_qty as availableQty',
			'product_promotion.sold_qty as sold_qty',
			'promotion.discount as discount_rate', 
			'promotion.period_from as period_from',
			'promotion.period_to as period_to', 
			'product_pricebook.mrp as price',
			DB::raw('CAST(((product_pricebook.mrp * (100 - promotion.discount)) / 100) as DECIMAL(15,2)) as discounted_amount'),
			DB::raw('(product_pricebook.mrp - CAST(((product_pricebook.mrp * (100 - promotion.discount)) / 100) as DECIMAL(15,2))) as balance'))
			->with([
				'active_bank_promotions' => function($query){
					$query->select(
							'promotion.id', 
							'promotion_type', 
							'promotion_code', 
							'discount', 
							'allocate_qty',
							'bank.name as bank_name',
							'bank_card.card_name',
							'bank_card.image as card_image'
						)
						->join('bank_promotion', 'promotion.id', '=', 'bank_promotion.promotion_id')
						->join('bank', 'bank.id', '=', 'bank_promotion.bank_id')
						->join('bank_card', 'bank_promotion.card_id', '=', 'bank_card.id');
				},
				'product_category.category.type',
				'product_specification',
				'images',
				'pro_product.order_detail'
			])
			->join('product_category', function($query){
				$query->on('product.id', '=', 'product_category.product_id')
					->where('product_category.status', 1)
					->whereNull('product_category.deleted_at');
			})
			->join('product_pricebook', function($query){
				$query->on('product.id', '=', 'product_pricebook.product_id')
					->where('product_pricebook.status', 1)
					->whereNull('product_pricebook.deleted_at');
			})
			->leftjoin('product_promotion', function($query){
				$query->on('product.id', '=', 'product_promotion.product_id')
					->where('product_promotion.status', 1)
					->whereNull('product_promotion.deleted_at');
			})
			->join('promotion', function($query){
				$query->on('product_promotion.promotion_id', '=', 'promotion.id')
					->where('product_promotion.status', 1)
					->where('promotion.status', 1)
					->where('promotion.promotion_type',DEAL_PROMOTION)
					->whereNull('product_promotion.deleted_at')
					->whereNull('promotion.deleted_at');
			})
			->leftjoin('stock', function($query){
				$query->on('product.id', '=', 'stock.product_id')
					->where('stock.status', 1)
					->where('product.status', 1)
					->whereNull('stock.deleted_at')
					->whereNull('product.deleted_at');
			})
			->join('pricebook_channel', function($query){
				$query->on('product_pricebook.pricebook_channel_id', '=', 'pricebook_channel.id')
					->whereIn('pricebook_channel.channel_id', function($query){
						$query->select('id')
							->from('channel')
							->where('channel.name', 'LIKE', '%'.CHANNEL_ONLINE.'%')
							->where('channel.status', 1)
							->whereNull('channel.deleted_at');
					})
					->where('pricebook_channel.status', 1)
					->whereNull('pricebook_channel.deleted_at');
			});

			if(is_array($product_id)){
				$products = $products->whereIn('product.id', $product_id)
					->groupBy('product.id')
					->get();	
			}else{
				$products = $products->where('product.id', $product_id)
					->first();	
			}

		return $products;
	}

	public function getProductMeasurement($pro_id){
		
		$result = ProductMeasurement::where('product_id', $pro_id)
			->whereNull('deleted_at')
			->get();

		if($result){
			return $result;
		}else{
			throw new \Exception("Delivery type not found!.");
		}
	}

	public function getProductChoicesModals($pro_id){

		$result = ProductChoice::where('category_id', function($query) use($pro_id){
				$query->select('category_id')->from('product_category')->where('product_id',$pro_id);
			})
			->whereNull('deleted_at')
			->get();

		if($result){
			return $result;
		}else{
			throw new \Exception("Choices not found!.");
		}
	}

	public function getProductChoiceOptions($choice_id){

		$result = ProductChoiceOption::where('choice_id', $choice_id)->whereNull('deleted_at')->get();

		if($result){
			return $result;
		}else{
			throw new \Exception("Options not found!.");
		}
	}

	public function getProductFabrics($pro_id){

		$result = Fabric::whereNull('deleted_at')->get();

		if($result){
			return $result;
		}else{
			throw new \Exception("Fabric not found!.");
		}
	}

}