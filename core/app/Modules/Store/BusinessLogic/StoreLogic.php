<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-08
 */
 
use App\Modules\Category\CategoryLogic;
use App\Modules\Category\CategoryRepository\CategoryRepository;
use App\Classes\Functions;

class StoreLogic {
	//variable declaration.
	public $comman       = null;
	public $categoryRepo = null;

	//assign dependancy injection.
	public function __construct(Functions $comman, CategoryRepository $categoryRepo){
		$this->comman       = $comman;
		$this->categoryRepo = $categoryRepo;
	}

	//save image in storage
	public function saveCategory($request){
		try {
			$category_image  = $request->file('category_image');
			$category_icon   = $request->file('category_icon');
			$img_dir_path    = 'file/category/image';
			$icon_dir_path   = 'file/category/icon';
			$img_uploaded    = null;
			$icon_uploaded   = null;

			//make dir if not exist.
	        $this->comman->makeDirNotExist($img_dir_path , 0777, true);
	        $this->comman->makeDirNotExist($icon_dir_path , 0777, true);

	        if(isset($category_image[0]))
	        {
	        	//move image to storage
	        	$img_uploaded = $this->comman->saveImage($category_image[0], $img_dir_path , '', '', 100);
	        }

	        if(isset($category_icon[0]))
	        {
	        	//move image to storage
	        	$icon_uploaded = $this->comman->saveImage($category_icon[0], $icon_dir_path , '', '', 100);
	        }

	        if(isset($img_uploaded['count']) && $img_uploaded['count'] > 0)
	        {
	        	if(isset($icon_uploaded['count']) && $icon_uploaded['count'] > 0)
	        	{
	        		//request , img path, status
					return $this->categoryRepo->saveCategory($request, $img_uploaded['path'], $icon_uploaded['path'], 1);
	        	}
	        	else
	        	{
	        		return $this->categoryRepo->saveCategory($request, $img_uploaded['path'], '', 1);
	        	}
			}
			else
			{
				if(isset($icon_uploaded['count']) && $icon_uploaded['count'] > 0)
	        	{
					return $this->categoryRepo->saveCategory($request, '', $icon_uploaded['path'], 1);
	        	}
	        	else
	        	{
	        		return $this->categoryRepo->saveCategory($request, '', '', 1);
	        	}
			}  	
		} catch (\Exception $e) {
			throw new \Exception('message'.$e->getMessage());
		}
	}

	//update category -- save image
	public function updateCategory($category_id, $request)
	{
		try {
			$category_image  = $request->file('category_image');
			$category_icon   = $request->file('category_icon');
			$img_dir_path    = 'file/category/image';
			$icon_dir_path   = 'file/category/icon';
			$img_uploaded    = null;
			$icon_uploaded   = null;

			//make dir if not exist.
	        $this->comman->makeDirNotExist($img_dir_path , 0777, true);
	        $this->comman->makeDirNotExist($icon_dir_path , 0777, true);

	        if(isset($category_image[0]))
	        {
	        	$img_uploaded = $this->comman->saveImage($category_image[0], $img_dir_path , '', '', 100);
	        }

	        if(isset($category_icon[0]))
	        {
	        	$icon_uploaded = $this->comman->saveImage($category_icon[0], $icon_dir_path , '', '', 100);
	        }

	        if(isset($img_uploaded['count']) && $img_uploaded['count'] > 0)
	        {
	        	if(isset($icon_uploaded['count']) && $icon_uploaded['count'] > 0)
	        	{
	        		//category_id, request , img path, icon_path status
					return $this->categoryRepo->updateCategory($category_id, $request, $img_uploaded['path'], $icon_uploaded['path'], 1);
	        	}
	        	else
	        	{
	        		return $this->categoryRepo->updateCategory($category_id, $request, $img_uploaded['path'], '', 1);
	        	}
			}
			else
			{
				if(isset($icon_uploaded['count']) && $icon_uploaded['count'] > 0)
	        	{
					return $this->categoryRepo->updateCategory($category_id, $request, '', $icon_uploaded['path'], 1);
	        	}
	        	else
	        	{
	        		return $this->categoryRepo->updateCategory($category_id, $request, '', '', 1);
	        	}
			}  	
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}
}