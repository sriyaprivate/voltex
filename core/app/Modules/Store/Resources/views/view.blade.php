@extends('layouts.master') @section('title','Item view')

@section('links')

  <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
  <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">
  
  <!-- owl.carousel.js -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

  <!-- animated-vertical-accordion-menu-mtree-js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/mtreeJs/mtree.css')}}" />

  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/PgwSlider-master/pgwslider.min.css')}}" />

  <!-- jquery.rateyo.v2.3.2 -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/jquery.rateyo.v2.3.2/jquery.rateyo.min.css')}}" />

  <link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

  <!-- toastr notification -->
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}"> -->
@stop

@section('css')
<style type="text/css">

    .cart{
        margin: 75px -25px 0px 0 !important;
    }

    .performa-invoice-cart{
        margin: 75px -25px 0px 0 !important;
    }
    
    .mb-44{
        margin-top: 0rem !important;
        margin-bottom: 30px !important;
    }

    .mb-top-44{
        margin-top: 25px;
        margin-bottom: 25px;
    }

    .promotion{
        padding: 3px 20px 4px 0px !important;
    }

    .promotion img{
        width: 70% !important;
    }

    .mb-3{
        text-decoration: underline;
        font-size: 15px !important;
        font-family: 'Helvetica Neue LT Std' !important;
    }

    .mb-3:hover{
        color: red;
    }

    .img-fluid{
        max-width: 55% !important;
    }

    .btn-measure{
        background-color: crimson;
        color: white;
        height: 25px;
        font-size: 12px !important;
        padding: 0 0.3rem !important;
        margin-top: 5px;
    }

    ul.mtree{
        padding: 0px !important;
    }

    .filters-container{
        border-bottom: 0px !important;
        margin-bottom: 10px;
    }

    form{
        margin: 0px 0 0 0 !important;
    }

    .breadcrumbs{
        display: block;
        overflow: hidden;
        padding: 10px 0;
        margin: 0;
        font-size: 13px;
        color: #999;
    }

    .breadcrumbs ul {
        padding: 0;
        margin: 0;
        list-style: none;
    }

    .breadcrumbs ul li {
        float: left;
        margin: 0 8px 0 0;
        font-size: 13px;
        line-height: 22px;
    }

    .white-block {
        display: block;
        padding: 24px 12px 30px;
        margin-bottom: 22px;
        min-height: 150px;
        background-color: #fff;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -moz-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        -webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        box-shadow: 0 1px 5px rgba(0,0,0,0.10);
    }

    .sidebar-title{
        display: block;
        margin: 0 0 24px;
        font-size: 20px;
        font-weight: 400;
        color: #011e32;
        font-family: 'geogrotesque_semibold';
        text-transform: uppercase;
    }

    .content-title{
        width: 100%;
        float: left;
        font-size: 26px!important;
        line-height: 30px;
        color: crimson;
        padding: 15px;
        margin: 0!important;
        font-family: 'geogrotesque_semibold';
    }

    .each-product{
        transition: all 0.3s ease-in-out;
        display: block;
        min-height: 200px;
        vertical-align: top;
        padding: 14px 12px;
        margin: 0 0 14px;
        background-color: #fff;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -moz-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        -webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        border: 1px solid transparent;
        -webkit-transition: all .5s ease;
        -moz-transition: all .5s ease;
        -ms-transition: all .5s ease;
        position: relative;
    }

    .content-detail{
        overflow: hidden;
        padding: 5px 0;
        border-bottom: 1px solid #e7ebed;
    }

    .content-code{
        padding: 1px 0 0;
        text-transform: uppercase;
        font-size: 13px;
        color: gray;
        font-family: 'Lato',sans-serif;
    }

    .instock{
        background: green; color: #fff; padding-left: 5px;padding-right: 5px;border-radius: 5px;
    }

    .show-on-hover{
        width: 90%;
        margin: 0 5%;
        height: 38px;
        border-bottom: 1px solid #e7ebed;
        position: absolute;
        left: 0;
        top: 0;
        z-index: 10;
        background-color: #fff;
        -webkit-border-top-left-radius: 4px;
        -webkit-border-top-right-radius: 4px;
        -moz-border-radius-topleft: 4px;
        -moz-border-radius-topright: 4px;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
        will-change: transform,opacity;
        -webkit-transition: -webkit-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -moz-transition: -moz-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -o-transition: -o-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -ms-transition: -ms-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        transition: transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -webkit-transform: translateY(-20%);
        -moz-transform: translateY(-20%);
        -o-transform: translateY(-20%);
        -ms-transform: translateY(-20%);
        transform: translateY(-20%);
        opacity: 0;
    }

    .quickview-btn:hover {
        color: #0466a4;
        text-decoration: none;
    }

    .container-fluid{
        padding-top: 100px
    }

    .zoomContainer{
        width: 230px !important;
        height: 230px !important;
    }

    .qty-row{
        width: 64%;
    }

    .qty-row input[type='text'] {
        height: 26px !important;
    }

    .cart-row{
        width: 36%;
    }

    .qty {
        width: 74%;
        height: 25px;
        text-align: center;
        color: #495057; 
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    
    @media only screen and (max-width: 374px) and (min-width: 321px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 74%!important;}
    }

    @media only screen and (max-width: 425px) and (min-width: 375px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 70%!important;}
    }

    @media only screen and (max-width: 575px) and (min-width: 426px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 75%!important;}
    }

    @media only screen and (max-width: 651px) and (min-width: 576px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 64%!important;}
    }

    @media only screen and (max-width: 754px) and (min-width: 652px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 70%!important;}
    }

    @media only screen and (max-width: 872px) and (min-width: 754px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 74%!important;}
    }

    @media only screen and (max-width: 951px) and (min-width: 873px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 79%!important;}
    }

    @media only screen and (max-width: 1024px) and (min-width: 952px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 81%!important;}
    }

    @media only screen and (max-width: 1440px) and (min-width: 1025px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 81%!important;}
    }

    @media only screen and (min-width: 1441px) {
        .qty-row {width: 85%!important;}
        .cart-row {width: 15%!important;}
        .qty {width: 81%!important;}
    }

    @media (max-width: 500px) {
        .zoomContainer{
            height: 230px !important;
        }

    }

    @media screen only (min-width: 1440px){
        .sub-content-wrap .product-detail-wrap{margin-top: 20px !important}
    }
</style>
@stop

@section('content')
    
    <div class="sub-content-wrap pro-view" style="display: flow-root;">

        <div class="product-detail-wrap">
            <div class="pd-menu-sidebar">
                <div class="white-block">
                    <h2>Categories</h2>
                    <div class="sidebar-category-nav">
                        <ul class="mtree transit collapsed show" id="menu">

                            @foreach($categories as $node)
                                {!! renderNode($node) !!}
                            @endforeach
                        </ul><!-- /.mtree menu -->
                    </div>
                </div>                
            </div>

            <div class="pd-left-content">
                <div class="pd-top-row" id="pro-view-div">
                    @if(count($product_details) > 0 && !empty(count($product_details)))
                        <div class="row">   

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 product-viewer clearfix" style="margin-top: 0">
                                <div id="product-image-carousel-container">
                                    @if(count($product_details->images))
                                        <ul id="product-carousel" class="celastislide-list">
                                        <?php $count = 0; ?>
                                        @foreach($product_details->images as $image)
                                            @if($count == 1)
                                                <li class="active-slide">
                                                    <a data-rel="prettyPhoto[product]" href="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" class="product-gallery-item">
                                                        <img src="{{ url('data/product_thumb/images/product/'.$image->image) }}" class="item-bordered">
                                                    </a>
                                                </li>
                                            @else
                                            <li>
                                                <a data-rel="prettyPhoto[product]" href="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$image->image) }}" class="product-gallery-item">
                                                    <img src="{{ url('data/product_thumb/images/product/'.$image->image) }}" alt="{{ $image->image }}" class="item-bordered">
                                                </a>
                                            </li>
                                            @endif
                                            
                                            <?php $count++; ?>
                                        @endforeach
                                        </ul><!-- End product-carousel -->
                                    @endif
                                </div>

                                <div id="product-image-container">
                                    @if(isset($product_details->discount_rate) && !empty($product_details->discount_rate))
                                        <div class="promotion-right r--0">
                                            <span class="dis ng-binding">{{ number_format($product_details->discount_rate?: '0') }}%</span>
                                            <span class="save">OFF</span>
                                        </div>
                                    @endif
                                    <figure>                
                                        @if(count($product_details->images))
                                            <img src="{{ url('data/product_thumb/images/product/'.$product_details->images[0]->image) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.$product_details->images[0]->image) }}" alt="sdf" id="product-image">
                                        @else
                                            <img src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" data-zoom-image="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="default product image." id="product-image">
                                        @endif                
                                    </figure>
                                </div><!-- product-image-container -->
                            </div><!-- End .col-md-6 -->

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 product-info-column" style="margin-top: 0;margin-bottom: 0">
                                <h1>
                                    {{ $product_details->name?:'-' }}
                                </h1>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">Country : <img src="{{url(UPLOAD_PATH.'uploads/images/flags/'.$product_details->country['flag'])}}" style="width: 30px" title="{{$product_details->country['name']}}"> {{ $product_details->country->name?:'-' }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">Product Code : <strong>{{ $product_details->code?:'-' }}</strong></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12"><strong>{{ $product_details->moq?:'-' }} pcs</strong> <b style="font-size: 12px">(Min Order Quantity)</b>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">Lead Time : <strong>{{ $product_details->lead_time }}</strong></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">Price Term : {{ $product_details->price_term?:'-' }}</div>
                                        </div>


                                        @if(Illuminate\Support\Facades\Auth::check())
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p style="font-size: 25px !important;padding-top: 10px;padding-right: 20px;float: left">
                                                        <span><strong style="color: #ff0000">$ {{number_format($product_details->selling_price,3)}}</strong></span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="{{ route('cart.add', ['product_id' => isValid($product_details->id)]) }}" method="get" id="form_{{ isValid($product_details->id) }}"  class="product_form">
                                                        <div class="qty-row" style="float: left">
                                                            <input type='button' style="float: left" value='+' class='qtyplus' field='quantity' />
                                                            <input type='text' style="margin-left: 10px" name='quantity' value='0' class='qty' />
                                                            <input type='button' style="float: right" value='-' class='qtyminus' field='quantity' />
                                                        </div>
                                                        <div class="cart-row" style="float: left;padding: 0px">
                                                            <input type="hidden" name="prev_url" value="{{ Request::getQueryString() }}">
                                                            <button type="submit" name="action" value="cart" title="Click to buy item" class="btn btn-measure add-to-cart" style="background-color: #eb232a;color: #fff !important;float: left;" data-product="{{$product_details->id}}">
                                                                <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 15px;padding-right: 3px;"></i>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row"></div>
                            </div>
                        </div>

                        <div class="row">
                            
                        </div>
                    @else
                        <h2 class="text-center text-secoundary not-found">Product not found!.</h2>
                    @endif
                </div>

                <div class="pd-top-row" id="pro-detail-div">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link" id="nav-description-tab" data-toggle="tab" href="#nav-description" role="tab" aria-controls="nav-description" aria-selected="true">Description</a>
                            <a class="nav-item nav-link active" id="nav-specs-tab" data-toggle="tab" href="#nav-specs" role="tab" aria-controls="nav-specs" aria-selected="false">Specification</a>
                            <a class="nav-item nav-link" id="nav-download-tab" data-toggle="tab" href="#nav-download" role="tab" aria-controls="nav-download" aria-selected="false">Download</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div style="padding: 25px 0 20px 0;" class="tab-pane fade" id="nav-description" role="tabpanel" aria-labelledby="nav-description-tab">
                            <div class="col-12">
                                <b>{{ $product_details->description?:'-' }}</b>
                            </div>
                        </div>
                        <div style="padding: 0px 0 20px 0;" class="tab-pane fade show active" id="nav-specs" role="tabpanel" aria-labelledby="nav-specs-tab">
                            <table bordercolor="#d7d7d7" cellspacing="0" cellpadding="0" width="70%" border="1" style="border-spacing: 1em">
                                <tbody>
                                    @foreach($spec_keys as $key=>$val)
                                        <tr <?php if ($key%2==0): ?>
                                            bgcolor="#d7d7d7"    
                                        <?php endif ?>>
                                            <td width="30%" style="padding: 6px"><b>{{ucwords(str_replace('_',' ',$val))}}</b></td>
                                            <td width="70%" style="padding: 6px">{{$specs->$val}} </td>
                                        </tr>                                        
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div style="padding: 20px 0 20px 30px;" class="tab-pane fade" id="nav-download" role="tabpanel" aria-labelledby="nav-download-tab">
                            @if(count($product_details['datasheets'])>0)
                                @foreach($product_details['datasheets'] as $sheets)
                                    <a href="{{ url('backend/core/storage/uploads/datasheet/product/'.$sheets->datasheet) }}" class="" id="openwindowchrome" download style="text-align: center">
                                        <i style="font-size: 30px;padding-left: 4.4%" class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        <br>{{$sheets->datasheet}}
                                    </a><br>
                                @endforeach
                            @else
                                No files
                            @endif
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@stop

@section('js')
  <!-- slick.js -->
  <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>

  <!-- owl.carousel.js -->
  <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>

  <script src="{{asset('assets/libraries/mtreeJs/jquery.velocity.min.js')}}"></script>
    <script src="{{asset('assets/libraries/mtreeJs/mtree.js')}}"></script>

  <!-- PgwSlider-master -->
  <script src="{{asset('assets/libraries/PgwSlider-master/pgwslider.min.js')}}"></script>

  <!-- jquery.rateyo.v2.3.2 -->
  <script src="{{asset('assets/libraries/jquery.rateyo.v2.3.2/jquery.rateyo.min.js')}}"></script>

  <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>
  <script src="{{asset('assets/libraries/jquery.rateyo.v2.3.2/jquery.rateyo.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/modernizr.custom.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.prettyPhoto.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.elastislide.js')}}"></script>
  <!-- <script src="{{asset('assets/js/vendor/jquery.elevateZoom.min.js')}}"></script> -->
  <script src="{{asset('assets/js/vendor/main.js')}}"></script>
  <!-- <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script> -->
  <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
    <script>
        $(document).ready(function(){
            $(".rateYo").rateYo({
                rating: 3.6,
                starWidth: "20px"
            });

            $('.pgwSlider').pgwSlider({
                listPosition: 'left',
                verticalCentering: 'true',
                adaptiveHeight: 'false',
                autoSlide: 'false',
                transitionEffect: 'Sliding',
                intervalDuration: '5000'
            });

            $('.qtyplus').click(function(e){
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name='+fieldName+']').val());
                // If is not undefined
                if (!isNaN(currentVal)) {
                    // Increment
                    $('input[name='+fieldName+']').val(currentVal + 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name='+fieldName+']').val(0);
                }
            });
            // This button will decrement the value till 0
            $(".qtyminus").click(function(e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name='+fieldName+']').val());
                // If it isn't undefined or its greater than 0
                if (!isNaN(currentVal) && currentVal > 0) {
                    // Decrement one
                    $('input[name='+fieldName+']').val(currentVal - 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name='+fieldName+']').val(0);
                }
            });
        });
    </script>
@stop