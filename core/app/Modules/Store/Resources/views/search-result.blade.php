@extends('layouts.master') @section('title','Store')

@section('links')
    <!-- slick.css -->
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">

    <!-- hover effects -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/hover-effect-ideas/css/set2.css')}}" />

    <!-- owl.carousel.js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/mtreeJs/mtree.css')}}" />

    <!-- ion.rangeSlider.skinFlat.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.skinFlat.css')}}" />

    <!-- jquery confirm alert -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />

    <!-- toastr notification -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop
<style type="text/css">
    .mb-44{
        margin-top: 0rem !important;
        margin-bottom: 30px !important;
    }

    .mb-top-44{
        margin-top: 25px;
        margin-bottom: 25px;
    }

    .promotion{
        padding: 3px 20px 4px 0px !important;
    }

    .promotion img{
        width: 70% !important;
    }

    .mb-3{
        text-decoration: underline;
        font-size: 15px !important;
        font-family: 'Helvetica Neue LT Std' !important;
    }

    .mb-3:hover{
        color: red;
    }

    .img-fluid{
        max-width: 55% !important;
    }

    .btn-measure{
        background-color: crimson;
        color: white;
        height: 25px;
        font-size: 12px !important;
        padding: 0 0.3rem !important;
        margin-top: 10px;
    }

    ul.mtree{
        padding: 0px !important;
    }

    .filters-container{
        border-bottom: 0px !important;
        margin-bottom: 10px;
    }

    form{
        margin: 0px 0 0 0 !important;
    }

    .breadcrumbs{
        display: block;
        overflow: hidden;
        padding: 10px 0;
        margin: 0;
        font-size: 13px;
        color: #999;
    }

    .breadcrumbs ul {
        padding: 0;
        margin: 0;
        list-style: none;
    }

    .breadcrumbs ul li {
        float: left;
        margin: 0 8px 0 0;
        font-size: 13px;
        line-height: 22px;
    }

    .white-block {
        display: block;
        padding: 24px 12px 30px;
        margin-bottom: 22px;
        min-height: 150px;
        background-color: #fff;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -moz-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        -webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        box-shadow: 0 1px 5px rgba(0,0,0,0.10);
    }

    .sidebar-title{
        display: block;
        margin: 0 0 24px;
        font-size: 20px;
        font-weight: 400;
        color: #011e32;
        font-family: 'geogrotesque_semibold';
        text-transform: uppercase;
    }

    .content-title{
        width: 100%;
        float: left;
        font-size: 26px!important;
        line-height: 30px;
        color: crimson;
        padding: 15px;
        margin: 0!important;
        font-family: 'geogrotesque_semibold';
    }

    .each-product{
        transition: all 0.3s ease-in-out;
        display: block;
        min-height: 200px;
        vertical-align: top;
        padding: 14px 12px;
        margin: 0 0 14px;
        background-color: #fff;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -moz-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        -webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        border: 1px solid transparent;
        -webkit-transition: all .5s ease;
        -moz-transition: all .5s ease;
        -ms-transition: all .5s ease;
        position: relative;
    }

    .content-detail{
        overflow: hidden;
        padding: 5px 0;
        border-bottom: 1px solid #e7ebed;
    }

    .content-code{
        padding: 1px 0 0;
        text-transform: uppercase;
        font-size: 13px;
        color: gray;
        font-family: 'Lato',sans-serif;
    }

    .instock{
        background: green; color: #fff; padding-left: 5px;padding-right: 5px;border-radius: 5px;
    }

    .show-on-hover{
        width: 90%;
        margin: 0 5%;
        height: 38px;
        border-bottom: 1px solid #e7ebed;
        position: absolute;
        left: 0;
        top: 0;
        z-index: 10;
        background-color: #fff;
        -webkit-border-top-left-radius: 4px;
        -webkit-border-top-right-radius: 4px;
        -moz-border-radius-topleft: 4px;
        -moz-border-radius-topright: 4px;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
        will-change: transform,opacity;
        -webkit-transition: -webkit-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -moz-transition: -moz-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -o-transition: -o-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -ms-transition: -ms-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        transition: transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -webkit-transform: translateY(-20%);
        -moz-transform: translateY(-20%);
        -o-transform: translateY(-20%);
        -ms-transform: translateY(-20%);
        transform: translateY(-20%);
        opacity: 0;
    }

    .quickview-btn:hover {
        color: #0466a4;
        text-decoration: none;
    }

    .container-fluid{
        padding-top: 100px
    }

</style>
@section('css')
@stop

@section('content')

  <!-- sign in/up panel -->
    <div class="sub-content-wrap" style="display: flow-root;">
        <!-- breadcrumb -->
        <div class="breadcrumbs">            
            <ul>
                <li style="float: left">
                    <a class="SectionTitleText" href="c-272-outlets-switches.aspx">{{$selected_categoryType}}</a>
                </li>
                @if(isset($selected_category))
                    <li class="arrow" style="float: left"> → </li><li>{{$selected_category}}</li>
                @endif
            </ul>            
        </div> <!-- /.breadcrumb -->

        <div class="left-sidebar">
            <div class="white-block">
                <h2>Categories</h2>
                <div class="sidebar-category-nav">
                    <ul class="mtree transit collapsed show" id="menu">

                        @foreach($categories as $node)
                            {!! renderNode($node) !!}
                        @endforeach
                    </ul><!-- /.mtree menu -->

                </div>
            </div>
        </div>

        <div class="right-content">
            <div class="title-row">
                <h1>@if(isset($selected_category)){{$selected_category}}@else{{$selected_categoryType}}@endif</h1>
                <div class="clearboth"></div>
            </div>
            <div class="clearboth"></div>
            <div class="grid">
                <div class="product-list">
                    @if(count($products) > 0)
                        @foreach($products as $product)
                            @if(isValid($product))
                                <div class="each-product-column">
                                    <div class="each-product" style="min-height: 301px;">
                                        <div class="show-on-hover">
                                            <a href="p-2662-voltex-shadowline-double-power-outlet-with-extra-switch-250v-10a.aspx" class="quickview-btn" data-itempopup="ITEM-002873">
                                                <i class="fa fa-eye" aria-hidden="true"></i> Quick View
                                            </a>
                                            <div class="video-icon">
                                                <a href="https://fast.wistia.net/embed/iframe/1fo5a14q8a?popover=true" class="wistia-popover[height=411,playerColor=00447c,width=730]" id="openwindowchrome">
                                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i> Watch Video
                                                </a>
                                            </div>
                                        </div>
                                        <div class="inner-box">
                                            <div class="imagebox">
                                                <div class="imagebox-inner">
                                                    <a href="{{ route('store.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN,  'item_name' => str_slug($product->display_name, '-'), 'item_id' => $product->id]) }}">
                                                        <div id="pnlEntityImage_2662" align="center">
                                                            @if(isset($product->images[0]->image))
                                                                <img width="150" height="150" id="imgEntity_2662" data-contententitytype="product" data-contentkey="ITEM-002873" data-contentcounter="2662" data-contenttype="image" class="content" src="{{ url('data/product_thumb/images/product/'.$product->images[0]->image) }}" alt="{{ $product->name?:'-' }}">
                                                            @else
                                                                <img width="150" height="150" id="imgEntity_2662" data-contententitytype="product" data-contentkey="ITEM-002873" data-contentcounter="2662" data-contenttype="image" class="content" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="{{ $product->name?:'-' }}">
                                                            @endif
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-info-col">
                                                <h3>
                                                    <a href="{{ route('store.item.view', ['mainSlug' => Request::route('mainSlug')?: GENTLEMEN,  'item_name' => str_slug($product->display_name, '-'), 'item_id' => $product->id]) }}">{{$product->display_name}}</a>
                                                </h3>
                                                <div class="each-row">
                                                    <div class="model-availability">
                                                        <div class="model-num" id="ItemInfoName_2662">{{$product->code}}</div>
                                                        <div class="availability">
                                                            <div id="pnlStockHint_2662" name="pnlStockHint_2662">
                                                                <img id="imgStockHint_2662" src="{{asset('assets/images/instock.png')}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="login-register-text">
                                                    <a class="login-btn" href="../signin.aspx">Login</a> or 
                                                    <a class="reg-btn" href="../createaccount.aspx">Register</a> to view pricing
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                    @endif
                </div>
            </div>
        </div>

    </div>

    <div style="display: block;z-index: 30000;width: 725.313px;top: 125px;left: 743.688px;position: fixed;background: crimson">
         
    </div>

@stop

@section('js')
    <!-- slick.js -->
    <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>

    <!-- owl.carousel.js -->
    <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <script src="{{asset('assets/libraries/mtreeJs/jquery.velocity.min.js')}}"></script>
    <script src="{{asset('assets/libraries/mtreeJs/mtree.js')}}"></script>

    <!-- ion.rangeSlider-2.2.0 -->
    <script src="{{asset('assets/libraries/ion.rangeSlider-2.2.0/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>

    <!-- jquery confirm alert -->
    <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>

    <!-- toastr notification -->
    <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
{{-- @include('cart::includes.cart-js') --}}
    <script>
        $(document).ready(function() {
            
            $('.add-to-cart').click(function(){
                console.log($(this).data('product'));
            });

            $('#fixed-menu').click(function(){
                $('#fixed-cart-panel').show();
            });

            $('#order-list-hide').click(function(){
                console.log("skldfnlsdnf");
                $('#fixed-cart-panel').hide();
            });

            $("#price_range").ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: {{ Request::get('price_range')? explode('-', Request::get('price_range'))[1] : (isset($maxPrice) && !empty($maxPrice)? $maxPrice : '50000') }},
                from: {{ Request::get('price_range')? explode('-', Request::get('price_range'))[0] : 0 }},
                to: {{ Request::get('price_range')? explode('-', Request::get('price_range'))[1] : (isset($maxPrice) && !empty($maxPrice)? $maxPrice : '50000') }},
                prefix: "Rs ",
                hide_min_max: true,
                input_values_separator: "-"
            });
        });
    
    </script>
@stop