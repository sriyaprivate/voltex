@extends('layouts.master') @section('title','Store')

@section('links')
    <!-- slick.css -->
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/libraries/slick-1.8.0/slick/slick-theme.css')}}">

    <!-- hover effects -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/hover-effect-ideas/css/set2.css')}}" />

    <!-- owl.carousel.js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.carousel.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/assets/owl.theme.default.min.css')}}" />

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/mtreeJs/mtree.css')}}" />

    <!-- ion.rangeSlider.skinFlat.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/ion.rangeSlider-2.2.0/css/ion.rangeSlider.skinFlat.css')}}" />

    <!-- jquery confirm alert -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.css')}}" />

    <!-- toastr notification -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
@stop
<style type="text/css">
    
    .cart{
        margin: 90px -25px 0px 0 !important;
    }

    .performa-invoice-cart{
        margin: 90px -25px 0px 0 !important;
    }

    .mb-44{
        margin-top: 0rem !important;
        margin-bottom: 30px !important;
    }

    .mb-top-44{
        margin-top: 25px;
        margin-bottom: 25px;
    }

    .promotion{
        padding: 3px 20px 4px 0px !important;
    }

    .promotion img{
        width: 70% !important;
    }

    .mb-3{
        text-decoration: underline;
        font-size: 15px !important;
        font-family: 'Helvetica Neue LT Std' !important;
    }

    .mb-3:hover{
        color: red;
    }

    .img-fluid{
        max-width: 55% !important;
    }

    .btn-measure{
        background-color: crimson;
        color: white;
        height: 25px;
        font-size: 12px !important;
        padding: 0 0.3rem !important;
        margin-top: 0px;
    }

    ul.mtree{
        padding: 0px !important;
    }

    .filters-container{
        border-bottom: 0px !important;
        margin-bottom: 10px;
    }

    form{
        margin: 0px 0 0 0 !important;
    }

    .breadcrumbs{
        display: block;
        overflow: hidden;
        padding: 10px 0;
        margin: 0;
        font-size: 13px;
        color: #999;
    }

    .breadcrumbs ul {
        padding: 0;
        margin: 0;
        list-style: none;
    }

    .breadcrumbs ul li {
        float: left;
        margin: 0 8px 0 0;
        font-size: 13px;
        line-height: 22px;
    }

    .white-block {
        display: block;
        padding: 24px 12px 30px;
        margin-bottom: 22px;
        min-height: 150px;
        background-color: #fff;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -moz-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        -webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        box-shadow: 0 1px 5px rgba(0,0,0,0.10);
    }

    .sidebar-title{
        display: block;
        margin: 0 0 24px;
        font-size: 20px;
        font-weight: 400;
        color: #011e32;
        font-family: 'geogrotesque_semibold';
        text-transform: uppercase;
    }

    .content-title{
        width: 100%;
        float: left;
        font-size: 26px!important;
        line-height: 30px;
        color: crimson;
        padding: 15px;
        margin: 0!important;
        font-family: 'geogrotesque_semibold';
    }

    .each-product{
        transition: all 0.3s ease-in-out;
        display: block;
        min-height: 200px;
        vertical-align: top;
        padding: 14px 12px;
        margin: 0 0 14px;
        background-color: #fff;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -moz-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        -webkit-box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        box-shadow: 0 1px 5px rgba(0,0,0,0.10);
        border: 1px solid transparent;
        -webkit-transition: all .5s ease;
        -moz-transition: all .5s ease;
        -ms-transition: all .5s ease;
        position: relative;
    }

    .content-detail{
        overflow: hidden;
        padding: 5px 0;
        border-bottom: 1px solid #e7ebed;
    }

    .content-code{
        padding: 1px 0 0;
        text-transform: uppercase;
        font-size: 13px;
        color: gray;
        font-family: 'Lato',sans-serif;
    }

    .show-on-hover{
        width: 90%;
        margin: 0 5%;
        height: 38px;
        border-bottom: 1px solid #e7ebed;
        position: absolute;
        left: 0;
        top: 0;
        z-index: 10;
        background-color: #fff;
        -webkit-border-top-left-radius: 4px;
        -webkit-border-top-right-radius: 4px;
        -moz-border-radius-topleft: 4px;
        -moz-border-radius-topright: 4px;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
        will-change: transform,opacity;
        -webkit-transition: -webkit-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -moz-transition: -moz-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -o-transition: -o-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -ms-transition: -ms-transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        transition: transform .5s cubic-bezier(.19,1,.22,1),opacity .5s cubic-bezier(.19,1,.22,1);
        -webkit-transform: translateY(-20%);
        -moz-transform: translateY(-20%);
        -o-transform: translateY(-20%);
        -ms-transform: translateY(-20%);
        transform: translateY(-20%);
        opacity: 0;
    }

    .quickview-btn:hover {
        color: #0466a4;
        text-decoration: none;
    }

    .container-fluid{
        padding-top: 100px
    }

    .qty-row{
        width: 72%;
    }

    .qty {
        width: 74%;
        height: 25px;
        text-align: center;
        color: #495057; 
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }

    .short_name{
        display: inline-block;
        width: 280px;
        /*white-space: nowrap;*/
        overflow: hidden !important;
        text-overflow: ellipsis;
    }

    @media only screen and (min-width: 320px) {
        .qty-row {width: 62%;}
        .qty {width: 64%;}
    }

    @media only screen and (max-width: 425px) and (min-width: 375px) {
        .qty-row {width: 72%;}
        .qty {width: 74%;}
    }

    @media only screen and (max-width: 1024px) and (min-width: 767px) {
        .qty-row {width: 56%;}
        .qty {width: 53%;}
    }

    @media only screen and (max-width: 1440px) and (min-width: 1024px) {
        .qty-row {width: 66%;}
        .qty {width: 66%;}
    }

    @media only screen and (min-width: 1441px) {
        .qty-row {width: 66%;}
        .qty {width: 66%;}
    }

</style>
@section('css')
@stop

@section('content')

  <!-- sign in/up panel -->
    <div class="sub-content-wrap" style="display: flow-root;">

        <div class="left-sidebar">
            <div class="white-block">
                <h2>Categories</h2>
                <div class="sidebar-category-nav">
                    <ul class="mtree transit" id="menu">

                        @foreach($categories as $node)
                            {!! renderNode($node) !!}
                        @endforeach
                    </ul><!-- /.mtree menu -->

                </div>
            </div>
        </div>

        <div class="right-content">
            <div class="grid" style="padding-top: 0px">
                <div class="product-list">
                    @if(count($products) > 0)
                        @foreach($products as $product)
                            @if(isValid($product))
                                <div class="each-product-column">
                                    <div class="each-product" style="min-height: 301px;">
                                        <div class="inner-box">
                                            <div class="imagebox">
                                                <div class="imagebox-inner">
                                                    <a href="{{ url('store/view-item')}}?type_id={{$selected_categoryType->id}}&cat_id={{$product->product_category->category_id}}&item_id={{$product->id}}">
                                                        <div id="pnlEntityImage_2662" align="center">
                                                            @if(isset($product->images[0]->image))
                                                                <img width="150" height="150" id="imgEntity_2662" data-contententitytype="product" data-contentkey="ITEM-002873" data-contentcounter="2662" data-contenttype="image" class="content" src="{{ url('data/product_thumb/images/product/'.$product->images[0]->image) }}" alt="{{ $product->name?:'-' }}">
                                                            @else
                                                                <img width="150" height="150" id="imgEntity_2662" data-contententitytype="product" data-contentkey="ITEM-002873" data-contentcounter="2662" data-contenttype="image" class="content" src="{{ url('data/product_thumb/images/product/'.DEFAULT_PRODUCT_IMG) }}" alt="{{ $product->name?:'-' }}">
                                                            @endif
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-info-col">
                                                <div class="each-row" style="height: 75px">
                                                    <h3 title="{{$product->display_name}}">
                                                        <a href="{{ url('store/view-item')}}?type_id={{$selected_categoryType->id}}&cat_id={{$product->product_category->category_id}}&item_id={{$product->id}}"><span class="short_name">{{$product->display_name}}</span></a>
                                                    </h3>
                                                </div>
                                                <div class="each-row">
                                                    <div class="model-availability">
                                                        <div class="model-num" id="ItemInfoName_2662" style="float: left">
                                                            <img class="pull-right" src="{{url(UPLOAD_PATH.'uploads/images/flags/'.$product->country['flag'])}}" style="width: 30px;height: 20px" title="{{$product->country['name']}}">
                                                        </div>
                                                        <div class="model-num" id="ItemInfoName_2662" style="margin-left: 10px;float: left">
                                                            <strong>{{$product->code}}</strong>
                                                        </div>
                                                        @if(Illuminate\Support\Facades\Auth::check())
                                                        <div class="model-num" id="ItemInfoName_2662" style="float: right">
                                                            <p style="font-size: 20px !important;padding-top: 0px;padding-right: 0px;float: left">
                                                                <span>
                                                                    <strong style="color: #ff0000">${{number_format($product->selling_price,3)}}</strong>
                                                                </span>
                                                            </p>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="each-row">
                                                    <div class="model-availability">
                                                        <div class="model-num" id="ItemInfoName_2662">
                                                            <strong style="color: #000">{{$product->moq}}</strong> pcs(MOQ)
                                                        </div>
                                                        <div class="model-num" id="ItemInfoName_2662" style="float: right">
                                                            <strong style="color: #000">@if($product->lead_time){{$product->lead_time}}@else 0 @endif</strong>
                                                            <small>Lead Time</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(Illuminate\Support\Facades\Auth::check())
                                                    <div class="each-row">
                                                        <form action="{{ route('cart.add', ['product_id' => isValid($product->id)]) }}" method="get" id="form_{{ isValid($product->id) }}"  class="product_form">
                                                            <div class="qty-row" style="float: left">
                                                                <input type='button' value='-' class='qtyminus' field='qty_{{$product->id}}' />
                                                                <input type='text' name='qty' id='qty_{{$product->id}}' min="{{$product->moq}}" value='{{$product->moq}}' class='qty' />
                                                                <input type='button' value='+' class='qtyplus' field='qty_{{$product->id}}' />
                                                            </div>
                                                            <div class="availability pull-right" style="float: left">
                                                                
                                                                    <input type="hidden" name="prev_url" value="{{ Request::getQueryString() }}">
                                                                    <button type="submit" name="action" value="cart" title="Click to buy item" class="btn btn-measure add-to-cart" style="background-color: #eb232a;color: #fff !important;" data-product="{{$product->id}}">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 15px;padding-right: 3px;"></i>
                                                                    </button>
                                                            </div>
                                                        </form>
                                                        <div class="availability">
                                                            <div id="pnlStockHint_2662" name="pnlStockHint_2662">
                                                                <span class="in-stock-badge">In Stock</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                    @endif
                </div>
            </div>
        </div>

    </div>

    <div style="display: block;z-index: 30000;width: 725.313px;top: 125px;left: 743.688px;position: fixed;background: crimson">
         
    </div>

@stop

@section('js')
    <!-- slick.js -->
    <script src="{{asset('assets/libraries/slick-1.8.0/slick/slick.min.js')}}"></script>

    <!-- owl.carousel.js -->
    <script src="{{asset('assets/libraries/owl-carousel2-2.2.1/dist/owl.carousel.min.js')}}"></script>

    <!-- animated-vertical-accordion-menu-mtree-js -->
    <script src="{{asset('assets/libraries/mtreeJs/jquery.velocity.min.js')}}"></script>
    <script src="{{asset('assets/libraries/mtreeJs/mtree.js')}}"></script>

    <!-- ion.rangeSlider-2.2.0 -->
    <script src="{{asset('assets/libraries/ion.rangeSlider-2.2.0/js/ion-rangeSlider/ion.rangeSlider.min.js')}}"></script>

    <!-- jquery confirm alert -->
    <script src="{{asset('assets/libraries/craftpip-jquery-confirm/dist/jquery-confirm.min.js')}}"></script>

    <!-- toastr notification -->
    <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>
@stop

@section('scripts')
{{-- @include('cart::includes.cart-js') --}}
    <script>
        $(document).ready(function() {
            
            $('#search').click(function(){
                console.log('sdhfbhksdf');
            });

            $('.add-to-cart').click(function(){
                console.log($(this).data('product'));
            });

            $('#fixed-menu').click(function(){
                $('#fixed-cart-panel').show();
            });

            $('#order-list-hide').click(function(){
                console.log("skldfnlsdnf");
                $('#fixed-cart-panel').hide();
            });

            $('.qtyplus').click(function(e){
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('#'+fieldName).val());
                // If is not undefined
                if (!isNaN(currentVal)) {
                    // Increment
                    $('#'+fieldName).val(currentVal + 1);
                } else {
                    // Otherwise put a 0 there
                    $('#'+fieldName).val(0);
                }
            });
            // This button will decrement the value till 0
            $(".qtyminus").click(function(e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name='+fieldName+']').val());
                // If it isn't undefined or its greater than 0
                if (!isNaN(currentVal) && currentVal > 0) {
                    // Decrement one
                    $('input[name='+fieldName+']').val(currentVal - 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name='+fieldName+']').val(0);
                }
            });

        });
    
    </script>
@stop