<?php
/**
 * STORE
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-08
 */

namespace App\Modules\Store\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\Category\CategoryLogic\CategoryLogic;
use App\Modules\Category\CategoryRepository\CategoryRepository;
use App\Modules\Product\BusinessLogics\ProductLogic;
use App\Modules\Product\Models\Product;
use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Modules\Product\Repositories\ProductRepository;
use App\Modules\Cart\Repositories\CartRepository;

use Illuminate\Support\Collection;
use App\Classes\Functions;

class StoreController extends Controller
{
    //variable declaration
    protected $categoryRepo     = null;
    protected $categoryLogic    = null;
    protected $productRepo      = null;
    protected $common           = null;
    protected $cartLogic        = null;
    protected $cartRepo         = null;

    //object assigned to the varible.
    public function __construct(CategoryLogic $categoryLogic, CategoryRepository $categoryRepo, ProductRepository $productRepo, Functions $common, CartLogic $cartLogic, CartRepository $cartRepo){
        $this->categoryLogic = $categoryLogic;
        $this->categoryRepo  = $categoryRepo;
        $this->productRepo   = $productRepo;
        $this->common        = $common;
        $this->cartLogic     = $cartLogic;
        $this->cartRepo      = $cartRepo;
    }

    /**
     * index
     * @return view
     */
    public function index(Request $request, $mainSlug = null, $slug = null, $category_id = null)
    {
        // return $request;
        try{
            $category_id = (int) trim($category_id);

            /**
             * @param : page name, return type tree or parents
             * @return array
             */

            if($mainSlug == null){
                $mainSlug = 1;
            }

            if($request->type_id){
                $type_id = $request->type_id;
            }else{
                $type_id = 1;
            }

            if($request->cat_id){
                $cat_id = $request->cat_id;
            }else{
                $cat_id = null;
            }

            $selected_categoryType    = $this->categoryRepo->getCategoryTypeById($type_id);

            if($type_id && $cat_id){
                $selected_category    = $this->categoryRepo->getCategoryById($type_id,$cat_id);
            }

            $categoryTypes    = $this->categoryRepo->getCategoryType();

            $categories    = $this->categoryRepo->getCategories($type_id);

            $products = $this->productRepo->_getProductByCategory($cat_id, $request, $mainSlug);


            if(isset($selected_category)){
                return view('store::index', compact('categories', 'products', 'type_id', 'selected_categoryType', 'selected_category'));
            }else{
                return view('store::index', compact('categories', 'products', 'type_id', 'selected_categoryType'));
            }

        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'store.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * index
     * @return view
     */
    public function search(Request $request)
    {
        $param = $request->term;
        try{
            
            $product = Product::where('code','like','%'.$param.'%')->orWhere('name','like','%'.$param.'%')->limit(10)->with('images','product_category.category.type')->get();
            if(count($product) > 0){
                $products = [];

                foreach ($product as $value) {
                    $dd = [];

                    $dd['id'] = $value->id;   
                    $dd['code'] = $value->code;   
                    $dd['name'] = $value->display_name;   
                    $dd['image'] = $value->images[0]->image;
                    $dd['cate_id'] = $value->product_category->category_id;
                    $dd['type_id'] = $value->product_category->category->type->id;

                    array_push($products, $dd);   
                }

                return $products;
            }else{
                return [];
            }
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
        
    }

    public function cart(Request $request)
    {
        try{
            return view('store::cart');
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'store.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    public function viewItem(Request $request)
    {

        try{

            $selected_categoryType    = $this->categoryRepo->getCategoryTypeById($request->type_id);

            $categories    = $this->categoryRepo->getCategories($request->type_id);

            $product_details = $this->productRepo->getProductById($request->item_id);

            $specs = json_decode($product_details['specs']);

            unset($specs->code,$specs->name,$specs->display_name,$specs->price_term,$specs->description,$specs->moq,$specs->selling_price,$specs->lead_time);

            $spec_keys = array_keys((array)$specs);

            if(count($product_details) > 0){
                return view('store::view', compact(
                    'product_details',
                    'categories',
                    'selected_categoryType',
                    'specs',
                    'spec_keys'
                ));
            }else{
                throw new \Exception("Product not found!");
            }
        }catch(\Exception $e){
            return $e->getMessage();
            return $this->common->redirectWithAlert(
                'store.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    public function view($mainSlug = null, $item_name, $item_id)
    {
        try{

            $categories    = $this->categoryRepo->getCategories();

            $product_details = $this->productRepo->getProductById((int) $item_id);

            if(count($product_details) > 0){
                return view('store::view', compact('product_details','categories'));
            }else{
                throw new \Exception("Product not found!");
            }
        }catch(\Exception $e){
            return $e->getMessage();
            return $this->common->redirectWithAlert(
                'store.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the check-out view.
     * @return Response
     */
    public function checkoutView()
    {
        try{
            $product_details  = $this->productRepo->getProductById(1);

            if(count($product_details) > 0){
                return view('store::check-out', compact('product_details'));
            }else{
                throw new \Exception("Product not found!");
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'store.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bigstore::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('store::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('store::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
