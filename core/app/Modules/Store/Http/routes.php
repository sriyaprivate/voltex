<?php
/**
 * STORE
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-11-08
 */

Route::group(['prefix' => 'store', 'middleware' => 'web',  'namespace' => 'App\\Modules\Store\Http\Controllers'], function()
{
    /*+++++++++++++++++++++++++++++++
    *          GET Request
    ++++++++++++++++++++++++++++++++*/
    
    Route::get('/', [
        'uses' => 'StoreController@index',
        'as'   => 'store.index'
    ]);

    Route::get('category/{slug?}/{category_id?}', [
        'uses' => 'StoreController@productByCategory',
        'as'   => 'store.category'
    ]);

    Route::get('search', [
        'uses' => 'StoreController@search',
        'as'   => 'store.search'
    ]);

    Route::get('view-item', [
        'uses' => 'StoreController@viewItem',
        'as'   => 'store.view-item'
    ]);

    Route::get('{mainSlug?}/item/{item_name?}/{item_id}', [
        'uses' => 'StoreController@view',
        'as'   => 'store.item.view'
    ]);

    Route::get('{mainSlug?}/{slug?}/{category_id?}', [
        'uses' => 'StoreController@index',
        'as'   => 'store.store'
    ]);
    

    /*+++++++++++++++++++++++++++++++
    *          POST Request
    ++++++++++++++++++++++++++++++++*/
});

