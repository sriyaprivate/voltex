<?php namespace App\Classes\Payment\Contracts;

interface PaymentGateway{
	public function generatePaymentForm($orderId, $amount);

	public function generateRequestSignature($orderId, $amount);

	public function generateResponseSignature($orderId);

	public function verifyRequestSignature($orderId, $amount, $signature);

	public function verifyResponseSignature($orderId, $signature);

	public function paymentResponse($request);

	public function formatAmount($amount);
}