<?php namespace App\Classes\Payment;

use App\Classes\Payment\Contracts\PaymentGateway;
use App\Models\Order;
use App\Modules\Payment\Entities\Payment;
use App\Modules\Payment\Entities\PaymentTransaction;
use DB;

class Invoice implements PaymentGateway{

	public function generatePaymentForm($orderId, $amount){
		$amount      = $this->formatAmount($amount);
		
		$order       = $this->getOrder($orderId);
		$orderId     = $order->order_no;
		$payment     = $this->getPayment($order->id);
		$transaction = $this->addPaymentTransaction($orderId, $payment->id);
		return $transaction;
	}

	private function getOrder($orderId){
		$order = Order::where('order_no',$orderId)->orWhere('id',$orderId)->first();

		if(!$order){
			throw new \Exception('Order not found');
		}

		return $order;
	}

	private function getPayment($orderId){
		$payment = Payment::where('order_id', $orderId)->first();

		if(!$payment){
			throw new \Exception('Payment not found');
		}

		return $payment;
	}

	private function addPaymentTransaction($orderId, $paymentId){
		$transaction = PaymentTransaction::create([
			'payment_id' => $paymentId,
			'transaction_no' => $orderId.'_'.date('YmdHis')
		]);

		if(!$transaction){
			throw new \Exception('Could not create transaction');
		}

		return $transaction;
	}

	private function getPaymentTransaction($orderId){
		$order = $this->getOrder($orderId);
		$transaction = PaymentTransaction::where('transaction_no', 'LIKE', $order->order_no.'_%')->whereNull('response')->first();
		
		if(!$transaction){
			throw new \Exception('Could not find transaction');
		}

		return $transaction;
	}

	public function generateRequestSignature($orderId, $amount){

		if(!$orderId){
			throw new \Exception('OrderId not found!');
		}

		if(!$amount){
			throw new \Exception("Amount not found!");
		}

		$data = config('payment.hnb.password').config('payment.hnb.merchant_no')
			.config('payment.hnb.acquirer_id').$orderId.$amount.config('payment.hnb.currency_code');

		return base64_encode(pack('H*',sha1($data)));

	}

	public function verifyRequestSignature($orderId, $amount, $signature){
		$verfiySig = $this->generateRequestSignature($orderId, $amount);

		return ($verfiySig === $signature);
	}

	public function paymentResponse($request){
		$transaction = $this->getPaymentTransaction($request->order_id);
		
		if(isset($transaction->payment_id)){
			$order = $this->getOrderByPaymentId($transaction->payment_id);
		}else{
			throw new \Exception("Payment id not found in payment transaction!.");
		}
		
		if(count($order) > 0 && isset($order->id)){
			$payment = $this->getPayment($order->id);
		}else{
			throw new \Exception("Order id not found!.");
		}

		if(isset($transaction) && count($transaction) > 0){
			$transaction->transaction_time   = date('Y-m-d H:i:s');
			$transaction->ipg_transaction_no = 'COD-'.$transaction->id.'-'.$request->order_id;
			$transaction->paid_amount        = $payment->amount;
			$transaction->error_code         = 1;
			$transaction->error_message      = 'Transaction approved.';
			$transaction->response           = json_encode($request->all());
			$transaction->status             = 1;
			$transaction->save();

			if(count($transaction) > 0){
				$response = [
					'id'                 => $transaction->id,
					'order_no'           => explode('_', $transaction->transaction_no)[0],
					'response_code'      => 1,
					'transaction_status' => $transaction->status,
					'transaction_id'     => $transaction->ipg_transaction_no,
					'reason'             => 'Transaction approved.',
					'requestArray'       => $request
				];
			}else{
				throw new \Exception("Transaction record couldn't save in table");
			}
		}

		return $response;
	}

	public function generateResponseSignature($orderId){

		if(!$orderId){
			throw new \Exception('OrderId not found!');
		}

		$data = config('payment.hnb.password').config('payment.hnb.merchant_no')
			.config('payment.hnb.acquirer_id').$orderId;

		return base64_encode(pack('H*',sha1($data)));

	}

	public function verifyResponseSignature($orderId, $signature){
		$verfiySig = $this->generateResponseSignature($orderId);

		return ($verfiySig === $signature);
	}

	public function formatAmount($amount){
		if(!$amount){
			throw new \Exception("Amount not found!");
		}

		return str_pad($amount*100, 12, '0', STR_PAD_LEFT);
	}

	public function getOrderByPaymentId($payment_id){
		if(isset($payment_id)){
			$order = DB::table('orders')
				->select('orders.*')
				->join('payment', function($query){
					$query->on('payment.order_id', '=', 'orders.id')
						->where('payment.status', DEFAULT_STATUS)
						->whereNull('payment.deleted_at');
				})
				->where('payment.id', $payment_id)
				->whereNull('orders.deleted_at')
				->first();
			
			return $order;
		}else{
			throw new \Exception("Amount not found!");
		}
	}
}