<?php
namespace App\Classes\Payment\Amex;

class Encrypt{
	protected $IPGClientIP = "127.0.0.1";
	protected $IPGClientPort = "10000";

	protected $ERRNO = "";
	protected $ERRSTR = "";
	protected $SOCKET_TIMEOUT = 2;
	protected $IPGSocket = "";
	protected $socket_creation_err=false;
	protected $error_message = "";
	protected $invoice_sent_error = "";
	protected $encryption_ERR = "";
	protected $Error_code ="";
	protected $Error_msg ="";
	protected $Invoice = "";
	protected $EncryptedInvoice = "";

	protected $IPGServerURL;

	public function __construct(){
		$this->IPGServerURL = config('payment.amex.ipg_server_url');
	}

	public function openSocketConnection(){
		if($this->IPGClientIP != "" && $this->IPGClientPort != ""){
    		$this->IPGSocket = fsockopen(
    			$this->IPGClientIP,
    			$this->IPGClientPort, 
    			$this->ERRNO, 
    			$this->ERRSTR, 
    			$this->SOCKET_TIMEOUT
    		);

    		return [
    			'socket_error' => false
    		];
    	}else{
    		return [
    			'socket_error' => true,
    			'message' => 'Could not establish a socket connection for given IPGClientIP'
    		];
    	}
	}

	public function formatXmlData($orderId, $amount){
		$Invoice = "";
    	$Invoice .= "<req>".
                    	"<mer_id>" . config('payment.amex.merchant_id') . "</mer_id>".
                    	"<mer_txn_id>" .$orderId. "</mer_txn_id>".
                    	"<action>" . config('payment.amex.action') . "</action>".
		    			"<txn_amt>" . $amount . "</txn_amt>".
		    			"<cur>" . config('payment.amex.currency_code') . "</cur>" .
		    			"<lang>en</lang>".
		    			"<ret_url>" . url(config('payment.amex.return_url')) . "</ret_url>".
		    			"<mer_var1>" .explode('_',$orderId)[0]. "</mer_var1>".
		    		"</req>";

		return $Invoice;
	}

	public function sendInvoiceDataToIPGClient($data){
		socket_set_timeout($this->IPGSocket, $this->SOCKET_TIMEOUT);

		if(fwrite($this->IPGSocket,$data) === false){
            return [
            	'invoice_error' => true,
            	'message' => "Invoice could not be written to socket connection"
            ];
        }else{
        	return [
            	'invoice_error' => false
            ];
        }
	}

	private function getEncryptedInvoiceData(){
		while (!feof($this->IPGSocket)){
            $this->EncryptedInvoice .= fread($this->IPGSocket, 8192);
        }

        if(!(
        	strpos($this->EncryptedInvoice, '<error_code>') === false && 
        	strpos($this->EncryptedInvoice, '</error_code>') === false && 
        	strpos($this->EncryptedInvoice, '<error_msg>') === false && 
        	strpos($this->EncryptedInvoice, '</error_msg>') === false
        )){

        	return [
        		'encryption_error' => true,
        		'message' => substr($this->EncryptedInvoice, (strpos($this->EncryptedInvoice, '<error_msg>')+11), (strpos($this->EncryptedInvoice, '</error_msg>') - (strpos($this->EncryptedInvoice, '<error_msg>')+11)))
        	];
        }else{
        	return [
        		'encryption_error' => false
        	];
        }
	}

	public function closeSocketConnection(){
		fclose($this->IPGSocket);
	}

	public function encryptInvoiceData($orderId, $amount){
		$data = $this->formatXmlData($orderId, $amount);

		$socket = $this->openSocketConnection();

		if(!$socket['socket_error']){
			$invoice = $this->sendInvoiceDataToIPGClient($data);

			if(!$invoice['invoice_error']){
				$encrypted = $this->getEncryptedInvoiceData();

				if(!$encrypted['encryption_error']){
					$this->closeSocketConnection();
					return [
						'error' => false,
						'data' => $this->EncryptedInvoice
					];
				}else{
					return [
						'error' => true,
						'message' => $encrypted['message']
					];
				}
			}else{
				return [
					'error' => true,
					'message' => $invoice['message']
				];
			}
		}else{
			return [
				'error' => true,
				'message' => $socket['message']
			];
		}
	}
}