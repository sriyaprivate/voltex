<?php
namespace App\Classes\Payment\Amex;

class Decrypt{
	protected $IPGClientIP = "127.0.0.1";
	protected $IPGClientPort = "10000";

	protected $ERRNO = "";
	protected $ERRSTR = "";
	protected $SOCKET_TIMEOUT = 2;
	protected $IPGSocket = "";

	protected $EncryptedReceipt = "";
	protected $DecryptedReceipt = "";

	protected $error_message = "";
	protected $encrypted_rcpt_sent_error = "";
	protected $encryptedRcpt_ERR = "";
	protected $decryptedRcpt_ERR = "";
	protected $socket_creation_err=false;

	public function __construct(){

	}

	public function openSocketConnection(){
		if($this->IPGClientIP != "" && $this->IPGClientPort != ""){
    		$this->IPGSocket = fsockopen(
    			$this->IPGClientIP,
    			$this->IPGClientPort, 
    			$this->ERRNO, 
    			$this->ERRSTR, 
    			$this->SOCKET_TIMEOUT
    		);

    		return [
    			'socket_error' => false
    		];
    	}else{
    		return [
    			'socket_error' => true,
    			'message' => 'Could not establish a socket connection for given IPGClientIP'
    		];
    	}
	}

	public function formatXmlData($orderId, $amount){
		$Invoice = "";
    	$Invoice .= "<req>".
                    	"<mer_id>" . config('payment.amex.merchant_id') . "</mer_id>".
                    	"<mer_txn_id>" .$orderId. "</mer_txn_id>".
                    	"<action>" . config('payment.amex.action') . "</action>".
		    			"<txn_amt>" . $amount . "</txn_amt>".
		    			"<cur>" . config('payment.amex.currency_code') . "</cur>" .
		    			"<lang>en</lang>".
		    			"<ret_url>" . url(config('payment.amex.return_url')) . "</ret_url>".
		    			"<mer_var1>" .$orderId. "</mer_var1>".
		    		"</req>";

		return $Invoice;
	}

	public function sendInvoiceDataToIPGClient($data){
		socket_set_timeout($this->IPGSocket, $this->SOCKET_TIMEOUT);

		if(fwrite($this->IPGSocket,$data) === false){
            return [
            	'invoice_error' => true,
            	'message' => "Invoice could not be written to socket connection"
            ];
        }else{
        	return [
            	'invoice_error' => false
            ];
        }
	}

	private function getDecryptedInvoiceData(){
		while (!feof($this->IPGSocket)){
            $this->DecryptedReceipt .= fread($this->IPGSocket, 8192);
        }

        if(!(
        	strpos($this->DecryptedReceipt, '<error_code>') === false && 
        	strpos($this->DecryptedReceipt, '</error_code>') === false && 
        	strpos($this->DecryptedReceipt, '<error_msg>') === false && 
        	strpos($this->DecryptedReceipt, '</error_msg>') === false
        )){

        	return [
        		'decryption_error' => true,
        		'message' => substr($this->DecryptedReceipt, (strpos($this->DecryptedReceipt, '<error_msg>')+11), (strpos($this->DecryptedReceipt, '</error_msg>') - (strpos($this->DecryptedReceipt, '<error_msg>')+11)))
        	];
        }else{
        	return [
        		'decryption_error' => false
        	];
        }
	}

	public function processDecryptedData(){
		$Error_code = "";
		$Error_msg = "";
		$Acc_No = "";
		$Action = "";
		$Bank_ref_ID = "";
		$Currency = "";
		$IPG_txn_ID = "";
		$Lang = "";
		$Merchant_txn_ID = "";
		$Merchant_var1 = "";
		$Merchant_var2 = "";
		$Merchant_var3 = "";
		$Merchant_var4 = "";
		$Name = "";
		$Reason = "";
		$Transaction_amount = "";
		$Transaction_status = "";
		$Server_time = "";

		if(!(
			strpos($this->DecryptedReceipt, '<acc_no>') === false && 
			strpos($this->DecryptedReceipt, '</acc_no>') === false
		)){
			$Acc_No = substr($this->DecryptedReceipt, 
				(strpos($this->DecryptedReceipt, '<acc_no>')+8), 
				(strpos($this->DecryptedReceipt, '</acc_no>') - (strpos($this->DecryptedReceipt, '<acc_no>')+8)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<action>') === false && 
        	strpos($this->DecryptedReceipt, '</action>') === false
        )){
            $Action = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<action>')+8), 
            	(strpos($this->DecryptedReceipt, '</action>')-(strpos($this->DecryptedReceipt, '<action>')+8)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<bank_ref_id>') === false && 
        	strpos($this->DecryptedReceipt, '</bank_ref_id>') === false
        )){
            $Bank_ref_ID = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<bank_ref_id>')+13), 
            	(strpos($this->DecryptedReceipt, '</bank_ref_id>')-(strpos($this->DecryptedReceipt, '<bank_ref_id>')+13)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<cur>') === false && 
        	strpos($this->DecryptedReceipt, '</cur>') === false
        )){
            $Currency = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<cur>')+5),
            	(strpos($this->DecryptedReceipt, '</cur>')-(strpos($this->DecryptedReceipt, '<cur>')+5)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<ipg_txn_id>') === false && 
        	strpos($this->DecryptedReceipt, '</ipg_txn_id>') === false
        )){
            $IPG_txn_ID = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<ipg_txn_id>')+12),
            	(strpos($this->DecryptedReceipt, '</ipg_txn_id>')-(strpos($this->DecryptedReceipt, '<ipg_txn_id>')+12)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<lang>') === false && 
        	strpos($this->DecryptedReceipt, '</lang>') === false
        )){
            $Lang = substr($this->DecryptedReceipt,
            	(strpos($this->DecryptedReceipt, '<lang>')+6),
            	(strpos($this->DecryptedReceipt, '</lang>')-(strpos($this->DecryptedReceipt, '<lang>')+6)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<mer_txn_id>') === false && 
        	strpos($this->DecryptedReceipt, '</mer_txn_id>') === false
        )){
            $Merchant_txn_ID = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<mer_txn_id>')+12),
            	(strpos($this->DecryptedReceipt, '</mer_txn_id>')-(strpos($this->DecryptedReceipt, '<mer_txn_id>')+12)) );
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<mer_var1>') === false && 
        	strpos($this->DecryptedReceipt, '</mer_var1>') === false
        )){
            $Merchant_var1 = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<mer_var1>')+10),
            	(strpos($this->DecryptedReceipt, '</mer_var1>')-(strpos($this->DecryptedReceipt, '<mer_var1>')+10)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<mer_var2>') === false && 
        	strpos($this->DecryptedReceipt, '</mer_var2>') === false
        )){
            $Merchant_var2 = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<mer_var2>')+10),
            	(strpos($this->DecryptedReceipt, '</mer_var2>')-(strpos($this->DecryptedReceipt, '<mer_var2>')+10)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<mer_var3>') === false && 
        	strpos($this->DecryptedReceipt, '</mer_var3>') === false
        )){
            $Merchant_var3 = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<mer_var3>')+10),
            	(strpos($this->DecryptedReceipt, '</mer_var3>')-(strpos($this->DecryptedReceipt, '<mer_var3>')+10)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<mer_var4>') === false && 
        	strpos($this->DecryptedReceipt, '</mer_var4>') === false
        )){
            $Merchant_var4 = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<mer_var4>')+10),
            	(strpos($this->DecryptedReceipt, '</mer_var4>')-(strpos($this->DecryptedReceipt, '<mer_var4>')+10)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<name>') === false && 
        	strpos($this->DecryptedReceipt, '</name>') === false
        )){
            $Name = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<name>')+6),
            	(strpos($this->DecryptedReceipt, '</name>')-(strpos($this->DecryptedReceipt, '<name>')+6)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<reason>') === false && 
        	strpos($this->DecryptedReceipt, '</reason>') === false
        )){
            $Reason = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<reason>')+8),
            	(strpos($this->DecryptedReceipt, '</reason>')-(strpos($this->DecryptedReceipt, '<reason>')+8)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<txn_amt>') === false && 
        	strpos($this->DecryptedReceipt, '</txn_amt>') === false
        )){
            $Transaction_amount = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<txn_amt>')+9),
            	(strpos($this->DecryptedReceipt, '</txn_amt>')-(strpos($this->DecryptedReceipt, '<txn_amt>')+9)));
        }
        
        if(!(
        	strpos($this->DecryptedReceipt, '<txn_status>') === false && 
        	strpos($this->DecryptedReceipt, '</txn_status>') === false
        )){
            $Transaction_status = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<txn_status>')+12),
            	(strpos($this->DecryptedReceipt, '</txn_status>')-(strpos($this->DecryptedReceipt, '<txn_status>')+12)));
        }

        if(!(
        	strpos($this->DecryptedReceipt, '<server_time>') === false && 
        	strpos($this->DecryptedReceipt, '</server_time>') === false
        )){
            $Server_time = substr($this->DecryptedReceipt, 
            	(strpos($this->DecryptedReceipt, '<server_time>')+13),
            	(strpos($this->DecryptedReceipt, '</server_time>')-(strpos($this->DecryptedReceipt, '<server_time>')+13)));
        }

        if($Transaction_status == 'ACCEPTED'){
        	return [
	        	'action' => $Action,
	        	'account_no' => $Acc_No,
	        	'bank_ref_id' => $Bank_ref_ID,
	        	'currency' => $Currency,
	        	'ipg_transaction_id' => $IPG_txn_ID,
	        	'merchant_transaction_id' => $Merchant_txn_ID,
	        	'merchant_var1' => $Merchant_var1,
	        	'merchant_var2' => $Merchant_var2,
	        	'merchant_var3' => $Merchant_var3,
	        	'merchant_var4' => $Merchant_var4,
	        	'customer_name' => $Name,
	        	'server_time' => $Server_time,
	        	'amount' => $Transaction_amount,
	        	'status' => 1
	        ];
        }else{
        	return [
	        	'action' => $Action,
	        	'account_no' => $Acc_No,
	        	'bank_ref_id' => $Bank_ref_ID,
	        	'currency' => $Currency,
	        	'ipg_transaction_id' => $IPG_txn_ID,
	        	'merchant_transaction_id' => $Merchant_txn_ID,
	        	'merchant_var1' => $Merchant_var1,
	        	'merchant_var2' => $Merchant_var2,
	        	'merchant_var3' => $Merchant_var3,
	        	'merchant_var4' => $Merchant_var4,
	        	'customer_name' => $Name,
	        	'server_time' => $Server_time,
	        	'amount' => $Transaction_amount,
	        	'reason' => $Reason,
	        	'status' => -1
	        ];
        }
	}

	public function closeSocketConnection(){
		fclose($this->IPGSocket);
	}

	public function decryptInvoiceData($data){
		$socket = $this->openSocketConnection();

		if(!$socket['socket_error']){
			$invoice = $this->sendInvoiceDataToIPGClient($data);

			if(!$invoice['invoice_error']){
				$decrypted = $this->getDecryptedInvoiceData();

				if(!$decrypted['decryption_error']){

					$data = $this->processDecryptedData();
					$this->closeSocketConnection();
					return [
						'error' => false,
						'data' => $data
					];
				}else{
					return [
						'error' => true,
						'message' => $decrypted['message']
					];
				}
			}else{
				return [
					'error' => true,
					'message' => $invoice['message']
				];
			}
		}else{
			return [
				'error' => true,
				'message' => $socket['message']
			];
		}
	}
}