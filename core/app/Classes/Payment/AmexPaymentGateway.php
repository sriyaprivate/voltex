<?php namespace App\Classes\Payment;

use App\Classes\Payment\Contracts\PaymentGateway;
use App\Classes\Payment\Amex\Encrypt;
use App\Classes\Payment\Amex\Decrypt;
use App\Models\Order;
use App\Modules\Payment\Entities\Payment;
use App\Modules\Payment\Entities\PaymentTransaction;
use DB;

class AmexPaymentGateway implements PaymentGateway{

	public function generatePaymentForm($orderId, $amount){
		if(!$orderId){
			throw new \Exception('order id is required');
		}

		if(!$amount){
			throw new \Exception('amount is required');
		}
		
		$amount = $this->formatAmount($amount);

		$order = $this->getOrder($orderId);
		$orderId = $order->order_no;
		$payment = $this->getPayment($order->id);
		$transaction = $this->addPaymentTransaction($orderId, $payment->id);
		$merchantOrderId = $transaction->transaction_no;

		$amexEncrypt = new Encrypt();
		$result = $amexEncrypt->encryptInvoiceData($merchantOrderId, $amount);

		if(!$result['error']){
			$html = '<form id="gatewayForm" method="post" action="'.config('payment.amex.ipg_server_url').'">';
			$html .= '<input type="hidden" name="encryptedInvoicePay" value="'.$result['data'].'">';
			$html .= '<input type="submit" value="Submit">';
			$html .= '</form>';

			return $html;
		}else{
			throw new \Exception($result['message']);
		}
		
	}

	private function getOrder($orderId){
		$order = Order::where('order_no',$orderId)->orWhere('id',$orderId)->first();

		if(!$order){
			throw new \Exception('Order not found');
		}

		return $order;
	}

	private function getPayment($orderId){
		$payment = Payment::where('order_id', $orderId)->first();

		if(!$payment){
			throw new \Exception('Payment not found');
		}

		return $payment;
	}

	private function addPaymentTransaction($orderId, $paymentId){
		$transaction = PaymentTransaction::create([
			'payment_id' => $paymentId,
			'transaction_no' => $orderId.'_'.date('YmdHis')
		]);

		if(!$transaction){
			throw new \Exception('Could not create transaction');
		}

		return $transaction;
	}

	private function getPaymentTransaction($transactionNo){
		$transaction = PaymentTransaction::where('transaction_no',$transactionNo)->first();

		if(!$transaction){
			throw new \Exception('Could not find transaction');
		}

		return $transaction;
	}

	public function generateRequestSignature($orderId, $amount){

	}

	public function verifyRequestSignature($orderId, $amount, $signature){
		
	}

	public function paymentResponse($request){
		if($request->encryptedReceiptPay != ""){
			$amexDecrypt = new Decrypt();
			$data = $amexDecrypt->decryptInvoiceData($request->encryptedReceiptPay);

			$transaction = $this->getPaymentTransaction($data['data']['merchant_transaction_id']);
			$transaction->transaction_time = $data['data']['server_time'];
			$transaction->ipg_transaction_no = $data['data']['ipg_transaction_id'];
			$transaction->paid_amount = $data['data']['amount'];
			$transaction->error_code = $data['data']['status'];
			$transaction->error_message = (isset($data['data']['reason']))?$data['data']['reason']:'Transaction Approved';
			$transaction->response = json_encode($data['data']);
			$transaction->status = $data['data']['status'];
			$transaction->save();

			if(isset($transaction->payment_id)){
				$order = $this->getOrderByPaymentId($transaction->payment_id);
			}else{
				throw new \Exception("Payment id not found in payment transaction!.");
			}

			if(count($order) > 0 && isset($order->id)){
				$payment = $this->getPayment($order->id);
			}else{
				throw new \Exception("Order id not found!.");
			}
			
			if(count($transaction) > 0){
				$response = [
					'id'                 => $transaction->id,
					'order_no'           => $order->order_no,
					'response_code'      => $transaction->error_code,
					'transaction_status' => $transaction->status,
					'transaction_id'     => $transaction->id,
					'reason'             => $transaction->error_message,
					'requestArray'       => $data
				];
			}else{
				throw new \Exception("Transaction record couldn't save in table");
			}
		}else{

			if(count($transaction) > 0){
				$response = [
					'id'                 => $transaction->id,
					'order_no'           => $order->order_no,
					'response_code'      => $transaction->error_code,
					'transaction_status' => $transaction->status,
					'reason'             => $transaction->error_message,
					'requestArray'       => $data
				];
			}else{
				throw new \Exception("Transaction record couldn't save in table");
			}
		}

		return $response;
	}

	public function generateResponseSignature($orderId){

	}

	public function verifyResponseSignature($orderId, $signature){
		
	}

	public function formatAmount($amount){
		return number_format((float)$amount, 2, '.', '');
	}

	public function getOrderByPaymentId($payment_id){
		if(isset($payment_id)){
			$order = DB::table('orders')
				->select('orders.*')
				->join('payment', function($query){
					$query->on('payment.order_id', '=', 'orders.id')
						->where('payment.status', DEFAULT_STATUS)
						->whereNull('payment.deleted_at');
				})
				->where('payment.id', $payment_id)
				->whereNull('orders.deleted_at')
				->first();
			
			return $order;
		}else{
			throw new \Exception("Amount not found!");
		}
	}
}