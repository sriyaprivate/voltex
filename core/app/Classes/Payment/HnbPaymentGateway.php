<?php namespace App\Classes\Payment;

use App\Classes\Payment\Contracts\PaymentGateway;
use App\Models\Order;
use App\Modules\Payment\Entities\Payment;
use App\Modules\Payment\Entities\PaymentTransaction;
use DB;

class HnbPaymentGateway implements PaymentGateway{

	public function generatePaymentForm($orderId, $amount){
		$amount = $this->formatAmount($amount);

		$order = $this->getOrder($orderId);
		$orderId = $order->order_no;
		$payment = $this->getPayment($order->id);
		$transaction = $this->addPaymentTransaction($orderId, $payment->id);
		$merchantOrderId = $transaction->transaction_no;

		$signature = $this->generateRequestSignature($merchantOrderId, $amount);

		$html = '<form method="post" action="'.config('payment.hnb.redirect_url').'" id="gatewayForm">';
		$html .= '<input id="Version" type="hidden" name="Version" value="1.0.0">';
		$html .= '<input id="MerID" type="hidden" value="'.config('payment.hnb.merchant_no').'" name="MerID" >';
		$html .= '<input id="AcqID" type="hidden" value="'.config('payment.hnb.acquirer_id').'" name="AcqID" >';
		$html .= '<input id="MerRespURL" type="hidden" value="'.url(config('payment.hnb.response_url')).'" name="MerRespURL">';
		$html .= '<input id="PurchaseCurrency" type="hidden" value="'.config('payment.hnb.currency_code').'" name="PurchaseCurrency">';
		$html .= '<input id="PurchaseCurrencyExponent" type="hidden" value="2" name="PurchaseCurrencyExponent">';
		$html .= '<input id="OrderID" type="hidden" value="'.$merchantOrderId.'" name="OrderID" >';
		$html .= '<input id="SignatureMethod" type="hidden" value="SHA1" name="SignatureMethod">';
		$html .= '<input id="Signature" type="hidden" value="'.$signature.'" name="Signature">';
		$html .= '<input id="CaptureFlag" type="hidden" value="A" name="CaptureFlag" >';
		$html .= '<input id="PurchaseAmt" type="hidden" value="'.$amount.'" name="PurchaseAmt" >';
		// $html .= '<input id="ShipToFirstName" type="hidden" value="Theodoros" name="ShipToFirstName" >';
		// $html .= '<input id="ShipToLastName" type="hidden" value="Savvides" name="ShipToLastName" >';
		$html .= '<input type="submit" value="Submit">';
		$html .= '</form>';

		return $html;
	}

	private function getOrder($orderId){
		$order = Order::where('order_no',$orderId)->orWhere('id',$orderId)->first();

		if(!$order){
			throw new \Exception('Order not found');
		}

		return $order;
	}

	private function getPayment($orderId){
		$payment = Payment::where('order_id', $orderId)->first();

		if(!$payment){
			throw new \Exception('Payment not found');
		}

		return $payment;
	}

	private function addPaymentTransaction($orderId, $paymentId){
		$transaction = PaymentTransaction::create([
			'payment_id' => $paymentId,
			'transaction_no' => $orderId.'_'.date('YmdHis')
		]);

		if(!$transaction){
			throw new \Exception('Could not create transaction');
		}

		return $transaction;
	}

	private function getPaymentTransaction($transactionNo){
		$transaction = PaymentTransaction::where('transaction_no',$transactionNo)->first();

		if(!$transaction){
			throw new \Exception('Could not find transaction');
		}

		return $transaction;
	}

	public function generateRequestSignature($orderId, $amount){

		if(!$orderId){
			throw new \Exception('OrderId not found!');
		}

		if(!$amount){
			throw new \Exception("Amount not found!");
		}

		$data = config('payment.hnb.password').config('payment.hnb.merchant_no')
			.config('payment.hnb.acquirer_id').$orderId.$amount.config('payment.hnb.currency_code');

		return base64_encode(pack('H*',sha1($data)));

	}

	public function verifyRequestSignature($orderId, $amount, $signature){
		$verfiySig = $this->generateRequestSignature($orderId, $amount);

		return ($verfiySig === $signature);
	}

	public function paymentResponse($request){
		$transaction = $this->getPaymentTransaction($request->OrderID);
		
		if(isset($transaction->payment_id)){
			$order = $this->getOrderByPaymentId($transaction->payment_id);
		}else{
			throw new \Exception("Payment id not found in payment transaction!.");
		}
		
		if(count($order) > 0 && isset($order->id)){
			$payment = $this->getPayment($order->id);
		}else{
			throw new \Exception("Order id not found!.");
		}

		if($request->ResponseCode == 1){

			$transaction->transaction_time   = date('Y-m-d H:i:s');
			$transaction->ipg_transaction_no = ($request->ReferenceNo)?$request->ReferenceNo: '';
			$transaction->paid_amount        = $payment->amount;
			$transaction->error_code         = $request->ReasonCode;
			$transaction->error_message      = $request->ReasonCodeDesc;
			$transaction->response           = json_encode($request->all());
			$transaction->status             = 1;
			$transaction->save();

			if(count($transaction) > 0){
				$response = [
					'id'                 => $transaction->id,
					'order_no'           => explode('_', $request->OrderID)[0],
					'response_code'      => $request->ReasonCode,
					'transaction_status' => $transaction->status,
					'transaction_id'     => $request->ReferenceNo,
					'reason'             => $transaction->error_message,
					'requestArray'       => $request
				];
			}else{
				throw new \Exception("Transaction record couldn't save in table");
			}
		}else{
			$transaction->transaction_time = date('Y-m-d H:i:s');
			$transaction->paid_amount      = $payment->amount;
			$transaction->error_code       = $request->ReasonCode;
			$transaction->error_message    = $request->ReasonCodeDesc;
			$transaction->response         = json_encode($request->all());
			$transaction->status           = -1;
			$transaction->save();

			if(count($transaction) > 0){
				$response = [
					'id'                 => $transaction->id,
					'order_no'           => explode('_', $request->OrderID)[0],
					'response_code'      => $request->ReasonCode,
					'transaction_status' => $transaction->status,
					'reason'             => $transaction->error_message,
					'requestArray'       => $request
				];
			}else{
				throw new \Exception("Transaction record couldn't save in table");
			}
		}

		return $response;
	}

	public function generateResponseSignature($orderId){

		if(!$orderId){
			throw new \Exception('OrderId not found!');
		}

		$data = config('payment.hnb.password').config('payment.hnb.merchant_no')
			.config('payment.hnb.acquirer_id').$orderId;

		return base64_encode(pack('H*',sha1($data)));

	}

	public function verifyResponseSignature($orderId, $signature){
		$verfiySig = $this->generateResponseSignature($orderId);

		return ($verfiySig === $signature);
	}

	public function formatAmount($amount){
		if(!$amount){
			throw new \Exception("Amount not found!");
		}

		return str_pad($amount*100, 12, '0', STR_PAD_LEFT);
	}

	public function getOrderByPaymentId($payment_id){
		if(isset($payment_id)){
			$order = DB::table('orders')
				->select('orders.*')
				->join('payment', function($query){
					$query->on('payment.order_id', '=', 'orders.id')
						->where('payment.status', DEFAULT_STATUS)
						->whereNull('payment.deleted_at');
				})
				->where('payment.id', $payment_id)
				->whereNull('orders.deleted_at')
				->first();
			
			return $order;
		}else{
			throw new \Exception("Amount not found!");
		}
	}
}