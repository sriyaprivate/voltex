<?php
namespace App\Classes;

use DB;

class DeliveryChargesCalculator{


	public function getFixedWeightRate($zoneId, $weight){
		$fixedRate = DB::table('zone_weights')
			->select('*')
			->where(function($query) use ($weight, $zoneId){
				$query->where('weight', '>=', $weight)
					->orWhere('weight', '>=', function($query) use ($zoneId){
						$query->select(DB::raw('MAX(weight)'))
							->from('zone_weights')
							->where('zone_id',$zoneId);
					});
			})
			->where('zone_id',$zoneId)
			->orderBy('weight', 'asc')
			->first();

		return $fixedRate;
	}

	public function getAdditionalWeightRates($zoneId, $weight){
		$additionalWeights = DB::table('zone_additional_weights')
			->select('*')
			->where('min_weight','<=',$weight)
			->where('zone_id', $zoneId)
			->orderBy('min_weight', 'asc')
			->get();

		return $additionalWeights;
	}

	public function getZone($cityId, $courrierId){
		$zone = DB::table('zones')
				->join('zone_cities','zones.id','=','zone_cities.zone_id')
				->where('zone_cities.city_id',$cityId)
				->where('zones.courrier_id',$courrierId)
				->select('zones.*')
				->first();

		return $zone;
	}

	public function calculateDeliveryCharges($cityId, $weight, $courrierId = 1){
		$zone = $this->getZone($cityId, $courrierId);

		if(!$zone){
			return [
				'result'  => 0,
				'message' => 'Sorry, We are not delivery to selected city!'
			];
		}

		$fixedRate = $this->getFixedWeightRate($zone->id, $weight);

		if(!$fixedRate){
			return [
				'result'  => 0,
				'message' => 'Sorry, does not deliver to selected city.'
			];
		}

		$additionalRates = $this->getAdditionalWeightRates($zone->id, $weight);

		$charges = 0;
		$usedWeight = 0;

		if($fixedRate){
			$charges += $fixedRate->price;
			$usedWeight = $fixedRate->weight;
		}


		if(count($additionalRates) > 0){
			foreach($additionalRates as $key => $rate){

				if($rate->max_weight && $weight >= $rate->max_weight){
					$chargeWeight = $rate->max_weight - $usedWeight;
				}else{
					$chargeWeight = $weight - $usedWeight;
				}

				$usedWeight += $chargeWeight;

				$charges += ($chargeWeight * $rate->price)/$rate->per_unit;
			}
		}

		return [
			'result' => 1,
			'data' => $charges
		];
	}
}