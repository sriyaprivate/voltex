<?php
namespace App\Classes;

use Image;
use File;
use URL;
use DB;
use App\Models\City;
use App\Models\District;
use Illuminate\Support\Facades\Auth;

class Functions
{

    //unixTimePhptoJava
    public static function unixTimePhpToJava($time)
    {
        $conTime = $time * 1000;
        return $conTime;
    }

    public static function unixTimeJavaToPhp($time)
    {
        $conTime = $time / 1000;
        return $conTime;
    }

    public static function formatDateToPhp($date)
    {
        $date_format = substr($date, 0, strlen($date) - 3);
        $time_format = substr($date, 13, strlen($date));
        return date("Y-m-d", strtotime(substr($date_format, 0, strlen($date_format) - 8))).' '.date("H:i:s", strtotime($time_format));
    }

    /**
     * Limit a string length if it is more thatn setted characters.
     *
     * @param string $text String that needs to shorten
     * @param int $maxchar Maximum characters allowed to show
     * @param string $end Which text should append at the end of the shortened string.
     *
     * @return string Shortened text
     *
     */
    public static function limit_string($text, $maxchar, $end='...'){
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);
            $output = '';
            $i = 0;
            while (1) {
                $length = strlen($output)+strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                }
                else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        }
        else {
            $output = $text;
        }
        return $output;
    }

    //message function
    public function redirectWithAlert($routeName, $alertType, $alertTitle, $alertMessage, $type = 'route'){
        try {
            if($alertType == "success")
            {
                $alertTitle = "Done!.";
            }
            else if($alertType == "error")
            {
                $alertTitle = "Notice!.";
            }
            else
            {
                $alertTitle = $alertTitle;   
            }
            
            if($type == 'route'){
                return redirect()->route($routeName)->with([
                    $alertType.'.title'   => $alertTitle,
                    $alertType.'.message' => $alertMessage
                ]);
            }elseif($type == 'url'){
                return redirect($routeName)->with([
                    $alertType.'.title'   => $alertTitle,
                    $alertType.'.message' => $alertMessage
                ]);
            }else{
                throw new \Exception("Something went wrong in 'redirectWithAlert' function!.");
            }
            
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public static function getCategoryTree($array, $name = 'name', $key = 'id', $seperator = '--'){
        $arr = [];
        foreach($array as $key1 => $dd){
            if($dd == 'root')
            {
                $arr[$key1] = $dd;
            }
            else
            {
                $arr[$dd[$key]] = str_repeat($seperator, $dd['depth']).$dd[$name];
            }
        }

        return $arr;
    }

    //created a directory
    public function makeDirNotExist($path, $permission = 0777, $status = true){
        try {
            return File::exists(storage_path($path)) or File::makeDirectory(storage_path($path), $permission, $status); 
        } catch (\Exception $e) {
            throw new \Exception("message : ".$e->getMessage());
        }
    }

    //save image
    public function saveImage($image, $full_dir_path, $width = null, $expectWidth = null, $img_quality = 90){
        try {
            if($width !== null && $expectWidth !== null && $width > $expectWidth)
            {
                $img_real_path = Image::make($image->getRealPath());
                $img_real_name = $image->getClientOriginalName();
                $gen_img_name  = date('Ymdhis')."-".$img_real_name;

                $img_real_path->resize($expectWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $path          = $full_dir_path.'/'.$gen_img_name;
                $saved         = $img_real_path->save(storage_path($path), $img_quality);
                return ['path' => $path, 'count' => 1];
            }
            else
            {
                $img_real_path = Image::make($image->getRealPath());
                $img_real_name = $image->getClientOriginalName();
                $gen_img_name  = date('Ymdhis')."-".$img_real_name;
                $path          = $full_dir_path.'/'.$gen_img_name;
                $saved         = $img_real_path->save(storage_path($path), $img_quality);
                return ['path' => $path, 'count' => 1];
            }
        } catch (\Exception $e) {
            throw new \Exception("message : ".$e->getMessage());
        }
    }

    //redirect with previous url params
    public static function redirectTo($url, $title= null, $message = null, $type = null){
        if(!empty($url)){
            $prev_url = URL::previous();
            
            if(!empty($prev_url)){

                $url_parts = parse_url(URL::previous());
                
                if(count($url_parts) > 0){

                    if(isset($url_parts['query']) && !empty($url_parts['query'])){
                        //params not exists in url add param
                        if(strpos($url, $url_parts['query']) == false){
                            $prev_query_string = $url_parts['query'];

                            if(!empty($title) && !empty($message)){
                                return redirect($url.'?'.$prev_query_string)->with([
                                    $type.'.title'   => $title,
                                    $type.'.message' => $message
                                ]);
                            }

                            return redirect($url.'?'.$prev_query_string);
                        }else{
                            if(!empty($title) && !empty($message)){
                                return redirect($url)->with([
                                    $type.'.title'   => $title,
                                    $type.'.message' => $message
                                ]);
                            }

                            return redirect($url);
                        }
                    }else{
                        if(!empty($title) && !empty($message)){
                            return redirect($url)->with([
                                $type.'.title'   => $title,
                                $type.'.message' => $message
                            ]);
                        }
                    }
                }
            }

            return redirect($url);
        }else{
            throw new \Exception("Invalid URL!.");
        }
    }

    //string split and join function
    public function strSplitAndJoin($seperator, $joiner, $str){
        if(!empty($seperator) && !empty($joiner) && !empty($str)){
            return implode($joiner, explode($seperator, $str));
        }else{
            throw new \Exception("Invalid params passed for strSplitAndJoin function");
        }
    }

    //get all districs
    //params: optional distric_id
    public function getDistricts($distric_id = null, $return_type = null){
        $districts = null;

        if(!empty($distric_id)){
            if($return_type == 'first'){
                $districts = District::where('id', $distric_id)->first();
            }else if($return_type == 'get'){
                $districts = District::where('id', $distric_id)->get();
            }else{
                $districts = District::where('id', $distric_id)->get();
            }
        }else{
            $districts = District::all();
        }

        return $districts;
    }

    //get all cities
    //params: optional city_id for get specific result
    public function getCities($city_id = null, $return_type = null){
        $cities = null;

        if(!empty($city_id)){
            if($return_type == 'first'){
                $cities = City::where('id', $city_id)->first();
            }else if($return_type == 'get'){
                $cities = City::where('id', $city_id)->get();
            }else{
                $cities = City::where('id', $city_id)->get();
            }
        }else{
            $cities = City::all();
        }

        return $cities;
    }

    //get nearest outlet by district id
    public function getNearestOutletByDistrictId($distric_id){
        if(!empty($distric_id)){
            //data array
            $data         = [];
            $district = '';
            $hasDistrict  = $this->getDistricts($distric_id, 'first');

            if(count($hasDistrict) > 0){
                $district = $hasDistrict->name;

                $outlets = City::select('location.*', 'city.name as city')
                    ->join('location', function($query){
                        $query->on('city.id', '=', 'location.city_id')
                            ->where('city.status', DEFAULT_STATUS)
                            ->where('location.status', DEFAULT_STATUS)
                            ->whereNull('city.deleted_at')
                            ->whereNull('location.deleted_at');
                    })
                    ->where('city.district_id', $distric_id)
                    ->get();
                
                if(count($outlets) > 0){
                    
                    $data['message']  = '';
                    $data['district'] = $district;
                    $data['outlets']  = $outlets;
                }else{
                    if(!empty($district)){
                        $msg = 'Try again with another nearset district to you!.';
                    }else{
                        throw new \Exception('Invalid District!.');
                    }

                    $data['message']  = $msg;
                    $data['district'] = $district;
                    $data['outlets']  = [];
                }   

                return $data;
            }else{
                throw new \Exception('Invalid District!.');
            }
        }else{
            throw new \Exception('Invalid District ID or empty district ID passed for "getNearestOutletByDistrictId" function!.');
        }
    }

    public static function commitOrRollBack($allOk, $msg){
        if(count($allOk) > 0){
            DB::commit();
            return $allOk;
        }else{
            DB::rollBack();
            throw new \Exception($msg);
        }
    }

    /**
     * get logged user details
     */
    public function getLoggedUser($user_id = null){

        if(!isset($user_id)){              
            $user = Auth::user();

            if(count($user) > 0){
                return $user;
            }else{
                return [];
            }
        }else{
            if($user_id){
                $user = User::where('id', $user_id)
                        ->where('status', 1)
                        ->whereNull('deleted_at')
                        ->first();

                if($user){
                    return $user;
                }else{
                    return [];    
                }
            }else{
                return [];
            }
        }    
    }

    /**
     * check variable is valid or not
     */
    public static function isValid($data, $error = false){
        if(isset($data) && count($data) > 0){              
            return $data;
        }else{
            return $error;
        }
        
    }
}