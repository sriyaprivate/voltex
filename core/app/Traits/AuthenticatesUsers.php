<?php

namespace App\Traits;

use DB;
use Session;

use App\Modules\Cart\Entities\Cart;
use App\Models\User;
use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Modules\Cart\Repositories\CartRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;

trait AuthenticatesUsers
{
    use RedirectsUsers, ThrottlesLogins;

    private $cartSession = null;
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.

        /*----------------------------
        /   our custom code
        /-----------------------------*/
        $cartLogic   = new CartLogic();
        $sessionData = $cartLogic->getTemparyCartSessionByBrowserSessionId();

        if(count($sessionData) > 0){
            $this->cartSession = $sessionData;
        }
        /*----------------------------
        /   our custom code
        /-----------------------------*/

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $this->redirectTo = session()->pull('redirectUrl');

        if ($this->attemptLogin($request)) {
            $tmp=User::where('username',$request->get('username'))->where('confirmed','=',1)->first();

            if($tmp){
                return $this->sendLoginResponse($request);
            }else{
                $this->guard()->logout();
                Session::flash('bad_login', 'Your account not verified. Your verification link set to your email. check it');
                return redirect()->route('login');
            }

        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        
        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $auth = $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );

        return $auth;
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        /*----------------------------
        /   our custom code
        /-----------------------------*/
        //set browser session cart with logged user

        //old code with confirm box
        //$cartLogic->setTemparyCartWithLoggedUser($this->cartSession);

        $cartLogic = new CartLogic();
        $cartLogic->mergeCartWithDB($this->cartSession);

        if(count($user) > 0){
            $user              = User::find($user->id);
            $user->last_login  = date('Y-m-d h:i:s');
            $user->save();

            return redirect('');
        }else{
            throw new \Exception('User not logged in.');
        }
        /*----------------------------
        /   our custom code
        /-----------------------------*/
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
