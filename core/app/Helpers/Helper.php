<?php
// renderNode
function renderNode($node) {
  $category_id = Route::current()->parameter('cat_id');
  $type_id = Request::get('type_id');
  $current_route = null;
  if(Request::get('cat_id')){
    $current_route = App\Modules\Category\Models\Category::find(Request::get('cat_id'));
  }

  if($node->isLeaf()){
    if(isset($node->id)){
      $html = '<li';
      $html .= '  class="border-bottom-2 ';
      if(Request::get('cat_id')){
        if(Request::get('cat_id') == $node->id){
          $html .= 'mtree-active';    
        }
      }

      $html .= '"';

      $html .= '><span class="no-icon"></span><a href="'.url('store').'?type_id='.$type_id.'&cat_id='.$node->id.'">'.$node->name.'</a></li>'; 
      
      return $html;
    }else{
      return '<li><a href="'.url('bigstore').'">'.$node->name.'</a></li>';
    }
  }else{
    $html  = '<li class="border-bottom-1 ';
    if(($current_route && $node->isAncestorOf($current_route)) || 
      (Request::get('cat_id') && Request::get('cat_id') == $node->id)){
      $html .= 'mtree-open';
    }

    $html .= '"><a href="'.url('store').'?type_id='.$type_id.'&cat_id='.$node->id.'">'.$node->name.'</a>';

      if(count($node->children) > 0){
        $html .= '<ul>';
        foreach($node->children as $child){
          $html .= renderNode($child);
        }
        $html .= '</ul>';
      }
    $html .= '</li>';
  }
  return $html;
}

function renderMobileSideMenuNode($node) {
  $category_id = Route::current()->parameter('cat_id');
  $type_id = Request::get('type_id');
  $current_route = null;
  if(Request::route('cat_id')){
    $current_route = App\Modules\Category\Models\Category::find(Request::route('cat_id'));
  }

  if($node->isLeaf()){
    if(isset($node->id)){
      $html = '<li style="list-style-type: none;"';

      if(Request::get('cat_id')){
        if(Request::get('cat_id') == $node->id){
          $html .= ' class="mtree-active-mobile"';    
        }
      }

      $html .= '><span class="no-icon"></span><a href="'.url('store').'?type_id='.$type_id.'&cat_id='.$node->id.'"><i class="fa fa-caret-right"></i> '.$node->name.'</a></li>'; 
      
      return $html;
    }else{
      return '<li style="list-style-type: none;"><a href="'.url('bigstore').'"><i class="fa fa-caret-right"></i> '.$node->name.'</a></li>';
    }
  }else{
    $html  = '<li style="list-style-type: none;"';
    $html .= '><a href="'.url('store').'?type_id='.$type_id.'&cat_id='.$node->id.'"><i class="fa fa-caret-right"></i> '.$node->name.'</a>';

      if(count($node->children) > 0){
        $html .= '<ul>';
        foreach($node->children as $child){
          $html .= renderMobileSideMenuNode($child);
        }
        $html .= '</ul>';
      }
    $html .= '</li>';
  }
  return $html;
}

//validate data
function isValid($data = null){
  try{
    if(count($data) > 0 && $data !== null && isset($data) && !empty($data)){
      return $data;
    }else{
      return false;
    }
  }catch(\Exception $e){
    return $e->getMessage();
  }
}

//prams product details and bank promotion details
function calculatePromotionPrice($product_details, $promotion){
  try{
    if(count($product_details) && count($promotion)){
      $price = 0;
      
      if(isset($product_details->discounted_amount) && !empty($product_details->discounted_amount)){
        $price = $product_details->discounted_amount;
      }else if(isset($product_details->price) && !empty($product_details->price)){
        $price = $product_details->price;
      }
      
      if(isset($promotion->discount) && !empty($promotion->discount)){
        return number_format((($price * (100 - $promotion->discount)) / 100), 2);   
      }else{
        return number_format($price, 2);   
      }
    }
  }catch(\Exception $e){
    return $e->getMessage();
  }
}

//this function is used to get difference between two dates

function dateDifference($from_date,$to_date,$format){
  try{
    if(!empty($from_date) && !empty($to_date) && !empty($format)){
        $from = date_create($from_date);
        $to   = date_create($to_date);
        $diff = date_diff($from,$to);
        return $diff->format($format);
    }else{
      return 0;
    }
  }catch(\Exception $e){
    return $e->getMessage();
  }
}

//this function is used to calculate promtion price

function calculatePromtion($product_price,$promotion_rate){
  try{

    $discount_rate    = 100 - $promotion_rate;
    $promotion_price  = ($discount_rate / 100) * $product_price;
    $promotion        = $product_price - $promotion_price;
    return ['promotion_price' => $promotion_price,'promotion' => $promotion]; 

  }catch(\Exception $e){
    return $e->getMessage();
  }
}

function calculatePercentage($total, $rate){
  try{

    $value  = (($total * $rate) / 100);

    if(!empty($value)){
      return $value;
    }else{
      return 0;
    }
  }catch(\Exception $e){
    return $e->getMessage();
  }
}

//seach in array
function arraySearch($search, $array){
  if(isset($search) && count($array) > 0){
    return in_array($search, $array);
  }else{
    return false;
  }
}

//disabled function
function isDisabled($disabled, $search, $action = 'cursor-not-allowed'){
  if(isValid($disabled) && isValid($search) && isValid($action)){
    if(isValid($disabled) && isValid($disabled['item_count']) > 1){
      return '';
    }else{
      if(isValid($disabled) && isValid($disabled['status'])){
        if(arraySearch($search, $disabled['array'])){
          return '';
        }else{
          return $action;
        }
      }else{
        return $action;
      }
    }
  }else{
      throw new \Exception("Something went wrong in 'isDisabled' function of helper class.");
  }
}

//show warranty
function showWarranty($product = null){
    $lable = '';

    if(isValid($product) && $product !== null){
      if(isValid($product->warrenty->warrenty_period) && $product->warrenty->period_type == 'y'){
        $lable = 'Year';
      }elseif(isValid($product->warrenty->warrenty_period) && $product->warrenty->period_type == 'm'){
        $lable = 'Month';
      }elseif(isValid($product->warrenty->warrenty_period) && $product->warrenty->period_type == 'd'){
        $lable = 'Day';
      }elseif(count($product->warrenty->warrenty_period) > 0 && $product->warrenty->period_type == 'l'){
        $lable = 'Life time';
      }else{
        return '';
      }
    }else{
      return '';
    }

    if(isValid($product->warrenty->warrenty_period) && $product->warrenty->warrenty_period > 1){
      if($product->warrenty->warrenty_period == 0){
        return $lable;
      }else{
        return $product->warrenty->warrenty_period.' '.$lable.'s';
      }
    }else{
      if($product->warrenty->warrenty_period == 0){
        return $lable;
      }else{
        return $product->warrenty->warrenty_period.' '.$lable;
      }
    }
}

//show warranty
function strExplode($str, $separator = '_', $posttion = 0){
  if(isValid($str)){
    $exploded =  explode($separator, $str);

    if($posttion !== null){
      if(count($exploded) > 0){
        return $exploded[$posttion];
      }else{
        return '-';  
      }
    }else{
      if(count($exploded) > 0){
        return $exploded;
      }else{
        return '-';  
      }
    }
  }else{
    return '-';
  }
}

//str explode and implode
function strExpImp($str, $expWith = ',', $impWith = '<br>'){
  if(isValid($str)){
    $exploded =  implode($impWith, explode($expWith, $str));

    if(count($exploded) > 0){
      return $exploded;
    }else{
      return '-';  
    }
  }else{
    return '-';
  }
}

function getOparetorText($oparetor){
  if(isset($oparetor) && !empty($oparetor)){
    if($oparetor && $oparetor == '>'){
      return 'Minimum';
    }elseif($oparetor && $oparetor == '<'){
      return 'Maximum';
    }elseif($oparetor && $oparetor == '=='){
      return 'Equal';
    }
  }else{
    return '-';
  }
}

//installment plan, cart object
function getInstallmentAmount($installment_plan, $interest_rate = 0, $objCart){
  if(isset($installment_plan) && !empty($installment_plan) && isset($interest_rate) && isset($objCart) && count($objCart)){
    if(isset($objCart->subTotal) && !empty($objCart->subTotal)){
      if($interest_rate == '0'){
        return $objCart->subTotal / $installment_plan;
      }else{
        return ((($objCart->subTotal * ($interest_rate + 100))/ 100) / $installment_plan);
      }
    }
  }else{
    return 0;
  }
}

//card totoal amount if greater than db installment rule return true
function hideIfNotSetToRule($objCart, $rule){
  if(isset($objCart) && count($objCart) && isset($objCart->subTotal) && !empty($objCart->subTotal) && isset($rule) && !empty($rule) && $objCart->subTotal > $rule){
    return true;
  }else{
    return false;
  }
}


function disabledRuleNotSet($cart, $city_id, $city_array, $rule, $amount){
  if(isValid($cart) && isValid($cart->subTotal) && isValid($city_id) && isValid($city_array) && arraySearch($city_id, $city_array)){
    if($rule == '>' && $cart->subTotal > $amount){
      return true;
    }elseif($rule == '>=' && $cart->subTotal >= $amount){
      return true;
    }
    elseif($rule == '<' && $cart->subTotal < $amount){
      return true;
    }
    elseif($rule == '<=' && $cart->subTotal <= $amount){
      return true;
    }
    elseif($rule == '==' && $cart->subTotal == $amount){
      return true;
    }else{
      return false;
    }
  }else{
    return false;
  }
}


//this function is used to calculate installment
function calculateInstallment($product_price,$installment_plan,$installment_fee,$intrest_rate){
  try{

    $installmemt_intrest      = 100 + $intrest_rate;
    $installment              = $product_price / $installment_plan;
    $installmet_with_intrest  = ($installment * $installmemt_intrest) / 100;
    $total_installment_amount = $installmet_with_intrest + $installment_fee;
    return $total_installment_amount;

  }catch(\Exception $e){
    return $e->getMessage();
  }
}

function metaGenerate($product_details, $default, $page, $extra_keyword){
  if(count($product_details) > 0){
    $title    = $default;
    $product  = '';
    $category = '';
    $web      = config('app.name');

    if(isset($product_details->display_name) && count($product_details->display_name)
        && !empty($product_details->display_name) 
        && isset($product_details->display_name)){
        $product = $product_details->display_name;
    }

    if(isset($product_details->product_category)
      && isset($product_details->product_category->category)
      && isset($product_details->product_category->category->display_name)
      && !empty($product_details->product_category->category->display_name)){
        $category = $product_details->product_category->category->display_name;
    }

    $title            = $product.' - '.$category.' - '.$page.' at '.$web;
    $meta_description = $product.' - '.$category.' exclusive from ostore.lk, '.ITEM_META_PREFIX;
    $meta_keyword     = $product.', '.$category.', '.$category.' '.$product .', '.$page.', '.$page.' at ostore,'.$extra_keyword.', online shopping sri lanka '.$web;

    return [
      'title'             => $title,
      'meta_description'  => $meta_description,
      'meta_keyword'      => $meta_keyword
    ];
  }
}

//free delivery
function checkRule($subTotal, $rule, $oparetor){
  if(isValid($subTotal) && isValid($rule) && isValid($oparetor)){
    
    if($oparetor == '>' && $subTotal > $rule){
      return true;
    }elseif($oparetor == '>=' && $subTotal >= $rule){
      return true;
    }
    elseif($oparetor == '<' && $subTotal < $rule){
      return true;
    }
    elseif($oparetor == '<=' && $subTotal <= $rule){
      return true;
    }
    elseif($oparetor == '==' && $subTotal == $rule){
      return true;
    }else{
      return false;
    }
  }else{
    return false;
  }
}

function showReviewCount($reviews, $rate){
    if(count($reviews) > 0 && count($rate) > 0){
        return collect($reviews)->where('rate', $rate)->count();
    }else{
      return '';
    }
}

function showReviewPercentage($feedback, $rate){
    $rate_count = showReviewCount($feedback['reviews'], $rate);
    $rate_per   = ($rate_count / $feedback['review_count']) * 100;
    return $rate_per;
}

function getFirstLetterOfWords($string){
  $result = null;
  if(isset($string) && !empty($string)){
    $expr = '/(?<=\s|^)[a-z]/i';
    preg_match_all($expr, $string, $matches);

    $result = implode('', $matches[0]);

    $result = strtoupper($result);
  }

  return $result;
}