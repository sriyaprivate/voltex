<?php
namespace App\Http\Middleware;

use Closure;
use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Modules\Account\Repositories\AccountRepo;
use App\Modules\Category\CategoryRepository\CategoryRepository;

class CartMiddleware{

    public function handle($request, Closure $next, $guard = null)
    {
        $wishlistCount       = '';
        $this->cartLogic     = new CartLogic();
        $this->accountRepo   = new AccountRepo;

        $loggedUser          = $this->cartLogic->getLoggedUser();
        $cart                = $this->cartLogic->getCart($loggedUser);
        $pfis                = $this->cartLogic->getPendingPIConfirmations($loggedUser);

        if(count($loggedUser) > 0){
            $wishlistCount       = count($this->accountRepo->getWishlistByUser($loggedUser->id));
        }

        $categories = CategoryRepository::getCategories();
        $category_types = CategoryRepository::getCategoryTypes();
        $most_selling_products  = CategoryRepository::getMostSellingProducts();

        // die($most_selling_products->chunk(3));
        
        view()->share([
            'cartItemCount' => count($cart) > 0 && !empty($cart->totalQty)? $cart->totalQty : '', 
            'wishlistCount' => count($wishlistCount) > 0 && !empty($wishlistCount)? $wishlistCount : '',
            'loggedInUser'  => $loggedUser,
            'pfis_count'    => count($pfis),
            'pfis'          => $pfis,
            'cart_details'  => $cart,
            'categories'    => $categories,
            'cat_types'     => $category_types,
            'm_s_pro'       => $most_selling_products->chunk(3)
        ]);

        return $next($request);
    }
}