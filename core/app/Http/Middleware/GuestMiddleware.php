<?php
namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use App\Modules\Cart\BusinessLogic\CartLogic;
use Session;

class GuestMiddleware{

    public function handle($request, Closure $next, $guard = null)
    {
        $this->cartLogic = new CartLogic();
        $loggedUser      = $this->cartLogic->getLoggedUser();
        $cart            = $this->cartLogic->getCart($loggedUser);
        
        view()->share('cartItemCount', count($cart) > 0 && !empty($cart->totalQty)? $cart->totalQty : '');
        
        if(Auth::check()){
            return $next($request);
        }else{
            session()->put('redirectUrl', 'cart/summary');

            $url = url('/guest/user');

            if($request->input('cart-changes') !== null && $request->input('cart-changes') == 'saved'){
                $url = $url.'?cart-changes=saved';
            }
            return redirect($url);
        }
    }
}