<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Mail\OrderMail;
use App\Classes\Functions;
use Mail;
use App\Jobs\SendOrderConfirmationEmail;
use Illuminate\Support\Carbon;

class MailController extends Controller {
	public $common;

	public function __construct(){

	}

	/**
	 * send order mail
	 *
	 * @return Mail
	 */
	public function orderConfirmationMail($order_id) {   
		try{
			$cart = new CartLogic(null);
			$this->common = new Functions();
			
			$order = $cart->getOrderById($order_id);
			
			if(Functions::isValid($order, false)){
				if(Functions::isValid($order->user->email, false)){
					//$send = Mail::to($order->user->email)->send(new OrderMail($order));
					//$emailJob = (new SendOrderConfirmationEmail($order))->delay(Carbon::now()->addSeconds(3));
					//return view('mails.orderConfirmationMail', compact('order')); 
   					dispatch(new SendOrderConfirmationEmail($order));
				}else{
					throw new \Exception("User email not found!.");	
				}
			}else{
				throw new \Exception("Invalid order data.");
			}
		}catch(\Exception $e){
			return $e->getMessage();
			return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
		}
	}
}
