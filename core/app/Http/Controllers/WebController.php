<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GuestRegistrationRequest;
use App\Http\Repositories\Login;
use App\Modules\Cart\BusinessLogic\CartLogic;
use App\Models\Newsletter;
use App\Classes\Functions;
use Validator;
use Session;
use DB;
use URL;

class WebController extends Controller {

	protected $loginRepo = null;
	protected $cartLogic = null;
	protected $common    = null;

	public function __construct(Login $loginRepo, CartLogic $cartLogic, Functions $common){
		$this->loginRepo  = $loginRepo;
		$this->cartLogic  = $cartLogic;
		$this->common     = $common;
	}

	/*
		|--------------------------------------------------------------------------
		| Welcome Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/


	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index() {        
        	return view('index');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function login() {        
        	return view('login');
	}

	/**
	 * Show the application sign up screen to the user.
	 *
	 * @return Response
	 */
	public function signup() {        
        	return view('signup');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function guest() {        
        	return view('guest');
	}

	/**
	 * Register guest user
	 * @author lahiru dilshan | lahiru4unew4@gmail.com
	 * @return Response
	 */
	public function guestRegistration(GuestRegistrationRequest $request) {    
		//get data in tempary add to cart by browser session id 
		$sessionData     = $this->cartLogic->getTemparyCartSessionByBrowserSessionId();
		
		//register and authenticate guest user
		$createdAndAuth  = $this->loginRepo->createGuestUser($request);

		//set tempary cart data with loggeduser

		//OLD CODE when logged confirm box show ask for add session data to user cart
		//$this->cartLogic->setTemparyCartWithLoggedUser($sessionData);

		$cartMergeResult = $this->cartLogic->mergeCartWithDB($sessionData);
		
		$payload = unserialize(base64_decode($sessionData->payload));
		
		if(count($payload) > 0 && isset($payload['cart']) && count($payload['cart']) > 0){
			return redirect()->route('cart.delivery');
		}else{
			return $createdAndAuth;
		}
	}

}
