<?php

namespace App\Http\Controllers\Auth;

use DB;
use Mail;
use Session;
use App\Models\User;
use App\Models\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Classes\Functions;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $common = null;

    public function __construct()
    {
        $this->middleware('guest');
        $this->common = new Functions();
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        Auth::guard()->logout();

        Session::flash('code_create', 'Your account has been successfully created. Verification email will be sent to your email address.');
        return redirect()->route('login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        //'email'         => 'required|string|email|max:255|unique:users,email,,,deleted_at,NULL,status,DEFAULT_STATUS',
        return Validator::make($data, [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'phone'         => 'nullable|regex:/^0[0-9]{9}/',
            'email'         => 'required|string|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
            'password'      => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        try{
            DB::beginTransaction();

            $tmp_user= User::create([
                'username'     => $data['email'],
                'email'        => $data['email'],
                'password'     => bcrypt($data['password']),
                'user_type_id' => USER_TYPE_FRONT,
                'channel_id'   => CHANNEL_ONLINE_ID,
                'status'       => DEFAULT_STATUS,
                'permissions' => '{"index":true}',
                'channel_id'   => CHANNEL_ONLINE_ID,
                'status'       => 0,
                'confirmed'    => 0,
            ]);
            
            if(count($tmp_user) > 0){

                $confirmation_code = str_random(30);

                $tmp_user->confirmation_code = $confirmation_code;

                $tmp_user->save();

                $customer = Customer::create([
                    'user_id'    => $tmp_user->id,
                    'first_name' => $data['first_name'],
                    'last_name'  => $data['last_name'],
                    'email'      => $tmp_user->email,
                    'phone_no'   => $data['phone']
                ]);

                Mail::send('auth.email.varify', ['confirmation_code'=>$confirmation_code,'email'=>$data['email'],'name'=>$data['first_name']." ".$data['last_name']], function($message) use ($data){
                    $message->to($data['email'], $data['email'])->from(env('MAIL_FROM_ADDRESS'))->subject('verify your email address');
                });

                if(count($customer) > 0){
                    DB::commit();
                    return $tmp_user;
                }else{
                    DB::callBack();
                    throw new \Exception("Customer couldn't be registered!.");    
                }
            }else{
                DB::callBack();
                throw new \Exception("User couldn't be registered!.");
            }
        }catch(\Exception $e){
            return $this->common->redirectWithAlert(
                'home.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            // return "Invalid code";
            Session::flash('bad_code', 'Invalid Code');
            return redirect()->route('login');
            // throw new InvalidConfirmationCodeException;
        }

        $user = User::where('confirmation_code',$confirmation_code)->first();

        if ( ! $user)
        {
            // return "Invalid code of user";
            Session::flash('bad_code', 'Invalid code of user');
            return redirect()->route('login');
            // throw new InvalidConfirmationCodeException;
        }

        $user->status = DEFAULT_STATUS;
        $user->confirmed = DEFAULT_STATUS;
        $user->confirmation_code = null;
        $user->save();

        Session::flash('code', 'You have successfully verified your account. Now you can log into the system');
        return redirect()->route('login');
        // return 'You have successfully verified your account';
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
