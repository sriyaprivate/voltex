<?php

namespace App\Http\Repositories;

use Auth;
use App\Models\User;
use App\Models\Customer;

class Login{
	public function createGuestUser($request){
		$email =  $request->input('guest_email');
		$user  = null;
		$user  = User::where('email', $email)
					->whereNull('deleted_at')
					->where('status', DEFAULT_STATUS)
					->orderBy('id', 'DESC')
					->first();

		if(count($user) > 0){
			if(isset($user->user_type_id) && $user->user_type_id == USER_TYPE_GUEST){
				return $this->doAuth($user, $email);
			}elseif(isset($user->user_type_id) && $user->user_type_id == USER_TYPE_FRONT){
				return redirect()->back()->withErrors(['guest_email' => 'The email has already been taken.']);
			}
		}elseif(count($user) == 0){
			$user                = new User;
			$user->email         = $email;
			$user->user_type_id  = USER_TYPE_GUEST;
			$user->status        = DEFAULT_STATUS;
			$user->permissions   = '{"index":true}';
			$user->save();

			return $this->doAuth($user, $email);
		}
	}

	public function loginUser($user){
		if(count($user) > 0){
			$isLogin = Auth::loginUsingId($user->id, true);

			if(count($isLogin) > 0){
				$user              = User::find($user->id);
				$user->last_login  = date('Y-m-d h:i:s');
				$user->save();
			}else{
				throw new \Exception("Guest user couldn't be logged!.");
			}
			return $isLogin;
		}else{
			throw new \Exception("Empty user data passed for 'loginUser' function.");
			
		}
	}

	public function doAuth($user, $email = null){
		if(count($user) && !empty($email)){
			$isCustomerExists  = Customer::where('user_id', $user->id)
									->where('email', $email)
									->where('status', DEFAULT_STATUS)
									->whereNull('deleted_at')
									->orderBy('id', 'DESC')
									->first();

			if(count($isCustomerExists) == 0){
				$customer                = new Customer;
				$customer->user_id       = $user->id;
				$customer->email         = $email;
				$customer->status        = DEFAULT_STATUS;
				$customer->save();
			}

			$isAuthenticated = $this->loginUser($user);

			if($isAuthenticated){
				return redirect()->route('home.index');
			}else{
				throw new \Exception("couldn't authenticate guest user!.");
			}
		}else{
			throw new \Exception("User object not found!.");
		}	
	}
}