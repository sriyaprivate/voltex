<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuestRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;
        //'guest_email' => 'required|unique:users,email,'.$id.',,deleted_at,NULL,user_type_id,USER_TYPE_FRONT'
        return [
            'guest_email' => 'required|email'
        ];
    }

    public function messages(){
        return [
            'guest_email.required' => 'Email is required.',
            'guest_email.unique'   => 'The email has already been taken.'
        ];
    }
}
