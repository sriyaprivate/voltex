<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_tag';

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function tag(){
        return $this->belongsTo('App\Models\Tag','tag_id','id');
    }
}
