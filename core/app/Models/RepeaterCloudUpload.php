<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 6/14/2018
 * Time: 11:20 AM
 */

namespace App\Models;

use Baum\Extensions\Eloquent\Model;

class RepeaterCloudUpload extends Model {

    protected $table = 'repeater_request_cloud_uploads';

    protected $guarded = ['id'];

}