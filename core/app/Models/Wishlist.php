<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wishlist extends Model
{
	use SoftDeletes;
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'wishlist';

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function product(){
		return $this->belongsTo('App\Modules\Product\Models\Product','product_id','id');
	}
}
