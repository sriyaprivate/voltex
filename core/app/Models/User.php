<?php 
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'email', 
        'password',
        'permissions',
        'channel_id',
        'status',
        'confirmed',
        'user_type_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static function whereConfirmationCode($confirmation_code)
    {
        return $this->hasOne('App\Models\User');
        // return $this->where('confirmation_code',$confirmation_code);
    }
}