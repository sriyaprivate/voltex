<?php namespace App\Models;

/**
* @author Author <lahiru4unew4@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+ 2017-01-19
*/

use Illuminate\Database\Eloquent\Model;


class StockTransaction extends Model {

	protected $table   = 'stock_transaction';

	protected $guarded = ['id']; 

	protected $timestamp = true;
}
