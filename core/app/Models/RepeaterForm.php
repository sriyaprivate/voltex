<?php
namespace App\Models;

use Baum\Extensions\Eloquent\Model;

class RepeaterForm extends Model {

    protected $table = 'repeater_form';

    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo('App\Models\RepeaterFormCategories','repeater_form_category_id','id');
    }


}