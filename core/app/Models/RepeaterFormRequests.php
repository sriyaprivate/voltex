<?php 
namespace App\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RepeaterFormRequests extends Model {

    use SoftDeletes;
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'repeater_form_details';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function form(){
        return $this->belongsTo('App\Models\RepeaterForm','form_id','id');
    }

    public function attachments(){
        return $this->hasMany('App\Models\RepeaterFormAttachment','request_id','id');
    }

    public function initiator(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function creator(){
        return $this->belongsTo('App\Models\User','request_by','id');
    }

}
