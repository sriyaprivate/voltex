<?php

namespace App\Listeners\Mails;

use App\Events\Mails\OrderConfirmationEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\MailController;

class SendOrderConfirmationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderConfirmationEmail  $event
     * @return void
     */
    public function handle(OrderConfirmationEmail $event)
    {
        $mail = new MailController();
        $mail->orderConfirmationMail($event->order_id);
    }
}
