<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| GET Routes
|--------------------------------------------------------------------------
*/
Route::get('guest/user', 'WebController@guest')->name('user.guest');
Route::get('signup', 'WebController@signup');
Route::get('contact-us', 'WebController@contactUs');
Route::get('about-us', 'WebController@aboutUs');
Route::get('choose-us', 'WebController@choseUs');
Route::get('terms-condition','WebController@termsCondition');
Route::get('privacy-policy','WebController@privacyPloicy');

/*
|--------------------------------------------------------------------------
| Email
|--------------------------------------------------------------------------
*/
Route::get('order/{order_id}/confirmation/mail', 'MailController@orderConfirmationMail');
Route::post('newsletter', 'WebController@newsletterEmailSave');

/*
|--------------------------------------------------------------------------
| AUTH Routes
|--------------------------------------------------------------------------
*/

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('register/verify/{token}', 'Auth\RegisterController@confirm');
Route::post('register', 'Auth\RegisterController@register');

Auth::routes();


/*
|--------------------------------------------------------------------------
| POST Routes
|--------------------------------------------------------------------------
*/
Route::post('guest/registration', [
	'uses' 	=> 'WebController@guestRegistration',
	'as'	=> 'guest.signup'
]);